#!/bin/bash
cd Simulation/nodered.config
npm ci
cd ../../Constraint-Language/ls5.pg646.constraints.parent/ls5.pg646.constraints
mvn generate-sources
cd ..
mvn install
cd ../../Frontend
npm ci
cd ../
tmux new-session -d 'cd Simulation; docker-compose up; read' \; \
new-window -d "cd Gateway/dev-tool; docker-compose up; read" \; \
new-window -d "sleep 5; cd Gateway; mvn spring-boot:run; read" \; \
new-window -d "sleep 10; cd Frontend; npm run dev; read" \; \
attach;