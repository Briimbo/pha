package ls5.pg646;

public abstract class PhaException extends Exception {

  private static final long serialVersionUID = 7513329963869924077L;

  protected PhaException(String message) {
    super(message);
  }

  protected PhaException(Throwable cause) {
    super(cause);
  }
}
