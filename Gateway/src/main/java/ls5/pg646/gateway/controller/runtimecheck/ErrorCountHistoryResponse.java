package ls5.pg646.gateway.controller.runtimecheck;

import java.util.List;
import ls5.pg646.gateway.controller.utils.JsonResponse;

public class ErrorCountHistoryResponse extends JsonResponse<List<ErrorsPerDay>> {

  public ErrorCountHistoryResponse() {
  }

  public ErrorCountHistoryResponse(List<ErrorsPerDay> errors) {
    super(errors);
  }

}
