package ls5.pg646.gateway.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import ls5.pg646.gateway.controller.floorplan.FloorPlanDto;
import ls5.pg646.gateway.controller.floorplan.FloorPlanListResponse;
import ls5.pg646.gateway.controller.floorplan.FloorPlanLoadResponse;
import ls5.pg646.gateway.controller.utils.BaseEmptyResponse;
import ls5.pg646.gateway.controller.utils.ErrorHandlingUtil;
import ls5.pg646.gateway.db.entities.FloorPlan;
import ls5.pg646.gateway.db.repositories.FloorPlanRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/floor-plan")
public class FloorPlanController {

  private static final Logger logger = LoggerFactory.getLogger(FloorPlanController.class);
  private static final String FLOORPLAN_TYPE_STRING = FloorPlan.class.getName()
      .replaceAll("([^^])([A-Z][a-z])", "$1-$2").toLowerCase();

  private final FloorPlanRepo floorPlanRepo;

  @Autowired
  public FloorPlanController(FloorPlanRepo floorPlanRepo) {
    this.floorPlanRepo = floorPlanRepo;
  }

  @PostMapping
  public FloorPlanLoadResponse saveFloorPlan(@RequestBody FloorPlanDto floorPlan) {
    FloorPlanLoadResponse response = new FloorPlanLoadResponse();
    if (floorPlan.getId() == null && floorPlanRepo.existsByNameIgnoreCase(floorPlan.getName())
        || floorPlan.getId() != null && floorPlanRepo.existsByNameIgnoreCaseAndIdIsNot(
        floorPlan.getName(), floorPlan.getId())) {
      logger.warn("Floor plan with name {} already exists", floorPlan.getName());
      ErrorHandlingUtil.reportDuplicatedValue(response, FLOORPLAN_TYPE_STRING, "name",
          floorPlan.getName());
      return response;
    }

    FloorPlan floorPlanObject;

    if (floorPlan.getId() != null) {
      Optional<FloorPlan> fp = floorPlanRepo.findById(floorPlan.getId());

      if (fp.isEmpty()) {
        logger.warn("requested floor plan with id {} was not found", floorPlan.getId());

        ErrorHandlingUtil.reportObjectNotFound(
            response, FLOORPLAN_TYPE_STRING, "id", floorPlan.getId().toString());

        return response;
      }

      floorPlanObject = fp.get();
    } else {
      floorPlanObject = new FloorPlan();
    }

    floorPlanObject.setFloor(floorPlan.getFloor());
    floorPlanObject.setFloorPlanData(floorPlan.getFloorPlanData());
    floorPlanObject.setName(floorPlan.getName());

    logger.debug("saving floor plan");
    response.setData(new FloorPlanDto(floorPlanRepo.save(floorPlanObject)));

    return response;
  }

  @GetMapping("/{id}")
  public FloorPlanLoadResponse getFloorPlan(@PathVariable("id") Integer id) {
    FloorPlanLoadResponse response = new FloorPlanLoadResponse();

    if (id.equals(-1)) {
      Iterator<FloorPlan> flpI = floorPlanRepo.findAll().iterator();

      if (flpI.hasNext()) {
        response.setData(new FloorPlanDto(flpI.next()));
        response.setSuccess();
      } else {
        ErrorHandlingUtil.reportObjectNotFound(response, FLOORPLAN_TYPE_STRING, "id",
            id + "");
      }

      return response;
    }

    Optional<FloorPlan> fp = floorPlanRepo.findById(id);

    if (fp.isPresent()) {
      response.setSuccess();
      response.setData(new FloorPlanDto(fp.get()));
    } else {
      logger.warn("requested floor plan with id '{}' was not found", id);

      ErrorHandlingUtil.reportObjectNotFound(response, FLOORPLAN_TYPE_STRING, "id", id.toString());
    }

    return response;
  }

  @GetMapping()
  public FloorPlanListResponse getFloorPlans() {
    FloorPlanListResponse response = new FloorPlanListResponse();

    List<FloorPlanDto> plans = new ArrayList<>();

    floorPlanRepo.findAllOrderByFloorDesc().forEach(f -> plans.add(new FloorPlanDto(f)));

    response.setData(plans);
    return response;
  }

  @DeleteMapping("/{id}")
  public BaseEmptyResponse deleteFloorPlan(@PathVariable int id) {
    BaseEmptyResponse response = new BaseEmptyResponse();

    Optional<FloorPlan> fp = floorPlanRepo.findById(id);
    if (fp.isPresent()) {
      floorPlanRepo.deleteById(id);
      response.setSuccess();
    } else {
      logger.warn("Deletion failed: floor plan with id '{}' was not found", id);

      ErrorHandlingUtil.reportObjectNotFound(response, FLOORPLAN_TYPE_STRING, "id",
          Integer.toString(id));
    }
    return response;
  }

  public FloorPlanRepo getFloorPlanRepo() {
    return floorPlanRepo;
  }
}
