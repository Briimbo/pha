package ls5.pg646.gateway.controller.homeassistant;

import java.util.List;

import ls5.pg646.gateway.controller.utils.JsonResponse;
import ls5.pg646.gateway.service.homeassistant.EntityState;


public class HomeAssistantStatesResponse extends JsonResponse<List<EntityState>>
{

}
