package ls5.pg646.gateway.controller.utils;

import ls5.pg646.gateway.controller.utils.JsonResponse.ResultDetails;
import ls5.pg646.gateway.service.exceptions.InvalidStateException;
import ls5.pg646.gateway.service.exceptions.InvalidValueException;

/**
 * Class for simpler error handling in the controller.
 */
public class ErrorHandlingUtil {

  private ErrorHandlingUtil() {
    throw new IllegalStateException("do not instantiate");
  }

  public static void handleException(JsonResponse<?> response, Exception ex) {
    if (ex instanceof InvalidStateException invalidStateException) {
      reportInvalidState(
          response,
          invalidStateException.getState(),
          invalidStateException.getObject());
    } else if (ex instanceof InvalidValueException invalidValueException) {
      reportInvalidValue(
          response,
          invalidValueException.getValue(),
          invalidValueException.getValueName());
    } else {
      reportInternalServerError(response, ex.getMessage());
    }
  }

  public static void reportInternalServerError(JsonResponse<?> res, String msg) {
    res.addResult(new JsonResponse.ResultDetails("error/internal", msg));
  }

  public static void reportInvalidState(JsonResponse<?> res, String state, String obj) {

    JsonResponse.ResultDetails details =
        new JsonResponse.ResultDetails(
            "error/state/invalid",
            "state '" + state + "' of object '" + obj + "' is invalid for this " + "operation");

    details.addParam("object", obj);
    details.addParam("state", state);
    res.addResult(details);
  }

  public static void reportInvalidValue(JsonResponse<?> res, String value, String valueName) {

    JsonResponse.ResultDetails details =
        new JsonResponse.ResultDetails(
            "error/value/invalid", "'" + value + "' for field '" + valueName + "' is invalid");

    details.addParam("value", value);
    details.addParam("valueName", valueName);
    res.addResult(details);
  }


  public static void reportObjectNotFound(JsonResponse<?> res, String objectType,
      String identifierName, String identifier) {

    JsonResponse.ResultDetails details =
        new JsonResponse.ResultDetails(
            "error/object-mgmt/not-found",
            objectType + " with '" + identifierName + "' set to '" + identifier
                + "' was not found");

    details.addParam("identifierName", identifierName);
    details.addParam("identifier", identifier);
    details.addParam("objectType", objectType);
    res.addResult(details);
  }

  public static void reportDuplicatedValue(JsonResponse<?> res, String objectType, String valueName,
      String value) {
    ResultDetails details = new ResultDetails("error/object-mgmt/duplicate-value",
        objectType + " with '" + valueName + "' set to '" + value
            + "' already exists but '" + valueName + "' must be unique.");

    details.addParam("valueName", valueName);
    details.addParam("value", value);
    details.addParam("objectType", objectType);
    res.addResult(details);
  }

}
