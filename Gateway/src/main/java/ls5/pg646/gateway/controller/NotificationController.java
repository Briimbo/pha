package ls5.pg646.gateway.controller;

import java.util.stream.Collectors;

import ls5.pg646.gateway.controller.notification.NotificationChannelCrudResponse;
import ls5.pg646.gateway.controller.notification.NotificationChannelDto;
import ls5.pg646.gateway.controller.notification.NotificationChannelLoadResponse;
import ls5.pg646.gateway.controller.notification.NotificationCrudResponse;
import ls5.pg646.gateway.controller.notification.NotificationDto;
import ls5.pg646.gateway.controller.notification.NotificationPage;
import ls5.pg646.gateway.controller.notification.NotificationPageResponse;
import ls5.pg646.gateway.controller.utils.BaseIdResponse;
import ls5.pg646.gateway.controller.utils.ErrorHandlingUtil;
import ls5.pg646.gateway.db.entities.Notification;
import ls5.pg646.gateway.service.exceptions.ObjectNotFoundException;
import ls5.pg646.gateway.service.notification.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

/** Controller for loading all home assistant related data. */
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/notification")
public class NotificationController {

  @Autowired private NotificationService service;

  /**
   * Get the notifications.
   *
   * @param page the page
   * @return the response containing the loaded page
   */
  @GetMapping("")
  public NotificationPageResponse loadNotifications(@RequestParam("page") int page) {
    NotificationPageResponse response = new NotificationPageResponse();

    Page<Notification> loadedPage = service.loadNotifications(page);
    NotificationPage pageInfo = new NotificationPage(loadedPage);
    pageInfo.setItems(loadedPage.get().map(NotificationDto::new).collect(Collectors.toList()));
    response.setData(pageInfo);

    response.setSuccess();
    return response;
  }

  /**
   * Get the unread notifications count.
   *
   * @return the response containing the loaded page
   */
  @GetMapping("/unread-count")
  public BaseIdResponse countUnread() {
    BaseIdResponse response = new BaseIdResponse();

    Integer count = service.countUnreadNotifications();
    response.setData(count);

    response.setSuccess();
    return response;
  }

  /**
   * Mark all notifications as read.
   *
   * @return the amount of read notifications
   */
  @PostMapping("/read-all")
  public BaseIdResponse markAllAsRead() {
    BaseIdResponse response = new BaseIdResponse();
    Integer n = service.markAllNotificationAsRead();
    response.setData(n);
    response.setSuccess();
    return response;
  }

  /**
   * Subscribe to all events.
   *
   * @return the amount of read notifications
   */
  @GetMapping(path = "/subscribe", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public SseEmitter subscribe() {
    return service.addEmitter();
  }

  /**
   * Delete all notifications.
   *
   * @return the amount of deleted notifications
   */
  @DeleteMapping("")
  public BaseIdResponse deleteAll() {
    BaseIdResponse response = new BaseIdResponse();
    long n = service.deleteNotifications();
    response.setData(Math.toIntExact(n));
    response.setSuccess();
    return response;
  }

  /**
   * Set read status of notifications.
   *
   * @return the updated notification
   */
  @PutMapping("/read/{id}")
  public NotificationCrudResponse updateRead(
      @PathVariable("id") Integer id, @RequestBody Boolean read) {
    NotificationCrudResponse response = new NotificationCrudResponse();
    try {
      Notification n = service.updateRead(id, read);
      response.setData(new NotificationDto(n));
      response.setSuccess();
      return response;
    } catch (ObjectNotFoundException e) {
      ErrorHandlingUtil.reportObjectNotFound(
          response, e.getObjectName(), e.getObjectIdName(), e.getObjectId());
      return response;
    }
  }

  /**
   * Delete notification.
   *
   * @return the deleted notification
   */
  @DeleteMapping("/{id}")
  public NotificationCrudResponse deleteById(@PathVariable("id") Integer id) {
    NotificationCrudResponse response = new NotificationCrudResponse();
    try {
      Notification n = service.deleteNotification(id);
      response.setData(new NotificationDto(n));
      response.setSuccess();
      return response;
    } catch (ObjectNotFoundException e) {
      ErrorHandlingUtil.reportObjectNotFound(
          response, e.getObjectName(), e.getObjectIdName(), e.getObjectId());
      return response;
    }
  }

  @DeleteMapping("/channel/{id}")
  public NotificationChannelCrudResponse deleteChannel(@PathVariable("id") Integer id)
      throws ObjectNotFoundException {
    var response = new NotificationChannelCrudResponse();

    try {
      var deleted = service.deleteChannel(id);

      response.setData(new NotificationChannelDto(deleted));
      response.setSuccess();
    } catch (ObjectNotFoundException ex) {
      ErrorHandlingUtil.reportObjectNotFound(response, "notificationChannel", "id", id + "");
    }

    return response;
  }

  @PostMapping("/channel/new")
  public NotificationChannelCrudResponse createNewChannel(
      @RequestBody NotificationChannelDto channel) {
    var response = new NotificationChannelCrudResponse();

    var deleted =
        service.createNewChannel(
            channel.getName(),
            channel.getService(),
            channel.getData(),
            channel.getTargets(),
            channel.isActive());

    response.setData(new NotificationChannelDto(deleted));
    response.setSuccess();

    return response;
  }

  @PutMapping("/channel/update")
  public NotificationChannelCrudResponse updateChannel(
      @RequestBody NotificationChannelDto channel) {
    var response = new NotificationChannelCrudResponse();

    try {
      var deleted =
          service.updateChannel(
              channel.getId(),
              channel.getName(),
              channel.getService(),
              channel.getData(),
              channel.getTargets(),
              channel.isActive());

      response.setData(new NotificationChannelDto(deleted));
      response.setSuccess();
    } catch (ObjectNotFoundException ex) {
      ErrorHandlingUtil.reportObjectNotFound(
          response, "notificationChannel", "id", channel.getId() + "");
    }

    return response;
  }

  @GetMapping("/channels")
  public NotificationChannelLoadResponse loadChannels() {
    var response = new NotificationChannelLoadResponse();
    var channels = service.loadChannels();

    response.setData(
        channels.stream().map(NotificationChannelDto::new).collect(Collectors.toList()));

    response.setSuccess();

    return response;
  }
}
