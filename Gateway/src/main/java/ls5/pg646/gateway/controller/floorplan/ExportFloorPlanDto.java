package ls5.pg646.gateway.controller.floorplan;

import java.util.Collection;

public record ExportFloorPlanDto(int id, String name, String backgroundImgBase64, int width,
                                 int height, Collection<FloorPlanEntity> entities) {

}