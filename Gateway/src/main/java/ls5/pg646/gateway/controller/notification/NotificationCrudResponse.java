package ls5.pg646.gateway.controller.notification;

import ls5.pg646.gateway.controller.utils.JsonResponse;

/** Class for all notification operations */
public class NotificationCrudResponse extends JsonResponse<NotificationDto> {}
