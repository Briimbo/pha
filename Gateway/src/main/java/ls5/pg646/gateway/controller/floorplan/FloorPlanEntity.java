package ls5.pg646.gateway.controller.floorplan;

public record FloorPlanEntity(double x, double y, String id, String deviceKind) {

  public boolean isNumeric() {
    return "temperature".equals(deviceKind()) || "humidity".equals(deviceKind());
  }
}
