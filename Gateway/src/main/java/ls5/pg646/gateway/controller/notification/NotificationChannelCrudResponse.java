package ls5.pg646.gateway.controller.notification;

import ls5.pg646.gateway.controller.utils.JsonResponse;

public class NotificationChannelCrudResponse extends JsonResponse<NotificationChannelDto> {}
