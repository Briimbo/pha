package ls5.pg646.gateway.controller.notification;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import ls5.pg646.gateway.db.entities.Notification;

@Getter
@Setter
public class NotificationDto
{
  private Integer id;

  /** the title of the notification. */
  private String title;

  /** the main message of the notification. */
  private String message;

  /** if the notification was read by the user. */
  private boolean read;

  /** the date the notification was created. */
  private Date createdAt;

  /** the type of the notification. */
  private Notification.Type type;

  /** the type of the linked object. */
  private Notification.LinkedObjectType linkedObjectType;

  /** the id of the linked object. */
  private Integer linkedObjectId;

  /** Default constructor. */
  public NotificationDto () {}

  public NotificationDto (Notification n) {
    this.createdAt = n.getCreatedAt();
    this.id = n.getId();
    this.linkedObjectId = n.getLinkedObjectId();
    this.linkedObjectType = n.getLinkedObjectType();
    this.read = n.isRead();
    this.message = n.getMessage();
    this.title = n.getTitle();
    this.type = n.getType();
  }

}
