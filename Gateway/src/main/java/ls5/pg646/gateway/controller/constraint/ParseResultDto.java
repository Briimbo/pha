package ls5.pg646.gateway.controller.constraint;

import java.util.Collections;
import java.util.List;
import javax.validation.constraints.NotNull;

/**
 * Parsing result after submitting the raw text, containing parsed constraints and syntax errors.
 *
 * <p>
 * This is a best-effort list. If syntax errors are present, parsing is attempted and a match for as
 * many constraints as possible is generated, but due to the existing errors, no guarantees can be 
 * made about the number of correctly parsed constraints -- an empty list is perfectly acceptable.
 * </p>
 *
 * @param constraints Correctly parsed constraints. Will never be null (uses empty list).
 * @param errors List of syntax errors.  Will never be null (uses empty list).
 */
public record ParseResultDto(
    @NotNull ConstraintDocumentDto document,
    @NotNull StructuredConstraintDocumentDto structured,
    @NotNull List<SyntaxErrorDto> errors) {
  
  /** Ensures that errors is not-null and unmodifiable.*/
  public ParseResultDto {
    if (errors == null) {
      errors = List.of();
    } else {
      errors = Collections.unmodifiableList(errors);
    }
  }

  /** Syntax errors as reported by Xtext. */
  public record SyntaxErrorDto(
      @NotNull int startLine, 
      @NotNull int endLine, 
      @NotNull String error) {
    public String toString() {
      return "Syntax error at line #%d-%d: %s".formatted(startLine, endLine, error);
    }
  }
}


