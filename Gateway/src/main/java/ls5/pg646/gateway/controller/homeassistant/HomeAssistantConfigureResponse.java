package ls5.pg646.gateway.controller.homeassistant;

import ls5.pg646.gateway.controller.utils.JsonResponse;

public class HomeAssistantConfigureResponse extends JsonResponse<ConfigureInstanceParams> {}
