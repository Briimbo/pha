package ls5.pg646.gateway.controller.utils;

import java.util.List;
import org.springframework.data.domain.Page;

public class PageResponse<T> {
  private long totalItems;

  private int totalPages;

  private List<T> items;

  public PageResponse(Page<?> page) {
    this.totalItems = page.getTotalElements();
    this.totalPages = page.getTotalPages();
  }

  public long getTotalItems() {
    return totalItems;
  }

  public void setTotalItems(long totalItems) {
    this.totalItems = totalItems;
  }

  public int getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(int totalPages) {
    this.totalPages = totalPages;
  }

  public List<T> getItems() {
    return items;
  }

  public void setItems(List<T> items) {
    this.items = items;
  }
}
