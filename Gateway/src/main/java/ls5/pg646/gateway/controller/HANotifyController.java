package ls5.pg646.gateway.controller;

import ls5.pg646.gateway.controller.utils.ServiceRequestData;
import ls5.pg646.gateway.service.exceptions.InternalServerError;
import ls5.pg646.gateway.service.exceptions.InvalidStateException;
import ls5.pg646.gateway.service.homeassistant.HomeAssistantConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/hanotify")
public class HANotifyController {

    @Autowired
    HomeAssistantConnector hac;

    /**
     * @param data - the JSON object
     * @throws InvalidStateException
     * @throws InterruptedException
     * @throws InternalServerError
     *
     * Sends JSON object to the /api/service/notify/<serviceName> endpoint
     * of the configured Home Assistant instance, which sends the notification data
     * to the specified service.
     */
    @PostMapping("")
    public void haNotify(@RequestBody ServiceRequestData data) throws InvalidStateException, InterruptedException, InternalServerError {
        hac.haNotify(data);
    }

}
