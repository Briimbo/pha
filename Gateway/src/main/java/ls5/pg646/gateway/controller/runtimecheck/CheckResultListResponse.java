package ls5.pg646.gateway.controller.runtimecheck;

import java.util.List;
import ls5.pg646.gateway.controller.utils.JsonResponse;
import ls5.pg646.gateway.service.runtimecheck.CheckResult;

public class CheckResultListResponse extends JsonResponse<List<CheckResult>> {

  public CheckResultListResponse() {
  }

  public CheckResultListResponse(List<CheckResult> data) {
    super(data);
  }
}
