package ls5.pg646.gateway.controller.constraint;

import javax.validation.constraints.NotNull;

/**
 * Parsed constraint (metadata only).
 */
public record ConstraintDto(@NotNull int documentId, @NotNull String name) {
  
  public String identifier() {
    return documentId + "#" + name;
  }
}
