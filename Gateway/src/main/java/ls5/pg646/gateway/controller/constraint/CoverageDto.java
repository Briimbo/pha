package ls5.pg646.gateway.controller.constraint;

import javax.validation.constraints.NotNull;

public record CoverageDto(@NotNull int totalEntities, @NotNull int totalSupportedEntities,
                          @NotNull int coveredEntities,
                          @NotNull int totalAutomations, @NotNull int coveredAutomations) {

}
