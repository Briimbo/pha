package ls5.pg646.gateway.controller.homeassistant;

import java.util.List;
import ls5.pg646.gateway.service.homeassistant.Group;

public class ExportGroupRequest {

  private List<Group> groups;

  private String floorName;

  private Long floorId;

  public List<Group> getGroups() {
    return groups;
  }

  public void setGroups(List<Group> groups) {
    this.groups = groups;
  }

  public String getFloorName() {
    return floorName;
  }

  public void setFloorName(String floorName) {
    this.floorName = floorName;
  }

  public Long getFloorId() {
    return floorId;
  }

  public void setFloorId(Long floorId) {
    this.floorId = floorId;
  }
}
