package ls5.pg646.gateway.controller.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonResponse<T> {

  private List<ResultDetails> results;

  private T data;

  public JsonResponse() {
    results = new ArrayList<>();
  }

  public JsonResponse(T data) {
    this();
    this.setSuccess();
    this.setData(data);
  }

  public void addResult(ResultDetails details) {
    results.add(details);
  }

  public void setSuccess() {
    addResult(new ResultDetails("success", "the request was processed successfully"));
  }

  public List<ResultDetails> getResults() {
    return results;
  }

  public void setResults(List<ResultDetails> results) {
    this.results = results;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

  public static class ResultDetails {

    private String code;

    private String msg;

    private Map<String, String> params;

    public ResultDetails() {
    }

    public ResultDetails(String code, String msg) {
      this.code = code;
      this.msg = msg;
    }

    public String getCode() {
      return code;
    }

    public void setCode(String code) {
      this.code = code;
    }

    public String getMsg() {
      return msg;
    }

    public void setMsg(String msg) {
      this.msg = msg;
    }

    public void addParam(String key, String value) {
      if (params == null) {
        params = new HashMap<>();
      }

      params.put(key, value);
    }

    public Map<String, String> getParams() {
      return params;
    }

    public void setParams(Map<String, String> params) {
      this.params = params;
    }
  }
}
