package ls5.pg646.gateway.controller.homeassistant;

public class ConfigureInstanceParams {

  /** the url to use. */
  private String url;

  /** the token to use. */
  private String token;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}
