package ls5.pg646.gateway.controller.floorplan;

import ls5.pg646.gateway.controller.utils.JsonResponse;

public class FloorPlanLoadResponse extends JsonResponse<FloorPlanDto> {}
