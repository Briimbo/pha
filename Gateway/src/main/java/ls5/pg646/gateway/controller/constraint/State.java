package ls5.pg646.gateway.controller.constraint;

/**
 * Validation result.
 */
public enum State {
  SUCCESS,
  FAIL,
  IN_PROGRESS,
  UNKNOWN
}