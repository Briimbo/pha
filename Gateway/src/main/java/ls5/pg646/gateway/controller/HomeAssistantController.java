package ls5.pg646.gateway.controller;

import java.io.IOException;
import ls5.pg646.PhaException;
import ls5.pg646.gateway.controller.floorplan.ExportFloorPlanDto;
import ls5.pg646.gateway.controller.homeassistant.ConfigureInstanceParams;
import ls5.pg646.gateway.controller.homeassistant.ExportGroupRequest;
import ls5.pg646.gateway.controller.homeassistant.HomeAssistantConfigureResponse;
import ls5.pg646.gateway.controller.homeassistant.HomeAssistantStatesResponse;
import ls5.pg646.gateway.controller.utils.BaseBooleanResponse;
import ls5.pg646.gateway.controller.utils.BaseEmptyResponse;
import ls5.pg646.gateway.controller.utils.ErrorHandlingUtil;
import ls5.pg646.gateway.controller.utils.JsonResponse.ResultDetails;
import ls5.pg646.gateway.service.homeassistant.HomeAssistantConnector;
import ls5.pg646.gateway.service.homeassistant.HomeAssistantInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** Controller for loading all home assistant related data. */
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/ha")
public class HomeAssistantController {

  private static final Logger logger = LoggerFactory.getLogger(HomeAssistantController.class);

  @Autowired private HomeAssistantConnector connector;

  @GetMapping("/entities")
  public HomeAssistantStatesResponse loadEntities() throws InterruptedException {
    HomeAssistantStatesResponse response = new HomeAssistantStatesResponse();
    try {
      response.setData(connector.loadEntityStates(true));
    } catch (InterruptedException e) {
      throw e;
    } catch (Exception e) {
      e.printStackTrace();
      ErrorHandlingUtil.handleException(response, e);
      return response;
    }

    response.setSuccess();
    return response;
  }

  @PostMapping("/config")
  public BaseBooleanResponse configureInstance(@RequestBody ConfigureInstanceParams req) {
    BaseBooleanResponse response = new BaseBooleanResponse();

    try {
      connector.configureInstance(req.getUrl(), req.getToken());
    } catch (Exception e) {
      logger.warn("Failed to configure HomeAssistant instance: ", e);
      ErrorHandlingUtil.handleException(response, e);
      return response;
    }

    response.setSuccess();
    return response;
  }

  @GetMapping("/config")
  public HomeAssistantConfigureResponse getConfig() {
    HomeAssistantConfigureResponse response = new HomeAssistantConfigureResponse();

    ConfigureInstanceParams responseData = new ConfigureInstanceParams();

    HomeAssistantInstance instance = connector.getConfig();

    responseData.setToken(instance.getToken());

    responseData.setUrl(instance.getAddress() != null ? instance.getAddress().toString() : null);

    response.setData(responseData);

    response.setSuccess();
    return response;
  }

  @PostMapping("/groups")
  public BaseEmptyResponse exportGroups(@RequestBody ExportGroupRequest exportRequest) {
    BaseEmptyResponse response = new BaseEmptyResponse();

    try {
      boolean addedGroups =
          connector.addGroups(
              exportRequest.getGroups(), exportRequest.getFloorName(), exportRequest.getFloorId());

      if (!addedGroups) {
        logger.warn("Failed to write groups to yaml file");
        response.addResult(
            new ResultDetails("error/group/add", "Failed to write groups to a local yaml file."));
        return response;
      }

      boolean reloadedGroups = connector.reloadGroups();
      if (!reloadedGroups) {
        logger.warn(
            "Failed to reload groups in HomeAssistant instance."
                + "The new configuration is probably invalid (e.g. duplicate groups).");
        response.addResult(
            new ResultDetails(
                "error/group/reload", "Failed to reload groups in your HomeAssistant instance."));
        return response;
      }

    } catch (Exception e) {
      if (!(e instanceof PhaException)) {
        logger.warn("Failed to export groups: ", e);
      }
      ErrorHandlingUtil.handleException(response, e);
      return response;
    }

    response.setSuccess();
    return response;
  }

  @PostMapping("/floor-plan")
  public BaseEmptyResponse exportFloorPlanToCard(@RequestBody ExportFloorPlanDto floorPlan) {
    BaseEmptyResponse response = new BaseEmptyResponse();
    try {
      connector.addFloorPlanCard(floorPlan);
      response.setSuccess();
    } catch (Exception e) {
      if (!(e instanceof PhaException)) {
        logger.warn("Failed to export floor plan", e);
      }
      ErrorHandlingUtil.handleException(response, e);
    }
    return response;
  }

  @DeleteMapping("/floor-plan/{id}")
  public BaseEmptyResponse deleteExportedFloorPlanCard(@PathVariable int id) {
    var response = new BaseEmptyResponse();
    try {
      connector.deleteFloorPlanCard(id);
      response.setSuccess();
    } catch (Exception e) {
      if (!(e instanceof PhaException)) {
        logger.warn("Failed to delete exported floor plan", e);
      }
      ErrorHandlingUtil.handleException(response, e);
    }
    return response;
  }

  @DeleteMapping("/groups/{id}")
  public BaseEmptyResponse deleteExportedGroups(@PathVariable int id) {
    var response = new BaseEmptyResponse();
    try {
      connector.deleteGroups(id);
    } catch (IOException e) {
      logger.warn("Failed to delete groups yaml file for floorplan id \"{}\"", id);
      response.addResult(
          new ResultDetails("error/group/delete", "Failed to delete group yaml file."));
      return response;
    }

    try {
      var reloadedGroups = connector.reloadGroups();
      if (!reloadedGroups) {
        logger.warn("Failed to reload groups in HomeAssistant instance.");
        response.addResult(
            new ResultDetails(
                "error/group/reload", "Failed to reload groups in your HomeAssistant instance."));
        return response;
      }

      response.setSuccess();
    } catch (Exception e) {
      if (!(e instanceof PhaException)) {
        logger.warn("Failed to delete exported groups", e);
      }
      ErrorHandlingUtil.handleException(response, e);
    }
    return response;
  }
}
