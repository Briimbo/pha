package ls5.pg646.gateway.controller.floorplan;


import lombok.Getter;
import lombok.Setter;
import ls5.pg646.gateway.db.entities.FloorPlan;


@Getter
@Setter
public class FloorPlanDto {

  private Integer id;

  private String name;

  private String floorPlanData;

  private Integer floor;


  public FloorPlanDto() {
  }


  public FloorPlanDto(FloorPlan fp) {
    setId(fp.getId());
    setFloorPlanData(fp.getFloorPlanData());
    setFloor(fp.getFloor());
    setName(fp.getName());
  }
}
