package ls5.pg646.gateway.controller.floorplan;

import java.util.List;

import ls5.pg646.gateway.controller.utils.JsonResponse;

public class FloorPlanListResponse extends JsonResponse<List<FloorPlanDto>> {}
