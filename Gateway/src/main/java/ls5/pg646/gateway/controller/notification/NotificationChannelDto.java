package ls5.pg646.gateway.controller.notification;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import ls5.pg646.gateway.db.entities.NotificationChannel;

@Getter
@Setter
public class NotificationChannelDto {
  private Integer id;

  /** the home assistant service to use */
  private String service;

  /** name for identification */
  private String name;

  /** if this channel should be used */
  private boolean active;

  /** list of targets to use, might be empty */
  private List<String> targets;

  /** additional data to use */
  private String data;

  public NotificationChannelDto() {}

  public NotificationChannelDto(NotificationChannel channel) {
    this.active = channel.isActive();
    this.name = channel.getName ();
    this.data = channel.getData();
    this.id = channel.getId();
    this.service = channel.getService();
    this.targets = channel.getTargets();
  }
}
