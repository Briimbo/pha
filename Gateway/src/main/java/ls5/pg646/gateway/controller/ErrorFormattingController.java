package ls5.pg646.gateway.controller;

import ls5.pg646.gateway.controller.utils.BaseBooleanResponse;
import ls5.pg646.gateway.controller.utils.ErrorHandlingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Controller for handling uncaught exceptions inside a rest api call.
 *
 * @author danielstefank
 */
@ControllerAdvice
public class ErrorFormattingController implements ErrorController {

  private static final Logger logger = LoggerFactory.getLogger(ErrorFormattingController.class);

  @ExceptionHandler(Exception.class)
  public ResponseEntity<BaseBooleanResponse> handleError(Exception exception) {
    logger.error("An unidentified error occurred in the rest interface", exception);
  
    BaseBooleanResponse response = new BaseBooleanResponse();
    ErrorHandlingUtil.reportInternalServerError(response, exception.getMessage());
    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
