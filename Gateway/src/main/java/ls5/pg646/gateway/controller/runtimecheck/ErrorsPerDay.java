package ls5.pg646.gateway.controller.runtimecheck;

import java.time.LocalDate;
import javax.validation.constraints.NotNull;

public final class ErrorsPerDay {

  private final @NotNull LocalDate date;
  private @NotNull int errors;

  public ErrorsPerDay(@NotNull LocalDate date, @NotNull int errors) {
    this.date = date;
    this.errors = errors;
  }

  public @NotNull LocalDate getDate() {
    return date;
  }

  public @NotNull int getErrors() {
    return errors;
  }

  public void inc() {
    errors += 1;
  }
}
