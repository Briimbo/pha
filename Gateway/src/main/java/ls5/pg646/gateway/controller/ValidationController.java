package ls5.pg646.gateway.controller;

import com.google.inject.Inject;
import com.google.inject.Injector;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.io.StringReader;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import ls5.pg646.constraints.ConstraintsStandaloneSetup;
import ls5.pg646.constraints.constraints.ConstraintDocument;
import ls5.pg646.constraints.constraints.Reading;
import ls5.pg646.gateway.controller.constraint.ConstraintDocumentDto;
import ls5.pg646.gateway.controller.constraint.ConstraintDto;
import ls5.pg646.gateway.controller.constraint.ConstraintListResponse;
import ls5.pg646.gateway.controller.constraint.CoverageDto;
import ls5.pg646.gateway.controller.constraint.CoverageResponse;
import ls5.pg646.gateway.controller.constraint.ParseResponse;
import ls5.pg646.gateway.controller.constraint.ParseResultDto;
import ls5.pg646.gateway.controller.constraint.ParseResultDto.SyntaxErrorDto;
import ls5.pg646.gateway.controller.constraint.StructuredConstraintDocumentDto;
import ls5.pg646.gateway.controller.constraint.ValidateResponse;
import ls5.pg646.gateway.controller.constraint.ValidationState;
import ls5.pg646.gateway.controller.constraint.ValidationStatsReponse;
import ls5.pg646.gateway.controller.utils.BaseBooleanResponse;
import ls5.pg646.gateway.controller.utils.BaseEmptyResponse;
import ls5.pg646.gateway.controller.utils.ErrorHandlingUtil;
import ls5.pg646.gateway.db.entities.ConstraintDocumentEntity;
import ls5.pg646.gateway.db.repositories.ConstraintDocumentRepository;
import ls5.pg646.gateway.service.homeassistant.HomeAssistantConnector;
import ls5.pg646.gateway.service.validation.ValidationService;
import ls5.pg646.gateway.service.validation.ValidationStats;
import ls5.pg646.validation.LogicNgConverter;
import ls5.pg646.validation.Utils;
import ls5.pg646.validation.symbolic.Constraint;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.parser.IParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/constraints/")
public class ValidationController {

  private static final String NOT_FOUND_MSG = "Document with Id %d not found.";

  @Inject
  private IParser parser;

  private final ConstraintDocumentRepository repository;

  @Autowired
  private HomeAssistantConnector connector;

  @Autowired
  ValidationService service;

  @Autowired
  private ValidationStats validationStats;


  @Autowired
  public ValidationController(ConstraintDocumentRepository repository, ValidationService service) {
    this.repository = repository;
    this.service = service;

    // integrate with spring-boot-di at some point (?)
    Injector injector = new ConstraintsStandaloneSetup().createInjectorAndDoEMFRegistration();
    injector.injectMembers(this);
  }

  @GetMapping("statistics")
  @Operation(summary = "Returns accumulated statistics about all static validation results")
  public ValidationStatsReponse getValidationStatistics() {
    return new ValidationStatsReponse(validationStats);
  }

  @GetMapping("tree")
  @Operation(summary = "Returns the structured tree of all documents and their constraints",
      description =
          """
              This list contains the structured data, ie. all constraints and their last validation state.
              """)
  public ConstraintListResponse getAll() {
    var response = new ConstraintListResponse();
    response.setSuccess();
    var documents = Utils.stream(repository.findAll())
        .map(this::parseEntity)
        .map(ParseResultDto::structured)
        .toList();
    response.setData(documents);
    return response;
  }

  @GetMapping("document/{id}")
  @Operation(summary = "Returns the given constraint document", description =
      """
          Returns the given constraint document. The response contains both the raw text as well as the
          metadata and structured data that can be extracted from the raw text (ie. the constraints and
          aliases used in those constraints).
          """,
      responses = {
          @ApiResponse(responseCode = "404", description =
              """
                  Not Found - The given document does not exist.
                  """
          )
      })
  @ResponseStatus(code = HttpStatus.OK)
  public ParseResponse get(@PathVariable int id) {
    var response = new ParseResponse();

    var documentEntity = repository.findById(id).orElseThrow(
        () -> new ResponseStatusException(HttpStatus.NOT_FOUND, NOT_FOUND_MSG.formatted(id)));

    var resultDto = parseEntity(documentEntity);
    response.setSuccess();
    response.setData(resultDto);
    return response;
  }

  @PatchMapping("document/{id}")
  public BaseEmptyResponse updateEnabled(@PathVariable int id, @RequestBody boolean enabled) {
    var response = new BaseEmptyResponse();
    try {
      var document = repository.findById(id).orElseThrow();
      document.setEnabled(enabled);
      repository.save(document);
      response.setSuccess();
    } catch (NoSuchElementException e) {
      ErrorHandlingUtil.reportObjectNotFound(response, "constraint document", "id",
          Integer.toString(id));
    }
    return response;
  }


  @PostMapping("/document/")
  @Operation(summary = "Creates a new constraint document or updates an existing one",
      description =
          """
              Expects `name` and `string` to be set. If `id` is set, updates the given resource,
              otherwise creates a new document.

              Returns the documents meta-data in `document`. All recognized constraints can be found in
              `document.constraints`. Aliases of literals in the constraints can be resolved by the mapping
              contained in `document.readings`

              If the document contains syntax errors, they are contained within the `errors` field.

              If syntax errors are present, constraint recognition is on a best-effort basis. The document
              is saved regardless of any syntax errors.
              """)
  public ParseResponse createOrUpdate(@RequestBody ConstraintDocumentDto data) {
    var response = new ParseResponse();

    ConstraintDocumentEntity documentEntity = repository.findById(data.id())
        .map(entity -> {
          entity.setRawText(data.text());
          entity.setName(data.name());
          return entity;
        })
        .orElseGet(() -> new ConstraintDocumentEntity(data.name(), data.text()));
    repository.save(documentEntity);

    var resultDto = parseEntity(documentEntity);
    response.setSuccess();
    response.setData(resultDto);
    return response;
  }

  @DeleteMapping("document/{id}")
  @Operation(summary = "Deletes the given constraint document", description =
      """
          Deletes the constraint document identified by `id`. If the document cannot be found,
          returns 404.

          Returns nothing.
          """)
  public BaseBooleanResponse delete(@PathVariable int id) {
    if (!repository.existsById(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, NOT_FOUND_MSG.formatted(id));
    }
    repository.deleteById(id);
    var response = new BaseBooleanResponse();
    response.setSuccess();
    response.setData(true);
    return response;
  }

  @PutMapping("validate")
  @Operation(summary = "Submits the given constraints for validation.")
  public ValidateResponse validate(
      @RequestBody List<ConstraintDto> constraints) {
    validationStats.incCheckedDocuments();
    var states = constraints.stream().map(this::submit).toList();
    var response = new ValidateResponse();
    response.setSuccess();
    response.setData(states);
    return response;
  }

  @PutMapping("validate/poll")
  @Operation(summary = "Gets the validation state for the given constraints.")
  public ValidateResponse poll(@RequestBody List<ConstraintDto> constraints) {
    var states = constraints.stream().map(service::poll).toList();
    var response = new ValidateResponse();
    response.setSuccess();
    response.setData(states);
    return response;
  }

  public ValidationState submit(ConstraintDto dto) {
    var documentEntity = repository.findById(dto.documentId()).orElseThrow();
    var constraint = getConstraintOrThrow(documentEntity, dto.name());
    var state = service.enqeue(dto);
    service.validate(dto, constraint);
    return state;
  }

  private Constraint getConstraintOrThrow(ConstraintDocumentEntity constraintDocumentEntity,
      String constraintName) {
    var constraintList = getConstraints(constraintDocumentEntity, constraintName);
    if (constraintList.isEmpty()) {
      throw new NoSuchElementException(
          "There is no constraint in document " + constraintDocumentEntity.getId() + " with name "
              + constraintName);
    }
    return constraintList.get(0);
  }

  private List<Constraint> getConstraints(ConstraintDocumentEntity constraintDocumentEntity,
      String constraintName) {
    IParseResult result = parser.parse(new StringReader(constraintDocumentEntity.getRawText()));
    var document = (ConstraintDocument) result.getRootASTElement();

    // TODO: LogicNgConverter::extractAliases
    var alias = document.getReadings().stream().collect(Collectors.toMap(Reading::getName,
        reading -> reading.getType() + "." + reading.getIdentifier()));
    var converter = new LogicNgConverter(connector, alias);

    return document.getConstraints().stream()
        .filter(d -> StringUtils.isEmpty(constraintName) || d.getName().equals(constraintName))
        .map(d -> converter.constraintFromExpression(d.getName(), d.getExpression()))
        .toList();
  }

  private ParseResultDto parseEntity(ConstraintDocumentEntity documentEntity) {
    var documentDto = new ConstraintDocumentDto(documentEntity.getId(),
        documentEntity.getName(), documentEntity.getRawText());

    IParseResult result = parser.parse(new StringReader(documentEntity.getRawText()));
    var document = (ConstraintDocument) result.getRootASTElement();

    var constraintSummaries = document == null ? List.<ConstraintDto>of() :
        document.getConstraints().stream()
            .map(it -> new ConstraintDto(documentEntity.getId(), it.getName()))
            .toList();

    var structuredDocumentDto = new StructuredConstraintDocumentDto(documentEntity.getId(),
        documentEntity.getName(), constraintSummaries, documentEntity.isEnabled());

    return new ParseResultDto(
        documentDto, structuredDocumentDto,
        Utils.stream(result.getSyntaxErrors())
            .map(it -> new SyntaxErrorDto(it.getStartLine(), it.getEndLine(),
                it.getSyntaxErrorMessage().getMessage()))
            .toList());
  }

  @PostMapping("debug")
  @Operation(summary = "Gets the validation state for the given constraints.")
  public String debug(@RequestBody String xtext) {
    IParseResult result = parser.parse(new StringReader(xtext));
    var document = (ConstraintDocument) result.getRootASTElement();

    // TODO: LogicNgConverter::extractAliases
    var alias = document.getReadings().stream().collect(Collectors.toMap(Reading::getName,
        reading -> reading.getType() + "." + reading.getIdentifier()));

    var converter = new LogicNgConverter(connector, alias);

    var constraints = document.getConstraints();
    var formulas = constraints.stream().map(constraint -> constraint.getName() + ":"
        + converter.formulaFromExpression(constraint.getExpression()));

    return formulas.collect(Collectors.joining("\n"));
  }

  @GetMapping("coverage")
  @Operation(summary = "Reports the current coverage statistics of constraints")
  public CoverageResponse getCoverage() {
    var response = new CoverageResponse();
    try {
      var smartHome = service.acquireSmartHome();

      var coveredEntities = Utils.stream(repository.findAll())
          .filter(ConstraintDocumentEntity::isEnabled)
          .flatMap(d -> getConstraints(d, null).stream())
          .flatMap(c -> smartHome.findRelevantSensors(c).stream())
          .collect(Collectors.toSet());

      var coveredAutomations = smartHome.findAutomationBySensor(coveredEntities).size();

      var totalEntities = connector.loadEntityStates(false).size();
      var totalSupportedEntities = connector.loadEntityStates(true).size();
      var totalAutomations = smartHome.automations().size();

      response.setData(
          new CoverageDto(totalEntities, totalSupportedEntities, coveredEntities.size(),
              totalAutomations, coveredAutomations));
    } catch (Exception ex) {
      ex.printStackTrace();
      ErrorHandlingUtil.handleException(response, ex);
    }

    return response;
  }
}