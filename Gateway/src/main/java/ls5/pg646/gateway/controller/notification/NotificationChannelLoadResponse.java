package ls5.pg646.gateway.controller.notification;

import java.util.List;

import ls5.pg646.gateway.controller.utils.JsonResponse;

public class NotificationChannelLoadResponse extends JsonResponse<List<NotificationChannelDto>> {}
