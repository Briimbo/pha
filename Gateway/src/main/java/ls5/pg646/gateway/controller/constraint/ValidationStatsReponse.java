package ls5.pg646.gateway.controller.constraint;

import ls5.pg646.gateway.controller.utils.JsonResponse;
import ls5.pg646.gateway.service.validation.ValidationStats;

public class ValidationStatsReponse extends JsonResponse<ValidationStats> {

  public ValidationStatsReponse() {
  }

  public ValidationStatsReponse(ValidationStats stats) {
    super(stats);
  }


}
