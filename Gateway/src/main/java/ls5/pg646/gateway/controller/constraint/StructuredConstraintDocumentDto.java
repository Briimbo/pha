package ls5.pg646.gateway.controller.constraint;

import java.util.Collections;
import java.util.List;
import javax.validation.constraints.NotNull;

/** Structured information (parsed constraints) about a constraint document. */
public record StructuredConstraintDocumentDto(
    @NotNull int id,
    @NotNull String name,
    //@NotNull Map<String, String> readings,
    @NotNull List<ConstraintDto> constraints,
    @NotNull boolean enabled) {

  /** Ensures that constraints is non-null and immutable.*/
  public StructuredConstraintDocumentDto {
    if (constraints == null) {
      constraints = List.of();
    } else {
      constraints = Collections.unmodifiableList(constraints);
    }
  }
}