package ls5.pg646.gateway.controller.constraint;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/** Raw text as input by the user. Can be used for create & update. */
public record ConstraintDocumentDto(
    int id,
    @NotNull @Size(min = 1, max = 100) String name,
    @NotNull String text
) {}
