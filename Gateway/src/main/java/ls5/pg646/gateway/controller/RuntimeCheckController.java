package ls5.pg646.gateway.controller;

import io.swagger.v3.oas.annotations.Operation;
import ls5.pg646.gateway.controller.runtimecheck.CheckResultListResponse;
import ls5.pg646.gateway.controller.runtimecheck.ErrorCountHistoryResponse;
import ls5.pg646.gateway.controller.utils.StringResponse;
import ls5.pg646.gateway.service.runtimecheck.RuntimeCheckStats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/runtime/")
public class RuntimeCheckController {

  @Autowired
  private RuntimeCheckStats runtimeCheckStats;

  @GetMapping
  @Operation(summary = "Returns the current state of runtime checking",
      description = "Returns the current state of runtime checking and includes "
          + "information about the time the state changed for each constraint")
  public CheckResultListResponse getResults() {
    return new CheckResultListResponse(runtimeCheckStats.getLatestUpdates());
  }

  @GetMapping("lastCheckTime")
  @Operation(summary = "Returns the timestamp of the last check run")
  public StringResponse getLastCheckTime() {
    var res = runtimeCheckStats.getLastCheckTime();
    return new StringResponse(res != null ? res.toString() : "");
  }

  @GetMapping("currentErrors")
  @Operation(summary = "Returns the results of all constraints currently failing")
  public CheckResultListResponse getCurrentErrors() {
    return new CheckResultListResponse(runtimeCheckStats.getCurrentErrors());
  }

  @GetMapping("history/{docId}/{constraintName}")
  @Operation(summary = "Returns all check results of a constraint, ordered by date")
  public CheckResultListResponse getHistory(@PathVariable int docId,
      @PathVariable String constraintName) {
    return new CheckResultListResponse(runtimeCheckStats.getHistory(docId, constraintName));
  }

  @GetMapping("history/errors")
  @Operation(summary = "Returns the sum of errors per day, ordered by date")
  public ErrorCountHistoryResponse getErrorCountHistory() {
    return new ErrorCountHistoryResponse(runtimeCheckStats.getErrorCountHistory());
  }
}
