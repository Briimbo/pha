package ls5.pg646.gateway.controller.utils;

import java.util.*;

public class ServiceRequestData {

    private String serviceName;
    private String message;

    private String title;
    private List<String> target;
    private Map<String, Object> data;

    public ServiceRequestData(String serviceName, String message) {
        this.serviceName = serviceName;
        this.message = message;
    }


    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Optional<String> getTitle() {
        return Optional.ofNullable(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Optional<List<String>> getTarget() {
        return Optional.ofNullable(target);
    }

    public void setTarget(List<String> target) {
        this.target = target;
    }

    public Optional<Map<String, Object>> getData() {
        return Optional.ofNullable(data);
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public void putData(String key, Object value) {
        if (data == null) data = new HashMap<>();
        data.put(key, value);
    }

    public void addTarget(String newTarget) {
        if (target == null) target = new ArrayList<>();
        target.add(newTarget);
    }
}
