package ls5.pg646.gateway.controller.parsing;


public class ParseRequest {

  private String input;


  public String getInput() {
    return input;
  }


  public void setInput(String input) {
    this.input = input;
  }
}
