package ls5.pg646.gateway.controller.utils;

public class StringResponse extends JsonResponse<String> {

  public StringResponse() {
  }

  public StringResponse(String data) {
    super(data);
  }
}
