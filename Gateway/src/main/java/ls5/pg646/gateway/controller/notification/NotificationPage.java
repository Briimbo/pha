package ls5.pg646.gateway.controller.notification;

import ls5.pg646.gateway.controller.utils.PageResponse;
import org.springframework.data.domain.Page;

public class NotificationPage extends PageResponse<NotificationDto> {
  public NotificationPage(Page<?> page) {
    super(page);
  }
}
