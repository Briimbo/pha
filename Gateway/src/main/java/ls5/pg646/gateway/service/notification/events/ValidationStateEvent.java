package ls5.pg646.gateway.service.notification.events;

import java.util.Map;

import ls5.pg646.gateway.controller.constraint.ValidationState;

/** Event for validation updates */
public class ValidationStateEvent implements BaseEvent<Map<String, ValidationState>> {

  /**
   * Constructor
   *
   * @param validationStates the new validation states
   */
  public ValidationStateEvent(Map<String, ValidationState> validationStates) {
    this.validationStates = validationStates;
  }

  private final Map<String, ValidationState> validationStates;

  @Override
  public Type getType() {
    return Type.VALIDATION_STATE_UPDATE;
  }

  @Override
  public Map<String, ValidationState> getData() {
    return validationStates;
  }
}
