package ls5.pg646.gateway.service.notification.events;

public class DeleteNotificationsEvent implements BaseEvent<Void> {

  /**
   * The type of the event.
   *
   * @return always {@code Type.DELETE_NOTIFICATIONS}
   */
  @Override
  public Type getType() {
    return Type.DELETE_NOTIFICATIONS;
  }

  /**
   * the data.
   *
   * @return always {@code null}
   */
  @Override
  public Void getData() {
    return null;
  }
}
