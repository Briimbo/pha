package ls5.pg646.gateway.service.exceptions;

public class InternalServerError extends ServiceException {

  private static final long serialVersionUID = -4313437390692081041L;

  public InternalServerError(String msg) {
    super(msg);
  }

  public InternalServerError(Throwable cause) {
    super(cause);
  }
}
