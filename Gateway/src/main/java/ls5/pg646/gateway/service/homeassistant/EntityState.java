package ls5.pg646.gateway.service.homeassistant;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import java.util.StringJoiner;
import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EntityState {

  @NotNull
  private String entityId;

  @NotNull
  private EntityAttributes attributes;

  @NotNull
  private Date lastChanged;

  private Date lastUpdated;

  @NotNull
  private String state;


  @JsonAlias("entity_id")
  public String getEntityId() {
    return entityId;
  }

  @JsonAlias("entity_id")
  public void setEntityId(String entityId) {
    this.entityId = entityId;
  }

  public EntityAttributes getAttributes() {
    return attributes;
  }

  public void setAttributes(EntityAttributes attributes) {
    this.attributes = attributes;
  }

  @JsonAlias("last_changed")
  public Date getLastChanged() {
    return lastChanged;
  }

  @JsonAlias("last_changed")
  public void setLastChanged(Date lastChanged) {
    this.lastChanged = lastChanged;
  }

  @JsonAlias("last_updated")
  public Date getLastUpdated() {
    return lastUpdated;
  }

  @JsonAlias("last_updated")
  public void setLastUpdated(Date lastUpdated) {
    this.lastUpdated = lastUpdated;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public void initialize() {
    attributes.initializeDeviceKind(getDomain());
  }

  /**
   * Returns the entity's domain from the entity_id, e.g. returns <i>switch</i> for entity_id
   * <i>switch.kitchen_heater</i>
   *
   * @return The entity's domain
   */
  public String getDomain() {
    return this.entityId.split("\\.")[0];
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", EntityState.class.getSimpleName() + "[", "]")
        .add("entityId='" + entityId + "'")
        .add("lastChanged=" + lastChanged)
        .add("lastUpdated=" + lastUpdated)
        .add("state='" + state + "'")
        .add("attributes='" + attributes + "'")
        .toString();
  }
}
