package ls5.pg646.gateway.service.homeassistant.pictureElements;

import java.util.Collection;

public record View(String title, String path, boolean panel, Collection<PictureElements> cards) {

}
