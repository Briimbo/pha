package ls5.pg646.gateway.service.notification.events;

/**
 * Interface for all available events.
 *
 * @param <T> the data of the event
 */
public interface BaseEvent<T> {
  Type getType();

  T getData();
}
