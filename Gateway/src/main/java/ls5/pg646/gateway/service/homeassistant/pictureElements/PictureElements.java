package ls5.pg646.gateway.service.homeassistant.pictureElements;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Collection;

public record PictureElements(@JsonProperty("image") String imageUrl, String type,
                              Collection<PictureElement> elements) {

  public PictureElements(String imageUrl, Collection<PictureElement> elements) {
    this(imageUrl, "picture-elements", elements);
  }
}
