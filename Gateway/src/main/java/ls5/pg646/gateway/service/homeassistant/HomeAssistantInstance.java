package ls5.pg646.gateway.service.homeassistant;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Class for connection to the home assistant API.
 */
@Component
public class HomeAssistantInstance {

  /**
   * address to use.
   */
  private URI address;

  private String token;

  @Value("${pha.app.default.ha.address}")
  private String defaultAddress;

  /**
   * token for auth.
   */
  @Value("${pha.app.default.ha.token}")
  private String defaultToken;

  @Value("${pha.app.default.ha.groupsDir}")
  private String groupsDir;

  @Value("${pha.app.default.ha.configDir}")
  private String configDir;

  @PostConstruct
  public void init() throws URISyntaxException {
    if (this.address == null)
      this.address = new URI(this.defaultAddress);
    this.token = Optional.ofNullable(System.getenv("SUPERVISOR_TOKEN"))
      .orElse(this.defaultToken);
  }

  public URI getAddress() {
    return address;
  }

  public void setAddress(URI address) {
    this.address = address;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getGroupsDir() {
    return groupsDir;
  }

  public void setGroupsDir(String groupsDir) {
    this.groupsDir = groupsDir;
  }

  public String getConfigDir() {
    return configDir;
  }

  public void setConfigDir(String configDir) {
    this.configDir = configDir;
  }
}
