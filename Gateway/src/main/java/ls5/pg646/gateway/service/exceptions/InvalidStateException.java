package ls5.pg646.gateway.service.exceptions;

public class InvalidStateException extends ServiceException {

  private static final long serialVersionUID = -5746787544195193527L;

  private final String state;

  private final String object;

  public InvalidStateException(String errorMessage, String object, String state) {
    super(errorMessage);
    this.object = object;
    this.state = state;
  }

  public String getState() {
    return state;
  }

  public String getObject() {
    return object;
  }
}
