package ls5.pg646.gateway.service.notification.events;

/**
 * type of available events.
 */
public enum Type {
  NEW_NOTIFICATION,

  DELETE_NOTIFICATION,

  UPDATE_NOTIFICATION,

  CLEAR_NOTIFICATIONS,

  VALIDATION_STATE_UPDATE,

  DELETE_NOTIFICATIONS,

  RUNTIME_CHECK
}
