package ls5.pg646.gateway.service.notification.events;

public class NewNotificationEvent extends BaseNotificationEvent {

  /**
   * The type of the event.
   *
   * @return always {@code Type.NEW_NOTIFICATION}
   */
  @Override
  public Type getType() {
    return Type.NEW_NOTIFICATION;
  }
}
