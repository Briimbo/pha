package ls5.pg646.gateway.service.notification.events;

/** Event fired when a notification is updated. */
public class UpdateNotificationEvent extends BaseNotificationEvent {

  /**
   * The type of the event.
   *
   * @return always {@code Type.UPDATE_NOTIFICATION}
   */
  @Override
  public Type getType() {
    return Type.UPDATE_NOTIFICATION;
  }
}
