package ls5.pg646.gateway.service.notification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ls5.pg646.gateway.controller.notification.NotificationDto;
import ls5.pg646.gateway.controller.utils.ServiceRequestData;
import ls5.pg646.gateway.db.entities.Notification;
import ls5.pg646.gateway.db.entities.NotificationChannel;
import ls5.pg646.gateway.db.repositories.NotificationChannelRepo;
import ls5.pg646.gateway.db.repositories.NotificationRepo;
import ls5.pg646.gateway.service.exceptions.InternalServerError;
import ls5.pg646.gateway.service.exceptions.InvalidStateException;
import ls5.pg646.gateway.service.exceptions.ObjectNotFoundException;
import ls5.pg646.gateway.service.homeassistant.HomeAssistantConnector;
import ls5.pg646.gateway.service.notification.events.BaseEvent;
import ls5.pg646.gateway.service.notification.events.BaseNotificationEvent;
import ls5.pg646.gateway.service.notification.events.ClearNotificationsEvent;
import ls5.pg646.gateway.service.notification.events.DeleteNotificationEvent;
import ls5.pg646.gateway.service.notification.events.DeleteNotificationsEvent;
import ls5.pg646.gateway.service.notification.events.NewNotificationEvent;
import ls5.pg646.gateway.service.notification.events.UpdateNotificationEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Service
public class NotificationService {

  private static final int PAGE_SIZE = 10;

  private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class.getSimpleName());


  /** the repo for crud operations on the notification. */
  @Autowired private NotificationRepo notificationRepo;

  /** the repo for crud operations on the notification channels. */
  @Autowired private NotificationChannelRepo notificationChannelRepo;

  /** connector for home assistant service calls. */
  @Autowired private HomeAssistantConnector haConnector;

  /** the listening clients. */
  private final List<SseEmitter> listeningClients = new ArrayList<>();

  /**
   * Count all unread notifications.
   *
   * @return number of all unread notifications
   */
  public Integer countUnreadNotifications() {
    return notificationRepo.countNotificationsByReadIsFalse();
  }

  /**
   * Mark all notifications as read.
   *
   * @return the count of the marked notifications
   */
  public Integer markAllNotificationAsRead() {
    List<Notification> notifications = notificationRepo.findNotificationByReadIsFalse();

    for (Notification n : notifications) {
      n.setRead(true);
      notificationRepo.save(n);
    }

    ClearNotificationsEvent event = new ClearNotificationsEvent();
    sendEvent(event);

    return notifications.size();
  }

  /**
   * add the client to the list of listening clients.
   *
   * @return the event emitter
   */
  public SseEmitter addEmitter() {
    SseEmitter emitter = new SseEmitter(Long.MAX_VALUE);

    emitter.onError(ex -> listeningClients.remove(emitter));

    listeningClients.add(emitter);

    return emitter;
  }

  /**
   * Create a new notification.
   *
   * @param title the title of the notification
   * @param message the message of the notification
   * @param type the type of the notification
   * @param object the linked object type. Maybe {@code null} if there is no linked object
   * @param objectId the linked object id. Maybe {@code null} if there is no linked object
   * @return the created notification
   */
  public Notification createNotification(
      String title,
      String message,
      Notification.Type type,
      Notification.LinkedObjectType object,
      Integer objectId,
      boolean sendToHA) {
    Notification n = new Notification();

    n.setTitle(title);
    n.setMessage(message);
    n.setType(type);
    n.setCreatedAt(new Date());
    n.setLinkedObjectId(objectId);
    n.setLinkedObjectType(object);
    n.setRead(false);

    notificationRepo.save(n);

    BaseNotificationEvent event = new NewNotificationEvent();
    event.setData(new NotificationDto(n));

    sendEvent(event);

    if (sendToHA) sendHANotification(title, message);

    return n;
  }

  /**
   * Load a page of the notifications.
   *
   * @param page the page number
   * @return the page
   */
  public Page<Notification> loadNotifications(int page) {
    Pageable paging = PageRequest.of(page, PAGE_SIZE, Sort.by("createdAt").descending());
    return notificationRepo.findAll(paging);
  }

  /**
   * Update the read flag on a notification.
   *
   * @param notificationId the id of the notification
   * @param read the new read flag
   * @return the updated notification
   * @throws ObjectNotFoundException when no notification is found by this id
   */
  public Notification updateRead(int notificationId, boolean read) throws ObjectNotFoundException {
    Optional<Notification> n = notificationRepo.findById(notificationId);

    if (n.isEmpty()) {
      throw new ObjectNotFoundException(
          "notification with given id was not found",
          "notification",
          Integer.toString(notificationId),
          "id");
    }

    Notification notification = n.get();

    notification.setRead(read);
    notificationRepo.save(notification);

    UpdateNotificationEvent event = new UpdateNotificationEvent();
    event.setData(new NotificationDto(notification));
    sendEvent(event);

    return notification;
  }

  /**
   * Delete all notifications.
   *
   * @return how many notifications have been deleted
   */
  public long deleteNotifications() {
    long count = notificationRepo.count();
    notificationRepo.deleteAll();

    DeleteNotificationsEvent event = new DeleteNotificationsEvent();
    sendEvent(event);

    return count;
  }

  /**
   * Delete a notification by its id.
   *
   * @param id the id of the notification to delete
   * @return the delete notification
   * @throws ObjectNotFoundException when no notification is found by this id
   */
  public Notification deleteNotification(int id) throws ObjectNotFoundException {
    Optional<Notification> opt = notificationRepo.findById(id);

    if (opt.isEmpty()) {
      throw new ObjectNotFoundException(
          "notification with given id was not found", "notification", Integer.toString(id), "id");
    }

    notificationRepo.deleteById(id);

    DeleteNotificationEvent event = new DeleteNotificationEvent();
    event.setData(new NotificationDto(opt.get()));
    sendEvent(event);

    return opt.get();
  }

  /**
   * Send an event to all listening clients.
   *
   * @param event event to send
   */
  public void sendEvent(BaseEvent<?> event) {
    listeningClients.forEach(
        e -> {
          try {
            e.send(event);
          } catch (IOException ex) {
            listeningClients.remove(e);
          }
        });
  }

  /**
   * Delete a notification channel
   *
   * @param id the if of the channel to delete
   * @return the deleted channel
   * @throws ObjectNotFoundException if th object was not found
   */
  public NotificationChannel deleteChannel(@NotNull Integer id) throws ObjectNotFoundException {

    var channel = notificationChannelRepo.findById(id);

    if (channel.isEmpty()) {
      throw new ObjectNotFoundException(
          "notification channel was not found", "id", id.toString(), "notificationChannel");
    }

    notificationChannelRepo.deleteById(id);

    return channel.get();
  }

  /**
   * Create a channel
   *
   * @param name the name of the channel
   * @param service the service to call
   * @param data the data
   * @param targets the targets
   * @param active if it should be active
   * @return the updated channel
   */
  public NotificationChannel createNewChannel(String name,
      String service, String data, List<String> targets, boolean active) {
    var channel = new NotificationChannel();

    channel.setName (name);
    channel.setService(service);
    channel.setData(data);
    channel.setTargets(targets);
    channel.setActive(active);

    return notificationChannelRepo.save(channel);
  }

  /**
   * Update a channel
   *
   * @param id the id of the channel to update
   * @param name name of the channel
   * @param service the service to call
   * @param data the data
   * @param targets the targets
   * @param active if it should be active
   * @return the updated channel
   * @throws ObjectNotFoundException if the channel with the given id was not found
   */
  public NotificationChannel updateChannel(
      @NotNull Integer id, String name, String service, String data, List<String> targets, boolean active)
      throws ObjectNotFoundException {
    var currentChannelOpt = notificationChannelRepo.findById(id);

    if (currentChannelOpt.isEmpty())
      throw new ObjectNotFoundException(
          "notification channel was not found", "id", id.toString(), "notificationChannel");

    var currentChannel = currentChannelOpt.get();

    currentChannel.setData(data);
    currentChannel.setName (name);
    currentChannel.setService(service);
    currentChannel.setActive(active);
    currentChannel.setTargets(targets);

    return notificationChannelRepo.save(currentChannel);
  }

  /**
   * load all channels
   *
   * @return the exising channels
   */
  public List<NotificationChannel> loadChannels() {
    var result = new ArrayList<NotificationChannel>();
    notificationChannelRepo.findAll().forEach(result::add);

    return result;
  }

  /**
   * Set a notification to all connected services
   *
   * @param title title of the notification
   * @param details details
   */
  private void sendHANotification(String title, String details) {
    notificationChannelRepo
        .findAll()
        .forEach(
            channel -> {
              if (channel.isActive()) {
                var data = new ServiceRequestData(channel.getService(), details);
                data.setTitle(title);
                data.setTarget(channel.getTargets());
                TypeReference<HashMap<String, Object>> typeRef = new TypeReference<>() {};
                ObjectMapper mapper = new ObjectMapper();

                try {
                  data.setData(mapper.readValue(channel.getData(), typeRef));
                  haConnector.haNotify(data);
                } catch (JsonProcessingException
                    | InternalServerError
                    | InterruptedException
                    | InvalidStateException e) {
                  // do nothing
                  LOGGER.error("Error sending notification", e);
                }
              }
            });
  }
}
