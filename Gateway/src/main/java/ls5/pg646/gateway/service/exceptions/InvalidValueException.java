package ls5.pg646.gateway.service.exceptions;

public class InvalidValueException extends ServiceException {

  private static final long serialVersionUID = 4033139695329307308L;

  private final String value;

  private final String valueName;

  public InvalidValueException(String errorMessage, String valueName, String value) {
    super(errorMessage);
    this.value = value;
    this.valueName = valueName;
  }

  public String getValue() {
    return value;
  }

  public String getValueName() {
    return valueName;
  }
}
