package ls5.pg646.gateway.service.validation;

import javax.validation.constraints.NotNull;
import org.springframework.stereotype.Component;

@Component
public class ValidationStats {

  /**
   * The sum of started document checks
   */
  private int checkedDocuments;

  /**
   * The sum of started constraint checks
   */
  private int checkedConstraints;

  /**
   * The sum of errors found across all runs. Error are counted per constraint per run, so multiple
   * errors per document per run are possible, but only one per constraint
   */
  private int errors;

  /**
   * The sum of aborted checks across all runs due to internal failures
   */
  private int aborts;

  public synchronized void incAborts() {
    aborts++;
  }

  public synchronized void incErrors() {
    errors++;
  }

  public synchronized void incCheckedDocuments() {
    checkedDocuments++;
  }

  public synchronized void incCheckedConstraints() {
    checkedConstraints++;
  }

  @NotNull
  public int getAborts() {
    return aborts;
  }

  @NotNull
  public int getErrors() {
    return errors;
  }

  @NotNull
  public int getCheckedDocuments() {
    return checkedDocuments;
  }

  @NotNull
  public int getCheckedConstraints() {
    return checkedConstraints;
  }
}
