package ls5.pg646.gateway.service.homeassistant;

import com.fasterxml.jackson.annotation.JsonAlias;

public class Group {

  private String name;

  @JsonAlias("entity_id")
  private String entityId;

  private String[] entities;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEntityId() {
    return entityId;
  }

  public void setEntityId(String entityId) {
    this.entityId = entityId;
  }

  public String[] getEntities() {
    return entities;
  }

  public void setEntities(String[] entities) {
    this.entities = entities;
  }
}
