package ls5.pg646.gateway.service.exceptions;

public class ObjectNotFoundException extends ServiceException {
  private final String objectIdName;

  private final String objectId;

  private final String objectName;

  public ObjectNotFoundException(
      String errorMessage, String objectIdName, String objectId, String objectName) {
    super(errorMessage);
    this.objectId = objectId;
    this.objectName = objectName;
    this.objectIdName = objectIdName;
  }

  public String getObjectIdName() {
    return objectIdName;
  }

  public String getObjectId() {
    return objectId;
  }

  public String getObjectName() {
    return objectName;
  }
}
