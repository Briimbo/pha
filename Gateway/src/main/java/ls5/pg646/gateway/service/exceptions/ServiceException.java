package ls5.pg646.gateway.service.exceptions;

import ls5.pg646.PhaException;

public class ServiceException extends PhaException {

  private static final long serialVersionUID = -5006650847947657281L;

  public ServiceException(String message) {
    super(message);
  }

  public ServiceException(Throwable cause) {
    super(cause);
  }
}
