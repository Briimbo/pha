package ls5.pg646.gateway.service.runtimecheck;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import ls5.pg646.gateway.controller.runtimecheck.ErrorsPerDay;
import org.springframework.stereotype.Component;

@Component
public class RuntimeCheckStats {

  private final Map<String, List<CheckResult>> resultsByConstraint = new HashMap<>();
  private final Map<String, CheckResult> lastChange = new HashMap<>();

  private final List<ErrorsPerDay> errorsPerDay = new ArrayList<>();

  private LocalDateTime lastCheckTime;


  public RuntimeCheckStats() {
    errorsPerDay.add(new ErrorsPerDay(LocalDate.now(), 0));
  }

  /**
   * Adds a check result to the collection of results for that constraint and updates the errors per
   * day and last change statistics accordingly.
   *
   * @param value the new check result
   */
  public void add(CheckResult value) {
    var key = getKey(value);
    var lst = resultsByConstraint.getOrDefault(key, new ArrayList<>());
    lst.add(value);
    resultsByConstraint.put(key, lst);

    var lastEntry = errorsPerDay.get(errorsPerDay.size() - 1);
    if (!lastEntry.getDate().isEqual(value.checkTime().toLocalDate())) {
      lastEntry = new ErrorsPerDay(value.checkTime().toLocalDate(), 0);
      errorsPerDay.add(lastEntry);
    }
    if (value.failed()) {
      lastEntry.inc();
    }

    if (!lastChange.containsKey(key) || lastChange.get(key).success() != value.success()) {
      lastChange.put(key, value);
    }
  }

  /**
   * Checks whether the given result is different to the previous result
   *
   * @param result the result of a new constraint check
   * @return <code>true</code> if the state changed, else <code>false</code>
   */
  public boolean didStateChange(CheckResult result) {
    var prevResult = lastChange.get(getKey(result));
    return prevResult == null || prevResult.success() != result.success();
  }

  /**
   * @return the last check result for each constraint that changed the state
   */
  public List<CheckResult> getLatestUpdates() {
    return lastChange.entrySet().stream()
        .sorted(Entry.comparingByKey())
        .map(Entry::getValue)
        .toList();
  }

  private String getKey(CheckResult value) {
    return getKey(value.docId(), value.name());
  }

  private String getKey(int docId, String constraintName) {
    return docId + "-" + constraintName;
  }

  public LocalDateTime getLastCheckTime() {
    return lastCheckTime;
  }

  public void setLastCheckTime(LocalDateTime lastCheckTime) {
    this.lastCheckTime = lastCheckTime;
  }

  /**
   * @return the results of constraints that are violated in the current SmartHome state
   */
  public List<CheckResult> getCurrentErrors() {
    return lastChange.values().stream()
        .filter(CheckResult::failed)
        .sorted(Comparator.comparing(CheckResult::checkTime))
        .toList();
  }

  public List<CheckResult> getHistory(int docId, String constraintName) {
    return resultsByConstraint.get(getKey(docId, constraintName));
  }

  /**
   * @return the sum of errors for each day, ordered by date (oldest first)
   */
  public List<ErrorsPerDay> getErrorCountHistory() {
    return errorsPerDay;
  }
}
