package ls5.pg646.gateway.service.notification.events;

public class DeleteNotificationEvent extends BaseNotificationEvent {

  /**
   * The type of the event.
   *
   * @return always {@code Type.DELETE_NOTIFICATION}
   */
  @Override
  public Type getType() {
    return Type.DELETE_NOTIFICATION;
  }
}
