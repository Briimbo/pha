package ls5.pg646.gateway.service.notification.events;

import ls5.pg646.gateway.service.runtimecheck.CheckResult;

/**
 * Event for validation updates
 */
public class RuntimeCheckEvent implements BaseEvent<CheckResult> {

  /**
   * Constructor
   *
   * @param checkResult The new (changed) check result for a runtime constraint
   */
  public RuntimeCheckEvent(CheckResult checkResult) {
    this.checkResult = checkResult;
  }

  private final CheckResult checkResult;

  @Override
  public Type getType() {
    return Type.RUNTIME_CHECK;
  }

  @Override
  public CheckResult getData() {
    return checkResult;
  }
}
