package ls5.pg646.gateway.service.notification.events;

import ls5.pg646.gateway.controller.notification.NotificationDto;

public abstract class BaseNotificationEvent implements BaseEvent<NotificationDto> {

  /** The affected Notification. */
  private NotificationDto data;

  /**
   * The affected Notification.
   *
   * @return the notification
   */
  @Override
  public NotificationDto getData() {
    return data;
  }

  /**
   * The affected Notification.
   *
   * @param data the data
   */
  public void setData(NotificationDto data) {
    this.data = data;
  }
}
