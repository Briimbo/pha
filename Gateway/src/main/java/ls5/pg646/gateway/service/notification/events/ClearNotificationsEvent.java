package ls5.pg646.gateway.service.notification.events;

public class ClearNotificationsEvent implements BaseEvent<Void> {

  /**
   * The type of the event.
   *
   * @return always {@code Type.CLEAR_NOTIFICATIONS}
   */
  @Override
  public Type getType() {
    return Type.CLEAR_NOTIFICATIONS;
  }

  @Override
  public Void getData() {
    return null;
  }
}
