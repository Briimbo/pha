package ls5.pg646.gateway.service.homeassistant;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
abstract class IgnoreEntityIdMixin {

  @JsonIgnore
  private String entityId;
}
