package ls5.pg646.gateway.service.validation;

import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.parse.Parser;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import ls5.pg646.adapter.ConditionAdapter;
import ls5.pg646.gateway.controller.constraint.ConstraintDto;
import ls5.pg646.gateway.controller.constraint.State;
import ls5.pg646.gateway.controller.constraint.ValidationState;
import ls5.pg646.gateway.controller.constraint.ValidationState.Phase;
import ls5.pg646.gateway.db.entities.Notification;
import ls5.pg646.gateway.db.entities.Notification.LinkedObjectType;
import ls5.pg646.gateway.service.exceptions.InternalServerError;
import ls5.pg646.gateway.service.exceptions.InvalidStateException;
import ls5.pg646.gateway.service.notification.NotificationService;
import ls5.pg646.gateway.service.notification.events.ValidationStateEvent;
import ls5.pg646.parsing.AutomationParser;
import ls5.pg646.parsing.DeviceClasses;
import ls5.pg646.validation.checks.AutomationFixesUserError;
import ls5.pg646.validation.checks.AutomationReachesErrorState;
import ls5.pg646.validation.checks.AutomationReachesErrorStateEnv;
import ls5.pg646.validation.checks.AutomatonCheck;
import ls5.pg646.validation.checks.SearchForAutomationCycles;
import ls5.pg646.validation.jgrapht.AutomatonBuilder;
import ls5.pg646.validation.symbolic.Constraint;
import ls5.pg646.validation.symbolic.FakeOracle;
import ls5.pg646.validation.symbolic.SmartHome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ValidationService {

  @Autowired
  private NotificationService notificationService;

  @Autowired
  private ValidationStats validationStats;

  public static final Logger logger = System.getLogger(ValidationService.class.getCanonicalName());

  ConcurrentHashMap<String, ValidationState> repository = new ConcurrentHashMap<>();

  @Value("${pha.app.default.ha.configDir}")
  String path;

  @Autowired
  DeviceClasses deviceClasses;

  public SmartHome acquireSmartHome() throws IOException {
    var automations = AutomationParser.fromConfig(path);
    return ConditionAdapter.parseToModelSmartHome(automations);
  }

  @Async("validation-service-excecutor")
  public void validate(ConstraintDto dto, Constraint constraint) {
    logger.log(Level.INFO, "Validating constraint " + dto.name());
    validationStats.incCheckedConstraints();

    Map<String, ValidationState> stateMap = new HashMap<>();

    var state = new ValidationState(dto);

    stateMap.put(dto.identifier(), state);
    state.setState(State.IN_PROGRESS);
    notificationService.sendEvent(new ValidationStateEvent(stateMap));
    repository.put(dto.identifier(), state);

    logger.log(Level.INFO, "Acquiring smart home");

    state.setPhase(Phase.HOME_ACQUISITION);

    notificationService.sendEvent(new ValidationStateEvent(stateMap));

    SmartHome home;
    try {
      home = acquireSmartHome();

    } catch (IOException e) {
      logger.log(Level.ERROR, e);
      state.setState(State.FAIL);
      notificationService.sendEvent(new ValidationStateEvent(stateMap));
      state.addError(Phase.HOME_ACQUISITION, "Failed to acquire smart home: " + e.getMessage());
      validationStats.incAborts();
      return;
    }

    FakeOracle oracle = new FakeOracle();
    try {
      var classes = deviceClasses.getDeviceClasses();
      home.findRelevantSensors(constraint).stream()
          .filter(sensor -> classes.getOrDefault(sensor, false))
          .forEach(oracle::setEnvironment);

    } catch (InvalidStateException | InternalServerError e) {
      state.addError(Phase.HOME_ACQUISITION, e.getMessage());
      logger.log(Level.ERROR, e.getMessage());
      state.setPhase(Phase.FINISHED);
      state.setState(State.UNKNOWN);
      validationStats.incAborts();
      return;
    } catch (InterruptedException e) {
      state.setPhase(Phase.FINISHED);
      state.setState(State.UNKNOWN);
      validationStats.incAborts();
      Thread.currentThread().interrupt();
    }

    logger.log(Level.INFO, "Building automaton");

    state.setPhase(Phase.AUTOMATON_BUILDING);
    notificationService.sendEvent(new ValidationStateEvent(stateMap));

    AutomatonBuilder builder = new AutomatonBuilder(home, oracle, constraint);
    var graph = builder.buildGraph();

    logger.log(Level.INFO, "Automaton built.");

    try {
      var exporter = builder.createDotExporter();

      var writer = new StringWriter();
      exporter.exportGraph(graph, writer);

      MutableGraph g = new Parser().read(writer.toString());
      var svg = Graphviz.fromGraph(g).width(600).render(Format.SVG).toString();
      state.setSvg(svg);
    } catch (IOException e) {
      state.addWarning(
          Phase.AUTOMATON_BUILDING,
          "Could not generate automation visualization: " + e.getMessage());
      logger.log(Level.ERROR, e);
    }

    Map<Phase, AutomatonCheck> checks =
        Map.of(
            Phase.CHECK_1,
            new AutomationReachesErrorState(),
            Phase.CHECK_2,
            new AutomationReachesErrorStateEnv(),
            Phase.CHECK_3,
            new AutomationFixesUserError(),
            Phase.CHECK_4,
            new SearchForAutomationCycles());

    var success = true;
    for (var e : checks.entrySet()) {
      logger.log(Level.INFO, "Phase" + e.getKey());
      state.setPhase(e.getKey());
      notificationService.sendEvent(new ValidationStateEvent(stateMap));
      var result = e.getValue().check(graph, constraint);
      success = success && result.checkSuccessful();
      state.getNotices().addAll(result.messages());
    }
    if (success) {
      state.setState(State.SUCCESS);
      notificationService.sendEvent(new ValidationStateEvent(stateMap));
    } else {
      state.setState(State.FAIL);
      notificationService.sendEvent(new ValidationStateEvent(stateMap));
      validationStats.incErrors();
    }
    state.setPhase(Phase.FINISHED);
    notificationService.sendEvent(new ValidationStateEvent(stateMap));
    notificationService.createNotification(
        "Validation finished",
        "Validation has finished",
        Notification.Type.MODEL_CHECKING_STATUS,
        LinkedObjectType.DOCUMENT,
        dto.documentId(),
        false);

    logger.log(Level.INFO, "Done");
  }

  public ValidationState poll(ConstraintDto dto) {
    return repository.getOrDefault(
        dto.identifier(),
        // if the constraint was not queued, indicate this in the response
        new ValidationState(dto, Phase.NOT_QUEUED));
  }

  public ValidationState enqeue(ConstraintDto dto) {
    var state = new ValidationState(dto);
    repository.put(dto.identifier(), state);
    return state;
  }
}
