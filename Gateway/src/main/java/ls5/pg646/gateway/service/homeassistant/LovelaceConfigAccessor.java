package ls5.pg646.gateway.service.homeassistant;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator.Feature;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import ls5.pg646.gateway.controller.floorplan.ExportFloorPlanDto;
import ls5.pg646.gateway.controller.floorplan.FloorPlanEntity;
import ls5.pg646.gateway.service.homeassistant.pictureElements.PictureElement;
import ls5.pg646.gateway.service.homeassistant.pictureElements.PictureElements;
import ls5.pg646.gateway.service.homeassistant.pictureElements.Style;
import ls5.pg646.gateway.service.homeassistant.pictureElements.View;

public class LovelaceConfigAccessor {

  private static final String IMG_DIR = "pha-floor-plan";
  private final Path configFile;
  private final Path wwwImgPath;


  public LovelaceConfigAccessor(Path path) throws IOException {
    configFile = getConfigFile(path);
    wwwImgPath = getImgDir(path);
    Files.createDirectories(wwwImgPath);
    if (Files.notExists(configFile)) {
      ObjectMapper mapper = getYamlMapper();
      mapper.writeValue(configFile.toFile(), LovelaceConfig.getDefault());
    }
  }

  public static Path getConfigFile(Path configPath) {
    return Paths.get(configPath.toString(), "pha_floorplans_lovelace.yaml");
  }

  public static Path getImgDir(Path configPath) {
    return Paths.get(configPath.toString(), "www", IMG_DIR);
  }

  private static String getViewPath(int floorPlanId) {
    return Integer.toString(floorPlanId);
  }

  private View makeView(ExportFloorPlanDto floorPlanDto) throws IOException {
    String imgPath = storeImage(floorPlanDto.backgroundImgBase64(), floorPlanDto.id());

    Collection<PictureElement> elements = floorPlanDto.entities().stream()
        .map(entity -> makePictureElement(entity, floorPlanDto.height(), floorPlanDto.width()))
        .toList();

    PictureElements pictureElements = new PictureElements(imgPath, elements);
    return new View(floorPlanDto.name(), getViewPath(floorPlanDto.id()), true,
        Collections.singleton(pictureElements));
  }

  private String getImgFileName(int floorPlanId) {
    return URLEncoder.encode(Integer.toString(floorPlanId), StandardCharsets.UTF_8) + ".png";
  }

  private PictureElement makePictureElement(FloorPlanEntity entity, int parentHeight,
      int parentWidth) {
    String top = Math.round(entity.y() / parentHeight * 10000) / 100f + "%";
    String left = Math.round(entity.x() / parentWidth * 10000) / 100f + "%";
    Style style = new Style(top, left);

    if (entity.isNumeric()) {
      return new PictureElement("state-label", entity.id(), style);
    } else {
      return new PictureElement("state-icon", entity.id(), style);
    }
  }

  private String storeImage(String base64Image, int floorPlanId) throws IOException {
    // remove the header, e.g. "data:image/png;base64,ACTUAL_DATA" => "ACTUAL_DATA"
    base64Image = base64Image.split(",")[1];
    byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);

    String filename = getImgFileName(floorPlanId);
    Path imgFile = Path.of(wwwImgPath.toString(), filename);
    Files.write(imgFile, imageBytes);

    return "/local/" + IMG_DIR + "/" + filename;
  }

  /**
   * Adds to or replaces the floor plan in the PHA lovelace config file.
   *
   * @param floorPlanDto the floor plan to write to the file
   * @return true if an existing floor plan with the same id was replaced
   * @throws IOException if there was an issue writing the background image or the config file
   */
  public boolean replaceOrAdd(ExportFloorPlanDto floorPlanDto) throws IOException {
    File file = configFile.toFile();
    ObjectMapper mapper = getYamlMapper();

    // use generic Map<String, Object> to allow unmapped user modifications to the config
    LovelaceConfig<Map<String, Object>> configMock = mapper.readValue(file, new TypeReference<>() {
    });
    boolean replaced = remove(floorPlanDto.id(), configMock);

    LovelaceConfig<Object> config = new LovelaceConfig<>(configMock.title,
        new ArrayList<>(configMock.views));
    config.views.add(makeView(floorPlanDto));
    mapper.writeValue(file, config);
    return replaced;
  }

  private boolean remove(int floorPlanId,
      LovelaceConfig<Map<String, Object>> configMock) {
    String viewPathToFind = getViewPath(floorPlanId);
    return configMock.views.removeIf(
        v -> v.containsKey("path") && v.get("path").toString().equals(viewPathToFind));
  }

  private ObjectMapper getYamlMapper() {
    YAMLFactory fac = new YAMLFactory();
    fac.configure(Feature.WRITE_DOC_START_MARKER, false);
    fac.configure(Feature.MINIMIZE_QUOTES, false);
    ObjectMapper mapper = new ObjectMapper(fac);
    mapper.setSerializationInclusion(Include.NON_NULL);
    return mapper;
  }

  private record LovelaceConfig<T>(String title, Collection<T> views) {

    static <S> LovelaceConfig<S> getDefault() {
      return new LovelaceConfig<>("Floor Plan", Collections.emptySet());
    }
  }

  /**
   * Deletes the floor plan card from the PHA lovelace config file and the background image if
   * existent.
   *
   * @param floorPlanId the id of the floor plan whose card should be deleted
   * @return true if a configuration was deleted
   * @throws IOException if there was an issue accessing the config file or deleting the image
   */
  public boolean delete(int floorPlanId) throws IOException {
    File file = configFile.toFile();
    ObjectMapper mapper = getYamlMapper();

    // use generic Map<String, Object> to allow unmapped user modifications to the config
    LovelaceConfig<Map<String, Object>> configMock = mapper.readValue(file, new TypeReference<>() {
    });
    boolean removed = remove(floorPlanId, configMock);
    mapper.writeValue(file, configMock);
    deleteImageIfExists(floorPlanId);
    return removed;
  }

  private void deleteImageIfExists(int floorPlanId) throws IOException {
    // remove the header, e.g. "data:image/png;base64,ACTUAL_DATA" => "ACTUAL_DATA"
    String filename = getImgFileName(floorPlanId);
    Path imgFile = Path.of(wwwImgPath.toString(), filename);
    Files.deleteIfExists(imgFile);
  }
}
