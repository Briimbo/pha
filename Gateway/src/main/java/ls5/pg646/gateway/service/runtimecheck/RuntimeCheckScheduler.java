package ls5.pg646.gateway.service.runtimecheck;

import com.google.inject.Inject;
import com.google.inject.Injector;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.stream.Collectors;

import ls5.pg646.constraints.ConstraintsStandaloneSetup;
import ls5.pg646.constraints.constraints.Constraint;
import ls5.pg646.constraints.constraints.ConstraintDocument;
import ls5.pg646.gateway.db.entities.ConstraintDocumentEntity;
import ls5.pg646.gateway.db.entities.Notification;
import ls5.pg646.gateway.db.entities.Notification.LinkedObjectType;
import ls5.pg646.gateway.db.repositories.ConstraintDocumentRepository;
import ls5.pg646.gateway.service.exceptions.InternalServerError;
import ls5.pg646.gateway.service.exceptions.InvalidStateException;
import ls5.pg646.gateway.service.homeassistant.HomeAssistantConnector;
import ls5.pg646.gateway.service.notification.NotificationService;
import ls5.pg646.gateway.service.notification.events.RuntimeCheckEvent;
import ls5.pg646.validation.LogicNgConverter;
import ls5.pg646.validation.Utils;
import ls5.pg646.validation.state.RuntimeEntityState;
import org.eclipse.xtext.parser.IParser;
import org.logicng.datastructures.Assignment;
import org.logicng.formulas.Formula;
import org.logicng.formulas.FormulaFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class RuntimeCheckScheduler {

  @Autowired
  private NotificationService notificationService;

  @Autowired
  private HomeAssistantConnector connector;

  @Autowired
  private RuntimeCheckStats runtimeCheckStats;

  @Inject
  private IParser parser;

  private FormulaFactory factory;

  @Autowired
  ConstraintDocumentRepository repository;
  
  @Value("${test}")
  private String test;
  
  public RuntimeCheckScheduler() {
    factory = new FormulaFactory();
    Injector injector = new ConstraintsStandaloneSetup().createInjectorAndDoEMFRegistration();
    injector.injectMembers(this);
  }

  /**
   * Returns all constraint documents parsed as AST.
   *
   * @return
   */
  public Map<Integer, ConstraintDocument> getAllConstraintDocuments() {
    return Utils.stream(repository.findAll())
        .filter(ConstraintDocumentEntity::isEnabled)
        .collect(Collectors.toMap(
            ConstraintDocumentEntity::getId,
            doc -> (ConstraintDocument) parser.parse(new StringReader(doc.getRawText()))
                .getRootASTElement()
        ));
  }

  public Assignment generateLogicNGAssignment() {
    try {
      Map<String, RuntimeEntityState> entityStates = connector.getCurrentEntityStates();
      Assignment res = new Assignment(entityStates.values().stream()
              .filter(state -> state instanceof RuntimeEntityState.BinaryState)
              .map(state -> factory.literal(state.name(), ((RuntimeEntityState.BinaryState) state).value()))
              .collect(Collectors.toList()));
      return res;
    } catch (InvalidStateException | InterruptedException | InternalServerError e) {
      throw new RuntimeException(e);
    }
  }

  @Scheduled(fixedDelay = 10000)
  public void scheduleRuntimeCheck() {
    if (test != null && test.equals("true"))
      return;
    System.out.println("Fixed delay task - " + System.currentTimeMillis() / 1000);
    var checkTime = LocalDateTime.now();
    Assignment assignment = generateLogicNGAssignment();

    System.out.println("Checking the following constraints: ...");
    var docs = getAllConstraintDocuments();
    docs.values().stream()
        .flatMap(doc -> doc.getConstraints().stream()).forEach(System.out::println);

    for (var entry : docs.entrySet()) {
      var doc = entry.getValue();
      if (doc == null) {
        continue;
      }
      var converter = new LogicNgConverter(connector, LogicNgConverter.extractAliases(doc));
      for (Constraint cons : doc.getConstraints()) {
        Formula form = converter.formulaFromExpression(cons.getExpression());
        var success = form.evaluate(assignment);
        System.out.println(cons.getName() + ": " + success + " " + form);
        handleCheckResult(new CheckResult(success, entry.getKey(), cons.getName(), checkTime));
      }
    }

    runtimeCheckStats.setLastCheckTime(checkTime);
  }

  private void handleCheckResult(CheckResult checkResult) {
    if (runtimeCheckStats.didStateChange(checkResult)) {
      notificationService.sendEvent(new RuntimeCheckEvent(checkResult));

      if (checkResult.failed()) {
        notificationService.createNotification(
            "Constraint " + checkResult.name() + " failed!",
            "Constraint " + checkResult.name() + " failed!",
            Notification.Type.RUNTIME_CONSTRAINT_VIOLATION, LinkedObjectType.RUNTIME_OVERVIEW,
            checkResult.docId(), true);
        System.out.println("Message: Constraint " + checkResult.name() + " failed!");
      }
    }
    runtimeCheckStats.add(checkResult);
  }
}
