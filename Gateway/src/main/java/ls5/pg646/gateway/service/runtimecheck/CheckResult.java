package ls5.pg646.gateway.service.runtimecheck;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;

public record CheckResult(@NotNull boolean success, @NotNull Integer docId, @NotNull String name,
                          @NotNull LocalDateTime checkTime) {

  boolean failed() {
    return !success();
  }
}
