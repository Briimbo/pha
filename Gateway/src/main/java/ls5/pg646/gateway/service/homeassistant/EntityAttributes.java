package ls5.pg646.gateway.service.homeassistant;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.NotNull;


@JsonIgnoreProperties(ignoreUnknown = true)
public class EntityAttributes {

  private String friendlyName;

  private String icon;

  private String deviceClass;

  /*
   * device_class if it exists or else the first part of the entityId (deviceType)
   */
  @NotNull
  private String deviceKind;

  @JsonAlias("friendly_name")
  public String getFriendlyName() {
    return friendlyName;
  }

  @JsonAlias("friendly_name")
  public void setFriendlyName(String friendlyName) {
    this.friendlyName = friendlyName;
  }


  @JsonAlias("icon")
  public String getIcon() {
    return icon;
  }

  @JsonAlias("icon")
  public void setIcon(String icon) {
    this.icon = icon;
  }

  @JsonAlias("device_class")
  public String getDeviceClass() {
    return deviceClass;
  }

  @JsonAlias("device_class")
  public void setDeviceClass(String deviceClass) {
    this.deviceClass = deviceClass;
  }

  public String getDeviceKind() {
    return deviceKind;
  }

  public void setDeviceKind(String deviceKind) {
    this.deviceKind = deviceKind;
  }

  public void initializeDeviceKind(String deviceType) {
    this.deviceKind = hasDeviceClass() ? deviceClass : deviceType;
  }

  public boolean hasDeviceClass() {
    return deviceClass != null && !deviceClass.isEmpty();
  }
}
