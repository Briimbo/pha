package ls5.pg646.gateway.service.homeassistant;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator.Feature;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ls5.pg646.gateway.controller.FloorPlanController;
import ls5.pg646.gateway.controller.floorplan.ExportFloorPlanDto;
import ls5.pg646.gateway.controller.utils.ServiceRequestData;
import ls5.pg646.gateway.service.exceptions.InternalServerError;
import ls5.pg646.gateway.service.exceptions.InvalidStateException;
import ls5.pg646.gateway.service.exceptions.InvalidValueException;
import ls5.pg646.gateway.service.exceptions.ServiceException;
import ls5.pg646.parsing.FloorPlanData;
import ls5.pg646.parsing.GroupParser;
import ls5.pg646.validation.state.RuntimeEntityState;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.GenerationType;

@Service
public class HomeAssistantConnector {

    private static final Logger LOGGER = LoggerFactory.getLogger("HassioLogger");

    private static final List<String> SUPPORTED_TYPES =
            List.of("switch", "light", "binary_sensor", "sensor", "person", "sun");

    private static final String USER_DIR = "user.dir";
    private static final String BEARER = "Bearer";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String APPLICATION_JSON = "application/json";
    public static final String AUTHORIZATION = "Authorization";
    // should maybe be used through a db object
    @Autowired
    private HomeAssistantInstance instance;

    @Autowired
    private FloorPlanController floorPlanController;

    final ObjectMapper MAPPER =
            new ObjectMapper(new JsonFactory())
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

    /**
     * Load all (supported?) entities through the home assistant api.
     *
     * @param supportedOnly true if only entities supported by constraint checking should be returned,
     *                      false if all entities are of interest
     * @return the list of entities and their states as configured in the HomeAssistant instance
     * @throws InvalidStateException if the HomeAssistant instance is not configured
     * @throws InternalServerError   if there is an error connecting to the HomeAssistant instance
     */
    public List<EntityState> loadEntityStates(boolean supportedOnly)
            throws InvalidStateException, InternalServerError, InterruptedException {
        if (instance.getAddress() == null) {
            throw new InvalidStateException(
                    "Home assistant instance is not configured", "ha-instance", "unconfigured");
        }

        ObjectMapper objectMapper = new ObjectMapper();

        try {

            var target = new URI(instance.getAddress() + "/api/states");
            LOGGER.debug("@Target URI: {}", target);
            LOGGER.debug("@Token: {}", instance.getToken());

            HttpRequest request =
                    HttpRequest.newBuilder()
                            .uri(new URI(instance.getAddress() + "/api/states"))
                            .timeout(Duration.ofSeconds(30))
                            .header(CONTENT_TYPE, APPLICATION_JSON)
                            .headers(AUTHORIZATION, "%s %s".formatted(BEARER, instance.getToken()))
                            .GET()
                            .build();

            String response = HttpClient.newHttpClient()
                    .send(request, HttpResponse.BodyHandlers.ofString())
                    .body();
            List<EntityState> entities = objectMapper.readValue(response, new TypeReference<>() {
            });
            entities.forEach(EntityState::initialize);

            return supportedOnly
                ? entities.stream()
                    .filter(e -> SUPPORTED_TYPES.contains(e.getDomain()))
                    .toList()
                : entities;

        } catch (URISyntaxException | IOException ex) {
            LOGGER.error("Exception occurred while loading entity states: {}", ex.getMessage());
            throw new InternalServerError(ex);
            //"could not connect to home assistant properly"
        }
    }

    /**
     * Sends a notification to the notification service specified in the {@link ServiceRequestData}.
     * The specified notification service must be already configured in Home Assistant.
     *
     * @param data The service data specifying the content and target of the notification.
     * @throws InvalidStateException If the Home Assistant instance is not configured correctly.
     * @throws InterruptedException If interrupted while sending the HTTP request.
     * @throws InternalServerError If a different error occurs.
     */
    public void haNotify(ServiceRequestData data)
            throws InvalidStateException, InterruptedException, InternalServerError {
        if (instance.getAddress() == null) {
            throw new InvalidStateException(
                    "Home assistant instance is not configured", "ha-instance", "unconfigured");
        }
        try {
            var endpoint =
                    new URI(instance.getAddress() + "/api/services/notify/" + data.getServiceName());

            LOGGER.info(endpoint.toString());

            ObjectMapper mapper = new ObjectMapper();

            ObjectNode node = mapper.createObjectNode();

            node.put("message", data.getMessage());

            data.getTitle().ifPresent(s -> node.put("title", s));
            data.getTarget()
                    .ifPresent(
                            targets -> {
                                var arrayNode = node.putArray("target");
                                targets.forEach(arrayNode::add);
                            });
            data.getData()
                    .ifPresent(
                            dataMap -> {
                                var dataObj = node.putObject("data");
                                dataMap.forEach((k, v) -> dataObj.put(k, v.toString()));
                            });

            String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(node);

            LOGGER.info(json);

            HttpRequest request =
                    HttpRequest.newBuilder()
                            .uri(endpoint)
                            .version(HttpClient.Version.HTTP_1_1)
                            .timeout(Duration.ofSeconds(30))
                            .header(CONTENT_TYPE, APPLICATION_JSON)
                            .header(AUTHORIZATION, "%s %s".formatted(BEARER, instance.getToken()))
                            .POST(BodyPublishers.ofString(json))
                            .build();

            var response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            LOGGER.info(response.toString());

        } catch (URISyntaxException | IOException ex) {
            LOGGER.error("{}", ex.getMessage());
            throw new InternalServerError(ex);
            //"could not connect to home assistant properly"
        }
    }

    /**
     * Adds the specified groups to HomeAssistant. It replaces them if they already exist. Writes the
     * groups to a file in the app properties groupsDir, which can be used in the HomeAssistant
     * configuration. Call {@link #reloadGroups()} to actually make them visible in the configured
     * HomeAssistant instance
     *
     * @param groups    groups to be added.
     * @param floorName the name of the floor
     * @param floorId   the id of the floor to override the previous groups
     * @throws ServiceException if there is an error writing to the output file
     */
    public boolean addGroups(List<Group> groups, String floorName, Long floorId)
            throws ServiceException {
        Path path = getGroupsPath(floorId);

        Map<String, Group> data =
                groups.stream().collect(Collectors.toMap(i -> getPhaGroupKey(i, floorName), g -> g));

        YAMLFactory fac = new YAMLFactory();
        fac.configure(Feature.WRITE_DOC_START_MARKER, false);
        fac.configure(Feature.MINIMIZE_QUOTES, true);
        ObjectMapper mapper = new ObjectMapper(fac);
        mapper.addMixIn(Group.class, IgnoreEntityIdMixin.class);

        try (OutputStream outputStream = new FileOutputStream(path.toAbsolutePath().toString())) {
            mapper.writeValue(outputStream, data);
            return true;
        } catch (IOException e) {
            String message =
                    "Error trying add groups to HomeAssistant. Failed to write groups to file " + path;
            LOGGER.error(message + ":\n", e);
            throw new ServiceException(message);
        }
    }

    private Path getGroupsPath(long floorPlanId) {
        String filename = "pha.groups-" + floorPlanId + ".yaml";
        return Path.of(System.getProperty(USER_DIR), instance.getGroupsDir(), filename);
    }

    private String getPhaGroupKey(Group group, String floorName) {
        return "pha."
                + floorName.toLowerCase().replace(' ', '_')
                + "."
                + group.getName().toLowerCase().replace(' ', '_');
    }

    /**
     * Calls the group.reload service to reload the groups from the configured files.
     *
     * @return true if reloading succeeded
     * @throws InternalServerError if calling the group.reload services API fails
     */
    public boolean reloadGroups() throws InternalServerError {
        try {
            HttpRequest request =
                    HttpRequest.newBuilder()
                            .uri(new URI(instance.getAddress() + "/api/services/group/reload"))
                            .timeout(Duration.ofSeconds(30))
                            .header(CONTENT_TYPE, APPLICATION_JSON)
                            .headers(AUTHORIZATION, BEARER + " " + instance.getToken())
                            .POST(BodyPublishers.noBody())
                            .build();
            HttpResponse<Void> response =
                    HttpClient.newHttpClient().send(request, BodyHandlers.discarding());
            return isOk(response);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Failed to execute reload groups via the service API:\n", e);
            throw new InternalServerError("could not connect to home assistant properly");
        } catch (InterruptedException ex) {
            LOGGER.error("InterruptedException when trying to reload groups via the service API", ex);
            Thread.currentThread().interrupt();
            throw new InternalServerError(
                    "Unexpected thread interruption when trying to reload groups in HomeAssistant");
        }
    }

    private <T> boolean isOk(HttpResponse<T> response) {
        return response.statusCode() >= 200 && response.statusCode() < 300;
    }

    /**
     * adds or replaces a lovelace card which contains the floor plan to the FloorPlans dashboard by
     * adding it to the yaml file for our configured dashboards. Writes the specified background image
     * to a file
     *
     * @param floorPlan the floor plan to visualize in the dashboard
     * @return true if the floor plan was added or replaced
     * @throws ServiceException if there was an issue writing the background file or yaml file
     */
    public boolean addFloorPlanCard(ExportFloorPlanDto floorPlan) throws ServiceException {
        Path path = Path.of(System.getProperty(USER_DIR), instance.getConfigDir());

        try {
            LovelaceConfigAccessor lovelaceConfig = new LovelaceConfigAccessor(path);
            return lovelaceConfig.replaceOrAdd(floorPlan);
        } catch (IOException e) {
            String message =
                    "Error trying to export floor plan to HomeAssistant. Failed to write image file (to "
                            + LovelaceConfigAccessor.getImgDir(path) + ") or yaml configuration file (to "
                            + LovelaceConfigAccessor.getConfigFile(path) + ")";

            LOGGER.error(message + ":\n", e);
            throw new ServiceException(message);
        }
    }

    /**
     * Configure the used home assistant instance.
     *
     * @param url   the url
     * @param token the token
     */
    public void configureInstance(String url, String token) throws InvalidValueException {
        try {
            instance.setAddress(new URI(url));
            instance.setToken(token);
        } catch (URISyntaxException e) {
            throw new InvalidValueException("could not parse url", "url", url);
        }
    }

    /**
     * get the current instance.
     */
    public HomeAssistantInstance getConfig() {
        return instance;
    }

    /**
     * Returns a map containing all entity states.
     *
     * <p>Keys are entity ids, values are RuntimeEntityState objects containing the values as boolean
     * or double.
     *
     * @return Map<entityId, RuntimeEntityState>
     */
    public Map<String, RuntimeEntityState> getCurrentEntityStates()
            throws InvalidStateException, InterruptedException, InternalServerError {
        List<EntityState> entityStates = loadEntityStates(true);
        Map<String, RuntimeEntityState> currentEntityStates = new HashMap<>();
        for (EntityState entityState : entityStates) {
            String entityId = entityState.getEntityId();
            String state = entityState.getState();
            if (state != null) {
                if (state.equalsIgnoreCase("off")
                        || state.equalsIgnoreCase("closed")
                        || state.equalsIgnoreCase("false")) {
                    currentEntityStates.put(entityId, new RuntimeEntityState.BinaryState(entityId, false));
                } else if (state.equalsIgnoreCase("on")
                        || state.equalsIgnoreCase("open")
                        || state.equalsIgnoreCase("true")) {
                    currentEntityStates.put(entityId, new RuntimeEntityState.BinaryState(entityId, true));
                } else if (NumberUtils.isCreatable(state) || state.equals("unavailable") || state.equals("unknown")) {
                    double stateNumber;
                    try {
                        stateNumber = Double.parseDouble(entityState.getState());
                    } catch (NumberFormatException e) {
                        stateNumber = Float.NaN;
                    }
                    currentEntityStates.put(
                            entityId,
                            new RuntimeEntityState.NumericState(entityId, stateNumber)
                    );
                } else {
                    LOGGER.warn("Unsupported entity skipped with id \"{}\" and state \"{}\" while parsing entity state", entityId, state);
                }
            } else {
                LOGGER.error("Exception when parsing entity state: {} state is null", entityId);
            }
        }
        return currentEntityStates;
    }

    /**
     * Deletes the exported lovelace card from HomeAssistant belonging to the floor plan
     *
     * @param floorPlanId the floor plan to be deleted
     * @return true if a configuration was deleted
     * @throws ServiceException if there was an issue accessing the related files
     */
    public boolean deleteFloorPlanCard(int floorPlanId) throws ServiceException {
        Path path = Path.of(System.getProperty(USER_DIR), instance.getConfigDir());

        try {
            LovelaceConfigAccessor lovelaceConfig = new LovelaceConfigAccessor(path);
            return lovelaceConfig.delete(floorPlanId);
        } catch (IOException e) {
            String message =
                    "Error trying to delete exported floor plan card from HomeAssistant. Failed to delete yaml configuration file (from "
                            + LovelaceConfigAccessor.getConfigFile(path) + ") or image file if it existed (from "
                            + LovelaceConfigAccessor.getImgDir(path);

            LOGGER.error(message + ":\n", e);
            throw new ServiceException(message);
        }
    }

    /**
     * Deletes all groups belonging to the floorplan with the specified id from HomeAssistant by
     * deleting the file. Call {@link #reloadGroups()} to actually make the change visible in the
     * configured HomeAssistant instance
     *
     * @param floorPlanId the id of the floor plan that the groups belong to
     * @throws IOException if an I/O error occurs accessing the group file
     */
    public void deleteGroups(int floorPlanId) throws IOException {
        var path = getGroupsPath(floorPlanId);
        Files.deleteIfExists(path);
    }

    /**
     * Returns all entities contained in a group.
     *
     * @param grp       - The id of the group we want to read entities from
     * @param configDir Setting a specific config directory only for testing purposes. Leave empty for normal use
     * @return the list of entities of the given group
     */
    public static List<String> entitiesInGroup(String grp, String... configDir) throws IOException {
        String config = configDir.length > 0 ? configDir[0] : "config/";

        var group = GroupParser.fromConfig(config).stream().filter(g -> g.getId().equals(grp)).findFirst();
        if (group.isPresent()) {
            return group.get().getEntities();
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * Returns all entities within the given range (in meters) of the given entity.
     *
     * @param entityId The reference entity
     * @param range    range in meters (SI Unit).
     * @return Set of all entities in the given range around the reference entity, without the entity itself.
     */
    public List<String> entitiesInRangeOf(String entityId, double range) {
        var floorPlanData = floorPlanController.getFloorPlanRepo().findAllOrderByFloorDesc();

        // Get floor plan that contains entityId
        var floorPlanOpt = floorPlanData
                .stream()
                .flatMap(floorPlan -> {
                    try {
                        return Stream.of(MAPPER.readValue(floorPlan.getFloorPlanData(), FloorPlanData.class));
                    } catch (JsonProcessingException e) {
                        LOGGER.error("Parsing floorplan data failed", e);
                        return Stream.empty();
                    }
                })
                .filter(plan -> plan.containsEntity(entityId))
                .findFirst();

        return floorPlanOpt.flatMap(floorPlan -> floorPlan.entities().stream()
                        // Find the entity in the floor plan
                        .filter(e -> e.id().equals(entityId))
                        .findFirst()
                        // Get all entities in range around that entity
                        .map(entity -> floorPlan.entities().stream()
                                .peek(e -> LOGGER.debug("{}: {}", e.id(), Math.hypot(e.x() - entity.x(), e.y() - entity.y())))
                                .filter(e -> !e.equals(entity))
                                .filter(e -> Math.hypot(e.x() - entity.x(), e.y() - entity.y()) * floorPlan.scale().metersPerPixel() <= range)
                                .map(FloorPlanData.Entity::id)
                                .toList()))
                .orElse(List.of());
    }

    /**
     * Returns all entities, which are part of the deviceClass
     *
     * @param deviceDomain - The name of the device domain each entity should have
     * @return the list of entities of the given deviceClass
     * @throws InvalidStateException if the HomeAssistant instance is not configured
     * @throws InternalServerError   if there is an error connecting to the HomeAssistant instance
     */
    public List<String> entitiesOfDomain(String deviceDomain)
            throws InvalidStateException, InternalServerError, InterruptedException {
        return loadEntityStates(true).stream()
                .filter(state -> deviceDomain.equals(state.getDomain()))
                .map(EntityState::getEntityId)
                .toList();
    }
}

