package ls5.pg646.gateway.db.converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.AttributeConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class ListToStringConverter implements AttributeConverter<List<String>, String> {
  @Override
  public String convertToDatabaseColumn(List<String> attribute) {

    ObjectMapper mapper = new ObjectMapper();

    ArrayNode node = mapper.createArrayNode();

    attribute.forEach(node::add);

    try {
      return mapper.writeValueAsString(node);
    } catch (JsonProcessingException ex) {
      // don't know what to do here. should probably never happen
      return "";
    }
  }

  @Override
  public List<String> convertToEntityAttribute(String dbData) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return Arrays.stream(mapper.readValue(dbData, String[].class)).toList();
    } catch (JsonProcessingException ex) {
      // don't know what to do here. should probably never happen
      return new ArrayList<>();
    }
  }
}
