package ls5.pg646.gateway.db.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import ls5.pg646.gateway.controller.constraint.State;

@Entity
@IdClass(ConstraintId.class)
public class ConstraintEntity {
  
  @Id
  private int documentId;
  
  @Id
  private String constraintName;
  
  private State state = State.IN_PROGRESS;
  
  protected ConstraintEntity() { }
  
  public ConstraintEntity(int documentId, String constraintName) {
    this.documentId = documentId;
    this.constraintName = constraintName;
  }
  
  public int getDocumentId() {
    return documentId;
  }

  public String getConstraintName() {
    return constraintName;
  }
  
  public State getState() {
    return state;
  }
  
  public void setState(State state) {
    this.state = state;
  }
}
