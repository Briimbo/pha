package ls5.pg646.gateway.db.repositories;

import java.util.List;
import ls5.pg646.gateway.db.entities.Notification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepo extends PagingAndSortingRepository<Notification, Integer> {
  int countNotificationsByReadIsFalse();

  List<Notification> findNotificationByReadIsFalse();
}
