package ls5.pg646.gateway.db.repositories;

import ls5.pg646.gateway.db.entities.ConstraintDocumentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConstraintDocumentRepository 
    extends CrudRepository<ConstraintDocumentEntity, Integer> {

}
