package ls5.pg646.gateway.db.repositories;

import java.util.List;
import ls5.pg646.gateway.db.entities.FloorPlan;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FloorPlanRepo extends CrudRepository<FloorPlan, Integer> {

  @Query("FROM FloorPlan ORDER BY floor DESC")
  List<FloorPlan> findAllOrderByFloorDesc();

  @Query("FROM FloorPlan")
  List<FloorPlan> findAllOrderByFloorDesc(Sort sort);

  boolean existsByNameIgnoreCaseAndIdIsNot(String name, Integer id);

  boolean existsByNameIgnoreCase(String name);
}
