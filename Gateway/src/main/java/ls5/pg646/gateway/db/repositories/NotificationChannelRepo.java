package ls5.pg646.gateway.db.repositories;

import ls5.pg646.gateway.db.entities.NotificationChannel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationChannelRepo extends CrudRepository<NotificationChannel, Integer>
{
}
