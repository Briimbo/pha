package ls5.pg646.gateway.db.entities;

import java.io.Serializable;
import java.util.Objects;

public class ConstraintId implements Serializable {
  
  private static final long serialVersionUID = -8466783692080752481L;

  private long documentId;
  
  private String constraintName;
  
  public ConstraintId() { }
  
  public ConstraintId(long documentId, String constraintName) {
    this.documentId = documentId;
    this.constraintName = constraintName;
  }

  public long getDocumentId() {
    return documentId;
  }

  public String getConstraintName() {
    return constraintName;
  }

  @Override
  public int hashCode() {
    return Objects.hash(constraintName, documentId);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    ConstraintId other = (ConstraintId) obj;
    return Objects.equals(constraintName, other.constraintName) && documentId == other.documentId;
  }
}
