package ls5.pg646.gateway.db.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ConstraintDocumentEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  private String name;

  private String rawText;

  private boolean enabled;

  protected ConstraintDocumentEntity() {
  }

  public ConstraintDocumentEntity(String name, String rawText) {
    this(name, rawText, true);
  }

  public ConstraintDocumentEntity(String name, String rawText, boolean enabled) {
    this.name = name;
    this.rawText = rawText;
    this.enabled = enabled;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getRawText() {
    return rawText;
  }

  public void setName(String name) {
    this.name = name;

  }

  public void setRawText(String rawText) {
    this.rawText = rawText;
  }
  

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  // use default hashCode / equals for now
  // cf. https://stackoverflow.com/questions/5031614/the-jpa-hashcode-equals-dilemma
}
