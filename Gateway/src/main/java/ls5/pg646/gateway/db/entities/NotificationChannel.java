package ls5.pg646.gateway.db.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
import ls5.pg646.gateway.db.converter.ListToStringConverter;

@Entity
@Getter
@Setter
public class NotificationChannel {

  /** the unique id of the notification. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  /** the name for identification to use */
  private String name;

  /** the home assistant service to use */
  private String service;

  /** if this channel should be used */
  private boolean active;

  /** list of targets to use, might be empty */
  @Column
  @Convert(converter = ListToStringConverter.class)
  private List<String> targets = new ArrayList<>();

  /** additional data to use */
  private String data;
}
