package ls5.pg646.gateway.db.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Notification {

  /** the unique id of the notification. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  /** the title of the notification. */
  private String title;

  /** the main message of the notification. */
  private String message;

  /** if the notification was read by the user. */
  private boolean read;

  /** the date the notification was created. */
  private Date createdAt;

  /** the type of the notification. */
  private Type type;

  /** the type of the linked object. */
  private LinkedObjectType linkedObjectType;

  /** the id of the linked object. */
  private Integer linkedObjectId;

  public enum Type {
    RUNTIME_CONSTRAINT_VIOLATION,

    FLOOR_PLAN,

    MODEL_CHECKING_STATUS
  }

  public enum LinkedObjectType {
    FLOOR_PLAN,

    DOCUMENT,

    RUNTIME_OVERVIEW
  }
}
