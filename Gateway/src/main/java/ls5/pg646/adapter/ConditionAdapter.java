package ls5.pg646.adapter;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import ls5.pg646.parsing.Action;
import ls5.pg646.validation.math.Condition;
import ls5.pg646.validation.math.Condition.BinaryCondition;
import ls5.pg646.validation.math.Relation;
import ls5.pg646.validation.symbolic.Automation;
import ls5.pg646.validation.symbolic.LogicFormulaConverter;
import ls5.pg646.validation.symbolic.SmartHome;
import ls5.pg646.validation.symbolic.Trigger;
import org.logicng.formulas.Formula;
import org.logicng.formulas.Literal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ConditionAdapter {

  // Sonarqube <3
  private static final String ON = "on";
  private static final String OFF = "off";
  private static final String ATTRIBUTE = "attribute";
  private static final String ABOVE = "above";
  private static final String BELOW = "below";


  /**
   * The keys of this map are service names (including their domain name) like "light.toggle". The
   * values are functions that take the previous state of the entity, and additionally a map
   * representing the data passed to a home assistant service.
   *
   * @see ServiceTransition
   */
  private static final Map<String, ServiceTransition> SERVICE_TRANSITIONS = new HashMap<>();

  /**
   * The keys of this map are service names (including their domain name) like "light.toggle". The
   * values are lists of possible result states for this service when it is called. So
   * "light.toggle" would map to a list containing "on" and "off".
   */
  private static final Map<String, List<String>> INPUT_STATES = new HashMap<>();


  private static final Logger LOGGER = LoggerFactory.getLogger("HassioLogger");

  static {
    // ------------------------- LIGHT -------------------------
    SERVICE_TRANSITIONS.put("light.turn_on", (inputCond, data) -> ON);
    SERVICE_TRANSITIONS.put("light.turn_off", (inputCond, data) -> OFF);
    SERVICE_TRANSITIONS.put("light.toggle", (inputCond, data) -> switch (inputCond) {
      case ON -> OFF;
      case OFF -> ON;
      default -> null;
    });

    // ------------------------- SWITCH -------------------------
    SERVICE_TRANSITIONS.put("switch.turn_on", (inputCond, data) -> ON);
    SERVICE_TRANSITIONS.put("switch.turn_off", (inputCond, data) -> OFF);
    SERVICE_TRANSITIONS.put("switch.toggle", (inputCond, data) -> switch (inputCond) {
      case ON -> OFF;
      case OFF -> ON;
      default -> null;
    });

    // ------------------------- INPUT BOOLEAN ----------------Li---------
    SERVICE_TRANSITIONS.put("input_boolean.turn_on", (inputCond, data) -> ON);
    SERVICE_TRANSITIONS.put("input_boolean.turn_off", (inputCond, data) -> OFF);
    SERVICE_TRANSITIONS.put("input_boolean.toggle", (inputCond, data) -> switch (inputCond) {
      case ON -> OFF;
      case OFF -> ON;
      default -> null;
    });

    // ------------------------- INPUT NUMBER -------------------------
    SERVICE_TRANSITIONS.put("input_number.set_value", (inputCond, data) -> data.get("value"));
  }

  static {
    // ------------------------- LIGHT -------------------------
    INPUT_STATES.put("light.toggle", List.of(ON, OFF));

    // ------------------------- LIGHT -------------------------
    INPUT_STATES.put("switch.toggle", List.of(ON, OFF));

    // ------------------------- INPUT BOOLEAN -------------------------
    INPUT_STATES.put("input_boolean.toggle", List.of(ON, OFF));
  }

  /**
   * Transforms parsedAutomations into model-checking api form.
   *
   * @param parsedAutomations A collection of parsed Automations
   * @return the automations in a format ready for the model-checking api
   */
  public static SmartHome parseToModelSmartHome(
      Collection<ls5.pg646.parsing.Automation> parsedAutomations) {

    List<Automation> automations = new ArrayList<>();
    int i = 0;

    for (ls5.pg646.parsing.Automation parsedAutomation : parsedAutomations) {
      var parsedTriggers = parseToModelTrigger(parsedAutomation.getTrigger());
      var parsedConditions = parseToModelCondition(parsedAutomation.getCondition());
      var parsedActions = parseToModelActions(parsedConditions, parsedAutomation.getAction());

      // Create new automations for each trigger and each action
      for (Trigger parsedTrigger : parsedTriggers) {
        for (ConditionActionPair parsedAction : parsedActions) {
          Automation automation = new Automation(
              parsedAutomation.getAlias() != null ? parsedAutomation.getAlias()
                  : "A%d".formatted(i++),
              parsedTrigger, parsedAction.conditions, parsedAction.actions);
          automations.add(automation);
        }
      }
    }

    for (Automation automation : automations) {
      if (LOGGER.isInfoEnabled()) {
        LOGGER.trace(automation.toString());
      }
    }

    return new SmartHome(automations);
  }


  /**
   * Transforms triggers into model checking representation.
   * <p>
   * So far supported triggers are state triggers and numeric state triggers.
   *
   * @param triggers A list of parsed triggers
   * @return A list of transformed triggers
   */
  static List<Trigger> parseToModelTrigger(List<ls5.pg646.parsing.Trigger> triggers) {

    LOGGER.trace("triggers = {}", triggers);

    List<Trigger> symbolicTriggers = new ArrayList<>();

    for (ls5.pg646.parsing.Trigger trigger : triggers) {
      switch (trigger.getPlatform()) {
        case "state" -> parseStateTrigger(trigger, symbolicTriggers);
        case "numeric_state" -> parseNumericStateTrigger(trigger, symbolicTriggers);
        default -> throw new IllegalStateException(
                "Unsupported Trigger Platform: %s".formatted(trigger.getPlatform()));
      }
    }

    return symbolicTriggers;
  }

  /**
   * Parses a state trigger and adds it to a list of symbolic triggers in model-checking format.
   * @param trigger The input state trigger
   * @param symbolicTriggers The list of symbolic triggers
   */
  static void parseStateTrigger(ls5.pg646.parsing.Trigger trigger, List<Trigger> symbolicTriggers) {
    // In this case, the state to which the entity changes to is irrelevant.
    // It can be either on or off
    if (trigger.getTo() == null && trigger.getFrom() == null) {
      symbolicTriggers.add(new Trigger.ContinuousTrigger(trigger.getEntityId()));
    } else if (trigger.getTo() != null) {
      // In case the allowed target states are given
      trigger.getTo().stream()
              .map(ON::equalsIgnoreCase)
              .map(isOn -> (Condition) new BinaryCondition(trigger.getEntityId(), isOn))
              .map(List::of)
              .map(binCond -> new Trigger.StateTrigger(trigger.getEntityId(), binCond))
              .forEach(symbolicTriggers::add);
    } else if (trigger.getFrom() != null) {
      List<String> targetStates = trigger.getFrom();
      targetStates.stream()
              .map(ON::equalsIgnoreCase)
              .map(isOn -> (Condition) new BinaryCondition(trigger.getEntityId(), !isOn))
              .map(List::of)
              .map(binCond -> new Trigger.StateTrigger(trigger.getEntityId(), binCond))
              .forEach(symbolicTriggers::add);
    }
  }

  /**
   * Parses a numeric state trigger and adds it to a list of symbolic triggers in model-checking format.
   * @param trigger The input state trigger
   * @param symbolicTriggers The list of symbolic triggers
   */
  static void parseNumericStateTrigger(ls5.pg646.parsing.Trigger trigger, List<Trigger> symbolicTriggers) {
    String attribute = trigger.getData().containsKey(ATTRIBUTE) ? ".%s".formatted(
            trigger.getData().get(ATTRIBUTE))
            : "";

    String valueName = trigger.getEntityId() + attribute;
    if (trigger.getData().get(ABOVE) != null ^ trigger.getData().get(BELOW) != null) {
      double value;
      Relation relation;
      if (trigger.getData().get(ABOVE) != null) {
        value = Double.parseDouble(trigger.getData().get(ABOVE).toString());
        relation = Relation.GREATER;
      } else {
        value = Double.parseDouble(trigger.getData().get(BELOW).toString());
        relation = Relation.LESSER;
      }
      symbolicTriggers.add(
              new Trigger.StateTrigger(
                      valueName,
                      List.of(new Condition.NumericCondition(valueName, relation, value))));
    } else {
      Double above = Double.parseDouble(trigger.getData().get(ABOVE).toString());
      Double below = Double.parseDouble(trigger.getData().get(BELOW).toString());

      symbolicTriggers.add(
              new Trigger.StateTrigger(
                      valueName,
                      List.of(new Condition.NumericCondition(valueName, Relation.GREATER, above),
                              new Condition.NumericCondition(valueName, Relation.LESSER, below))));
    }
  }


  /**
   * Transforms the raw condition into the disjunctive normal form.
   *
   * @param cond The collection of condititons to transform into DNF
   * @return conditions in disjunctive normal form
   */
  static List<List<Condition>> parseToModelCondition(
      Collection<ls5.pg646.parsing.Condition> cond) {
    if (cond == null || cond.isEmpty()) {
      return List.of();
    }
    Formula dnfFormula = LogicFormulaConverter.asFormula(cond);
    var dnfList = new ArrayList<List<Condition>>();
    for (Formula dnfClause : dnfFormula) {
      // Will be filled with the literals in the dnf clause modeled as Condition
      // objects
      List<Condition> clauseConditionList = new ArrayList<>();

      for (Formula literalFormula : dnfClause) {

        Condition parsedCondition;
        var literal = (Literal) literalFormula;
        parsedCondition = parseConditionLiteral(literal);
        clauseConditionList.add(parsedCondition);
      }
      dnfList.add(clauseConditionList);
    }

    if (dnfFormula instanceof Literal literal) {
      Condition parsedCondition = parseConditionLiteral(literal);

      List<Condition> clauseConditionList = new ArrayList<>();
      clauseConditionList.add(parsedCondition);
      dnfList.add(clauseConditionList);
    }

    return dnfList;
  }

  /**
   * Transforms a {@link Literal} in a DNF clause into a {@link Condition}.
   *
   * @param literal The literal to be transform
   * @return The representation of the literal as a {@link Condition} object.
   */
  static Condition parseConditionLiteral(Literal literal) {
    Condition parsedCondition;
    if (literal.name().contains(Relation.EQUALS.operator())) {
      String[] tokens = literal.name().split(Relation.EQUALS.operator());
      parsedCondition = new Condition.BinaryCondition(tokens[0], ON.equalsIgnoreCase(tokens[1]));
    } else {
      Relation relation =
          literal.name().contains(Relation.LESSER.operator()) ? Relation.LESSER : Relation.GREATER;
      String[] tokens = literal.name().split(relation.operator());
      parsedCondition = new Condition.NumericCondition(tokens[0], relation,
          Double.parseDouble(tokens[1]));
    }
    return parsedCondition;
  }

  /**
   * Parses actions. In case of some actions, e.g. {@code toggle}, we need to now the prior state to
   * know the resulting state, thus we need to pass in all prior conditions.
   *
   * <p>
   * In case the prior condition is unknown (because the entity is neither part of the trigger nor
   * any condition), automations for all possible prior states cases are emitted.
   *
   * @param conditions A list of clauses, one clause of which always has to be true.
   * @param actions    The list of input actions
   * @return A list which contains preconditions and the respective resulting states that will occur
   * when the automation is run, given the preconditions
   */
  static List<ConditionActionPair> parseToModelActions(List<List<Condition>> conditions,
      List<Action> actions) {

    var actionTransitions = parseActionsSeparate(actions);

    var cartesian = cartesianProduct(actionTransitions);

    List<ConditionActionPair> resultingActions = new ArrayList<>();

    if (conditions.isEmpty()) {
      for (List<ConditionActionPair> actionsWithPreconditions : cartesian) {
        List<Condition> newInputConditions = new ArrayList<>();
        mergeActions(newInputConditions, actionsWithPreconditions, resultingActions);
      }
    } else {
      for (List<Condition> clause : conditions) {
        for (List<ConditionActionPair> actionsWithPreconditions : cartesian) {
          List<Condition> newInputConditions = new ArrayList<>(clause);
          mergeActions(newInputConditions, actionsWithPreconditions, resultingActions);
        }
      }
    }

    return resultingActions;
  }

  /**
   * Takes a list of actions, and transforms each individual action to the model checking
   * representation.
   * <p>
   * Each action is transformed into a list of preconditions and a list of resulting states
   * respectively.
   * </p>
   *
   * @param actions The actions to parse.
   */
  static List<List<ConditionActionPair>> parseActionsSeparate(List<Action> actions) {
    List<List<ConditionActionPair>> actionTransitions = new ArrayList<>();
    for (Action action : actions) {
      var data = action.getData();
      var service = action.getService();
      var target = action.getTarget().get("entity_id");

      List<ConditionActionPair> transitionsForThisAction = new ArrayList<>();

      if (INPUT_STATES.containsKey(service)) {
        // In this case, there is a defined set of input state of the target,
        // that will lead to different output states.
        // There can only be binary conditions,
        // because numeric conditions cannot have a predefined set of input values.
        var inputStates = INPUT_STATES.get(service);

        var inputConditions = inputStates.stream()
            .map(state -> new Condition.BinaryCondition(target, ON.equals(state)))
            .collect(Collectors.toList());
        var outputConditions = inputStates.stream()
            .map(state -> SERVICE_TRANSITIONS.get(service).execute(state, data))
            .map(state -> new Condition.BinaryCondition(target, ON.equals(state)))
            .collect(Collectors.toList());

        for (int i = 0; i < inputConditions.size(); i++) {
          transitionsForThisAction.add(new ConditionActionPair(List.of(inputConditions.get(i)),
              List.of(outputConditions.get(i))));
        }

      } else {
        // In this case, the input state of the target does not matter
        var actionResult = SERVICE_TRANSITIONS.get(service).execute(null, data);
        Condition actionCondition;

        // If the result of the service is a number,
        // create a numeric condition. Else, create a binary condition
        try {
          Double floatResult = Double.parseDouble(actionResult);
          actionCondition = new Condition.NumericCondition(target, Relation.EQUALS, floatResult);
        } catch (NumberFormatException e) {
          actionCondition = new Condition.BinaryCondition(target, ON.equals(actionResult));
        }

        transitionsForThisAction.add(new ConditionActionPair(List.of(), List.of(actionCondition)));
      }
      actionTransitions.add(transitionsForThisAction);
    }

    return actionTransitions;
  }

  /**
   * Merges the pre- and resulting conditions of actions while also incorporating externally defined
   * conditions (in the home assistant automation yaml).
   *
   * @param newInputConditions       The (potentially empty) set of externally defined conditions.
   * @param actionsWithPreconditions The list of action results and the corresponding required
   *                                 preconditions, for the result to happen when this automation is
   *                                 executed. Each entry in this list represents the preconditions
   *                                 and the resulting conditions for a singular action in the
   *                                 automation.
   * @param resultingActions         The resulting set of actions containing the full actions with
   *                                 all precondtitions (including predefined ones) and the
   *                                 resulting actions.
   */
  static void mergeActions(List<Condition> newInputConditions,
      List<ConditionActionPair> actionsWithPreconditions,
      List<ConditionActionPair> resultingActions) {
    // Extract all preconditions from the action and add them to the new input
    // conditions
    actionsWithPreconditions.stream()
        .map(ConditionActionPair::conditions)
        .forEach(newInputConditions::addAll);

    // newInputConditions could have contained Conditions that were defined in the
    // yaml file
    // So, adding the preconditions of an action,
    // could lead to these two sets of conditions not being satisfiable at the same
    // time
    // If there are conflicting conditions (like "a && !a"), this will be null

    // If there were conflicting conditions, return
    boolean noConflicts = cleanPreconditions(newInputConditions);
    if (!noConflicts) {
      return;
    }

    // Get a list of all the combined output conditions that are true
    // after all actions have been run with the conditions in newInputConditions
    List<Condition> newOutputConditions = new ArrayList<>();
    Set<String> setVariables = new HashSet<>();
    for (int i = actionsWithPreconditions.size() - 1; i >= 0; i--) {
      var condAction = actionsWithPreconditions.get(i);
      var actions = condAction.actions();
      for (int j = actions.size() - 1; j >= 0; j--) {
        var condResult = actions.get(j);
        if (!setVariables.contains(condResult.name())) {
          newOutputConditions.add(condResult);
          setVariables.add(condResult.name());
        }
      }
    }
    newOutputConditions = Lists.reverse(newOutputConditions);

    // Return the pair of input conditions and resulting conditions
    resultingActions.add(new ConditionActionPair(newInputConditions, newOutputConditions));
  }

  static <T> List<List<T>> cartesianProduct(List<List<T>> lists) {
    List<List<T>> cartesian = lists.get(0).stream()
        .map(List::of)
        .collect(Collectors.toList());

    // This for loop's invariant is that after every iteration
    // "cartesian" always contains the cartesian product
    // of the elements in "lists" of index 0 to i (inclusively)

    for (int i = 1; i < lists.size(); i++) {
      List<List<T>> temp = new ArrayList<>();

      for (List<T> cartesianProductElement : cartesian) {
        for (T newElement : lists.get(i)) {
          List<T> newCartesianProductElement = new ArrayList<>(cartesianProductElement);
          newCartesianProductElement.add(newElement);
          temp.add(newCartesianProductElement);
        }
      }
      cartesian = temp;
    }

    return cartesian;
  }

  /**
   * Checks preconditions for contradictions and removes duplicate conditions from the input list.
   *
   * @param preConditions A list of conditions
   * @return false, if the list of conditions is contradictory. true otherwise.
   */
  static boolean cleanPreconditions(List<Condition> preConditions) {

    Map<String, Condition> seenConditions = new HashMap<>();

    for (int i = 0; i < preConditions.size(); i++) {
      Condition condition = preConditions.get(i);

      if (!seenConditions.containsKey(condition.name())) {
        seenConditions.put(condition.name(), condition);
        continue;
      }
      if (condition instanceof Condition.BinaryCondition binary
          && ((Condition.BinaryCondition) seenConditions.get(binary.name())).value()
          != binary.value()) {
        return false;
      }
    }

    var set = new LinkedHashSet<Condition>();
    set.addAll(preConditions);
    preConditions.clear();
    preConditions.addAll(set);

    return true;
  }

  /**
   * Represents a state transition that occurs when calling a service
   */
  @FunctionalInterface
  private interface ServiceTransition {

    /**
     * Calculates the state that would result when a service is called with the given data.
     *
     * @param inputCond The previous condition of the entity the service is called on.
     * @param data      A map of data that is passed to the service call. For example, the service
     *                  "input_number.set_value" requires the value which the entity should be set
     *                  to. This would be stored in this map as for example {"value": "25"}.
     * @return The state the entity would have after the service was called with the given data.
     */
    String execute(String inputCond, Map<String, String> data);
  }

  /**
   * Represents a pair of preconditions and the resulting conditions that would occur after
   * executing the action, given the preconditions.
   *
   * @param conditions The list of preconditions
   * @param actions    The list of resulting conditions that hold after the action is executed
   */
  static record ConditionActionPair(
      List<Condition> conditions, List<Condition> actions) {

  }

}
