package ls5.pg646.parsing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParserUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger("HassioLogger");
    private static final String INCLUDE_STRING = "!include";
    private static final int INDENT_SPACES = 2;


    private ParserUtil() {}

    /**
     * Resolves all custom include directives in Home Assistant's YAML files.
     * This also works for recursive includes. For more information on how these directives work
     * see Home Assistant's site on "Splitting up the configuration".
     *
     * @param filePath The Path to the configuration yaml file
     * @return A stream where each element contains a line of the fully resolved configuration.
     */
    public static Stream<String> resolveDirectives(Path filePath) {
        // Gets the working dir, which is the directory in which the file denoted by the path is located
        final var workingDir = filePath.getParent();

        // Try to read the file. If that's not possible, return an empty String.
        final String input;
        try {
            input = Files.readString(filePath);
        } catch (IOException e) {
            return Stream.empty();
        }

        return input.lines()
                .flatMap(line -> {
                    // Get the index in the line at which the indlude directive begins (if any)
                    final var includeDirectiveIndex = line.indexOf(INCLUDE_STRING);
                    if (includeDirectiveIndex == -1) {
                        // If there is none, return this line unmodified
                        return Stream.of(line);
                    } else {
                        try {
                            // get the text before the include directive. This usually is a key like "automations:"
                            final var includeLinePrefix = line.substring(0, includeDirectiveIndex);
                            // This should consist of the include of the include directive
                            // and the path to the file/directory that should be included
                            final var includeDirective = line.substring(includeDirectiveIndex + 1).trim()
                                    .split(" ");
                            final var includeMethod = IncludeMethod.fromYamlString(includeDirective[0]);
                            final var includePath = workingDir.resolve(includeDirective[1]);

                            // Resolve all files that are to be included and get their content according to the include method
                            final Stream<String> resolved = switch (includeMethod) {
                                // A normal include directive just includes a single file at the given path
                                case INCLUDE -> resolveDirectives(includePath);
                                // An include dir list directive includes all files in a directory and its subdirectories
                                // and Include each as a list element
                                case INCLUDE_DIR_LIST -> Files.walk(includePath)
                                        .filter(ParserUtil::isYamlFile)
                                        .sorted()
                                        .map(ParserUtil::resolveDirectives)
                                        // Transform each parsed yaml file into a list element, by indenting it and adding a hyphen to its first line
                                        .map(stream -> "-" + stream.collect(Collectors.joining("\n"))
                                                .indent(INDENT_SPACES).substring(1))
                                        .flatMap(String::lines);
                                // An include dir merge list directive includes all files in a directory and its subdirectories
                                // All these files contain lists and the lists are just concatenated
                                case INCLUDE_DIR_MERGE_LIST -> Files.walk(includePath)
                                        .sorted()
                                        .filter(ParserUtil::isYamlFile)
                                        .flatMap(ParserUtil::resolveDirectives);
                                case INCLUDE_DIR_NAMED -> Files.walk(includePath)
                                        .filter(ParserUtil::isYamlFile)
                                        .flatMap(path -> {
                                            // Get the file name without its file extension
                                            final var fileName = path.getFileName().toString().replaceAll("\\.yml|\\.yaml", "");
                                            // Resolve all directives in the file and indent them
                                            final var resolvedYaml = ParserUtil
                                                    .resolveDirectives(path)
                                                    .map(l -> l.indent(INDENT_SPACES));
                                            // Write the file name as the dictionary's key and then write the content of the yaml file
                                            return Stream.concat(Stream.of(fileName + ":"), resolvedYaml);
                                        });
                                case INCLUDE_DIR_MERGE_NAMED -> Files.walk(includePath)
                                        .filter(ParserUtil::isYamlFile)
                                        .flatMap(ParserUtil::resolveDirectives);
                                // Every other include directive is unsupported
                                default -> Stream.empty();
                            };

                            // The original line is transformed into the prefix before the include directive plus
                            // the content of the included files line by line
                            return Stream.concat(Stream.of(includeLinePrefix),
                                    resolved.map(s -> s.indent(INDENT_SPACES))).map(String::stripTrailing);
                        } catch (IOException e) {
                            LOGGER.error("Error resolving directive", e);
                            return Stream.empty();
                        }
                    }
                });
    }

    /**
     * Checks if a file is a yaml file or not. This works solely off file extensions,
     * so it will not recognize yaml files that have a non-standard file extension (or none at all).
     *
     * @param path The path to check
     * @return true, if the file has a file extension of ".yaml" or ".yml", false otherwise
     */
    public static boolean isYamlFile(Path path) {
        String pathStr = path.toString();
        return pathStr.endsWith(".yml") || pathStr.endsWith(".yaml");
    }
}