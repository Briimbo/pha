package ls5.pg646.parsing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupParser {

    public static final String GROUP = "group:";

    private GroupParser() {
    }

    private static final ObjectMapper MAPPER =
            new ObjectMapper(new YAMLFactory())
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

    /**
     * Retrieves the configured groups from the Home Assistant configuration file at the given path.
     *
     * @param configDir The path to the directory containing the configuration.yaml file.
     * @return The groups specified in the configuration.
     * @throws IOException If there is an error reading the files.
     */
    public static Collection<Group> fromConfig(String configDir) throws IOException {
        Path configPath = Paths.get(configDir, "configuration.yaml");

        // Get the content of the config file, with all include directives resolved
        final String configYaml = ParserUtil.resolveDirectives(configPath)
                .collect(Collectors.joining("\n"))
                .stripIndent()
                .trim();

        // Parse all automations in the config string
        return parseGroups(configYaml);
    }

    /**
     * Extracts the group field from the configuration yaml and parses them as a collection of groups.
     * @param configYaml The configuration yaml as a string.
     * @return The groups as a collection of {@link Group}
     * @throws JsonProcessingException If the yaml is malformed or group objects cannot be created from it.
     */
    private static Collection<Group> parseGroups(String configYaml)
            throws JsonProcessingException {

        // Extract the group config block
        String groupYaml =
                configYaml
                        .lines()
                        .dropWhile(line -> !line.startsWith(GROUP))
                        .takeWhile(line -> line.startsWith(GROUP) || line.isEmpty() || Character.isWhitespace(line.charAt(0)))
                        .collect(Collectors.joining("\n"))
                        .stripIndent()
                        .replaceFirst(GROUP, "")
                        .stripIndent()
                        .trim();

        if (groupYaml.isEmpty()) {
          return Collections.EMPTY_SET;
        }

        // Read it as a list of automations
        var groups = MAPPER.readValue(groupYaml, new TypeReference<Map<String, Group>>() {
        });
        // We need to add the id manually since it's just the key in the yaml dictionary
        groups.forEach((id, group) -> group.setId("group." + id));
        return groups.values();
    }
}
