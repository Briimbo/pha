package ls5.pg646.parsing;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Condition {

  private ConditionType conditionType;
  /**
   * A list of subconditions, if it applies to this type of condition.
   * Only And, Or and Not conditions can have subconditions.
   */
  private List<Condition> conditions;

  /**
   * The content of the data field for State and Numeric State conditions.
   */
  private Map<String, Object> data = new HashMap<>();

  public ConditionType conditionType() {
    return conditionType;
  }

  public List<Condition> subConditions() {
    return conditions;
  }

  public Map<String, Object> data() {
    return data;
  }

  @JsonAnySetter
  public void setUnknown(String key, Object value) {
    data.put(key, value);
  }

  @JsonSetter("condition")
  public void setConditionType(String condition) {
    this.conditionType = ConditionType.parseConditionType(condition);
  }

  @Override
  public String toString() {
    return toString(0);
  }

  public String toString(int offset) {
    StringBuilder output = new StringBuilder(Automation.addIndentation(offset));
    output
        .append("Condition{\n")
        .append(Automation.addIndentation(offset + 1))
        .append("condition='")
        .append(conditionType)
        .append('\'')
        .append(", conditions=[");
    if (conditions != null) {
      for (Condition condition : conditions) {
        output.append("\n").append(condition.toString(offset + 2));
      }
      output.append("\n").append(Automation.addIndentation(offset + 1));
    }
    output
        .append("]," + "\n")
        .append(Automation.addIndentation(offset + 1))
        .append("data=")
        .append(data)
        .append("\n")
        .append(Automation.addIndentation(offset))
        .append("},");
    return output.toString();
  }
}
