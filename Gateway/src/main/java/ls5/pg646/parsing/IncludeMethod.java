package ls5.pg646.parsing;

/**
 * Include methods which are used by Home Assistant to split up the configuration.
 */
public enum IncludeMethod {
  INCLUDE,
  INCLUDE_DIR_LIST,
  INCLUDE_DIR_NAMED,
  INCLUDE_DIR_MERGE_LIST,
  INCLUDE_DIR_MERGE_NAMED,
  NONE;

  /**
   * Gets the corresponding enum constant for the include method as it appears in the yaml file.
   * @param yamlString The yaml name of the include method.
   * @return The corresponding include method if it exists, {@link #NONE} otherwise.
   */
  public static IncludeMethod fromYamlString(String yamlString) {
    if (yamlString == null || yamlString.equals("")) {
      return NONE;
    }
    return IncludeMethod.valueOf(yamlString.toUpperCase());
  }

  /**
   * Gets the yaml name of this include method.
   * @return The name of the include method as it would appear in a Home Assistant yaml file.
   */
  public String yamlString() {
    if (this == NONE) {
      return "";
    }
    return this.name().toLowerCase();
  }

  public boolean any() {
    return this != NONE;
  }
}
