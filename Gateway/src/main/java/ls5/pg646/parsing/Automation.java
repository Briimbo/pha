package ls5.pg646.parsing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.stream.Stream;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Automation {

  private List<Trigger> trigger;
  private List<Condition> condition;
  private List<Action> action;
  private String id;
  private String alias;
  private String description;
  @JsonProperty("use_blueprint")
  private Blueprint useBlueprint;
  private String mode;

  public static String addIndentation(int indentation) {
    return "| ".repeat(indentation);
  }

  public Stream<Automation> splitOnTriggers() {
    if (hasSingleTrigger()) {
      return Stream.of(this);
    }

    return trigger.stream()
        .map(
            tr -> {
              var automation = new Automation();

              automation.action = action;
              automation.trigger = List.of(tr);
              automation.id = id;
              automation.alias = alias;
              automation.description = description;
              automation.useBlueprint = useBlueprint;
              automation.mode = mode;
              automation.condition = condition;

              return automation;
            });
  }

  public boolean hasSingleTrigger() {
    return trigger.size() == 1;
  }

  @Override
  public String toString() {
    var builder = new StringBuilder("Automation{");
    builder.append("\n").append(addIndentation(1))
        .append(id)
        .append(",\n")
        .append(addIndentation(1))
        .append(alias)
        .append(",\n")
        .append(addIndentation(1))
        .append(description)
        .append(",\n").append(addIndentation(1));
    if (action != null) {
      builder.append("Actions=");
      for (Action singleAction : action) {
        builder.append(singleAction.toString(2));
      }
    }
    builder.append("\n").append(addIndentation(1));
    if (condition != null) {
      builder.append("Conditions=\n");
      for (Condition singleCondition : condition) {
        builder.append(singleCondition.toString(2));
      }
    }
    builder.append("\n").append(addIndentation(1));
    if (trigger != null) {
      builder.append("Triggers=");
      for (Trigger singleTrigger : trigger) {
        builder.append(singleTrigger.toString(2));
      }
    }
    builder.append("\n").append(addIndentation(1))
        .append(mode)
        .append("\n}");

    return builder.toString();
  }
}
