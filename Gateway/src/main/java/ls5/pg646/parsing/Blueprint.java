package ls5.pg646.parsing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Blueprint {

  private String path;
  private Input input;

  @Override
  public String toString() {
    return "Blueprint{" + "path='" + path + '\'' + ", input=" + input + '}';
  }
}
