package ls5.pg646.parsing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class LightTarget {

  @JsonProperty("device_id")
  private String deviceId;

  @Override
  public String toString() {
    return "LightTarget{" + "device_id='" + deviceId + '\'' + '}';
  }
}
