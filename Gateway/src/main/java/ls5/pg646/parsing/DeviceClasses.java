package ls5.pg646.parsing;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import ls5.pg646.gateway.service.exceptions.InternalServerError;
import ls5.pg646.gateway.service.exceptions.InvalidStateException;
import ls5.pg646.gateway.service.homeassistant.EntityState;
import ls5.pg646.gateway.service.homeassistant.HomeAssistantConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeviceClasses {

  /**
   * A set of binary sensors that are by considered to be environmental.
   * Since non-binary sensors are always considered to be environmental,
   * this stores the exceptions for binary sensors only.
   */
  private static final Set<String> BINARY_ENVIRONMENT_CLASSES = Set.of(
      "battery",
      "battery_charging",
      "cold",
      "connectivity",
      "gas",
      "heat",
      "light",
      "moisture",
      "smoke",
      "sound",
      "vibration");

  @Autowired
  private HomeAssistantConnector connector;

  /**
   * @return A map that maps from an entity id to whether or not the entity's state is modified by
   * the environment. If it is modified by the environment, the value is true. If it is manual, it is
   * false.
   */
  public Map<String, Boolean> getDeviceClasses()
      throws InvalidStateException, InternalServerError, InterruptedException {
    return connector.loadEntityStates(true).stream()
        // We are only interested in entities that have a device class
        .filter(state -> state.getAttributes().hasDeviceClass())
        .collect(
            Collectors.toMap(EntityState::getEntityId, state -> {
              var deviceClass = state.getAttributes().getDeviceClass();
              // Only binary sensors can possibly non-environmental.
              // So if the entity is not a binary sensor it is considered to be environmental by default.
              if (state.getEntityId().split("\\.")[0].equals("binary_sensor")) {
                // Otherwise, check if this binary sensor is explicitly considered to be environmental.
                return BINARY_ENVIRONMENT_CLASSES.contains(deviceClass);
              } else {
                return true;
              }
            })
        );
  }

}
