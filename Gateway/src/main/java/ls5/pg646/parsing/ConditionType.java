package ls5.pg646.parsing;

/**
 * This enum maps to the condition types found in Home Assistant.
 */
public enum ConditionType {
  STATE, NUMERIC_STATE, AND, OR, NOT;

  public static ConditionType parseConditionType(String type) {
    return switch (type) {
      case "state" -> STATE;
      case "numeric_state" -> NUMERIC_STATE;
      case "and" -> AND;
      case "or" -> OR;
      case "not" -> NOT;
      default -> null;
    };
  }
}
