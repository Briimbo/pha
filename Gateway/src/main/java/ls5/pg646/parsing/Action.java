package ls5.pg646.parsing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Action {

  private String service;
  private String type;
  @JsonProperty("device_id")
  private String deviceId;
  private Map<String, String> target;
  private Map<String, String> data;
  private String domain;

  public String toString() {
    return toString(0);
  }

  public String toString(int offset) {
    return "\n"
        + Automation.addIndentation(offset)
        + "Action{\n"
        + Automation.addIndentation(offset + 1)
        + "service='"
        + service
        + '\''
        + ",\n"
        + Automation.addIndentation(offset + 1)
        + "type='"
        + type
        + '\''
        + ",\n"
        + Automation.addIndentation(offset + 1)
        + "device_id='"
        + deviceId
        + '\''
        + ",\n"
        + Automation.addIndentation(offset + 1)
        + "entity_id='"
        + target
        + '\''
        + ",\n"
        + Automation.addIndentation(offset + 1)
        + "domain='"
        + domain
        + '\''
        + ",\n"
        + Automation.addIndentation(offset + 1)
        + "data='"
        + data
        + '\''
        + "\n"
        + Automation.addIndentation(offset)
        + "},";
  }
}
