package ls5.pg646.parsing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class AutomationParser {

  /**
   * Allows parsing yaml files
   */
  private static final ObjectMapper MAPPER =
      new ObjectMapper(new YAMLFactory())
          .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
          .configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

  /**
   * This class doesn't need a constructor
   */
  private AutomationParser() {
  }

  public static Collection<Automation> fromConfig(String configDir) throws IOException {
    Path configPath = Paths.get(configDir, "configuration.yaml");

    // Get the content of the config file, with all include directives resolved
    final String configYaml = ParserUtil.resolveDirectives(configPath)
        .collect(Collectors.joining("\n"))
        .stripIndent()
        .trim();

    // Parse all automations in the config string
    return parseAutomations(configYaml);
  }

  /**
   * Transforms the Home Assistant automation configuration to {@link Automation} objects.
   *
   * @param configYaml The Home Assistant configuration.yaml file with all include directives reolved.
   * @return A collection that contains an object for each automation in the configuration.
   * @throws JsonProcessingException If the YAML is malformed..
   *
   * @see ParserUtil#resolveDirectives(Path) To resolve include directives.
   */
  private static Collection<Automation> parseAutomations(String configYaml)
      throws JsonProcessingException {
    // Extract the automation config block
    String automationYaml =
        configYaml
            .lines()
            .dropWhile(line -> !line.startsWith("automation:"))
            .skip(1)
            .takeWhile(line -> line.isEmpty() || Character.isWhitespace(line.charAt(0)))
            .collect(Collectors.joining("\n"))
            .stripIndent();

    // Read it as a list of automations
    return MAPPER.readValue(automationYaml, new TypeReference<List<Automation>>() {
    });
  }
}
