package ls5.pg646.parsing;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Trigger {

  private List<String> to;
  private List<String> from;
  private List<String> state;
  @JsonProperty("entity_id")
  private String entityId;
  private String domain;
  private String platform;
  private String type;
  private String subtype;
  private Map<String, Object> data = new HashMap<>();

  @JsonAnySetter
  public void setUnknown(String key, Object value) {
    data.put(key, value);
  }

  @Override
  public String toString() {
    return toString(0);
  }

  public String toString(int offset) {
    return "\n"
        + Automation.addIndentation(offset)
        + "Trigger{\n"
        + Automation.addIndentation(offset + 1)
        + "to='"
        + to
        + '\''
        + ",\n"
        + Automation.addIndentation(offset + 1)
        + "entity_id='"
        + entityId
        + '\''
        + ",\n"
        + Automation.addIndentation(offset + 1)
        + "domain='"
        + domain
        + '\''
        + ",\n"
        + Automation.addIndentation(offset + 1)
        + "platform='"
        + platform
        + '\''
        + ",\n"
        + Automation.addIndentation(offset + 1)
        + "type='"
        + type
        + '\''
        + ",\n"
        + Automation.addIndentation(offset + 1)
        + "subtype='"
        + subtype
        + '\''
        + "\n"
        + Automation.addIndentation(offset)
        + "},";
  }
}
