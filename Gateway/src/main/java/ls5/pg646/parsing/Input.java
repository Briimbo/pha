package ls5.pg646.parsing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Input {

  private int brightness;
  @JsonProperty("motion_entity")
  private String motionEntity;
  @JsonProperty("no_motion_wait")
  private String noMotionWait;
  @JsonProperty("light_target")
  private LightTarget lightTarget;
  private String from;
  private String to;

  @Override
  public String toString() {
    return "Input{"
        + "brightness="
        + brightness
        + ", motion_entity='"
        + motionEntity
        + '\''
        + ", no_motion_wait='"
        + noMotionWait
        + '\''
        + ", light_target="
        + lightTarget
        + ", from='"
        + from
        + '\''
        + ", to='"
        + to
        + '\''
        + '}';
  }
}
