package ls5.pg646.parsing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * This class contains data representing a floor plan.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public record FloorPlanData(List<Entity> entities, boolean draw,
        boolean usePolyline,
        boolean unsavedChanges,
        Scale scale) {

    // TODO Add Shape
    public record Room(String id, String name, boolean removed){}

    public record Entity(double x, double y, String id, EntityData entity, String image, boolean hovered){}
    public record EntityData(String entityId, EntityAttributes attributes, String lastChanged, String lastUpdated, String state, String domain){}
    public record EntityAttributes(String friendlyName, String icon, String deviceClass, String deviceKind){}
    public record Scale(float metersPerPixel){}

    public boolean containsEntity(String id) {
        return entities.stream().map(Entity::id).anyMatch(id::equals);
    }
}
