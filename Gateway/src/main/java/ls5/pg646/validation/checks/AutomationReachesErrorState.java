package ls5.pg646.validation.checks;

import java.time.LocalDateTime;
import ls5.pg646.gateway.controller.constraint.ValidationState.Notice;
import ls5.pg646.gateway.controller.constraint.ValidationState.Phase;
import ls5.pg646.validation.jgrapht.SmartHomeState;
import ls5.pg646.validation.jgrapht.SmartHomeTransition;
import ls5.pg646.validation.math.Condition;
import ls5.pg646.validation.symbolic.Constraint;
import org.jgrapht.Graph;

public class AutomationReachesErrorState implements AutomatonCheck {

  @Override
  public CheckResult check(
      Graph<SmartHomeState, SmartHomeTransition> automaton, Constraint constraint) {
    var badTransitions = automaton.edgeSet().stream()
        .filter(
            e ->
                e.type().equals(SmartHomeTransition.Type.AUTO)
                    && !e.target().isAcceptingState(constraint))
        .toList();
    var messages = badTransitions.stream()
        .map(t ->
            new Notice(
                Notice.Type.ERROR, LocalDateTime.now(),
                Phase.CHECK_1,
                getErrorMessage(t)
            )
        ).toList();

    return new CheckResult(
        badTransitions.isEmpty(),
        messages);
  }

  private String getErrorMessage(SmartHomeTransition t) {
    return "The following automation transition leads to an error state:"
        + "\n\tInitial State:\t" + t.source().toMessage()
        + "\n\tAutomation:\t" + t.guard().stream().map(Condition::toMessage)
        .reduce((a, s) -> a + ", " + s).orElse("EMPTY AUTOMATION TRIGGERS")
        + "\n\tFinal State:\t" + t.target().toMessage();
  }
}
