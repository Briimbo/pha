package ls5.pg646.validation.checks;

import java.time.LocalDateTime;
import java.util.LinkedList;
import ls5.pg646.gateway.controller.constraint.ValidationState.Notice;
import ls5.pg646.gateway.controller.constraint.ValidationState.Notice.Type;
import ls5.pg646.gateway.controller.constraint.ValidationState.Phase;
import ls5.pg646.validation.jgrapht.SmartHomeState;
import ls5.pg646.validation.jgrapht.SmartHomeTransition;
import ls5.pg646.validation.symbolic.Constraint;
import org.jgrapht.Graph;
import org.jgrapht.alg.shortestpath.BFSShortestPath;
import org.jgrapht.graph.MaskSubgraph;

public class AutomationFixesUserError implements AutomatonCheck {

  @Override
  public CheckResult check(Graph<SmartHomeState, SmartHomeTransition> automaton,
      Constraint constraint) {
    var subgraph = new MaskSubgraph<SmartHomeState, SmartHomeTransition>(automaton,
        v -> false, e -> !e.type().equals(SmartHomeTransition.Type.AUTO));
    var acceptingVertices
        = subgraph.vertexSet().stream().filter(v -> v.isAcceptingState(constraint)).toList();

    var messages = new LinkedList<Notice>();
    var nonAcceptingVertices = subgraph.vertexSet().stream()
        .filter(v -> !v.isAcceptingState(constraint)).toList();
    for (var nonAccepting : nonAcceptingVertices) {
      var pathExists = acceptingVertices.stream()
          .anyMatch(v -> BFSShortestPath.findPathBetween(subgraph, nonAccepting, v) != null);
      if (!pathExists) {
        messages.add(new Notice(Type.ERROR, LocalDateTime.now(), Phase.CHECK_3,
            nonAccepting.toMessage()));
      }
    }

    return new CheckResult(
        messages.isEmpty(),
        messages
    );
  }
}
