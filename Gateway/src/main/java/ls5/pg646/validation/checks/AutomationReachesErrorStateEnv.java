package ls5.pg646.validation.checks;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import ls5.pg646.gateway.controller.constraint.ValidationState.Notice;
import ls5.pg646.gateway.controller.constraint.ValidationState.Phase;
import ls5.pg646.validation.jgrapht.SmartHomeState;
import ls5.pg646.validation.jgrapht.SmartHomeTransition;
import ls5.pg646.validation.symbolic.Constraint;
import org.jgrapht.Graph;

public class AutomationReachesErrorStateEnv implements AutomatonCheck {

  @Override
  public CheckResult check(
      Graph<SmartHomeState, SmartHomeTransition> automaton, Constraint constraint) {
    var autoErrorCheck = new AutomationReachesErrorState();
    if (!autoErrorCheck.check(automaton, constraint).checkSuccessful()) {
      return new CheckResult(false, List.of(
          new Notice(
              Notice.Type.ERROR, LocalDateTime.now(),
              Phase.CHECK_2,
              "Errors already occur in CHECK 1!"
          )
      ));
    }

    Queue<SmartHomeState> queue = new LinkedList<>();
    Set<SmartHomeState> enqueued = new HashSet<>();
    enqueued.addAll(
        automaton.edgeSet().stream()
            .filter(e -> e.type().equals(SmartHomeTransition.Type.AUTO))
            .map((SmartHomeTransition::target))
            .toList());
    queue.addAll(enqueued);
    while (!queue.isEmpty()) {
      SmartHomeState curr = queue.poll();
      var badEnvTrans = automaton.edgeSet().stream()
          .filter(
              e ->
                  e.source().equals(curr)
                      && e.type().equals(SmartHomeTransition.Type.ENV)
                      && !e.target().isAcceptingState(constraint))
          .toList();
      if (!badEnvTrans.isEmpty()) {
        var messages = badEnvTrans.stream()
            .map(t ->
                new Notice(
                    Notice.Type.ERROR, LocalDateTime.now(),
                    Phase.CHECK_2,
                    getErrorMessage(t)
                )
            ).toList();
        return new CheckResult(false, messages);
      }
      var nextStates =
          automaton.edgeSet().stream()
              .filter(
                  e ->
                      e.source().equals(curr)
                          && e.type().equals(SmartHomeTransition.Type.ENV)
                          && !enqueued.contains(e.target()))
              .map(SmartHomeTransition::target)
              .toList();
      queue.addAll(nextStates);
      enqueued.addAll(nextStates);
    }
    return new CheckResult(true, List.of());
  }

  private String getErrorMessage(SmartHomeTransition t) {
    return "Environment event leads to error state:"
        + "\n\tInitial State:\t" + t.source().toMessage()
        + "\n\tEvent:\t\tChange of " + t.label()
        + "\n\tFinal State:\t" + t.target().toMessage();
  }
}
