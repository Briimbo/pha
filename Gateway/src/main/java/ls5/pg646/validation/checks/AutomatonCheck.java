package ls5.pg646.validation.checks;

import java.util.Collection;
import ls5.pg646.gateway.controller.constraint.ValidationState.Notice;
import ls5.pg646.validation.jgrapht.SmartHomeState;
import ls5.pg646.validation.jgrapht.SmartHomeTransition;
import ls5.pg646.validation.symbolic.Constraint;
import org.jgrapht.Graph;

public interface AutomatonCheck {
  
  public static record CheckResult(boolean checkSuccessful, Collection<Notice> messages) {}
  
  public CheckResult check(
      Graph<SmartHomeState, SmartHomeTransition> automaton,
      Constraint constraint); // do we need more than the graph?
}
