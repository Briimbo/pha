package ls5.pg646.validation.checks;

import java.time.LocalDateTime;
import java.util.LinkedList;
import ls5.pg646.gateway.controller.constraint.ValidationState.Notice;
import ls5.pg646.gateway.controller.constraint.ValidationState.Notice.Type;
import ls5.pg646.gateway.controller.constraint.ValidationState.Phase;
import ls5.pg646.validation.jgrapht.SmartHomeState;
import ls5.pg646.validation.jgrapht.SmartHomeTransition;
import ls5.pg646.validation.symbolic.Constraint;
import org.jgrapht.Graph;
import org.jgrapht.alg.cycle.CycleDetector;
import org.jgrapht.graph.MaskSubgraph;

public class SearchForAutomationCycles implements AutomatonCheck {

  @Override
  public CheckResult check(Graph<SmartHomeState, SmartHomeTransition> automaton,
      Constraint constraint) {
    var subgraph = new MaskSubgraph<SmartHomeState, SmartHomeTransition>(automaton,
        v -> false, e -> !e.type().equals(SmartHomeTransition.Type.AUTO));
    var cycleDetector = new CycleDetector<SmartHomeState, SmartHomeTransition>(subgraph);
    var cycles = cycleDetector.findCycles();
    var messages = new LinkedList<Notice>();
    if (!cycles.isEmpty()) {
      messages.add(new Notice(Type.ERROR, LocalDateTime.now(), Phase.CHECK_4,
          "The following states form a cycle:" + cycles.stream().map(SmartHomeState::toMessage)
              .reduce("", (a, s) -> a + "\n\t- " + s)));
    }
    return new CheckResult(
        cycles.isEmpty(),
        messages
    );
  }
}
