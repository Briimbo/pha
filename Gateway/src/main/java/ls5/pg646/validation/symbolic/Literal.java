package ls5.pg646.validation.symbolic;

public interface Literal {

  String name();
}
