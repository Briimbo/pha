package ls5.pg646.validation.symbolic;

import java.util.Collection;
import ls5.pg646.parsing.Condition;
import org.logicng.formulas.Formula;
import org.logicng.formulas.FormulaFactory;
import org.logicng.transformations.dnf.DNFFactorization;

/**
 * A class providing methods to convert Home Assistant {@link Condition} objects
 * into LogicNG {@link Formula} objects in disjunctive normal form.
 */
public class LogicFormulaConverter {

  public static final String ENTITY_ID = "entity_id";
  public static final String ATTRIBUTE = "attribute";

  private LogicFormulaConverter() {
  }

  /**
   * Converts a collection of Home Assistant conditions to a LogicNG {@link Formula}.
   * The input collection is treated as a conjunction.
   *
   * @param conditions The conditions to be converted.
   * @return The input conditions in form of a LogicNG {@link Formula} in disjunctive normal form.
   */
  public static Formula asFormula(Collection<Condition> conditions) {
    var factory = new FormulaFactory();
    return new DNFFactorization().apply(
        factory.and(conditions.stream()
            .map(cond -> asFormula(factory, cond))
            .toArray(Formula[]::new)
        ),
        false
    );
  }

  /**
   * Converts a Home Assistant condition to a LogicNG {@link Formula}.
   * This is an internal convenience method to allow reusing the {@link FormulaFactory} object
   * in recursive calls.
   *
   * The public API is {@link #asFormula(Collection)}.
   *
   * @param factory The {@link FormulaFactory} object used to create new formulas.
   * @param condition The condition to be converted.
   * @return The input condition in form of a LogicNG {@link Formula}
   *
   * @see #asFormula(Collection)
   */
  private static Formula asFormula(FormulaFactory factory, Condition condition) {
    return switch (condition.conditionType()) {
      case STATE -> factory.variable(
          condition.data().get(ENTITY_ID) + "==" + condition.data().get("state"));
      case NUMERIC_STATE -> {
        final Integer above = (Integer) condition.data().get("above");
        final Integer below = (Integer) condition.data().get("below");
        final String attribute =
            condition.data().containsKey(ATTRIBUTE) ? "." + condition.data().get(ATTRIBUTE)
                : "";
        if (above == null ^ below == null) {
          if (above != null) {
            yield factory.variable(condition.data().get(ENTITY_ID) + attribute + ">" + above);
          } else {
            yield factory.variable(condition.data().get(ENTITY_ID) + attribute + "<" + below);
          }
        }

        yield factory.and(
            factory.variable(condition.data().get(ENTITY_ID) + attribute + ">" + above),
            factory.variable(condition.data().get(ENTITY_ID) + attribute + "<" + below));
      }
      case AND -> factory.and(condition.subConditions().stream()
          .map(cond -> asFormula(factory, cond))
          .toArray(Formula[]::new)
      );
      case OR -> factory.or(condition.subConditions().stream()
          .map(cond -> asFormula(factory, cond))
          .toArray(Formula[]::new)
      );
      case NOT -> condition.subConditions().size() == 1
          ? factory.not(asFormula(factory, condition.subConditions().get(0))) :
          factory.and(
              condition.subConditions().stream()
                  .map(cond -> asFormula(factory, cond))
                  .map(factory::not)
                  .toArray(Formula[]::new)
          );
    };
  }
}
