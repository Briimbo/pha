package ls5.pg646.validation.symbolic;

import java.util.HashSet;
import java.util.Set;

public class FakeOracle implements Oracle {

  private final Set<String> environment = new HashSet<>();

  @Override
  public boolean isEnvironment(String sensor) {
    return environment.contains(sensor);
  }

  public boolean setEnvironment(String sensor) {
    return environment.add(sensor);
  }

  public Set<String> getEnvironment() {
    return environment;
  }

  @Override
  public String toString() {
    return "FakeOracle [environment=" + environment + "]";
  }
}
