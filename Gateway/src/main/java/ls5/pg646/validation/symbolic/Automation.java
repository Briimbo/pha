package ls5.pg646.validation.symbolic;

import static java.util.stream.Stream.concat;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import ls5.pg646.validation.jgrapht.SmartHomeState;
import ls5.pg646.validation.math.Condition;

/**
 * <p>This class is a symbolic representation of an automation in a smart home system.<br/>
 * Currently, it maps the semantics of HomeAssistant.</p>
 *
 * <p>An automation can only have one trigger. For multi-trigger automations,
 * you need to create one automation for each trigger. The {@link #multiple} factory method can be
 * used to conveniently create multiple automations from a list of triggers.</p>
 *
 * <p>The string representation of automations follows the syntax as laid
 * out in the wiki: <pre>[name]: [trigger], {conditions} --> {actions}</pre>, where the arrow
 * symbolizes the transformation.
 * </p>
 *
 * <p>
 * If multiple symbolic automations are created from a single automation in HA, its name should
 * follow the format {@code <original-name>$<n>}, where {@code n} is a unique suffix, preferably
 * numerical.
 * </p>
 *
 * @param name       Label identifying the automation, with suffix (see above)
 * @param trigger    The trigger that invokes the automation. Must be unique. Use {@link #multiple}
 *                   to conveniently instantiate multi-trigger automations.
 * @param conditions The list of conditions that must hold true for the automation to actually
 *                   execute when the trigger fires
 * @param actions    The list of observable effects onto the system, which will be assumed to be
 *                   true after the automation has fully executed.
 */
public record Automation(String name, Trigger trigger,
                         List<Condition> conditions, List<Condition> actions) {

  public Automation {
    conditions = Collections.unmodifiableList(conditions);
    actions = Collections.unmodifiableList(actions);
  }

  /**
   * Convenience factory method to create a single automation.
   *
   * @param name       The name of the automation
   * @param trigger    Trigger of the automation
   * @param conditions All conditions of the automation
   * @param actions    All actions of the automation
   * @return An instance of this class, using the above arguments
   */
  public static Automation single(
      String name, Trigger trigger, List<Condition> conditions, List<Condition> actions) {
    return new Automation(name, trigger, conditions, actions);
  }

  /**
   * <p>Convenience factory method to create multiple automations with the same
   * conditions and actions, but multiple triggers.</p>
   *
   * <p>Use this to instantiate multi-trigger automations conveniently.</p>
   *
   * <p>The automations will use the given name with the suffix {@code $n} added
   * to distinguish them.</p>
   *
   * @param name       The original name of the automation
   * @param triggers   All triggers of the automation
   * @param conditions All conditions of the automation
   * @param actions    All actions of the automation
   * @return A list of automations with a single trigger each, using {@code $n} as name suffix for
   *        distinction.
   */
  public static List<Automation> multiple(
      String name, List<Trigger> triggers, List<Condition> conditions, List<Condition> actions) {

    record WithIndex(int index, List<Trigger> triggers) {

    }

    return IntStream
        .iterate(0, i -> i + 1)
        .mapToObj(i -> new WithIndex(i, triggers))
        .flatMap(i -> i.triggers.stream()
            .map(trigger -> single(name + "$" + i.index, trigger, conditions, actions)))
        .toList();
  }

  /**
   * Returns the string representation of this automation.
   *
   * <p>The representation follows the syntax as laid out in the wiki:
   * <pre>[name]: [trigger], {conditions} --> {actions}</pre>,
   * where the arrow symbolizes the transformation.
   * </p>
   */
  @Override
  public String toString() {
    return "%s: %s, {%s} --> {%s}".formatted(name, trigger.toString(),
        conditionsToString(conditions), conditionsToString(actions));
  }

  private String conditionsToString(List<Condition> conditions) {
    return conditions.stream()
        .map(Condition::toString)
        .collect(Collectors.joining(" && "));
  }

  /**
   * Extracts the names of the sensors this automation depends on. This is both the name of the
   * trigger, as well as all sensors used in any condition, but NOT any of the actions.
   *
   * @return A stream of all sensors that have an influence on the execution of this automation
   */
  public Stream<String> preceedingSensors() {
    return concat(Stream.of(trigger().name()),
        conditions().stream().map(Condition::name))
        .distinct();
  }

  /**
   * Checks whether or not this automation is influenced by any of the sensors in the given set. A
   * sensor influences an automation if it appears in either the trigger or any of the conditions.
   *
   * @return whether or not any sensor in the set has an influence on this automation
   */
  public boolean isInfluencedByAny(Set<String> sensors) {
    return preceedingSensors()
        .anyMatch(sensors::contains);
  }

  /**
   * Checks whether or not this automation has an influence on any of the sensors in the given set.
   * An automation influences a sensor if the sensors appears in any of the actions of the
   * automation.
   *
   * @return whether or not this automation changes any of the sensors in the set
   */
  public boolean hasInfluenceOnAny(Set<String> sensors) {
    return succeedingSensors()
        .anyMatch(sensors::contains);
  }

  /**
   * Extracts the names of the sensors this automation has an influence on. These are the names of
   * sensors that appear in the action of the automation, but NOT the trigger or conditions.
   *
   * @return A stream of all sensors this automation influences
   */
  public Stream<String> succeedingSensors() {
    return actions.stream().map(Condition::name)
        .distinct();
  }

  /**
   * Returns the names of all sensors this automation interacts in any way with.
   *
   * <p>These are the name of the trigger, the names of any sensors appearing in any
   * condition, and the names of all sensors appearing in the actions.</p>
   *
   * @return A stream of all sensors influencing or being influenced by this automation
   */
  public Stream<String> allSensors() {
    return concat(preceedingSensors(), succeedingSensors()).distinct();
  }

  public boolean executesIn(SmartHomeState state) {
    return trigger.firesIn(state)
        && conditions.stream().allMatch(condition -> condition.holdsIn(state));
  }
}
