package ls5.pg646.validation.symbolic;

import static java.util.stream.Collectors.toSet;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A symbolic representation of a SmartHome. This smart home only holds a list of automations and
 * some convenience methods to look automations up in various ways.
 */
public record SmartHome(List<Automation> automations) {

  public SmartHome {
    automations = Collections.unmodifiableList(automations);
  }

  /**
   * <p>Returns a list of automations that interfere in any way with the given constraint.</p>
   * <p>This method recursively looks up all automations in the smart home and determines
   * if there is a path through which an automation may change a condition that is governed by the
   * given constrained. If such a path is found, the automation is returned as part of this
   * list.</p>
   * <p>This method makes no guarantee as to the order of the list, it is arbitrary
   * (read: implementation-detail subject to change at any point).</p>
   *
   * @param constraint the {@link Constraint} to find interfering automations for
   * @return a list of automations that directly or indirectly have an influence on a sensor
   * appearing in the given constraint
   */
  public List<Automation> findInterferingAutomations(Constraint constraint) {
    return findAutomationBySensor(findRelevantSensors(constraint));
  }

  /**
   * Returns a list of automations that have an influence on / change the given set of sensors (have
   * the sensor appear in their actions).
   *
   * @return set of automations which influence the given sensors
   */
  public List<Automation> findAutomationBySensor(Set<String> sensors) {

    return automations.stream()
        .filter(automation -> automation.hasInfluenceOnAny(sensors))
        .toList();
  }

  /**
   * This method finds sensors that (directly or indirectly) have an influence on this constraint.
   * See {@link SmartHome#findRelevantSensors(Set)} for more information.
   *
   * @param constraint The constraint to start from
   * @return A set of sensors that have a direct or indirect influence on this constraint via
   * triggered automations that change any of the sensors appearing in the constraint
   * @see SmartHome#findRelevantSensors(Set)
   */
  public Set<String> findRelevantSensors(Constraint constraint) {
    return findRelevantSensors(constraint.getInterferingSensors());
  }

  /**
   * <p>This method finds relevant sensors that have any influence on the set of
   * given sensors through direct automation or indirectly through a chain of automations.</p>
   *
   * <p>This method starts with the given sensors, and looks up any automation
   * which have an influence on them (change them). If any are found, the sensors which have an
   * influence on those automations are added to the current set of relevant sensors, and a new
   * search for relevant automations is triggered.<br> This is continued until no new automations
   * can be found (a fix point is reached), and the set is returned.</p>
   *
   * @param sensors the initial set of sensors under consideration
   * @return the set of sensors that in any way (directly or indirectly) influence the given set of
   * sensors
   */
  protected Set<String> findRelevantSensors(Set<String> sensors) {

    Set<String> current = new HashSet<>(sensors); // current set of sensors

    // We collect literals as long as we still find new ones, once we reach
    // a fix point (next == current), we stop

    var isFixpoint = false;
    do {
      var automations = findAutomationBySensor(current);
      var newlyFound = automations.stream()
          .flatMap(Automation::preceedingSensors)
          .collect(toSet());
      isFixpoint = current.containsAll(newlyFound);
      if (!isFixpoint) {
        current.addAll(newlyFound);
      }
    } while (!isFixpoint);

    return current;
  }
}