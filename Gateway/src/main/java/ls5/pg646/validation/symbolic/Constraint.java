package ls5.pg646.validation.symbolic;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import ls5.pg646.validation.Utils;
import ls5.pg646.validation.math.Condition;

/**
 * A constraint is a description of non-accepting states given in disjunctive normal form (DNF).
 *
 * <p>This is modeled as a list of lists. The inner lists represent the conjunctive clauses,
 * the outer list represents the disjunction of these clauses.</p>
 *
 * @param name A name to identify the constraint by.
 */
public record Constraint(String name, List<List<Condition>> constraints) {

  public Constraint {
    if (constraints == null) {
      throw new IllegalArgumentException("The list of condition MUST NOT be null.");
    }

    constraints = constraints.stream()
        .map(Collections::unmodifiableList)
        .toList();
  }

  /**
   * Factory method to conveniently instantiate constraints.
   *
   * @param name       Identifier given to the constraint
   * @param conditions conjunctive clause of the conditions that must all hold
   * @return A new constraint
   */
  public static Constraint check(String name, Condition... conditions) {
    return new Constraint(name, List.of(Arrays.asList(conditions)));
  }

  /**
   * Returns true iff the given literal appears in any of the conditions of this constraint.
   *
   * @return true iff the literal appears in any condition
   */
  public boolean contains(String literal) {
    return constraints.stream()
        .flatMap(List::stream)
        .map(Condition::name)
        .anyMatch(name -> name.equals(literal));
  }

  /**
   * Returns all sensors that are interfering with this constraint <i>directly</i>.
   *
   * @return A set of sensors that appear directly in this constraint.
   */
  public Set<String> getInterferingSensors() {
    return constraints().stream()
        .flatMap(List::stream)
        .map(Condition::name)
        .collect(Collectors.toSet());
  }

  public String toString() {
    return "%s: %s".formatted(name,
        constraints.stream()
            .map(clause -> "(" + Utils.join(" && ", clause) + ")")
            .collect(Collectors.joining(" || ")));
  }
}
