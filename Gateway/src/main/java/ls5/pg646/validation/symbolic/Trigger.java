package ls5.pg646.validation.symbolic;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import ls5.pg646.validation.Utils;
import ls5.pg646.validation.jgrapht.EntityState;
import ls5.pg646.validation.jgrapht.SmartHomeState;
import ls5.pg646.validation.math.Condition;

public sealed interface Trigger extends Literal {

  static ContinuousTrigger always(String name) {
    return new ContinuousTrigger(name);
  }

  @SafeVarargs
  static StateTrigger when(String name, Condition... conditions) {
    return new StateTrigger(name, Arrays.asList(conditions));
  }

  boolean firesIn(SmartHomeState state);

  record ContinuousTrigger(String name) implements Trigger {

    @Override
    public boolean firesIn(SmartHomeState state) {
      return state.getEntityStates().stream()
          .map(EntityState::name)
          .anyMatch(name -> name.equals(this.name));
    }

    @Override
    public String toString() {
      return name;
    }
  }

  record StateTrigger(String name, List<Condition> conditions) implements Trigger {

    public StateTrigger {
      conditions = Collections.unmodifiableList(conditions);
      if (!conditions.stream().allMatch(condition -> condition.name().equals(name))) {
        throw new IllegalArgumentException(
            "Conditions must all use the same literal for the same trigger.");
      }
    }

    @Override
    public boolean firesIn(SmartHomeState state) {
      return state.getEntityStates().stream()
          // only keep entities with the same literal as this trigger
          .filter(entity -> entity.name().equals(this.name()))
          // if any of them match all conditions, the trigger executes
          .anyMatch(entity -> conditions.stream().allMatch(condition -> condition.holdsIn(state)));
    }

    @Override
    public String toString() {
      return Utils.join(" && ", conditions);
    }
  }
}