package ls5.pg646.validation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

import ls5.pg646.validation.math.Relation;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.logicng.formulas.And;
import org.logicng.formulas.Formula;
import org.logicng.formulas.FormulaFactory;
import org.logicng.formulas.Literal;
import org.logicng.formulas.Or;
import org.logicng.formulas.Variable;
import org.logicng.transformations.dnf.DNFFactorization;
import ls5.pg646.constraints.constraints.ClassQuery;
import ls5.pg646.constraints.constraints.ConstraintDocument;
import ls5.pg646.constraints.constraints.Expression;
import ls5.pg646.constraints.constraints.Greater;
import ls5.pg646.constraints.constraints.GreaterOrEqual;
import ls5.pg646.constraints.constraints.GroupQuery;
import ls5.pg646.constraints.constraints.IntConstant;
import ls5.pg646.constraints.constraints.LogicalAnd;
import ls5.pg646.constraints.constraints.LogicalOr;
import ls5.pg646.constraints.constraints.QuantifiedExpression;
import ls5.pg646.constraints.constraints.Quantifier;
import ls5.pg646.constraints.constraints.QueryExpression;
import ls5.pg646.constraints.constraints.QueryIntersection;
import ls5.pg646.constraints.constraints.QueryUnion;
import ls5.pg646.constraints.constraints.RangeQuery;
import ls5.pg646.constraints.constraints.Reading;
import ls5.pg646.constraints.constraints.RealConstant;
import ls5.pg646.constraints.constraints.Reference;
import ls5.pg646.constraints.constraints.Smaller;
import ls5.pg646.constraints.constraints.SmallerOrEqual;
import ls5.pg646.constraints.constraints.UnitaryMinus;
import ls5.pg646.constraints.constraints.UnitaryNot;
import ls5.pg646.constraints.constraints.impl.IntConstantImpl;
import ls5.pg646.constraints.constraints.impl.ReferenceImpl;
import ls5.pg646.constraints.constraints.impl.SmallerOrEqualImpl;
import ls5.pg646.constraints.constraints.impl.UnitaryMinusImpl;
import ls5.pg646.gateway.service.exceptions.InternalServerError;
import ls5.pg646.gateway.service.exceptions.InvalidStateException;
import ls5.pg646.gateway.service.homeassistant.HomeAssistantConnector;
import ls5.pg646.validation.math.Condition;
import ls5.pg646.validation.math.Condition.BinaryCondition;
import ls5.pg646.validation.symbolic.Constraint;

/**
 * Handles the conversion of Constraints into LogicNG Formulas and from there into Conditions.
 * 
 * Constraints are desugared and converted into LogicNG formulas, before being converted by LogicNG 
 * into DNF. The resulting DNF is then converted into a 
 */
public class LogicNgConverter {

  private final FormulaFactory factory;
  private final Map<String, String> alias;
  
  private final HomeAssistantConnector connector;

  private final Map<String, Condition.NumericCondition> placeholders;

  public LogicNgConverter(HomeAssistantConnector connector, Map<String, String> alias) {
    this(connector, new FormulaFactory(), alias);
  }

  public LogicNgConverter(HomeAssistantConnector connector, FormulaFactory factory, Map<String, String> alias) {
    this.factory = factory;
    this.alias = alias;
    this.connector = connector;
    placeholders = new HashMap<>();
  }

  public Constraint constraintFromExpression(String name, Expression expression) {
    return constraintFromFormula(name, formulaFromExpression(expression, new HashMap<>()));
  }

  /**
   * Converts an arbitrary LogicNG formula into a constraint. Therefore, the formula
   * is refactored into DNF.
   * 
   * @param name The name of the constraint
   * @param formula An arbitrary LogicNG formula
   * @return The Constraint as usable by our model-checking
   */
  public Constraint constraintFromFormula(String name, Formula formula) {
    var dnfFactory = new DNFFactorization();
    formula = dnfFactory.apply(formula, false);

    return new Constraint(name, conditionsFromFormula(formula));
  }

  /**
   * Takes a formula <b>in DNF</b> (disjunction of conjunctions) 
   * and converts all clauses into a list of conditions.
   * 
   * @param formula The formula in DNF
   * @return Conditions as usable by our model-checking.
   */
  public List<List<Condition>> conditionsFromFormula(Formula formula) {
	  
	return switch (formula) {
      case Or or -> // take all clauses (conjunctive) and list them
        or.stream().map(this::conditionsFromClause).toList();     
	  case And and -> // we only have the one clause
	  	List.of(and.stream().map(this::conditionFromSymbol).toList());
      case Variable variable ->
        List.of(List.of(conditionFromSymbol(variable)));
      case Literal literal ->
        List.of(List.of(conditionFromSymbol(literal)));
      default ->
        throw exceptionForObject(formula);
    };
  }

  /**
   * Takes a <b>conjunctive</b> clause and converts it into a list of conditions.
   * @param formula as disjunctive clause
   * @return List of conditions equivalent to this clause
   */
  public List<Condition> conditionsFromClause(Formula formula) {
    return switch (formula) {
	  case And and ->
	  	and.stream().map(this::conditionFromSymbol).toList();
      case Variable variable ->
        List.of(conditionFromSymbol(variable));
      case Literal literal ->
        List.of(conditionFromSymbol(literal));
      default ->
        throw exceptionForObject(formula);
    };
  }

  private static Condition.NumericCondition negateNumericCondition(Condition.NumericCondition nc){
    Relation newRel = null;
    switch(nc.relation()){
      case EQUALS -> {
        throw new IllegalArgumentException("Numeric conditions with EQUAL relation cannot be negated!");
      }
      case LESSER -> {
        newRel = Relation.GREATER_OR_EQUAL;
      }
      case GREATER -> {
        newRel = Relation.LESS_OR_EQUAL;
      }
      case LESS_OR_EQUAL -> {
        newRel = Relation.GREATER;
      }
      case GREATER_OR_EQUAL -> {
        newRel = Relation.LESSER;
      }
    }
    return new Condition.NumericCondition(nc.name(), newRel, nc.value());
  }


  /**
   * Creates the proper condition (binary or numeric) from the given LogicNG symbol.
   * @param formula The logicNG symbol
   * @return
   */
  public Condition conditionFromSymbol(Formula formula) {
    switch (formula) {
      case Variable variable:
        if (placeholders.containsKey(variable.name()))
          return placeholders.get(variable.name());
        else
          return new BinaryCondition(variable.name().trim(), variable.phase());
      case Literal literal:
        if (placeholders.containsKey(literal.name())){
          if(literal.phase())
            return placeholders.get(literal.name());
          else
            return negateNumericCondition(placeholders.get(literal.name()));
        }
        else return new BinaryCondition(literal.name().trim(), literal.phase());
      default:
        throw exceptionForObject(formula);
    }
  }
  
  /**
   * Creates the negation/complement of the given set. This is basically all other entities
   * not in the given set.
   * 
   * @param without The entities to calculate the complement of.
   * @return The set of all entities without the given entities
   */
  public Collection<String> negate(Collection<String> without) 
      throws InvalidStateException, InternalServerError, InterruptedException {
    var all = connector.getCurrentEntityStates().keySet();
    all.removeAll(without);
    return all;
  }
  
  /**
   * Resolves the given query expression and returns the collection of entities that are in the
   * set of entities described by this query.
   * 
   * @param query The query to resolve
   * @param gamma The current context, holding the mapping of arguments to entities.
   * @return The set of entities described by this query
   */
  public Collection<String> resolveQueryExpression(QueryExpression query, Map<String, String> gamma) {
    switch (query) {
      case ClassQuery classQuery:
        try {
          var inClass = connector.entitiesOfDomain(classQuery.getClazz());
          return classQuery.isNot() ? negate(inClass): inClass;
        } catch (InvalidStateException | InternalServerError | InterruptedException e) {
          e.printStackTrace();
          return List.of();
        }
        
      case RangeQuery rangeQuery:
        try {
          var reference = rangeQuery.getReference();
          var entity = resolveReference(reference, gamma);
          var inRange = connector.entitiesInRangeOf(entity, rangeQuery.getRange());
          return rangeQuery.isNot() ? negate(inRange) : inRange;
        } catch (InvalidStateException | InternalServerError | InterruptedException e) {
          e.printStackTrace();
          return List.of();
        }
        
      case GroupQuery groupQuery:
        try {
          var inGroup = HomeAssistantConnector.entitiesInGroup(groupQuery.getGroup());
          return groupQuery.isNot() ? negate(inGroup): inGroup;
        } catch (InvalidStateException | InternalServerError | InterruptedException | IOException e) {
          e.printStackTrace();
          return List.of();
        }
        
      case QueryUnion union:
        var unionSet = new ArrayList<String>();
        unionSet.addAll(resolveQueryExpression(union.getLeft(), gamma));
        unionSet.addAll(resolveQueryExpression(union.getRight(), gamma));
        return unionSet;
        
      case QueryIntersection intersection:
        var intersectionSet = new ArrayList<String>();
        intersectionSet.addAll(resolveQueryExpression(intersection.getLeft(), gamma));
        intersectionSet.retainAll(resolveQueryExpression(intersection.getRight(), gamma));
        return intersectionSet;
        
        
      default: throw new UnsupportedOperationException(
          String.format("Cannot resolve query expression [%s]: %s", 
              query.eClass().getName(), NodeModelUtils.findActualNodeFor(query).getText()));
    }
  }
  
  /**
   * Resolves a reference -- which can either be an alias or an argument from a quantor.
   * 
   * @param reference The reference to resolve
   * @param gamma The current context, containing the current mapping of arguments to entities
   * @return The concrete, resolved entity
   */
  public String resolveReference(Reference reference, Map<String, String> gamma) {
    var key = NodeModelUtils.findActualNodeFor(reference).getText().trim();
    return gamma.containsKey(key) ? gamma.get(key) : alias.get(key);
  }
  
  /**
   * Converts a given constraint (sub-) expression into a LogicNG formula.
   * @param expression The expression to convert
   * @return Equivalent formula in LogicNG.
   */
  public Formula formulaFromExpression(Expression expression) {
    return formulaFromExpression(expression, new HashMap<>());
  }
  
  /**
   * Creates a new derived map in which the given key/value pair is replaced, without
   * altering the original map.
   * 
   * @param gamma the old gamma, which stays untouched.
   * @param key the key for which to replace the value (or insert)
   * @param value the value to insert
   * @return a new map with the updated mappings
   */
  private Map<String, String> replace(Map<String, String> gamma, String key, String value) {
    var gammaPrime = new HashMap<>(gamma);
    gammaPrime.put(key, value);
    return gammaPrime;
  }

  private Condition.NumericCondition numericConditionFromExpression(Expression expression, Map<String, String> gamma) {
    double numericValue = 0.0;
    switch (expression) {
      
      case Smaller rel:
        switch(rel.getRight()) {
          case IntConstant intVal: numericValue = intVal.getValue(); break;
          case RealConstant realVal: numericValue = realVal.getValue(); break;
          default: throw exceptionForObject(expression);
        }

        if (rel.getLeft() instanceof UnitaryMinus minRel
                && minRel.getExpression() instanceof Reference ref){//minus in front of name, switch relation and value
          numericValue *= -1.0;
          String name = resolveReference(ref, gamma);
          return Condition.geq(name, numericValue);
        }
        else if (rel.getLeft() instanceof Reference ref){//no minus, keep relation and value
          String name = resolveReference(ref, gamma);
          return Condition.lt(name, numericValue);
        }
        else
          throw exceptionForObject(expression);
        
      case SmallerOrEqualImpl rel:
        if (rel.getRight() instanceof IntConstant intVal)
          numericValue = intVal.getValue();
        else if (rel.getRight() instanceof RealConstant realVal)
          numericValue = realVal.getValue();
        else
          throw exceptionForObject(expression);

        if (rel.getLeft() instanceof UnitaryMinus minRel2
                && minRel2.getExpression() instanceof Reference ref2){//minus in front of name, switch relation and value
          numericValue *= -1.0;
          String name = resolveReference(ref2, gamma);
          return Condition.gt(name, numericValue);
        }
        else if (rel.getLeft() instanceof Reference ref3){//no minus, keep relation and value
          String name = resolveReference(ref3, gamma);
          return Condition.leq(name, numericValue);
        }
        else
          throw exceptionForObject(expression);
        
      case Greater rel:
        if (rel.getRight() instanceof IntConstantImpl intVal)
          numericValue = intVal.getValue();
        else if(rel.getRight() instanceof RealConstant realVal2)
          numericValue = realVal2.getValue();
        else
          throw exceptionForObject(expression);

        if (rel.getLeft() instanceof UnitaryMinusImpl minRel3
                && minRel3.getExpression() instanceof Reference ref4){//minus in front of name, switch relation and value
          numericValue *= -1.0;
          String name = resolveReference(ref4, gamma);
          return Condition.leq(name, numericValue);
        }
        else if(rel.getLeft() instanceof Reference ref5){//no minus, keep relation and value
          String name = resolveReference(ref5, gamma);
          return Condition.gt(name, numericValue);
        }
        else
          throw exceptionForObject(expression);
        
      case GreaterOrEqual rel:
        if(rel.getRight() instanceof IntConstant intVal)
          numericValue = intVal.getValue();
        else if(rel.getRight() instanceof RealConstant realVal3)
          numericValue = realVal3.getValue();
        else
          throw exceptionForObject(expression);

        if(rel.getLeft() instanceof UnitaryMinus minRel4
                && minRel4.getExpression() instanceof ReferenceImpl ref6){//minus in front of name, switch relation and value
          numericValue *= -1.0;
          String name = resolveReference(ref6, gamma);
          return Condition.geq(name, numericValue);
        }
        else if(rel.getLeft() instanceof Reference ref6){//no minus, keep relation and value
          String name = resolveReference(ref6, gamma);
          return Condition.gt(name, numericValue);
        }
        else
          throw exceptionForObject(expression);
      default:
        throw exceptionForObject(expression);
    }
  }
  
  public static String stringFromExpression(Expression expression) {
    return NodeModelUtils.findActualNodeFor(expression).getText().trim();
  }

  /**
   * Converts a given constraint expression into a LogicNG formula.
   * Private because if NumericConditions exist, the generated formula is not useful
   * or even misleading on its own!
   * 
   * @param expression The (sub-) expression to convert
   * @param gamma The current context, mapping argument names to actual entities.
   * @return a formula representing the expression
   */
  private Formula formulaFromExpression(Expression expression, Map<String, String> gamma) {
    switch (expression) {
      // this is the only expression that adds to the gamma
      case QuantifiedExpression quantor:
        var entities = resolveQueryExpression(quantor.getQuery(), gamma);
        BinaryOperator<Formula> operator = quantor.getQuantifier().equals(Quantifier.ALL) 
            ? factory::and 
            : factory::or;
        
        return entities.stream()
          .map(entity -> formulaFromExpression(quantor.getExpression(), 
              replace(gamma, quantor.getArgument().getName(), entity)))
          .reduce(operator)
          // forall empty => true, exists empty => false
          .orElse(factory.constant(quantor.getQuantifier().equals(Quantifier.ALL)));
      
      case LogicalOr or:
        return factory.or(
          formulaFromExpression(or.getLeft(), gamma),
          formulaFromExpression(or.getRight(), gamma)
      );
      case LogicalAnd and:
        return factory.and(
          formulaFromExpression(and.getLeft(), gamma),
          formulaFromExpression(and.getRight(), gamma)
      );
      
      case UnitaryNot negation:
        return factory.not(formulaFromExpression(negation.getExpression(), gamma));
      case Reference reference:
        return factory.literal(resolveReference(reference, gamma), true);
        
      case Smaller rel: {
        String name = stringFromExpression(rel);
        if(!placeholders.containsKey(name))
          placeholders.put(name, numericConditionFromExpression(rel, gamma));
        return factory.literal(name, true);
      }
      case SmallerOrEqual rel:{
        String name = stringFromExpression(rel);
        if(!placeholders.containsKey(name))
          placeholders.put(name, numericConditionFromExpression(rel, gamma));
        return factory.literal(name, true);
      }
      case Greater rel:{
        String name = stringFromExpression(rel);
        if(!placeholders.containsKey(name))
          placeholders.put(name, numericConditionFromExpression(rel, gamma));
        return factory.literal(name, true);
      }
      case GreaterOrEqual rel:{
        String name = stringFromExpression(rel);
        if(!placeholders.containsKey(name))
          placeholders.put(name, numericConditionFromExpression(rel, gamma));
        return factory.literal(name, true);
      }
      default:
        throw exceptionForObject(expression);
    }
  }

  private static IllegalArgumentException exceptionForObject(Object object) {
    return new IllegalArgumentException("Unexpected value: ["
    + object.getClass().getSimpleName() + "] " + object);
  }
  
  /**
   * Returns the alias map for the given constraint document ina format ready for LogicNgConverter
   * @param document
   * @return
   */
  public static Map<String, String> extractAliases(ConstraintDocument document) {
    return document.getReadings().stream().collect(Collectors.toMap(Reading::getName,
        reading -> reading.getType() + "." + reading.getIdentifier()));
  }
}
