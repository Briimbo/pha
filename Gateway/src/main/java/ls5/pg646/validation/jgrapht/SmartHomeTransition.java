package ls5.pg646.validation.jgrapht;

import static ls5.pg646.validation.Utils.join;

import java.util.List;
import ls5.pg646.validation.math.Condition;

public record SmartHomeTransition(Type type,
                                  SmartHomeState source, SmartHomeState target,
                                  List<Condition> guard, String label) {

  public String toLabel() {
    return this.label + (!guard.isEmpty() ? " (" + join(",", guard) + ")" : "");
  }

  public enum Type {
    AUTO("blue"), MAN("orange"), ENV("black");

    String color;

    Type(String color) {
      this.color = color;
    }

    public String color() {
      return color;
    }
  }
}