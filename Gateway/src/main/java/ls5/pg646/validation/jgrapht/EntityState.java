package ls5.pg646.validation.jgrapht;

import ls5.pg646.validation.math.Interval;
import ls5.pg646.validation.symbolic.Literal;

public sealed interface EntityState extends Literal {

  record BinaryState(String name, boolean value) implements EntityState {

    @Override
    public String toString() {
      return (!value ? "!" : "") + name;
    }
  }

  record NumericState(String name, Interval value) implements EntityState {

    @Override
    public String toString() {
      return "%s ∈ %s".formatted(name, value);
    }
  }
}