package ls5.pg646.validation.jgrapht;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Stream.concat;
import static org.jgrapht.nio.DefaultAttribute.createAttribute;

import com.google.common.collect.Sets;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.parse.Parser;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import ls5.pg646.validation.jgrapht.EntityState.BinaryState;
import ls5.pg646.validation.jgrapht.EntityState.NumericState;
import ls5.pg646.validation.jgrapht.SmartHomeTransition.Type;
import ls5.pg646.validation.math.Condition;
import ls5.pg646.validation.math.Condition.BinaryCondition;
import ls5.pg646.validation.math.Condition.NumericCondition;
import ls5.pg646.validation.math.Interval;
import ls5.pg646.validation.math.MathUtil;
import ls5.pg646.validation.symbolic.Automation;
import ls5.pg646.validation.symbolic.Constraint;
import ls5.pg646.validation.symbolic.Oracle;
import ls5.pg646.validation.symbolic.SmartHome;
import ls5.pg646.validation.symbolic.Trigger.StateTrigger;
import org.jgrapht.Graph;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.jgrapht.nio.dot.DOTExporter;

public class AutomatonBuilder {

  /**
   * The symbolic smarthome, containing the data as given by the gateway.
   */
  final SmartHome home;
  /**
   * The oracle, predicting which sensors are environment. This will also be given by the gateway.
   */
  final Oracle oracle;
  /**
   * The constraint for which the automaton is to be build.
   */
  final Constraint constraint;
  /**
   * Computed relevant domains of numeric sensors.
   *
   * <p>The counterpart for binary sensors is {@link AutomatonBuilder#binaryDomains}</p>
   */
  final Map<String, List<Interval>> numericDomains;
  /**
   * Computed list of relevant binary sensors.
   *
   * <p>The counterpart for numeric sensors is {@link AutomatonBuilder#numericDomains}.</p>
   */
  final List<String> binaryDomains;
  /**
   * <p>Computed list of relevant automations for the given constraint in the given smart home.</p>
   */
  final List<Automation> automations;

  /**
   * Constructs a new AutomatonBuilder for the given SmartHome, using the given oracle for
   * environment predictions, and constrained to the relevant states and transitions needed to check
   * the given constraint.
   *
   * @param home       The SmartHome to model, as given by the Gateway
   * @param oracle     The Oracle predicting environment transitions, as given by the Gateway
   * @param constraint The Constraint to generate the automaton for, as given by the User
   */
  public AutomatonBuilder(SmartHome home, Oracle oracle, Constraint constraint) {
    this.home = home;
    this.oracle = oracle;
    this.constraint = constraint;

    this.automations = home.findInterferingAutomations(constraint);
    var relevantSensors = home.findRelevantSensors(constraint);

    // conditions from automations
    var automationConditions = automations.stream()
        .flatMap(automation -> concat(
            automation.conditions().stream(),
            Stream.of()))
        .filter(condition -> relevantSensors.contains(condition.name()))
        .toList();

    // conditions from automation triggers
    var triggerConditions = automations.stream()
        .map(Automation::trigger)
        .filter(StateTrigger.class::isInstance)
        .map(StateTrigger.class::cast)
        .map(StateTrigger::conditions)
        .flatMap(Collection::stream)
        .toList();

    // conditions from constraint
    var constraintConditions = constraint.constraints().stream()
        .flatMap(List::stream)
        .toList();

    var allConditions = Stream.of(automationConditions, triggerConditions, constraintConditions)
        .flatMap(Collection::stream)
        .collect(groupingBy(Condition::getClass));

    var binary = allConditions.getOrDefault(BinaryCondition.class, List.of());
    var numeric = allConditions.getOrDefault(NumericCondition.class, List.of())
        .stream().map(NumericCondition.class::cast).toList();

    this.binaryDomains = binary.stream()
        .map(Condition::name)
        .distinct()
        .toList();

    this.numericDomains = MathUtil.computeIntervals(numeric);
  }

  /**
   * Return neighboring intervals.
   */
  protected List<Interval> getNeighbours(String sensor, Interval current) {
    var domain = numericDomains.get(sensor);
    if (domain.size() <= 1) {
      return List.of();
    }

    int indexOfCurrent = domain.indexOf(current);
    if (indexOfCurrent == 0) {
      return List.of(domain.get(1));
    } else if (indexOfCurrent == domain.size() - 1) {
      return List.of(domain.get(indexOfCurrent - 1));
    } else {
      return List.of(domain.get(indexOfCurrent - 1), domain.get(indexOfCurrent + 1));
    }
  }

  public List<Interval> getAllDomains(String sensor) {
    return numericDomains.get(sensor);
  }

  public List<SmartHomeState> generateRelevantStates() {
    return Sets.cartesianProduct(
            concat(
                numericDomains.entrySet().stream()
                    .map(e -> e.getValue().stream()
                        .map(i -> new NumericState(e.getKey(), i))
                        .collect(toSet())),
                binaryDomains.stream()
                    .map(n -> Set.of(
                        new BinaryState(n, false), new BinaryState(n, true)))
            ).toList())
        .stream()
        .map(s -> new SmartHomeState(Set.copyOf(s)))
        .toList();
  }

  protected Stream<SmartHomeTransition> generateManualTransitions(SmartHomeState current) {
    return current.getEntityStates().stream()
        .filter(s -> !oracle.isEnvironment(s.name()))
        .flatMap(
            env -> switch (env) {
              case BinaryState state -> Stream.of(new SmartHomeTransition(Type.MAN,
                  current,
                  new SmartHomeState(current.getEntityStates().stream()
                      .map(s -> s.name().equals(env.name())
                          ? new BinaryState(s.name(), !state.value()) : s)
                      .collect(toSet())), List.of(), state.name()));
              case NumericState state -> getNeighbours(state.name(), state.value()).stream()
                  .map(neigh -> new SmartHomeTransition(Type.MAN, current, new SmartHomeState(
                      current.getEntityStates().stream()
                          .map(s -> s.name().equals(env.name())
                              ? new NumericState(s.name(), neigh)
                              : s)
                          .collect(toSet())), List.of(), state.name()));
            });
  }

  protected Stream<SmartHomeTransition> generateEnvironmentTransitions(SmartHomeState current) {
    return current.getEntityStates().stream()
        .filter(s -> oracle.isEnvironment(s.name()))
        .flatMap(
            env -> switch (env) {
              case BinaryState state -> Stream.of(new SmartHomeTransition(
                  Type.ENV, current,
                  new SmartHomeState(current.getEntityStates().stream()
                      .map(s -> s.name().equals(env.name())
                          ? new BinaryState(s.name(), !state.value()) : s)
                      .collect(toSet())), List.of(), state.name()));
              case NumericState state -> getNeighbours(state.name(), state.value()).stream()
                  .map(neigh -> new SmartHomeTransition(Type.ENV, current, new SmartHomeState(
                      current.getEntityStates().stream()
                          .map(s -> s.name().equals(env.name())
                              ? new NumericState(s.name(), neigh)
                              : s)
                          .collect(toSet())), List.of(), state.name()));
            });
  }

  /**
   * Returns the resulting state of the smart home after applying the given automation to the given
   * state.
   *
   * @param current    The state the automation is triggered in
   * @param automation The automation to apply
   * @return The resulting state of the smart home after applying the automation
   */
  protected SmartHomeState applyAutomation(SmartHomeState current, Automation automation) {
    var newBinaryStates = automation.actions().stream()
        .filter(BinaryCondition.class::isInstance)
        .map(BinaryCondition.class::cast)
        .map(condition -> new BinaryState(condition.name(), condition.value()));

    var newNumericStates = automation.actions().stream()
        .filter(NumericCondition.class::isInstance)
        .map(NumericCondition.class::cast)
        // There will always be only exactly one matching domain.
        // So instead of using map/filter/findFirst and then having an Optional<NumericState>, we
        // can just flatMap because we know that the flatMapped Stream will have exactly one state.
        .flatMap(action -> this.numericDomains.get(action.name()).stream()
            .filter(domain -> domain.matches(action))
            .map(domain -> new NumericState(action.name(), domain))
        );

    var newStates = Stream.concat(newBinaryStates, newNumericStates);

    var statusQuo = current.getEntityStates().stream()
        .collect(Collectors.toMap(EntityState::name, identity()));
    // change status quo to target states
    newStates.forEach(state -> statusQuo.put(state.name(), state));

    var succeedingState = statusQuo.values().stream().collect(toSet());
    return new SmartHomeState(succeedingState);
  }

  protected Stream<SmartHomeTransition> generateAutomationTransitions(SmartHomeState current) {
    return this.automations.stream()
        .filter(automation -> automation.executesIn(current))
        .map(automation -> new SmartHomeTransition(Type.AUTO, current,
            applyAutomation(current, automation), triggerOf(automation), automation.name()))
        // disallow self-loop for now, as they are not supported at this point
        .filter(transition -> !transition.source()
            .equals(transition.target()));
  }

  private List<Condition> triggerOf(Automation automation) {
    if (automation.trigger() instanceof StateTrigger trigger) {
      return trigger.conditions();
    }
    return List.of();
  }

  /**
   * Generates all transitions for the given state, including all transitions for automations,
   * manual transitions and environment effects.
   *
   * @param current The currently evaluated state
   * @return The transitions possible in this state (as stream)
   */
  protected Stream<SmartHomeTransition> generateTransitions(SmartHomeState current) {

    return Stream.of(
            generateAutomationTransitions(current),
            generateManualTransitions(current),
            generateEnvironmentTransitions(current))
        .reduce(Stream::concat)
        .orElse(Stream.empty());
  }

  public Graph<SmartHomeState, SmartHomeTransition> buildGraph() {
    Graph<SmartHomeState, SmartHomeTransition> graph =
        new SimpleDirectedGraph<>(SmartHomeTransition.class);

    var relevantStates = generateRelevantStates();

    relevantStates.stream().forEach(graph::addVertex);

    relevantStates.stream()
        .flatMap(this::generateTransitions)
        .forEach(t -> graph.addEdge(t.source(), t.target(), t));

    return graph;
  }

  public DOTExporter<SmartHomeState, SmartHomeTransition> createDotExporter() {
    DOTExporter<SmartHomeState, SmartHomeTransition> exporter =
        new DOTExporter<>(SmartHomeState::toDotId);
    exporter.setEdgeIdProvider(SmartHomeTransition::toString);
    exporter.setEdgeAttributeProvider(
        e -> Map.of(
            "label", createAttribute(e.toLabel()),
            "color", createAttribute(e.type().color())
        ));
    exporter.setVertexAttributeProvider(
        v -> Map.of(
            "label", createAttribute(v.toLabel()),
            "color", createAttribute(v.isAcceptingState(constraint) ? "black" : "red")
        ));
    return exporter;
  }

  public void saveGraphToPng(String filePath) throws IOException {
    var exporter = createDotExporter();

    var writer = new StringWriter();
    exporter.exportGraph(buildGraph(), writer);

    MutableGraph g = new Parser().read(writer.toString());
    Graphviz.fromGraph(g).width(600).render(Format.PNG).toFile(new File(filePath));
  }

  public void saveGraphToSvg(String filePath) throws IOException {
    var exporter = createDotExporter();

    var writer = new StringWriter();
    exporter.exportGraph(buildGraph(), writer);

    MutableGraph g = new Parser().read(writer.toString());
    Graphviz.fromGraph(g).width(600).render(Format.SVG).toFile(new File(filePath));
  }
}