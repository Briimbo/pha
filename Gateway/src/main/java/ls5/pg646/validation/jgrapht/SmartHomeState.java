package ls5.pg646.validation.jgrapht;

import static java.util.stream.Collectors.joining;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import ls5.pg646.validation.jgrapht.EntityState.BinaryState;
import ls5.pg646.validation.jgrapht.EntityState.NumericState;
import ls5.pg646.validation.symbolic.Constraint;

/**
 * This class represents a state of the SmartHome. It is a collection of entity states.
 */
public class SmartHomeState {

  private final Map<String, EntityState> entityIdToState;

  public SmartHomeState(Set<EntityState> stateSet) {
    this.entityIdToState =
        stateSet.stream().collect(Collectors.toMap(EntityState::name, Function.identity()));
  }

  public boolean contains(String entityId) {
    return this.entityIdToState.containsKey(entityId);
  }

  public EntityState getState(String entityId) {
    if (entityIdToState.containsKey(entityId)) {
      return entityIdToState.get(entityId);
    } else {
      throw new IllegalArgumentException(
          "'%s' is not contained in this smart home state".formatted(entityId));
    }
  }

  public Collection<EntityState> getEntityStates() {
    return Collections.unmodifiableCollection(entityIdToState.values());
  }

  @Override
  public String toString() {
    return "%s".formatted(entityIdToState.values().stream()
        .map(EntityState::toString)
        .collect(joining(", "))
    );
  }

  /**
   * Generate node label. Uses line breaks to make the label easier readable.
   */
  public String toLabel() {
    return entityIdToState.values().stream().map(EntityState::toString).collect(joining("\n"));
  }

  public String toMessage() {
    return entityIdToState.values().stream().map(e -> switch (e) {
      case BinaryState b -> b.name() + " is " + (b.value() ? "ON" : "OFF");
      case NumericState n -> (
          n.name() + " is between " + n.value().left() + " and " + n.value().right()).replace(
          "Infinity", "∞");
    }).collect(joining("; "));
  }

  /**
   * Returns the unique ID for this vertex, compatible with the DOT file format.
   *
   * <p>The important parts are:
   * No line breaks, and the whole string is wrapped in double quotes.</p>
   */
  public String toDotId() {
    return "\"" + this + "\"";
  }

  /**
   * Checks whether or not this state fulfills the given constraint (i.e. is accepting).
   *
   * @param constraint The constraint to evaluate for acceptance
   * @return true iff this state does not violate the given constraint
   */
  public boolean isAcceptingState(Constraint constraint) {
    return constraint.constraints().stream()
        .anyMatch(clause -> clause.stream()
            .allMatch(condition -> condition.holdsIn(entityIdToState.get(condition.name()))));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SmartHomeState aaVertex = (SmartHomeState) o;
    return
        Objects.equals(entityIdToState.keySet(), aaVertex.entityIdToState.keySet())
            && Objects.equals(entityIdToState.values().stream().collect(Collectors.toSet()),
            aaVertex.entityIdToState.values().stream().collect(Collectors.toSet()));
  }

  @Override
  public int hashCode() {
    return Objects.hash(entityIdToState.values().stream().collect(Collectors.toSet()));
  }
}
