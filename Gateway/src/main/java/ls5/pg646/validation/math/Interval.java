package ls5.pg646.validation.math;

import ls5.pg646.validation.math.Condition.NumericCondition;

public record Interval(double left, boolean leftInclusive, double right, boolean rightInclusive) {

  public Interval {
    if (left > right) {
      throw new IllegalArgumentException("Left must be smaller then right");
    }
    if (Double.isNaN(left) || Double.isNaN(right)) {
      throw new IllegalArgumentException("Neither value may be NaN");
    }
    if ((left == right) && (!leftInclusive || !rightInclusive)) {
      throw new IllegalArgumentException(
          "Singleton intervals cannot both include and exclude the value");
    }
  }

  public boolean isInside(double x) {
    return
        (left < x || (leftInclusive && left <= x))
            && (right > x || (rightInclusive && right >= x));
  }

  @Override
  public String toString() {
    return "%s%s, %s%s".formatted(
        leftInclusive ? "[" : "(",
        left,
        right,
        rightInclusive ? "]" : ")").replace("Infinity", "∞");
  }

  public boolean matches(Relation relation, double value) {
    return switch (relation) {
      case EQUALS -> isInside(value);

      case LESSER -> right < value
          || (!rightInclusive && right <= value);

      case LESS_OR_EQUAL -> right <= value;

      case GREATER -> left > value
          || (!leftInclusive && left >= value);

      case GREATER_OR_EQUAL -> left >= value;
    };
  }

  public boolean matches(NumericCondition condition) {
    return this.matches(condition.relation(), condition.value());
  }
}
