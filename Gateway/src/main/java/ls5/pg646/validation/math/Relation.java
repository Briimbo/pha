package ls5.pg646.validation.math;

public enum Relation {
  // DO NOT REORDER - ORDER IS IMPORTANT!
  EQUALS("=="),
  LESSER("<"),
  GREATER(">"),
  LESS_OR_EQUAL("<="),
  GREATER_OR_EQUAL(">=");

  private final String operator;

  Relation(String operator) {
    this.operator = operator;
  }

  public String operator() {
    return operator;
  }

  @Override
  public String toString() {
    return operator;
  }
}