package ls5.pg646.validation.math;

import java.util.Arrays;
import java.util.List;
import ls5.pg646.validation.jgrapht.EntityState;
import ls5.pg646.validation.jgrapht.EntityState.BinaryState;
import ls5.pg646.validation.jgrapht.EntityState.NumericState;
import ls5.pg646.validation.jgrapht.SmartHomeState;
import ls5.pg646.validation.symbolic.Literal;

public sealed interface Condition extends Literal {

  static NumericCondition is(String name, Relation relation, Double value) {
    return new NumericCondition(name, relation, value);
  }

  static BinaryCondition is(String name, boolean value) {
    return new BinaryCondition(name, value);
  }

  static List<Condition> all(Condition... conditions) {
    return Arrays.asList(conditions);
  }

  static List<Condition> none() {
    return List.of();
  }

  static NumericCondition eq(String name, double value) {
    return new NumericCondition(name, Relation.EQUALS, value);
  }

  static NumericCondition lt(String name, double value) {
    return new NumericCondition(name, Relation.LESSER, value);
  }

  static NumericCondition gt(String name, double value) {
    return new NumericCondition(name, Relation.GREATER, value);
  }

  static NumericCondition leq(String name, double value) {
    return new NumericCondition(name, Relation.LESS_OR_EQUAL, value);
  }

  static NumericCondition geq(String name, double value) {
    return new NumericCondition(name, Relation.GREATER_OR_EQUAL, value);
  }

  boolean holdsIn(EntityState state);

  default boolean holdsIn(SmartHomeState state) {
    return state.getEntityStates().stream()
        .filter(entity -> entity.name().equals(this.name()))
        .anyMatch(this::holdsIn);
  }

  String toMessage();

  record BinaryCondition(String name, boolean value) implements Condition {

    @Override
    public String toString() {
      return (!value ? "!" : "") + name;
    }

    @Override
    public boolean holdsIn(EntityState state) {
      return (state instanceof BinaryState binary) && binary.value() == value;
    }

    public String toMessage() {
      return name + " turns " + (value ? "ON" : "OFF");
    }
  }

  record NumericCondition(String name, Relation relation, Double value)
      implements Condition {

    @Override
    public String toString() {
      return "%s %s %s".formatted(name, relation, value);
    }

    @Override
    public boolean holdsIn(EntityState state) {
      return (state instanceof NumericState numeric) && numeric.value().matches(this);
    }

    public String toMessage() {
      return switch (relation) {
        case EQUALS -> name + " becomes " + value;
        case GREATER, GREATER_OR_EQUAL -> name + " exceeds " + value;
        case LESSER, LESS_OR_EQUAL -> name + " falls below " + value;
      };
    }
  }
}
