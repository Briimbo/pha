package ls5.pg646.validation.math;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import ls5.pg646.validation.math.Condition.NumericCondition;

@Slf4j
public final class MathUtil {

  private MathUtil() {
  }

  public static Map<String, List<Interval>> computeIntervals(
      List<NumericCondition> conditions) {
    var comparisonsByDomain = conditions.stream()
        .collect(Collectors.groupingBy(Condition::name));
    var intervalsByDomain = new HashMap<String, List<Interval>>();
    double lastValue = Double.NEGATIVE_INFINITY;
    boolean lastInclusive = true;
    for (var entry : comparisonsByDomain.entrySet()) {
      var intervals = new ArrayList<Interval>();
      var entries = entry.getValue();
      entries.sort((lhs, rhs) -> lhs.relation().compareTo(rhs.relation()));
      entries.sort((lhs, rhs) -> lhs.value().compareTo(rhs.value()));

      Interval last = null;
      Interval next = null;
      for (var comparison : entries) {
        switch (comparison.relation()) {
          case GREATER_OR_EQUAL -> {
            if (last != null && last.rightInclusive() && last.right() == comparison.value()) {
              intervals.remove(intervals.size() - 1);
              if (last.left() < last.right()) {
                var correctedInterval = new Interval(last.left(), last.leftInclusive(),
                    last.right(), false);
                intervals.add(correctedInterval);
                last = correctedInterval;
              }
              next = new Interval(lastValue, true, lastValue, true);
              lastInclusive = true;
            } else {
              if (lastValue < comparison.value()) {
                next = new Interval(lastValue, !lastInclusive, comparison.value(), false);
                lastInclusive = false;
              }
            }
          }
          case LESSER -> {
            if (lastValue < comparison.value()) {
              next = new Interval(lastValue, !lastInclusive, comparison.value(), false);
              lastInclusive = false;
            }
          }
          case LESS_OR_EQUAL, GREATER -> {
            if (lastValue < comparison.value() || !lastInclusive) {
              next = new Interval(lastValue, !lastInclusive, comparison.value(), true);
              lastInclusive = true;
            }
          }
          case EQUALS -> {
            if (last != null && last.rightInclusive() && last.right() == comparison.value()) {
              intervals.remove(intervals.size() - 1);
              if (last.left() < last.right()) {
                var correctedInterval = new Interval(last.left(), last.leftInclusive(),
                    last.right(), false);
                intervals.add(correctedInterval);
                last = correctedInterval;
                lastInclusive = false;
              } else if (!intervals.isEmpty()) {
                lastInclusive = intervals.get(intervals.size() - 1).rightInclusive();
              }
            }
            if (lastValue < comparison.value()) {
              next = new Interval(lastValue, !lastInclusive, comparison.value(), false);
              if (!next.equals(last)) {
                intervals.add(next);
                last = next;
              }
            }
            next = new Interval(comparison.value(), true, comparison.value(), true);
            lastInclusive = true;
          }
          default -> log.debug("Sonarqube is happy now");
        }
        if (next != null && !next.equals(last)) {
          intervals.add(next);
          last = next;
          lastValue = comparison.value();
        }
      }
      intervals.add(new Interval(lastValue, !lastInclusive, Double.POSITIVE_INFINITY, false));
      intervalsByDomain.put(entry.getKey(), intervals.stream().distinct().toList());
    }

    return intervalsByDomain;
  }
}
