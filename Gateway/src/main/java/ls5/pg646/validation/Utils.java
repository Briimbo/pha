package ls5.pg646.validation;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public final class Utils {

  private Utils() {
    throw new IllegalStateException("Do not instantiate");
  }

  public static <T> String join(String delimiter, Collection<T> collection) {
    return collection.stream()
        .map(Object::toString)
        .collect(Collectors.joining(delimiter));
  }
  
  public static <T> Stream<T> stream(Iterable<T> iterable) {
    return StreamSupport.stream(iterable.spliterator(), false);
  }
}
