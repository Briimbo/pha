package ls5.pg646.validation.state;

import ls5.pg646.validation.symbolic.Literal;

/**
 * Runtime entity state. Contains the entity id as {@code name} and the current {@code value}.
 */
public sealed interface RuntimeEntityState extends Literal {
  
  public Object value();

  public record BinaryState(String name, Boolean value) implements RuntimeEntityState { }
  
  public record NumericState(String name, Double value) implements RuntimeEntityState { }
}
