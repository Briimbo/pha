package ls5.pg646.adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import ls5.pg646.validation.math.Condition.BinaryCondition;
import ls5.pg646.validation.math.Condition.NumericCondition;
import ls5.pg646.validation.symbolic.Automation;
import ls5.pg646.validation.symbolic.SmartHome;
import ls5.pg646.validation.symbolic.Trigger;
import ls5.pg646.adapter.ConditionAdapter.ConditionActionPair;
import ls5.pg646.parsing.Action;
import ls5.pg646.validation.math.Condition;
import ls5.pg646.validation.math.Relation;

final class ConditionAdapterTest {

  private ls5.pg646.parsing.Condition stateCond(String entityName, String stateName) {
    var cond = new ls5.pg646.parsing.Condition();
    cond.setConditions(Lists.emptyList());
    cond.setConditionType("state");
    cond.setUnknown("entity_id", entityName);
    cond.setUnknown("state", stateName);
    return cond;
  }

  private ls5.pg646.parsing.Condition numStateCond(String entityName, String attribute, Integer above, Integer below) {
    var cond = new ls5.pg646.parsing.Condition();
    cond.setConditions(Lists.emptyList());
    cond.setConditionType("numeric_state");
    cond.setUnknown("entity_id", entityName);
    if (attribute != null) {
      cond.setUnknown("attribute", attribute);
    }
    if (above != null) {
      cond.setUnknown("above", above);
    }
    if (below != null) {
      cond.setUnknown("below", below);
    }
    return cond;
  }

  private ls5.pg646.parsing.Condition orCond(ls5.pg646.parsing.Condition... conds) {
    var cond = new ls5.pg646.parsing.Condition();
    cond.setConditionType("or");
    cond.setConditions(Arrays.asList(conds));
    return cond;
  }

  @Test
  void cartesianProductTest() {
    var sets = List.of(List.of(1, 2), List.of(3, 4));
    var cart = ConditionAdapter.cartesianProduct(sets);

    Assertions.assertEquals(List.of(List.of(1, 3), List.of(1, 4), List.of(2, 3), List.of(2, 4)), cart,
        "Cartesian Product incorrect");
  }

  @Test
  void cleanPreconditionsTest() {
    var cond1 = new BinaryCondition("a", true);
    var ncond1 = new BinaryCondition("a", false);
    var cond2 = new BinaryCondition("b", false);
    var ncond2 = new BinaryCondition("b", true);
    var cond3 = new NumericCondition("c", Relation.GREATER, 10.0);
    var ncond3 = new NumericCondition("c", Relation.LESS_OR_EQUAL, 10.0);
    var cond4 = new NumericCondition("d", Relation.GREATER_OR_EQUAL, 5.0);
    var ncond4 = new NumericCondition("d", Relation.LESSER, 5.0);

    {
      final List<Condition> control = List.of(cond1, cond2, cond3, cond4);
      final var result = new ArrayList<Condition>();
      result.addAll(control);
      Assertions.assertTrue(
          ConditionAdapter.cleanPreconditions(result),
          "Satisfiable set of conditions deemed unsatisfiable: a & !b & c>10 & d>=10");
      Assertions.assertEquals(control, result, "Satisfiable set modified, despite no need for it");
    }
    {
      final List<Condition> control = List.of(ncond1, cond2, ncond3, cond4);
      final var result = new ArrayList<Condition>();
      result.addAll(control);
      Assertions.assertTrue(
          ConditionAdapter.cleanPreconditions(result),
          "Satisfiable set of conditions deemed unsatisfiable: !a & !b & c<=10 & d>=10");
      Assertions.assertEquals(control, result, "Satisfiable set modified, despite no need for it");
    }
    {
      final List<Condition> control = List.of(ncond1, ncond2, ncond2, ncond3, ncond3, ncond4);
      final var result = new ArrayList<Condition>();
      result.addAll(control);
      Assertions.assertTrue(
          ConditionAdapter.cleanPreconditions(result),
          "Satisfiable set of conditions deemed unsatisfiable: !a & b & b & c<=10 & d<10 & d<10");
      final List<Condition> expected = List.of(ncond1, ncond2, ncond3, ncond4);
      Assertions.assertEquals(expected, result, "Duplicates not removed from set of clauses");
    }

    {
      final List<Condition> control = List.of(ncond1, cond1);
      final var result = new ArrayList<Condition>();
      result.addAll(control);
      Assertions.assertFalse(
          ConditionAdapter.cleanPreconditions(result),
          "Unsatisfiable set of conditions deemed satisfiable: !a & a");
    }
    {
      final List<Condition> control = List.of(ncond1, cond2, ncond3, ncond2, cond4);
      final var result = new ArrayList<Condition>();
      result.addAll(control);
      Assertions.assertFalse(
          ConditionAdapter.cleanPreconditions(result),
          "Unsatisfiable set of conditions deemed satisfiable: !a & !b & c<=10 & b & d>=5");
    }
  }

  // Java ist dumm
  private Condition binCondFactory(String name, boolean value) {
    return new BinaryCondition(name, value);
  }

  private Condition numCondFactory(String name, Relation relation, Double value) {
    return new NumericCondition(name, relation, value);
  }

  @Test
  void mergeActionsTest() {
    var cond1 = new BinaryCondition("a", true);
    var ncond1 = new BinaryCondition("a", false);
    var cond2 = new BinaryCondition("b", false);
    var ncond2 = new BinaryCondition("b", true);
    var cond3 = new NumericCondition("c", Relation.GREATER, 10.0);
    var ncond3 = new NumericCondition("c", Relation.LESS_OR_EQUAL, 10.0);
    var cond4 = new NumericCondition("d", Relation.GREATER_OR_EQUAL, 5.0);
    var ncond4 = new NumericCondition("d", Relation.LESSER, 5.0);

    {
      var output = new ArrayList<ConditionActionPair>();
      List<Condition> inCond = new ArrayList<>(List.of(cond1, cond2));
      var actions = List.of(new ConditionAdapter.ConditionActionPair(List.of(ncond1), List.of(cond3, cond4)),
          new ConditionAdapter.ConditionActionPair(List.of(cond3), List.of(cond1, ncond2)),
          new ConditionAdapter.ConditionActionPair(List.of(ncond2), List.of(ncond3, ncond4)),
          new ConditionAdapter.ConditionActionPair(List.of(ncond4), List.of(cond2, cond3)));

      ConditionAdapter.mergeActions(inCond, actions, output);

      Assertions.assertEquals(List.of(), output,
          "Contradictory input conditions should not have produced an action. The list should be empty.");
    }

    {
      var output = new ArrayList<ConditionActionPair>();
      List<Condition> inCond = new ArrayList<>(List.of(cond1, cond2));
      var actions = List.of(new ConditionAdapter.ConditionActionPair(List.of(cond1), List.of(cond3, cond4)),
          new ConditionAdapter.ConditionActionPair(List.of(ncond3), List.of(cond1, ncond2)),
          new ConditionAdapter.ConditionActionPair(List.of(cond2), List.of(ncond3, ncond4)),
          new ConditionAdapter.ConditionActionPair(List.of(ncond4), List.of(cond2, cond3)));

      ConditionAdapter.mergeActions(inCond, actions, output);

      Assertions.assertEquals(
          List.of(new ConditionActionPair(List.of(cond1, cond2, ncond3, ncond4), List.of(cond1, ncond4, cond2, cond3))),
          output,
          "The input is valid and therefore should have produced an action. Duplicate input conditions should have been removed and in the output the last-set-value of each variable should be the output for said variable.");
    }
  }

  @Test
  void parseToModelConditionTest() {
    var inConds = List.of(
        stateCond("a", "on"),
        numStateCond("b", null, 10, null),
        orCond(
            stateCond("c", "off"),
            numStateCond("d", null, null, 5)));

    // Transform to sets because order doesn't matter
    var modelConditions = ConditionAdapter.parseToModelCondition(inConds)
        .stream()
        .map(list -> list.stream().collect(Collectors.toSet()))
        .collect(Collectors.toSet());

    var expected = Set.of(
        Set.<Condition>of(
            binCondFactory("a", true),
            numCondFactory("b", Relation.GREATER, 10.0),
            binCondFactory("c", false)),
        Set.<Condition>of(
            binCondFactory("a", true),
            numCondFactory("b", Relation.GREATER, 10.0),
            numCondFactory("d", Relation.LESSER, 5.0)));

    Assertions.assertEquals(expected, modelConditions, "Conditions incorrectly transformed");
  }

  @Test
  void parseToModelActionsTest() {
    // Tested example from parseToModelConditionTest
    var conditions = List.of(
        List.<Condition>of(
            binCondFactory("a", true),
            numCondFactory("b", Relation.GREATER, 10.0),
            binCondFactory("c", false)),
        List.<Condition>of(
            binCondFactory("a", true),
            numCondFactory("b", Relation.GREATER, 10.0),
            numCondFactory("d", Relation.LESSER, 5.0)));

    var a1 = new Action();
    a1.setService("light.turn_on");
    a1.setDomain("light");
    a1.setTarget(Map.of("entity_id", "a"));
    var a2 = new Action();
    a2.setService("input_boolean.toggle");
    a2.setTarget(Map.of("entity_id", "c"));
    var actions = List.of(
        a1, a2);

    var result = ConditionAdapter.parseToModelActions(conditions, actions);

    var expected = List.of(
        new ConditionActionPair(List.of(
            binCondFactory("a", true),
            numCondFactory("b", Relation.GREATER, 10.0),
            binCondFactory("c", false)),
            List.of(
                binCondFactory("a", true),
                binCondFactory("c", true))),
        new ConditionActionPair(List.of(
            binCondFactory("a", true),
            numCondFactory("b", Relation.GREATER, 10.0),
            numCondFactory("d", Relation.LESSER, 5.0),
            binCondFactory("c", true)),
            List.of(
                binCondFactory("a", true),
                binCondFactory("c", false))),
        new ConditionActionPair(List.of(
            binCondFactory("a", true),
            numCondFactory("b", Relation.GREATER, 10.0),
            numCondFactory("d", Relation.LESSER, 5.0),
            binCondFactory("c", false)),
            List.of(
                binCondFactory("a", true),
                binCondFactory("c", true))));

    Assertions.assertEquals(expected.size(), result.size(), "Incorrect amount of resulting actions");

    for (int i = 0; i < expected.size(); i++) {
      Assertions.assertEquals(expected.get(i), result.get(i), "Action " + i + " incorrect ");
    }
  }

  @Test
  void parseToModelTriggerTest() {
    var contState = new ls5.pg646.parsing.Trigger();
    contState.setPlatform("state");
    contState.setEntityId("a");
    var toState = new ls5.pg646.parsing.Trigger();
    toState.setPlatform("state");
    toState.setEntityId("b");
    toState.setTo(List.of("on"));
    var numAbove = new ls5.pg646.parsing.Trigger();
    numAbove.setPlatform("numeric_state");
    numAbove.setEntityId("c");
    numAbove.setUnknown("above", 10.0);
    var numBelow = new ls5.pg646.parsing.Trigger();
    numBelow.setPlatform("numeric_state");
    numBelow.setEntityId("d");
    numBelow.setUnknown("below", 5.0);
    var numBoth = new ls5.pg646.parsing.Trigger();
    numBoth.setPlatform("numeric_state");
    numBoth.setEntityId("e");
    numBoth.setUnknown("above", 4.0);
    numBoth.setUnknown("below", 8.0);

    var result = ConditionAdapter.parseToModelTrigger(List.of(contState, toState, numAbove, numBelow, numBoth));
    var expected = List.of(
        new Trigger.ContinuousTrigger("a"),
        new Trigger.StateTrigger("b", List.of(
            new Condition.BinaryCondition("b", true))),
        new Trigger.StateTrigger("c", List.of(
            new Condition.NumericCondition("c", Relation.GREATER, 10.0))),
        new Trigger.StateTrigger("d", List.of(
            new Condition.NumericCondition("d", Relation.LESSER, 5.0))),
        new Trigger.StateTrigger("e", List.of(
            new Condition.NumericCondition("e", Relation.GREATER, 4.0),
            new Condition.NumericCondition("e", Relation.LESSER, 8.0))));

    Assertions.assertEquals(expected.size(), result.size(), "Incorrect amount of resulting triggers");
    for (int i = 0; i < expected.size(); i++) {
      Assertions.assertEquals(expected.get(i), result.get(i), "Trigger " + i + " incorrect ");
    }
  }

  @Test
  void parseToModelTriggerUnsupportedTest() {
    var unsupportedTrigger = new ls5.pg646.parsing.Trigger();
    unsupportedTrigger.setPlatform("unsupported");

    final List<ls5.pg646.parsing.Trigger> unsupportedTriggers = List.of(unsupportedTrigger);
    Assertions.assertThrows(IllegalStateException.class, () -> ConditionAdapter.parseToModelTrigger(unsupportedTriggers));
  }

  @Test
  void parseToModelSmartHomeTest() {

    final String alias = "Sehr tolle Automation wow! Alle meine Freunde waren begeistert!";

    // Triggers
    var contState = new ls5.pg646.parsing.Trigger();
    contState.setPlatform("state");
    contState.setEntityId("a");
    var toState = new ls5.pg646.parsing.Trigger();
    toState.setPlatform("state");
    toState.setEntityId("b");
    toState.setTo(List.of("on"));
    var numAbove = new ls5.pg646.parsing.Trigger();
    numAbove.setPlatform("numeric_state");
    numAbove.setEntityId("c");
    numAbove.setUnknown("above", 10.0);
    var numBelow = new ls5.pg646.parsing.Trigger();
    numBelow.setPlatform("numeric_state");
    numBelow.setEntityId("d");
    numBelow.setUnknown("below", 5.0);
    var numBoth = new ls5.pg646.parsing.Trigger();
    numBoth.setPlatform("numeric_state");
    numBoth.setEntityId("e");
    numBoth.setUnknown("above", 4.0);
    numBoth.setUnknown("below", 8.0);
    var triggers = new ArrayList<>(List.of(contState, toState, numAbove, numBelow, numBoth));

    // Conditions
    var conditions = new ArrayList<>(List.of(
        stateCond("a", "on"),
        numStateCond("b", null, 10, null),
        orCond(
            stateCond("c", "off"),
            numStateCond("d", null, null, 5))));

    // Actions
    var a1 = new Action();
    a1.setService("light.turn_on");
    a1.setDomain("light");
    a1.setTarget(Map.of("entity_id", "a"));
    var a2 = new Action();
    a2.setService("input_boolean.toggle");
    a2.setTarget(Map.of("entity_id", "c"));
    var actions = new ArrayList<>(List.of(
        a1, a2));

    var automation = new ls5.pg646.parsing.Automation();
    automation.setTrigger(triggers);
    automation.setCondition(conditions);
    automation.setAction(actions);
    automation.setId("test_automation");
    automation.setAlias(alias);

    SmartHome result = ConditionAdapter.parseToModelSmartHome(Set.of(automation));

    var expectedTriggers = List.of(
        new Trigger.ContinuousTrigger("a"),
        new Trigger.StateTrigger("b", List.of(
            new Condition.BinaryCondition("b", true))),
        new Trigger.StateTrigger("c", List.of(
            new Condition.NumericCondition("c", Relation.GREATER, 10.0))),
        new Trigger.StateTrigger("d", List.of(
            new Condition.NumericCondition("d", Relation.LESSER, 5.0))),
        new Trigger.StateTrigger("e", List.of(
            new Condition.NumericCondition("e", Relation.GREATER, 4.0),
            new Condition.NumericCondition("e", Relation.LESSER, 8.0))));

    var expectedCondAct = List.of(
        new ConditionActionPair(List.of(
            binCondFactory("c", false),
            binCondFactory("a", true),
            numCondFactory("b", Relation.GREATER, 10.0)),
            List.of(
                binCondFactory("a", true),
                binCondFactory("c", true))),
        new ConditionActionPair(List.of(
            numCondFactory("d", Relation.LESSER, 5.0),
            binCondFactory("a", true),
            numCondFactory("b", Relation.GREATER, 10.0),
            binCondFactory("c", true)),
            List.of(
                binCondFactory("a", true),
                binCondFactory("c", false))),
        new ConditionActionPair(List.of(
            numCondFactory("d", Relation.LESSER, 5.0),
            binCondFactory("a", true),
            numCondFactory("b", Relation.GREATER, 10.0),
            binCondFactory("c", false)),
            List.of(
                binCondFactory("a", true),
                binCondFactory("c", true))));

    var expectedAutomations = new ArrayList<Automation>();

    for (Trigger trigger : expectedTriggers) {
      for (ConditionActionPair conditionActionPair : expectedCondAct) {
        expectedAutomations.add(
            new Automation(
                alias,
                trigger,
                conditionActionPair.conditions(),
                conditionActionPair.actions()));
      }
    }

    var expectedSmartHome = new SmartHome(expectedAutomations);

    Assertions.assertEquals(expectedAutomations.size(), result.automations().size(), "Incorrect amount of resulting automations");
    for (int i = 0; i < expectedAutomations.size(); i++) {
      Assertions.assertEquals(expectedAutomations.get(i), result.automations().get(i), "Automation " + i + " incorrect ");
    }

    Assertions.assertEquals(expectedSmartHome, result, "Smarthome not like expected");
  }

}
