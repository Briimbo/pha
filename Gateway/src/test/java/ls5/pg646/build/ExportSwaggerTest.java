//package ls5.pg646.build;
//
//import java.io.File;
//import java.io.IOException;
//import java.nio.file.Files;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//
//@SpringBootTest
//class ExportSwaggerTest {
//  
//  @Autowired
//  private TestRestTemplate restTemplate;
//  
//  /**
//   * This is a nasty workaround to export the swagger.json. 
//   * The swagger-maven-plugin is outdated and doesn't work with current swagger versions.
//   * @throws IOException 
//   */
//  @Test
//  public void exportSwagger() throws IOException {
//     String swagger = this.restTemplate.getForObject("/v2/api-docs", String.class);
//     File targetFile = new File("target/output/swagger.v1.json");
//     Files.writeString(targetFile.toPath(), swagger);
//  }
//
//}
