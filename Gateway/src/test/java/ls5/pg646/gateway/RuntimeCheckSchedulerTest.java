package ls5.pg646.gateway;

import org.junit.jupiter.api.Test;
import org.logicng.datastructures.Assignment;
import org.logicng.formulas.Formula;
import org.logicng.formulas.FormulaFactory;

public class RuntimeCheckSchedulerTest {

    @Test
    void testLogicNGBehaviour() {
        FormulaFactory ff = new FormulaFactory();
        Assignment ass = new Assignment();
        ass.addLiteral(ff.literal("X", false));
        ass.addLiteral(ff.literal("Y", true));
        Formula formula = ff.and(ff.literal("X", false), ff.literal("Y", true));
        System.out.println(formula.evaluate(ass));
    }
}

