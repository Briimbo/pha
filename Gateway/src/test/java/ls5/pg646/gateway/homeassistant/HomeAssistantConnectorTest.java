package ls5.pg646.gateway.homeassistant;

import ls5.pg646.gateway.service.homeassistant.HomeAssistantConnector;
import ls5.pg646.parsing.Group;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

class HomeAssistantConnectorTest {

    private final static String RESOURCE_PATH = "./src/test/resources/";

    @Test
    void testEntitiesInGroup() throws IOException {
        final Group awesomePeople = new Group();
        awesomePeople.setId("group.awesome_people");
        awesomePeople.setName("Awesome People");
        awesomePeople.setAll(false);
        awesomePeople.setEntities(List.of("device_tracker.dad_smith", "device_tracker.mom_smith"));

        final var containedEntities = HomeAssistantConnector.entitiesInGroup("group.awesome_people", RESOURCE_PATH + "test-parse-group-yaml/");

        Assertions.assertEquals(awesomePeople.getEntities(), containedEntities, "Unexpected entities");
    }

    @Test
    void testEntitiesInRangeOf() {}

    @Test
    void testEntitiesOfClass() {}
}
