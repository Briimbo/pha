package ls5.pg646.parsing;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import ls5.pg646.validation.symbolic.Automation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import ls5.pg646.adapter.ConditionAdapter;

class AutomationParserTest {

  private final static Path RESOURCE_PATH = Paths.get("./src/test/resources/");

  @ParameterizedTest
  @ValueSource(strings = { "test-include-direct", "test-include-dir-list", "test-include-dir-merge-list" })
  void testResolveDirectories(String resourceFolder) throws IOException {
    final var files = RESOURCE_PATH.resolve(resourceFolder);
    final var configPath = files.resolve("configuration.yaml");
    final var expectedResult = Files.readString(files.resolve("result.yaml")).trim();

    final var resolved = ParserUtil.resolveDirectives(configPath)
        .collect(Collectors.joining("\n")).trim();

    Assertions.assertEquals(expectedResult, resolved);
  }
  
  
  @Test
  void testFromAndTo() throws IOException {
    final var files = RESOURCE_PATH.resolve("test-trigger-to-from");

    var automations = AutomationParser.fromConfig(files.toString());
    var home = ConditionAdapter.parseToModelSmartHome(automations);
    
   var result = (home.automations().stream()
        .map(Automation::toString)
        .reduce((a, b) -> a + "\n" + b).get());

   final var expectedResult = Files.readString(files.resolve("result.txt")).trim();

   Assertions.assertEquals(expectedResult, result);
  }
}
