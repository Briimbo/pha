package ls5.pg646.parsing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

class GroupParserTest {

  private final static Path RESOURCE_PATH = Paths.get("./src/test/resources/");

  @Test
  void testParseGroupYaml() throws IOException {
    final var files = RESOURCE_PATH.resolve("test-parse-group-yaml");

    final Group kitchen = new Group();
    kitchen.setId("group.kitchen");
    kitchen.setName("Kitchen Group");
    kitchen.setAll(true);
    kitchen.setEntities(List.of("switch.kitchen_pin_3"));

    final Group climate = new Group();
    climate.setId("group.climate");
    climate.setName("Climate Group");
    climate.setAll(false);
    climate.setEntities(List.of("sensor.bedroom_temp", "sensor.porch_temp"));

    final Group awesomePeople = new Group();
    awesomePeople.setId("group.awesome_people");
    awesomePeople.setName("Awesome People");
    awesomePeople.setAll(false);
    awesomePeople.setEntities(List.of("device_tracker.dad_smith", "device_tracker.mom_smith"));

    final var expectedResult = List.of(kitchen, climate, awesomePeople);
    final var actualResult = GroupParser.fromConfig(files.toString());

    Assertions.assertEquals(expectedResult.size(), actualResult.size(), "Unexpected size");

    for (Group grp : expectedResult) {
      Assertions.assertTrue(actualResult.contains(grp), "Collection does not contain " + grp.getId());
    }

    Assertions.assertTrue(expectedResult.containsAll(actualResult) && actualResult.containsAll(expectedResult),
        "menno");
  }

  @Test
  void testParseEmptyGroupYaml() throws IOException {
    final var files = RESOURCE_PATH.resolve("test-parse-empty-group-yaml");

    final var actualResult = GroupParser.fromConfig(files.toString());

    Assertions.assertTrue(actualResult.isEmpty(), "Groups not empty despite empty config");
  }
}
