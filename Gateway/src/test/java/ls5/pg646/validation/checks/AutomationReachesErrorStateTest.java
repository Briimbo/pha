package ls5.pg646.validation.checks;

import ls5.pg646.validation.jgrapht.AutomatonBuilder;
import ls5.pg646.validation.jgrapht.SmartHomeState;
import ls5.pg646.validation.jgrapht.SmartHomeTransition;
import ls5.pg646.validation.math.Condition;
import ls5.pg646.validation.symbolic.Constraint;
import ls5.pg646.validation.symbolic.FakeOracle;
import ls5.pg646.validation.symbolic.SmartHomeTest;
import org.jgrapht.Graph;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ls5.pg646.validation.math.Condition.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AutomationReachesErrorStateTest {
  public static Constraint getConstraint() {
    List<Condition> h = List.of(is("F2", false));
    List<Condition> f = List.of(is("H2", false));
    return new Constraint("Not_Heat_And_Window", List.of(f, h));
  }

  public static Graph<SmartHomeState, SmartHomeTransition> getAutomaton() {
    var home = SmartHomeTest.fiveRoomApartment();

    var constraint = getConstraint();
    var oracle = new FakeOracle();
    oracle.setEnvironment("T2");

    var builder = new AutomatonBuilder(home, oracle, constraint);
    return builder.buildGraph();
  }

  @Test
  void findsHeaterWindowExampleError() {
    var checker = new AutomationReachesErrorState();
    assertFalse(checker.check(getAutomaton(), getConstraint()).checkSuccessful());
  }

  @Test
  void verifiesHeaterWindowExampleWithoutError() {
    var checker = new AutomationReachesErrorState();
    var constraint = getConstraint();
    var automaton = getAutomaton();
    var transStrVar = // FIXME Ugly way to check for equality, but creating transitions manually
        // entirely is very ugly too...
        "SmartHomeTransition[type=AUTO, source=!H2, F2, T2 ∈ (-∞, 20.0), target=H2, F2, T2 ∈ (-∞, 20.0), guard=[T2 < 20.0], label=Bath_Cold]";
    var invalidTrans =
        automaton.edgeSet().stream()
            .filter(
                e ->
                    e.type().equals(SmartHomeTransition.Type.AUTO)
                        && !e.target().isAcceptingState(constraint))
            .toList();
    assertTrue(invalidTrans.size() == 1 && invalidTrans.get(0).toString().equals(transStrVar));
    automaton.removeEdge(invalidTrans.get(0));
    assertTrue(checker.check(automaton, constraint).checkSuccessful());
  }
}
