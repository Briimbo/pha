package ls5.pg646.validation.checks;

import ls5.pg646.validation.jgrapht.AutomatonBuilder;
import ls5.pg646.validation.math.Condition;
import ls5.pg646.validation.symbolic.Constraint;
import ls5.pg646.validation.symbolic.FakeOracle;
import ls5.pg646.validation.symbolic.SmartHomeTest;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static ls5.pg646.validation.checks.AutomationReachesErrorStateTest.getAutomaton;
import static ls5.pg646.validation.checks.AutomationReachesErrorStateTest.getConstraint;
import static ls5.pg646.validation.math.Condition.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AutomationReachesErrorStateEnvTest {
  @Test
  void findsHeaterWindowExampleError() {
    var checker = new AutomationReachesErrorStateEnv();
    assertFalse(checker.check(getAutomaton(), getConstraint()).checkSuccessful());
  }

  @Test
  void findsHeaterWindowExampleWithEnvError() {
    var checker = new AutomationReachesErrorStateEnv();
    List<Condition> h = List.of(is("F2", true));
    List<Condition> f = List.of(is("H2", true));
    List<Condition> f2 = List.of(lt("T2", 20.0));
    List<Condition> f3 = List.of(gt("T2", 25.0));
    var constraint = new Constraint("F2_or_H2_or_T2!=20-25", List.of(f, h, f2, f3));
    var home = SmartHomeTest.fiveRoomApartment();

    var oracle = new FakeOracle();
    oracle.setEnvironment("T2");

    var builder = new AutomatonBuilder(home, oracle, constraint);
    try {
      builder.saveGraphToPng("./target/output/findsHeaterWindowExampleWithEnvError.png");
    } catch (IOException e) {
      e.printStackTrace();
    }

    assertFalse(checker.check(builder.buildGraph(), constraint).checkSuccessful());
  }

  @Test
  void verifiesHeaterWindowExampleWithNoEnvError() {
    var checker = new AutomationReachesErrorStateEnv();
    List<Condition> h = List.of(is("F2", true));
    List<Condition> f = List.of(is("H2", true));
    List<Condition> f2 = List.of(lt("T2", 20.0));
    List<Condition> f3 = List.of(gt("T2", 25.0));
    var constraint = new Constraint("F2_or_H2_or_T2!=20-25", List.of(f, h, f2, f3));
    var home = SmartHomeTest.fiveRoomApartment();

    var oracle = new FakeOracle();

    var builder = new AutomatonBuilder(home, oracle, constraint);
    try {
      builder.saveGraphToPng("./target/output/verifiesHeaterWindowExampleWithNoEnvError.png");
    } catch (IOException e) {
      e.printStackTrace();
    }

    assertTrue(checker.check(builder.buildGraph(), constraint).checkSuccessful());
  }
}
