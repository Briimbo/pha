package ls5.pg646.validation.jgrapht;

import static ls5.pg646.validation.math.Condition.is;
import static ls5.pg646.validation.symbolic.Constraint.check;
import ls5.pg646.validation.symbolic.FakeOracle;
import ls5.pg646.validation.symbolic.SmartHomeTest;

public class AutomatonBuilderDemo {

  public static void main(String[] args) {
    var home = SmartHomeTest.fiveRoomApartment();
    var constraint = check("Bath_Heat_Win", is("F2", true), is("H2", true));
    var oracle = new FakeOracle();
    oracle.setEnvironment("T2");

    var builder = new AutomatonBuilder(home, oracle, constraint);

    var automaton = builder.buildGraph();
    var exporter = builder.createDotExporter();
    exporter.exportGraph(automaton, System.out);
  }
}
