package ls5.pg646.validation.jgrapht;

import static ls5.pg646.validation.math.Condition.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Collection;
import java.util.function.Function;
import java.util.regex.Pattern;
import org.junit.jupiter.api.Test;
import ls5.pg646.adapter.ConditionAdapter;
import ls5.pg646.parsing.AutomationParser;
import ls5.pg646.validation.math.Condition;
import ls5.pg646.validation.symbolic.Automation;
import ls5.pg646.validation.symbolic.Constraint;
import ls5.pg646.validation.symbolic.FakeOracle;
import ls5.pg646.validation.symbolic.SmartHomeTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
class AutomatonBuilderTest {

  public static final String AUTOMATION_NAME_PATTERN_STR = "(?<name>\\w+)($(?<suffix>\\w+))?";
  public static final Pattern AUTOMATION_NAME_PATTERN = Pattern.compile(AUTOMATION_NAME_PATTERN_STR);

  public static record NameAndSuffix(String name, int suffix) { }

  public static final Function<Automation, NameAndSuffix> EXTRACT_NAME = automation -> {
    var matcher = AUTOMATION_NAME_PATTERN.matcher(automation.name());
    if (matcher.find()) {
      return new NameAndSuffix(matcher.group("name"), Integer.parseInt(matcher.group("suffix")));
    }
    return new NameAndSuffix(automation.name(), -1);
  };


  @Test
  void testDomainGeneration() {
    var home = SmartHomeTest.fiveRoomApartment();
    List<Condition> h = List.of(is("F2", false));
    List<Condition> f = List.of(is("H2", false));
    var constraint = new Constraint("Not_Heat_And_Window", List.of(f, h));
    var oracle = new FakeOracle();

    var builder = new AutomatonBuilder(home, oracle, constraint);

    assertEquals(2, builder.automations.size(), "should have two automations");
    assertEquals(2, builder.binaryDomains.size(),
        "should have two binary sensors (window, heater)");
    assertEquals(1, builder.numericDomains.size(), "should have one numeric sensor (temp)");
    assertEquals(3, builder.numericDomains.get("T2").size(),
        "temp should have three intervalls (<20, > 25 and between)");
  }

  @Test //@Disabled(value = "Skipped until fixed")
  void testConfigurationParsingHeaterExample() throws IOException {
    List<Condition> heater = List.of(is("switch.bathroom_heater", false));
    List<Condition> window = List.of(is("switch.bathroom_window", false));
    var constraint = new Constraint("Bathroom_Window_and_Heater_Not_Both", List.of(heater, window));

    var path = "../Simulation/hassio.config/";
    var parsedAutomations = AutomationParser.fromConfig(path);
    var home = ConditionAdapter.parseToModelSmartHome(parsedAutomations);
    var oracle = new FakeOracle();
    oracle.setEnvironment("sensor.bathroom_temperature");

    var builder = new AutomatonBuilder(home, oracle, constraint);

    assertEquals(2, builder.automations.size(), "should have two automations");
    assertEquals(2, builder.binaryDomains.size(),
        "should have two binary sensors (window, heater)");
    assertEquals(1, builder.numericDomains.size(), "should have one numeric sensor (temp)");
    assertEquals(3, builder.numericDomains.get("sensor.bathroom_temperature").size(),
        "temp should have three intervalls (<20, > 25 and between)");
  }

  @Test
  void testConfiguration() throws IOException {
    List<Condition> fst = List.of(is("input_boolean.example1", false));
    List<Condition> snd = List.of(is("input_boolean.example2", false));
    var constraint = new Constraint("NotBoth", List.of(fst, snd));

    var path = "./src/test/resources/";
    var parsedAutomations = AutomationParser.fromConfig(path);
    var home = ConditionAdapter.parseToModelSmartHome(parsedAutomations);
    var oracle = new FakeOracle();
    oracle.setEnvironment("input_number.example_number1");

    var builder = new AutomatonBuilder(home, oracle, constraint);

    assertEquals(5, builder.automations.size(), "should have five automations");
    assertEquals(2, builder.binaryDomains.size(),
        "should have two binary sensors (example1, example2)");
    assertEquals(1, builder.numericDomains.size(), "should have one numeric sensor");
    assertEquals(3, builder.numericDomains.get("input_number.example_number1").size(),
        "should have three intervalls (<= 10, > 10 < 20, >= 20)");
  }

  @Test
  void testMultiTriggerWithoutConditions() throws IOException {

    var path = "./src/test/resources/test-multi-trigger-without-conditions/";
    var parsedAutomations = AutomationParser.fromConfig(path);
    var home = ConditionAdapter.parseToModelSmartHome(parsedAutomations);

    assertEquals(2, home.automations().size());

    //home.automations().forEach(System.out::println);

  }

  @Test
  void testMultiTriggerWithConditions() throws IOException {

    var path = "./src/test/resources/test-multi-trigger-with-conditions/";
    var parsedAutomations = AutomationParser.fromConfig(path);
    var home = ConditionAdapter.parseToModelSmartHome(parsedAutomations);

    assertEquals(2, home.automations().size());

    //home.automations().forEach(System.out::println);
  }


  @ParameterizedTest
  @ValueSource(strings = { "./src/test/resources/test-toggle-with-conditions/", "./src/test/resources/test-toggle-without-conditions/", "./src/test/resources/test-include-dir-merge-list" })
  void testToggle(String path) throws IOException {

    var expectedAutomations = Integer.parseInt(Files.readString(Paths.get(path, "expected_automations.txt")).trim());
    var parsedAutomations = AutomationParser.fromConfig(path);
    var home = ConditionAdapter.parseToModelSmartHome(parsedAutomations);

    home.automations().forEach(System.out::println);

    assertEquals(expectedAutomations, home.automations().size());
  }

}
