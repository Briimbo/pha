package ls5.pg646.validation.jgrapht;

import static ls5.pg646.validation.math.Condition.is;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.parse.Parser;
import ls5.pg646.adapter.ConditionAdapter;
import ls5.pg646.gateway.service.homeassistant.HomeAssistantInstance;
import ls5.pg646.parsing.AutomationParser;
import ls5.pg646.parsing.DeviceClasses;
import ls5.pg646.validation.math.Condition;
import ls5.pg646.validation.symbolic.Constraint;
import ls5.pg646.validation.symbolic.FakeOracle;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = {"ls5.pg646.parsing", "ls5.pg646.gateway.service.homeassistant"})
public class AutomatonToPngDemo implements CommandLineRunner {

  static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HHmmss");
  static String fileNameFormat = "./target/output/%s-%s";
  
  @Autowired
  DeviceClasses deviceClasses;
  
  @Autowired HomeAssistantInstance instance;
  
  /**
   * token for auth
   */
  @Value("${pha.app.default.ha.token}")
  private String defaultToken;
  

  public static void main(String[] args) throws IOException {
    SpringApplication.run(AutomatonToPngDemo.class, args).close();
  }

  @Override
  public void run(String... args) throws Exception {
    List<Condition> heater = List.of(is("switch.bathroom_heater", false));
    List<Condition> window = List.of(is("switch.bathroom_window", false));
    var constraint = new Constraint("Bathroom_Window_and_Heater_Not_Both", List.of(heater, window));
    
    var path = "../Simulation/hassio.config/";
    var parsedAutomations = AutomationParser.fromConfig(path);
    var home = ConditionAdapter.parseToModelSmartHome(parsedAutomations);
    
    var oracle = new FakeOracle();
    oracle.setEnvironment("sensor.bathroom_temperature");
    
    System.out.println(oracle);
    
    var targetName = String.format(fileNameFormat, "simulation-heater-example",
    LocalDateTime.now().format(formatter));
  
    var builder = new AutomatonBuilder(home, oracle, constraint);
    var exporter = builder.createDotExporter();
    
    var writer = new StringWriter();
    exporter.exportGraph(builder.buildGraph(), writer);
    MutableGraph g = new Parser().read(writer.toString());
    Graphviz.fromGraph(g).width(600).render(Format.SVG).toFile(new File(targetName + ".svg"));
  }
}
