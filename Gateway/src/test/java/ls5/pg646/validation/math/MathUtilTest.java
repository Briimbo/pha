package ls5.pg646.validation.math;

import static org.junit.jupiter.api.Assertions.*;
import java.util.List;
import org.junit.jupiter.api.Test;

class MathUtilTest {

  @Test
  void testWithOverlapping() {
    var c1 = new Condition.NumericCondition("c1", Relation.GREATER, 10d);
    var c2 = new Condition.NumericCondition("c1", Relation.LESSER, 20d);
    
    var result = MathUtil.computeIntervals(List.of(c1, c2));
    System.out.println(result);
    
    assertEquals(3, result.get("c1").size(), "Found three intervalls: (-Inf, 10], (10, 20), [20, +Inf)");
  }

}
