package ls5.pg646.validation.math;

import static ls5.pg646.validation.math.Relation.GREATER;
import static ls5.pg646.validation.math.Relation.GREATER_OR_EQUAL;
import static ls5.pg646.validation.math.Relation.LESSER;
import static ls5.pg646.validation.math.Relation.LESS_OR_EQUAL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class IntervalTest {

  @DisplayName("[x,y] matches < n")
  @ParameterizedTest(name = "{index} {0} {1} {2} == {3}")
  @MethodSource("rightInklusiveLesserValues")
  void rightInklusiveLesser(Interval i, Relation r, Double value, boolean expected) {
    assertEquals(expected, i.matches(r, value),
        "%s %s %s should be %s".formatted(i, r, value, expected));
  }

  private static Stream<Arguments> rightInklusiveLesserValues() {
    Interval i = new Interval(3d, true, 5d, true);
    return Stream.of(
        Arguments.of(i, LESSER, 20d, true),
        Arguments.of(i, LESSER, 5d, false),
        Arguments.of(i, LESSER, 4d, false),
        Arguments.of(i, LESSER, 3d, false),
        Arguments.of(i, LESSER, 0d, false)
    );
  }

  @DisplayName("[x,y] matches <= n")
  @ParameterizedTest(name = "{index} {0} {1} {2} == {3}")
  @MethodSource("rightInklusiveLesserEqualValues")
  void rightInklusiveLesserEqual(Interval i, Relation r, Double value, boolean expected) {
    assertEquals(expected, i.matches(r, value),
        "%s %s %s should be %s".formatted(i, r, value, expected));
  }

  private static Stream<Arguments> rightInklusiveLesserEqualValues() {

    Interval i = new Interval(3d, true, 5d, true);

    return Stream.of(
        Arguments.of(i, LESS_OR_EQUAL, 20d, true),
        Arguments.of(i, LESS_OR_EQUAL, 5d, true),
        Arguments.of(i, LESS_OR_EQUAL, 4d, false),
        Arguments.of(i, LESS_OR_EQUAL, 3d, false),
        Arguments.of(i, LESS_OR_EQUAL, 0d, false)
    );
  }


  @DisplayName("[x,y) matches < n")
  @ParameterizedTest(name = "{index} {0} {1} {2} == {3}")
  @MethodSource("rightExklusiveLesserValues")
  void rightExklusiveLesser(Interval i, Relation r, Double value, boolean expected) {
    assertEquals(expected, i.matches(r, value),
        "%s %s %s should be %s".formatted(i, r, value, expected));
  }

  private static Stream<Arguments> rightExklusiveLesserValues() {

    Interval i = new Interval(3d, true, 5d, false);

    return Stream.of(
        Arguments.of(i, LESSER, 20d, true),
        Arguments.of(i, LESSER, 5d, true),
        Arguments.of(i, LESSER, 4d, false),
        Arguments.of(i, LESSER, 3d, false),
        Arguments.of(i, LESSER, 0d, false)
    );
  }

  @DisplayName("[x,y) matches <= n")
  @ParameterizedTest(name = "{index} {0} {1} {2} == {3}")
  @MethodSource("rightExklusiveLesserEqualValues")
  void rightExclusiveLesserEqual(Interval i, Relation r, Double value, boolean expected) {
    assertEquals(expected, i.matches(r, value),
        "%s %s %s should be %s".formatted(i, r, value, expected));
  }

  private static Stream<Arguments> rightExklusiveLesserEqualValues() {

    Interval i = new Interval(3d, true, 5d, false);

    return Stream.of(
        Arguments.of(i, LESS_OR_EQUAL, 20d, true),
        Arguments.of(i, LESS_OR_EQUAL, 5d, true),
        Arguments.of(i, LESS_OR_EQUAL, 4d, false),
        Arguments.of(i, LESS_OR_EQUAL, 3d, false),
        Arguments.of(i, LESS_OR_EQUAL, 0d, false)
    );
  }

  /////////////////////////////////////////////////////////////////////////////

  @DisplayName("[x,y] matches > n")
  @ParameterizedTest(name = "{index} {0} {1} {2} == {3}")
  @MethodSource("leftInklusiveGreaterValues")
  void leftInklusiveGreater(Interval i, Relation r, Double value, boolean expected){
    assertEquals(expected, i.matches(r, value),
        "%s %s %s should be %s".formatted(i, r, value, expected));
  }

  private static Stream<Arguments> leftInklusiveGreaterValues() {
    Interval i = new Interval(3d, true, 5d, true);
    return Stream.of(
        Arguments.of(i, GREATER, 20d, false),
        Arguments.of(i, GREATER, 5d, false),
        Arguments.of(i, GREATER, 4d, false),
        Arguments.of(i, GREATER, 3d, false),
        Arguments.of(i, GREATER, 0d, true)
    );
  }

  @DisplayName("[x,y] matches >= n")
  @ParameterizedTest(name = "{index} {0} {1} {2} == {3}")
  @MethodSource("leftInklusiveGreaterEqualValues")
  void leftInklusiveGreaterEqual(Interval i, Relation r, Double value, boolean expected) {
    assertEquals(expected, i.matches(r, value),
        "%s %s %s should be %s".formatted(i, r, value, expected));
  }

  private static Stream<Arguments> leftInklusiveGreaterEqualValues() {

    Interval i = new Interval(3d, true, 5d, true);

    return Stream.of(
        Arguments.of(i, GREATER_OR_EQUAL, 20d, false),
        Arguments.of(i, GREATER_OR_EQUAL, 5d, false),
        Arguments.of(i, GREATER_OR_EQUAL, 4d, false),
        Arguments.of(i, GREATER_OR_EQUAL, 3d, true),
        Arguments.of(i, GREATER_OR_EQUAL, 0d, true)
    );
  }


  @DisplayName("(x,y] matches > n")
  @ParameterizedTest(name = "{index} {0} {1} {2} == {3}")
  @MethodSource("leftExklusiveGreaterValues")
  void leftExklusiveGreater(Interval i, Relation r, Double value, boolean expected) {
    assertEquals(expected, i.matches(r, value),
        "%s %s %s should be %s".formatted(i, r, value, expected));
  }

  private static Stream<Arguments> leftExklusiveGreaterValues() {

    Interval i = new Interval(3d, false, 5d, true);

    return Stream.of(
        Arguments.of(i, GREATER, 20d, false),
        Arguments.of(i, GREATER, 5d, false),
        Arguments.of(i, GREATER, 4d, false),
        Arguments.of(i, GREATER, 3d, true),
        Arguments.of(i, GREATER, 0d, true)
    );
  }

  @DisplayName("(x,y] matches >= n")
  @ParameterizedTest(name = "{index} {0} {1} {2} == {3}")
  @MethodSource("leftExklusiveGreaterEqualValues")
  void leftExklusiveGreaterEqual(Interval i, Relation r, Double value, boolean expected) {
    assertEquals(expected, i.matches(r, value),
        "%s %s %s should be %s".formatted(i, r, value, expected));

  }

  private static Stream<Arguments> leftExklusiveGreaterEqualValues() {

    Interval i = new Interval(3d, false, 5d, true);

    return Stream.of(
        Arguments.of(i, GREATER_OR_EQUAL, 20d, false),
        Arguments.of(i, GREATER_OR_EQUAL, 5d, false),
        Arguments.of(i, GREATER_OR_EQUAL, 4d, false),
        Arguments.of(i, GREATER_OR_EQUAL, 3d, true),
        Arguments.of(i, GREATER_OR_EQUAL, 0d, true)
    );
  }

  /////////////////////////////////////////////////////////////////////////////

  @DisplayName("Closed Interval contains")
  @ParameterizedTest(name = "{index} {0} contains {1} should be {2}")
  @MethodSource("testClosedContainsValues")
  void testClosedContains(Interval i, Double value, boolean expected) {

    assertEquals(expected, i.isInside(value));

  }

  private static Stream<Arguments> testClosedContainsValues() {

    Interval i = new Interval(3d, true, 5d, true);

    return Stream.of(
        Arguments.of(i, 2d, false),
        Arguments.of(i, 3d, true),
        Arguments.of(i, 4d, true),
        Arguments.of(i, 5d, true),
        Arguments.of(i, 6d, false),
        Arguments.of(i, Double.NaN, false),
        Arguments.of(i, Double.NEGATIVE_INFINITY, false),
        Arguments.of(i, Double.POSITIVE_INFINITY, false)
    );
  }

  @DisplayName("Left Open Interval contains")
  @ParameterizedTest(name = "{index} {0} contains {1} should be {2}")
  @MethodSource("testLeftOpenContainsValues")
  void testLeftOpenContains(Interval i, Double value, boolean expected) {

    assertEquals(expected, i.isInside(value));

  }

  private static Stream<Arguments> testLeftOpenContainsValues() {

    Interval i = new Interval(3d, false, 5d, true);

    return Stream.of(
        Arguments.of(i, 2d, false),
        Arguments.of(i, 3d, false),
        Arguments.of(i, 4d, true),
        Arguments.of(i, 5d, true),
        Arguments.of(i, 6d, false),
        Arguments.of(i, Double.NaN, false),
        Arguments.of(i, Double.NEGATIVE_INFINITY, false),
        Arguments.of(i, Double.POSITIVE_INFINITY, false)
    );
  }

  @DisplayName("Right Open Interval contains")
  @ParameterizedTest(name = "{index} {0} contains {1} should be {2}")
  @MethodSource("testRightOpenContainsValues")
  void testRightOpenContains(Interval i, Double value, boolean expected) {

    assertEquals(expected, i.isInside(value));

  }

  private static Stream<Arguments> testRightOpenContainsValues() {

    Interval i = new Interval(3d, true, 5d, false);

    return Stream.of(
        Arguments.of(i, 2d, false),
        Arguments.of(i, 3d, true),
        Arguments.of(i, 4d, true),
        Arguments.of(i, 5d, false),
        Arguments.of(i, 6d, false),
        Arguments.of(i, Double.NaN, false),
        Arguments.of(i, Double.NEGATIVE_INFINITY, false),
        Arguments.of(i, Double.POSITIVE_INFINITY, false)
    );
  }

  @DisplayName("Open Interval contains")
  @ParameterizedTest(name = "{index} {0} contains {1} should be {2}")
  @MethodSource("testOpenContainsValues")
  void testOpenContains(Interval i, Double value, boolean expected) {

    assertEquals(expected, i.isInside(value));

  }

  private static Stream<Arguments> testOpenContainsValues() {

    Interval i = new Interval(3d, false, 5d, false);

    return Stream.of(
        Arguments.of(i, 2d, false),
        Arguments.of(i, 3d, false),
        Arguments.of(i, 4d, true),
        Arguments.of(i, 5d, false),
        Arguments.of(i, 6d, false),
        Arguments.of(i, Double.NaN, false),
        Arguments.of(i, Double.NEGATIVE_INFINITY, false),
        Arguments.of(i, Double.POSITIVE_INFINITY, false)
    );
  }

  @DisplayName("Singleton contains")
  @ParameterizedTest(name = "{index} {0} contains {1} should be {2}")
  @MethodSource("testSingletonContainsValues")
  void testSingletonContains(Interval i, Double value, boolean expected) {

    assertEquals(expected, i.isInside(value));

  }

  private static Stream<Arguments> testSingletonContainsValues() {

    Interval i = new Interval(4d, true, 4d, true);

    return Stream.of(
        Arguments.of(i, 2d, false),
        Arguments.of(i, 3d, false),
        Arguments.of(i, 4d, true),
        Arguments.of(i, 5d, false),
        Arguments.of(i, 6d, false),
        Arguments.of(i, Double.NaN, false),
        Arguments.of(i, Double.NEGATIVE_INFINITY, false),
        Arguments.of(i, Double.POSITIVE_INFINITY, false)
    );
  }

  @DisplayName("Interval validation")
  @ParameterizedTest(name = "{index} Interval ({0}, {1}, {2}, {3}) should not be valid")
  @MethodSource("testIntervalConstruction")
  void testIntervalConstructorThrows (Double left, boolean leftInclusive,
    Double right, boolean rightInclusive) {
    assertThrows(IllegalArgumentException.class,
        () -> new Interval(left, leftInclusive, right, rightInclusive));
  }

  private static Stream<Arguments> testIntervalConstruction() {
    return Stream.of(
        Arguments.of(3d, false, 3d, false),
        Arguments.of(3d, false, 3d, true),
        Arguments.of(3d, true, 3d, false),
        Arguments.of(5d, false, 4d, false),
        Arguments.of(Double.NaN, true, 4d, true),
        Arguments.of(4d, true, Double.NaN, true),
        Arguments.of(Double.NaN, true, Double.NaN, true)
    );
  }
}
