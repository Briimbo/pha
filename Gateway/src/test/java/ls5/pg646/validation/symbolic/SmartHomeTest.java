package ls5.pg646.validation.symbolic;

import static ls5.pg646.validation.math.Condition.all;
import static ls5.pg646.validation.math.Condition.gt;
import static ls5.pg646.validation.math.Condition.is;
import static ls5.pg646.validation.math.Condition.lt;
import static ls5.pg646.validation.math.Condition.none;
import static ls5.pg646.validation.symbolic.Automation.single;
import static ls5.pg646.validation.symbolic.Constraint.check;
import static ls5.pg646.validation.symbolic.Trigger.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SmartHomeTest {

  /**
   * The SmartHome as given in §5 of the Validation Overview
   *
   * @return
   */
  public static SmartHome fiveRoomApartment() {
    var a1 = single("Bath_Cold", when("T2", lt("T2", 20d)), none(), all(is("H2", true)));
    var a2 = single("Bath_Hot", when("T2", gt("T2", 25d)), none(), all(is("H2", false)));

    var a3 = single("Kitchen_Cold", when("T3", lt("T3", 20d)), none(), all(is("H3", true)));
    var a4 = single("Kitchen_Hot", when("T3", gt("T3", 25d)), none(), all(is("H3", false)));
    var a5 = single("Kitchen_Win", when("F3", is("F3", true)), all(is("H3", true)),
        all(is("H3", false)));

    return new SmartHome(List.of(a1, a2, a3, a4, a5));
  }

  /**
   * First Room With - T1, H1, F1 Second Room With - T2, H2, F2 Corridor with - T3, F3
   *
   * @return
   */
  public static SmartHome twoConnectedRooms(boolean multiAutomation) {
    var a1 = single("T1_Cold", when("T1", lt("T1", 20d)), none(), all(is("H1", true)));
    var a2 = single("T1_Hot", when("T1", gt("T1", 25d)), none(), all(is("H1", false)));

    var a3 = single("T2_Cold", when("T2", lt("T2", 20d)), none(), all(is("H2", true)));
    var a4 = single("T2_Hot", when("T2", gt("T2", 25d)), none(), all(is("H2", false)));

    var a51 = single("T3_Cold1", when("T3", lt("T3", 20d)), none(), all(is("H1", true)));
    var a52 = single("T3_Cold2", when("T3", gt("T3", 25d)), none(), all(is("H2", true)));

    if (multiAutomation) {
      var a512 =
          single("T3_Cold", when("T3", lt("T3", 20d)), none(), all(is("H2", true), is("H1", true)));
      return new SmartHome(List.of(a1, a2, a3, a4, a51, a52, a512));
    }
    return new SmartHome(List.of(a1, a2, a3, a4, a51, a52));
  }

  @Test
  @DisplayName("Nothing else interferes with window & heater in bathroom")
  void bathroomIsAlone() {
    var home = fiveRoomApartment();
    var c1 = check("Bath_Heat_Win", is("F2", true), is("H2", true));
    var interfering = home.findInterferingAutomations(c1);
    assertEquals(2, interfering.size());

    assertTrue(interfering.stream().anyMatch(a -> a.name().equals("Bath_Cold")));
    assertTrue(interfering.stream().anyMatch(a -> a.name().equals("Bath_Hot")));

    assertTrue(interfering.stream().noneMatch(a -> !a.name().startsWith("Bath_")));
  }

  @Test
  @DisplayName("Corridor does not pull in both rooms")
  void corridorDoesNotIncludeBothRooms() {

    var c1 = check("T1_Check", is("F1", true), is("H1", true));
    var c2 = check("T2_Check", is("F2", true), is("H2", true));

    var home = twoConnectedRooms(false);

    var inf_c1 = home.findInterferingAutomations(c1);
    assertEquals(3, inf_c1.size());

    var inf_c2 = home.findInterferingAutomations(c2);
    assertEquals(3, inf_c2.size());

    home = twoConnectedRooms(true);

    inf_c1 = home.findInterferingAutomations(c1);
    assertEquals(4, inf_c1.size());

    inf_c2 = home.findInterferingAutomations(c2);
    assertEquals(4, inf_c2.size());
  }

  @Test
  @DisplayName("3 sensors (H2,F2,T2) interfere with bathroom constraint C1")
  void bathRoomNeeds3Sensors() {

    var c1 = check("Bath_Heat_Win", is("F2", true), is("H2", true));

    var home = fiveRoomApartment();

    var states = home.findRelevantSensors(c1);
    assertEquals(3, states.size());
    assertTrue(states.contains("H2"));
    assertTrue(states.contains("F2"));
    assertTrue(states.contains("T2"));
  }

  @Test
  @DisplayName("4 sensors (1,T1,F1,T3) interfere with constraint C1 in corridor example")
  void corridorNeeds4Sensors() {

    var c1 = check("T1_Check", is("F1", true), is("H1", true));

    var home = twoConnectedRooms(false);

    var states = home.findRelevantSensors(c1);
    assertEquals(4, states.size());

    assertTrue(states.contains("H1"));
    assertTrue(states.contains("F1"));
    assertTrue(states.contains("T1"));

    assertTrue(states.contains("T3"));

    assertFalse(states.contains("F2"));
    assertFalse(states.contains("H2"));
    assertFalse(states.contains("T2"));
  }

  @Test
  @DisplayName("4 sensors (1,T1,F1,T3) interfere with constraint C1 in corridor example, with multi-action")
  void corridorNeeds4SensorsWithMultiAction() {

    var c1 = check("T1_Check", is("F1", true), is("H1", true));

    var home = twoConnectedRooms(true);

    var states = home.findRelevantSensors(c1);
    assertEquals(4, states.size());

    assertTrue(states.contains("H1"));
    assertTrue(states.contains("F1"));
    assertTrue(states.contains("T1"));

    assertTrue(states.contains("T3"));

    assertFalse(states.contains("F2"));
    assertFalse(states.contains("H2"));
    assertFalse(states.contains("T2"));
  }
}
