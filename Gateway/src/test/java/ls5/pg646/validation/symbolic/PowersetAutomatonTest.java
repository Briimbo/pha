// package ls5.pg646.validation.symbolic;

// import org.jgrapht.Graph;
// import org.jgrapht.graph.SimpleDirectedGraph;
// import org.jgrapht.nio.dot.DOTExporter;
// import org.junit.jupiter.api.Test;
// import ls5.pg646.validation.jgrapht.AAEdge;
// import ls5.pg646.validation.jgrapht.AAVertex;
// import ls5.pg646.validation.jgrapht.PowersetAutomatonBuilder;
// import ls5.pg646.validation.jgrapht.State;
// import java.io.StringWriter;
// import java.io.Writer;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.Map;
// import java.util.Set;

// import static org.junit.jupiter.api.Assertions.*;

// public class PowersetAutomatonTest {
//   @Test
//   void buildPowersetAutomaton() {
//     var b = new PowersetAutomatonBuilder();
//     b.addBinarySensor("L1");
//     b.addBinarySensor("L2");
//     b.addBinarySensor("L3");
//     // b.addBinarySensor("F1");
//     // b.addBinarySensor("F2");
//     var graph = b.buildGraph();
//     DOTExporter<AAVertex, AAEdge> exporter = new DOTExporter<>(v -> v.toString());
//     exporter.setEdgeIdProvider(e -> e.toString());
//     Writer writer = new StringWriter();
//     exporter.exportGraph(graph, writer);
//     //System.out.println(writer.toString());
//   }

//   @Test
//   void setTest() {
//     Set<State> s1 = new HashSet<>();
//     Set<State> s2 = new HashSet<>();
//     s1.add(new BinaryState("a", false));
//     s1.add(new BinaryState("b", true));
//     s1.add(new BinaryState("c", true));
//     s2.add(new BinaryState("c", true));
//     s2.add(new BinaryState("b", true));
//     s2.add(new BinaryState("a", false));
//     assertTrue (s1.equals(s2));
//     Map<Set<State>, String> t = new HashMap<>();
//     t.put(s1, "ggg");
//     assertTrue (t.containsKey(s2));
//   }

//   @Test
//   void testVertexIdentity() {
//     Graph<AAVertex, AAEdge> graph = new SimpleDirectedGraph<>(AAEdge.class);
//     State s11 = new State("light1", true);

//     State s21 = new State("light2", false);
//     Set<State> states1 = new HashSet<>();
//     states1.add(s11);
//     states1.add(s21);
//     AAVertex t1 = new AAVertex(states1);

//     State s12 = new State("light1", true);
//     State s22 = new State("light2", false);
//     Set<State> states2 = new HashSet<>();
//     states2.add(s12);
//     states2.add(s22);
//     AAVertex t2 = new AAVertex(states2);
//     assertEquals (t1, t2);

//     graph.addVertex(t1);
//     // loops are not allowed, so this should throw
//     assertThrows(IllegalArgumentException.class, () -> graph.addEdge(t2, t2));
//   }
// }
