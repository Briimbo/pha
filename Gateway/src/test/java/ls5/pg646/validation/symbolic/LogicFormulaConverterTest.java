package ls5.pg646.validation.symbolic;

import java.util.Arrays;
import java.util.List;
import ls5.pg646.parsing.Condition;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.logicng.formulas.FormulaFactory;

class LogicFormulaConverterTest {

  private Condition stateCond(String entityName, String stateName) {
    var cond = new Condition();
    cond.setConditions(Lists.emptyList());
    cond.setConditionType("state");
    cond.setUnknown("entity_id", entityName);
    cond.setUnknown("state", stateName);
    return cond;
  }

  private Condition numStateCond(String entityName, String attribute, Integer above, Integer below) {
    var cond = new Condition();
    cond.setConditions(Lists.emptyList());
    cond.setConditionType("numeric_state");
    cond.setUnknown("entity_id", entityName);
    if (attribute != null) {
      cond.setUnknown("attribute", attribute);
    }
    if (above != null) {
      cond.setUnknown("above", above);
    }
    if (below != null) {
      cond.setUnknown("below", below);
    }
    return cond;
  }

  private Condition andCond(Condition... conds) {
    var cond = new Condition();
    cond.setConditionType("and");
    cond.setConditions(Arrays.asList(conds));
    return cond;
  }

  private Condition orCond(Condition... conds) {
    var cond = new Condition();
    cond.setConditionType("or");
    cond.setConditions(Arrays.asList(conds));
    return cond;
  }

  private Condition notCond(Condition c) {
    var cond = new Condition();
    cond.setConditionType("not");
    cond.setConditions(List.of(c));
    return cond;
  }

  @Test
  void stateConditionConvertTest() {
    var cond1 = stateCond("light.test_light", "on");
    var result = LogicFormulaConverter.asFormula(List.of(cond1));
    var formulaFactory = new FormulaFactory();
    var control = formulaFactory.variable("light.test_light==on");

    Assertions.assertEquals(control, result, "State Condition incorrectly converted");
  }

  @Test
  void numStateConditionConvertTest() {
    var formulaFactory = new FormulaFactory();
    // Only with above value
    {
      var cond1 = numStateCond("sensor.test_temp", null, 10, null);
      var result = LogicFormulaConverter.asFormula(List.of(cond1));
      var control = formulaFactory.variable("sensor.test_temp>10");

      Assertions.assertEquals(control, result,
          "Numeric State Condition with only above value incorrectly converted.");
    }

    // Only with below value
    {
      var cond1 = numStateCond("sensor.test_temp", null, null, 10);
      var result = LogicFormulaConverter.asFormula(List.of(cond1));
      var control = formulaFactory.variable("sensor.test_temp<10");

      Assertions.assertEquals(control, result,
          "Numeric State Condition with only below value incorrectly converted.");
    }

    // With both above and below values
    {
      var cond1 = numStateCond("sensor.test_temp", null, 10, 15);
      var result = LogicFormulaConverter.asFormula(List.of(cond1));
      var control = formulaFactory.and(formulaFactory.variable("sensor.test_temp<15"), formulaFactory.variable("sensor.test_temp>10"));

      Assertions.assertEquals(control, result,
          "Numeric State Condition with both above and below value incorrectly converted.");
    }

    {

      var cond1 = numStateCond("sensor.test_temp", "test_attr", 10, 15);
      var result = LogicFormulaConverter.asFormula(List.of(cond1));
      var control = formulaFactory.and(formulaFactory.variable("sensor.test_temp.test_attr<15"), formulaFactory.variable("sensor.test_temp.test_attr>10"));

      Assertions.assertEquals(control, result,
          "Numeric State Condition with both above and below value incorrectly converted.");
    }
  }

  @Test
  void multiConditionParseTest() {
    var fac = new FormulaFactory();
    var cond1 = stateCond("switch.test_switch", "on");
    var cond2 = numStateCond("sensor.test_temp", null, 10, null);
    var result = LogicFormulaConverter.asFormula(List.of(cond1, cond2));
    var control = fac.and(fac.variable("sensor.test_temp>10"), fac.variable("switch.test_switch==on"));

    Assertions.assertEquals(control, result,
        "Multiple conditions not correctly converted as conjunction.");
  }

  @Test
  void andConditionParseTest() {
    var fac = new FormulaFactory();
    var cond1 = stateCond("switch.test_switch", "on");
    var cond2 = numStateCond("sensor.test_temp", null, 10, null);
    var andCond = andCond(cond1, cond2);
    var result = LogicFormulaConverter.asFormula(List.of(andCond));
    var control = fac.and(fac.variable("sensor.test_temp>10"), fac.variable("switch.test_switch==on"));

    Assertions.assertEquals(control, result,
        "And condition incorrectly converted.");
  }

  @Test
  void orConditionParseTest() {
    var fac = new FormulaFactory();
    var cond1 = stateCond("switch.test_switch", "on");
    var cond2 = numStateCond("sensor.test_temp", null, 10, null);
    var orCond = orCond(cond1, cond2);
    var result = LogicFormulaConverter.asFormula(List.of(orCond));
    var control = fac.or(fac.variable("sensor.test_temp>10"), fac.variable("switch.test_switch==on"));

    Assertions.assertEquals(control, result,
        "Or condition incorrectly converted.");
  }


  @Test
  void notConditionParseTest() {
    var fac = new FormulaFactory();
    var cond1 = stateCond("switch.test_switch", "on");
    var notCond = notCond(cond1);
    var result = LogicFormulaConverter.asFormula(List.of(notCond));
    var control = fac.not(fac.variable("switch.test_switch==on"));

    Assertions.assertEquals(control, result,
        "Not condition incorrectly converted.");
  }


  @Test
  void nestedConditionParseTest() {
    var fac = new FormulaFactory();
    var cond1 = stateCond("switch.test_switch", "on");
    var cond2 = numStateCond("sensor.test_temp", null, 10, null);
    var cond3 = stateCond("binary_sensor.test_move", "off");
    var result = LogicFormulaConverter.asFormula(List.of(cond1, orCond(cond2, notCond(cond3))));
    var v1 = fac.variable("switch.test_switch==on");
    var v2 = fac.variable("sensor.test_temp>10");
    var v3 = fac.variable("binary_sensor.test_move==off");
    var control = fac.or(fac.and(v1, v2), fac.and(v1, fac.not(v3)));

    Assertions.assertEquals(control, result,
        "Nested conditions incorrectly converted.");
  }
}
