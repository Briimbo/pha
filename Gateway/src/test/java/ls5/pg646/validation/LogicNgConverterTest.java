package ls5.pg646.validation;

import static ls5.pg646.validation.math.Condition.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import java.util.Map;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import com.google.inject.Inject;
import ls5.pg646.constraints.constraints.ConstraintDocument;
import ls5.pg646.constraints.tests.ConstraintsInjectorProvider;
import ls5.pg646.gateway.GatewayApplication;
import ls5.pg646.gateway.service.exceptions.InternalServerError;
import ls5.pg646.gateway.service.exceptions.InvalidStateException;
import ls5.pg646.gateway.service.homeassistant.HomeAssistantConnector;
import ls5.pg646.validation.math.Condition;
import ls5.pg646.validation.symbolic.Constraint;

@ExtendWith(InjectionExtension.class)
@InjectWith(ConstraintsInjectorProvider.class)
@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = { GatewayApplication.class }, properties = {"test=true"})
@EnableConfigurationProperties
@Disabled
class LogicNgConverterTest {
  
  @Inject
  ParseHelper<ConstraintDocument> parseHelper;
  
  @Autowired
  HomeAssistantConnector connector;
  
  @Value("${pha.app.default.ha.address}")
  private String defaultAddress;

  /**
   * token for auth.
   */
  @Value("${pha.app.default.ha.token}")
  private String defaultToken;
  
  @Test
  void testAreCorrectlySetUp() {
    assertEquals("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhYmQ3OTM3ZjY5YTY0ZTYxODEwNDM3NTUzYWVmNTRlNyIsImlhdCI6MTY0MjA3MDgwOSwiZXhwIjoxOTU3NDMwODA5fQ.IPlkwlCxF4lz3UfxEq5rtefmAoVE_Qjt9it-p5sn5Yw", defaultToken);
    assertEquals("http://localhost:8123", defaultAddress);
  }

  //@Disabled
  @Test
  void canConvertToDnf() throws Exception {
    // TODO: check if switch. or sensor.
      var document = parseHelper.parse("""
          read switch.kitchen_window as W;
          read switch.kitchen_heater as H;

          constraint Not_Both: !(H && W);
      """);
      assertNotNull(document);
      var errors = document.eResource().getErrors();
      assertTrue(errors.isEmpty(), "Unexpected errors: " + errors);
      
      var definition = document.getConstraints().stream()
          .filter(constraint -> constraint.getName().equals("Not_Both"))
          .findFirst()
          .orElseThrow();
      
      var converter = new LogicNgConverter(connector, Map.of("H", " switch.kitchen_heater", "W", "switch.kitchen_window"));
      var constraint = converter.constraintFromExpression(definition.getName(), definition.getExpression());
      
      List<Condition> heater = List.of(is("switch.kitchen_window", false));
      List<Condition> window = List.of(is("switch.kitchen_heater", false));
      var referenceConstraint = new Constraint("Not_Both", List.of(window, heater));
      
      assertEquals(referenceConstraint, constraint);
  }

    @Test
    void canConvertNumericToDnf() throws Exception {
      // TODO: check if switch. or sensor.
        var document = parseHelper.parse("""
          read sensor.kitchen_temperature as T;
          read switch.kitchen_window as W;
          read switch.kitchen_heater as H;

          constraint Not_Both: !(H && W) || -T < 42.3;
      """);
        assertNotNull(document);
        var errors = document.eResource().getErrors();
        assertTrue(errors.isEmpty(), "Unexpected errors: " + errors);

        var definition = document.getConstraints().stream()
                .filter(constraint -> constraint.getName().equals("Not_Both"))
                .findFirst()
                .orElseThrow();

        var converter = new LogicNgConverter(connector, LogicNgConverter.extractAliases(document));
        var constraint = converter.constraintFromExpression(definition.getName(), definition.getExpression());
       

        List<Condition> heater = List.of(is("switch.kitchen_window", false));
        List<Condition> window = List.of(is("switch.kitchen_heater", false));
        List<Condition> numeric = List.of(geq("sensor.kitchen_temperature", -42.3));
        var referenceConstraint = new Constraint("Not_Both", List.of(window, heater, numeric));

        assertEquals(referenceConstraint, constraint);
    }

  @Test
  void canConvertNegatedNumericToDnf() throws Exception {
    // TODO: check if switch. or sensor.
    var document = parseHelper.parse("""
          read sensor.kitchen_temperature as T;
          read switch.kitchen_window as W;
          read switch.kitchen_heater as H;

          constraint Not_Both: !(H && T > 25);
      """);
    assertNotNull(document);
    var errors = document.eResource().getErrors();
    assertTrue(errors.isEmpty(), "Unexpected errors: " + errors);

    var definition = document.getConstraints().stream()
        .filter(constraint -> constraint.getName().equals("Not_Both"))
        .findFirst()
        .orElseThrow();

    var converter = new LogicNgConverter(connector, LogicNgConverter.extractAliases(document));
    var constraint = converter.constraintFromExpression(definition.getName(), definition.getExpression());


    List<Condition> window = List.of(is("switch.kitchen_heater", false));
    List<Condition> numeric = List.of(leq("sensor.kitchen_temperature", 25.0));
    var referenceConstraint = new Constraint("Not_Both", List.of(window, numeric));

    assertEquals(referenceConstraint, constraint);
  }

  @Test @Disabled
  void canCreateUnionOfGroupAndClass() throws Exception {
    // TODO: check if switch. or sensor.
    var document = parseHelper.parse("""
        read switch.kitchen_window as W;
        read switch.kitchen_heater as H;

        constraint AllUnion: forall x | x in group 'bathroom' OR x is lamp . !(x && W && H);
    """);
    assertNotNull(document);
    var errors = document.eResource().getErrors();
    assertTrue(errors.isEmpty(), "Unexpected errors: " + errors);
    
    var definition = document.getConstraints().stream()
        .filter(constraint -> constraint.getName().equals("AllUnion"))
        .findFirst()
        .orElseThrow();
    
    var converter = new LogicNgConverter(connector, Map.of("H", " switch.kitchen_heater", "W", "switch.kitchen_window"));
    var constraint = converter.constraintFromExpression(definition.getName(), definition.getExpression());
    
    List<Condition> heater = List.of(is("switch.kitchen_window", false));
    List<Condition> window = List.of(is("switch.kitchen_heater", false));
    //var referenceConstraint = new Constraint("Not_Both", List.of(window, heater));
    
    System.out.println(LogicNgConverter.stringFromExpression(definition.getExpression()));
    System.out.println(converter.formulaFromExpression(definition.getExpression()));
    System.out.println(constraint);
    
    assertNotNull(constraint);
  }
  
  @Test @Disabled
  void canCreateIntersectionOfGroupAndClass() throws Exception {
    // TODO: check if switch. or sensor.
    var document = parseHelper.parse("""
        read switch.kitchen_window as W;
        read switch.kitchen_heater as H;

        constraint AnyIntersection: exists x | x in group 'bathroom' AND x is lamp . !(x && W && H);
    """);
    assertNotNull(document);
    var errors = document.eResource().getErrors();
    assertTrue(errors.isEmpty(), "Unexpected errors: " + errors);
    
    var definition = document.getConstraints().stream()
        .filter(constraint -> constraint.getName().equals("AnyIntersection"))
        .findFirst()
        .orElseThrow();
    
    var converter = new LogicNgConverter(connector, LogicNgConverter.extractAliases(document));
    var constraint = converter.constraintFromExpression(definition.getName(), definition.getExpression());
    
    System.out.println(LogicNgConverter.stringFromExpression(definition.getExpression()));
    System.out.println(converter.formulaFromExpression(definition.getExpression()));
    System.out.println(constraint);
    
    assertNotNull(constraint);
  }
  
  @Test @Disabled
  void canDesugarForAllClass() throws Exception {
    var document = parseHelper.parse("""
        constraint DesugarForAll: forall x | x is switch . !x;
    """);
    assertNotNull(document);
    var errors = document.eResource().getErrors();
    assertTrue(errors.isEmpty(), "Unexpected errors: " + errors);
    
    var definition = document.getConstraints().stream()
        .filter(constraint -> constraint.getName().equals("DesugarForAll"))
        .findFirst()
        .orElseThrow();
    
    try {
      var converter = new LogicNgConverter(connector, Map.of());
      var constraint = converter.constraintFromExpression(definition.getName(), definition.getExpression());
   
      var referenceConstraintString =
    		  "DesugarForAll: (!switch.hallway_heater && !switch.bathroom_heater && !switch.kitchen_heater && !switch.livingroom_heater && !switch.bedroom_heater)";
      
      assertNotNull(constraint);
      assertEquals(referenceConstraintString, constraint.toString());
    
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
  
  @Test
  void canDesugarAnyClass() throws Exception {
    var document = parseHelper.parse("""
        constraint DesugarExists: exists x | x is switch . !x;
    """);
    assertNotNull(document);
    var errors = document.eResource().getErrors();
    assertTrue(errors.isEmpty(), "Unexpected errors: " + errors);
    
    var definition = document.getConstraints().stream()
        .filter(constraint -> constraint.getName().equals("DesugarExists"))
        .findFirst()
        .orElseThrow();
    
    try {
      var converter = new LogicNgConverter(connector, Map.of());
      var constraint = converter.constraintFromExpression(definition.getName(), definition.getExpression());
   
      var referenceConstraintString =
    		  "DesugarExists: (!switch.hallway_heater) || (!switch.bathroom_heater) || (!switch.kitchen_heater) || (!switch.livingroom_heater) || (!switch.bedroom_heater)";
      
      
      assertNotNull(constraint);
      assertEquals(referenceConstraintString, constraint.toString());
    
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
  
  @Test
  void canFindFiveSwitches() throws InvalidStateException, InternalServerError, InterruptedException {
    // this should find all five heaters
    assertEquals(5, connector.entitiesOfDomain("switch").size());
  }
  
  
  @Test
  void canFindFifteenSensors() throws InvalidStateException, InternalServerError, InterruptedException {
    //6* humidity + temp and 3 * speedtest = 15
    assertEquals(15, connector.entitiesOfDomain("sensor").size());
  }
  
  @Test
  void canFindFiveLights() throws InvalidStateException, InternalServerError, InterruptedException {
    // this should find all five heaters
    assertEquals(5, connector.entitiesOfDomain("light").size());
  }
}
