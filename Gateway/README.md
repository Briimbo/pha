# Painless HomeAutomation Backend

This backend uses Spring and XText.

1. Go to `Constraint-Language\ls5.pg646.constraints.parent\ls5.pg646.constraints`
2. Run 'mvn generate-sources'
3. Go to `Constraint-Language\ls5.pg646.constraints.parent\`
4. Run `mvn install`
5. Go to `Gateway/dev-tools`
6. Run `docker compose up -d` to start postgres
7. Go to `Gateway`, run `mvn spring-boot:run`

The backend is now functional and running. It will automatically re-compile
when you change any `.java` file, so that changes are immediately available.

We have multiple `<profile>.properties` files available. If you run the Gateway with
`mvn spring-boot:run`, the `application.properties` will be used by default. This
configuration connects to your locally running database.

If you use `docker compose up` at the top level of the repo, the Gatway will use
the `container.properties`, which sets the correct database connection to connect
to the containerized database.

## Run the frontend

Follow the instructions in `Frontend/README.md` to also start the frontend locally.

## API "Documentation"

### /api/hanotify
Send a POST request containing json data to this endpoint to send messages
to the specified service. The JSON data can look like the following example.
The example posts a message to the Discord Channel with given id via the
Crystalline Discord bot using the Discord integration. 
```json
{
    "serviceName": "crystalline",
    "message": "@Frontend, notifications sollten jetzt möglich sein",
    "target" : [ "905501917906616380" ],
    "title": "Heyho",
    "data": {}
}
```

The field `serviceName` is the name of the Home Assistant notify service without "`notify.`".
The rest of the json file is the same as is required by the specific notify service.
For example, if the notify service is called "`notify.crystalline`" and sends a message to Discord (as above), then the `serviceName` field should be just `crystalline` target is a list of channel ids etc.
