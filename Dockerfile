ARG BUILD_FROM

FROM node:16-bullseye as frontend-dependencies
COPY ./Frontend/package.json /build/frontend/
COPY ./Frontend/package-lock.json /build/frontend/
WORKDIR /build/frontend
RUN npm ci

FROM node:16-bullseye as frontend
COPY --from=frontend-dependencies /build/frontend/node_modules/ /build/frontend/node_modules/
COPY ./Frontend/ /build/frontend/
WORKDIR /build/frontend
RUN apt-get install patch
RUN ./apply_patches.sh
RUN npm run build

FROM maven:3.8.4-openjdk-17 as constraint-dependencies
COPY ./Constraint-Language/ls5.pg646.constraints.parent/pom.xml /build/constraint-language/ls5.pg646.constraints.parent/pom.xml
COPY ./Constraint-Language/ls5.pg646.constraints.parent/ls5.pg646.constraints/pom.xml /build/constraint-language/ls5.pg646.constraints.parent/ls5.pg646.constraints/pom.xml
COPY ./Constraint-Language/ls5.pg646.constraints.parent/ls5.pg646.constraints.ide/pom.xml /build/constraint-language/ls5.pg646.constraints.parent/ls5.pg646.constraints.ide/pom.xml

WORKDIR /build/constraint-language/ls5.pg646.constraints.parent/ls5.pg646.constraints/
RUN mvn dependency:resolve

#WORKDIR /build/constraint-language/ls5.pg646.constraints.parent/ls5.pg646.constraints.ide/
#RUN mvn dependency:resolve

FROM maven:3.8.4-openjdk-17 as constraint-ide
COPY --from=constraint-dependencies /root/.m2 /root/.m2
COPY ./Constraint-Language/ /build/constraint-language/

WORKDIR /build/constraint-language/ls5.pg646.constraints.parent/
RUN mvn install

FROM maven:3.8.4-openjdk-17 as backend-dependencies
COPY --from=constraint-ide /root/.m2 /root/.m2
COPY ./Gateway/pom.xml /build/gateway/pom.xml
WORKDIR /build/gateway
RUN mvn dependency:resolve

FROM maven:3.8.4-openjdk-17 as backend
COPY --from=backend-dependencies /root/.m2 /root/.m2
COPY ./Constraint-Language /build/constraint-language
WORKDIR /build/constraint-language/ls5.pg646.constraints.parent
RUN cd ls5.pg646.constraints/ && mvn generate-sources && cd ..
RUN mvn install

COPY ./Gateway /build/gateway
COPY --from=frontend /build/frontend/dist/ /build/gateway/src/main/resources/static/
COPY ./Gateway/ /build/gateway/
WORKDIR /build/gateway
# save contents to file
RUN ls -R src/main/resources/static/ > contents.txt
RUN mvn -DskipTests=true package

FROM $BUILD_FROM
ENV LANG C.UTF-8

# install java
RUN apk update && \
  apk add \
  --no-cache \
  --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community \
  openjdk17 && \
  rm -rf /var/cache/apk/*

# check every 15s if container is up & running
HEALTHCHECK --interval=15s --timeout=1s \
  CMD curl -f https://localhost/ || exit 1

# as entrypoint, start our spring app


COPY --from=backend /build/gateway/target /app
WORKDIR /app
RUN cp *.jar app.jar
ENTRYPOINT ["java", "--enable-preview", "-jar","app.jar", "--spring.config.location=classpath:/plugin.properties"]