import { mdiCodeTags, mdiTextBoxSearch } from '@mdi/js'
import { Box, Tab, Tabs, Typography } from '@mui/material'

import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'

import { useErrorHandling } from '@hooks/useAPI'

import FAIcon from '@components/Icon'
import Parsing from '@components/constraints/Parsing'
import DocumentResult from '@components/constraints/validation/DocumentResult'

import { ValidationControllerApi } from '@generated/openapi'

const DocumentDetails = () => {
  const history = useHistory()
  const { id, page } = useParams<{ id: string; page: 'parsing' | 'results' }>()
  const [name, setName] = useState('')

  const indexToPage = {
    0: 'parsing',
    1: 'results',
    parsing: 0,
    results: 1,
  }

  const [selectedTab, setSelectedTab] = useState(indexToPage[page])

  const handleChange = (_: React.SyntheticEvent, newValue: 0 | 1) => {
    history.push(`/document/${id}/${indexToPage[newValue]}`)
    setSelectedTab(newValue)
  }

  const validationApi = new ValidationControllerApi()
  const { call: getDocument } = useErrorHandling(() =>
    validationApi.getUsingGET({ id: +id })
  )

  useEffect(() => {
    getDocument().then(
      ({ result, success }) =>
        success && result?.data && setName(result?.data?.document.name)
    )
  }, [id, page])

  return (
    <>
      <Typography variant="h2">
        Document#{id}: {name}
      </Typography>
      <Box sx={{ width: '100%', marginBottom: 5 }}>
        <Tabs
          value={selectedTab}
          onChange={handleChange}
          aria-label="nav tabs example"
        >
          <Tab
            value={0}
            label="parsing"
            icon={<FAIcon d={mdiCodeTags} sx={{ marginInlineEnd: 1 }} />}
            iconPosition="start"
          />
          <Tab
            value={1}
            label="results"
            icon={<FAIcon d={mdiTextBoxSearch} sx={{ marginInlineEnd: 1 }} />}
            iconPosition="start"
          />
        </Tabs>
      </Box>
      {selectedTab === 0 && <Parsing onNameChanged={(name) => setName(name)} />}
      {selectedTab === 1 && <DocumentResult />}
    </>
  )
}

export default DocumentDetails
