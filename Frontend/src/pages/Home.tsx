import { Grid } from '@mui/material'

import React from 'react'

import AnimatedLogo from '@components/AnimatedLogo'
import DocumentationWidget from '@components/DocumentationWidget'
import CoverageWidget from '@components/constraints/CoverageWidget'
import ValidationStatWidget from '@components/constraints/validation/ValidationStatWidget'
import NotificationChannelWidget from '@components/notifications/NotificationChannelWidget'
import RuntimeErrorsHistoryWidget from '@components/runtimeChecking/RuntimeErrorsHistoryWidget'
import RuntimeErrorsWidget from '@components/runtimeChecking/RuntimeErrorsWidget'

import './Home.css'

const Home = () => {
  return (
    <>
      <Grid container columnSpacing={3}>
        <Grid item xs={12} lg={4} margin="auto">
          <header className="App-header">
            <AnimatedLogo />
          </header>
        </Grid>
        <Grid item xs={12} md={6} lg={4}>
          <CoverageWidget sx={{ height: 420 }} />
        </Grid>
        <Grid item xs={12} md={6} lg={4}>
          <ValidationStatWidget sx={{ height: 420 }} />
        </Grid>
        <Grid item xs={12} lg={6} xl={8}>
          <RuntimeErrorsWidget sx={{ height: 500 }} />
        </Grid>
        <Grid item xs={12} lg={6} xl={4}>
          <RuntimeErrorsHistoryWidget sx={{ height: 500 }} />
        </Grid>
        <Grid item xs={12}>
          <NotificationChannelWidget />
        </Grid>
        <Grid item xs={12}>
          <DocumentationWidget />
        </Grid>
      </Grid>
    </>
  )
}

export default Home
