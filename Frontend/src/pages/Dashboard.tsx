import { WithTheme } from '@emotion/react'
import { mdiChevronLeft, mdiMenu } from '@mdi/js'
import {
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  AppBar as MuiAppBar,
  StyledComponentProps,
  SwipeableDrawer,
  Theme,
  Toolbar,
  Typography,
  useTheme,
} from '@mui/material'
import { styled } from '@mui/material/styles'

import React, { useLayoutEffect, useRef, useState } from 'react'
import { Link } from 'react-router-dom'

import { useContentWidth } from '@contexts/contentWithContext'

import FAIcon from '@components/Icon'
import NotificationInbox from '@components/NotificationInbox'
import Notifications from '@components/Notifications'
import Router, { routes } from '@components/Router'
import ThemeSwitch from '@components/ThemeSwitch'

const drawerWidth = 240

const DrawerHeader = styled('div')(
  (props: WithTheme<StyledComponentProps, Theme>) => ({
    display: 'flex',
    alignItems: 'center',
    padding: props.theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...props.theme.mixins.toolbar,
    justifyContent: 'flex-end',
  })
)

const Main = styled('main')((props: WithTheme<{ open: boolean }, Theme>) => ({
  flexGrow: 1,
  padding: props.theme.spacing(3),
  transition: props.theme.transitions.create('margin', {
    easing: props.theme.transitions.easing.sharp,
    duration: props.theme.transitions.duration.leavingScreen,
  }),
  // necessary for content to be below app bar
  marginTop: props.theme.mixins.toolbar.minHeight,
  marginLeft: 0,
  ...(props.open && {
    transition: props.theme.transitions.create('margin', {
      easing: props.theme.transitions.easing.easeOut,
      duration: props.theme.transitions.duration.enteringScreen,
    }),
    marginLeft: `${drawerWidth}px`,
  }),
}))

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})((props: WithTheme<{ open: boolean }, Theme>) => ({
  transition: props.theme.transitions.create(['margin', 'width'], {
    easing: props.theme.transitions.easing.sharp,
    duration: props.theme.transitions.duration.leavingScreen,
  }),
  ...(props.open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: props.theme.transitions.create(['margin', 'width'], {
      easing: props.theme.transitions.easing.easeOut,
      duration: props.theme.transitions.duration.enteringScreen,
    }),
  }),
  zIndex: 1300,
}))

const BasicLink = styled(Link)`
  text-decoration: none;
  color: ${(t) => t.theme.palette.text.primary};
  :visited {
    color: ${(t) => t.theme.palette.text.primary};
  }
`

const Dashboard = ({
  colorMode,
}: {
  colorMode: {
    mode: 'light' | 'dark'
    toggleColorMode: (mode: 'light' | 'dark') => void
  }
}) => {
  const [open, setOpen] = useState(true)
  const theme = useTheme()

  const { setWidth, setHeight } = useContentWidth()

  const main = useRef<HTMLElement>(null)

  let movement_timer: ReturnType<typeof setTimeout> | null = null

  const RESET_TIMEOUT = 100

  const test_dimensions = () => {
    if (main.current) {
      setWidth(main.current.offsetWidth)
      setHeight(main.current.offsetHeight)
    }
  }

  useLayoutEffect(() => {
    test_dimensions()
  }, [])

  window.addEventListener('resize', () => {
    if (movement_timer) clearInterval(movement_timer)
    movement_timer = setTimeout(test_dimensions, RESET_TIMEOUT)
  })

  return (
    <>
      <AppBar position="fixed" open={open} theme={theme}>
        <Toolbar>
          <IconButton
            onClick={() => setOpen(!open)}
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2, ...(open && { display: 'none' }) }}
          >
            <FAIcon d={mdiMenu} />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <BasicLink to="/">Painless Home Automation</BasicLink>
          </Typography>
          <ThemeSwitch colorMode={colorMode} />
          <NotificationInbox />
        </Toolbar>
      </AppBar>
      <SwipeableDrawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="left"
        open={open}
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
      >
        <DrawerHeader theme={theme}>
          <IconButton onClick={() => setOpen(false)}>
            <FAIcon d={mdiChevronLeft} />
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List>
          {routes
            .filter((r) => !r.hideInMenu)
            .map((r) => (
              <BasicLink to={r.path} key={r.path}>
                <ListItem disablePadding>
                  <ListItemButton>
                    <ListItemIcon>
                      <FAIcon d={r.icon} />
                    </ListItemIcon>
                    <ListItemText primary={r.name} />
                  </ListItemButton>
                </ListItem>
              </BasicLink>
            ))}
        </List>
      </SwipeableDrawer>
      <Notifications />
      <Main
        open={open}
        theme={theme}
        ref={main}
        sx={{
          overflow: 'auto',
          backgroundColor: theme.palette.background.default,
        }}
      >
        <div style={{ width: '100%', minHeight: window.innerHeight - 104 }}>
          <Router />
        </div>
      </Main>
    </>
  )
}

export default Dashboard
