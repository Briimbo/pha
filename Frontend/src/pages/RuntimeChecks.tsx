import { mdiCheckCircle, mdiCloseCircle, mdiEye, mdiEyeOff } from '@mdi/js'
import {
  Card,
  CardContent,
  Grid,
  IconButton,
  Link as MdiLink,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
  keyframes,
  lighten,
} from '@mui/material'

import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import { useErrorHandling } from '@hooks/useAPI'
import useEventSubscription from '@hooks/useEventSubscription'

import FAIcon from '@components/Icon'
import CheckHistory from '@components/runtimeChecking/CheckHistory'

import {
  CheckResult,
  RuntimeCheckControllerApi,
  StructuredConstraintDocumentDto,
  ValidationControllerApi,
} from '@generated/openapi'

const pulse = (colorHex: string) => {
  return keyframes`
    from, 50%, to {
      color: ${colorHex}
    }
    25% {
      color: ${lighten(colorHex, 0.4)}
    }
  `
}

export type DocumentIdMap = { [id: string]: StructuredConstraintDocumentDto }

export const getActiveConstraints = (
  documents: DocumentIdMap,
  constraints: CheckResult[]
) => {
  return constraints.filter(
    (checkResult) =>
      documents[checkResult.docId]?.enabled &&
      documents[checkResult.docId]?.constraints.some(
        (constraint) => constraint.name == checkResult.name
      )
  )
}

const RuntimeChecks = () => {
  const [state, setState] = useState<CheckResult[]>([])
  const [documents, setDocuments] = useState<DocumentIdMap>({})
  const [newResult, setNewResult] = useState<CheckResult>()
  const [lastCheckTime, setLastCheckTime] = useState<Date>()
  const [hideOutdated, setHideOutdated] = useState(true)

  const runtimeApi = new RuntimeCheckControllerApi()
  const { call: getResults } = useErrorHandling(() =>
    runtimeApi.getResultsUsingGET()
  )
  const { call: getLastCheckTime } = useErrorHandling(() =>
    runtimeApi.getLastCheckTimeUsingGET()
  )

  const validationApi = new ValidationControllerApi()
  const { call: getDocuments } = useErrorHandling(() =>
    validationApi.getAllUsingGET()
  )

  const pollState = () => {
    getResults().then(
      ({ success, result }) => success && result?.data && setState(result.data)
    )
    getLastCheckTime().then(
      ({ success, result }) =>
        success && result?.data && setLastCheckTime(new Date(result.data))
    )
    getDocuments().then((res) => {
      if (res.success && res.result?.data) {
        const mapping = Object.fromEntries(
          res.result.data.map((d) => [d.id.toString(), d])
        )
        setDocuments(mapping)
      }
    })
  }

  useEffect(pollState, [])

  useEventSubscription((event) => {
    if (event.type === 'RUNTIME_CHECK') {
      setNewResult(event.data)
    }
  })

  useEffect(() => {
    if (newResult) {
      const idx = state.findIndex(
        (cr) => cr.docId === newResult.docId && cr.name === newResult.name
      )
      if (idx >= 0) {
        const newState = [...state]
        newState[idx] = newResult
        setState(newState)
        setLastCheckTime(new Date(newResult.checkTime))
      } else {
        pollState()
      }
      setNewResult(undefined)
    }
  }, [newResult])

  const getRelevantResults = () => {
    if (!hideOutdated) return state
    return getActiveConstraints(documents, state)
  }

  return (
    <Card>
      <CardContent>
        <Grid
          container
          justifyContent="space-between"
          sx={{ marginBottom: '1rem' }}
        >
          <Grid item>
            <Typography gutterBottom variant="h4" component="span">
              Monitoring
            </Typography>
          </Grid>
          <Grid
            item
            sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}
          >
            <Tooltip
              title={
                hideOutdated
                  ? 'outdated constraints hidden'
                  : 'outdated constraints shown'
              }
            >
              <span>
                <IconButton onClick={() => setHideOutdated(!hideOutdated)}>
                  <FAIcon d={hideOutdated ? mdiEyeOff : mdiEye} />
                </IconButton>
              </span>
            </Tooltip>
            <Typography sx={{ marginInline: 2 }}>
              Last Update: {lastCheckTime?.toLocaleTimeString() ?? 'Never'}
            </Typography>
          </Grid>
        </Grid>
        <TableContainer component={Paper}>
          <Table aria-label="documents">
            <colgroup>
              <col style={{ width: '5%' }} />
              <col style={{ width: '10%' }} />
              <col style={{ width: '70%' }} />
              <col style={{ width: '5%' }} />
              <col style={{ width: '10%' }} />
            </colgroup>
            <TableHead>
              <TableRow>
                <TableCell>Result</TableCell>
                <TableCell>DocumentId</TableCell>
                <TableCell>Constraint</TableCell>
                <TableCell>History</TableCell>
                <TableCell>Failed At</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {state.length > 0
                ? getRelevantResults().map((checkResult) => (
                    <TableRow key={checkResult.docId + '-' + checkResult.name}>
                      <TableCell>
                        <Tooltip
                          title={checkResult.success ? 'Passed' : 'Failed'}
                        >
                          <span
                            defaultValue={
                              lastCheckTime?.toString() /* for synchronization */
                            }
                          >
                            <FAIcon
                              sx={{
                                animation: `${pulse(
                                  checkResult.success ? '#008000' : '#ff0000'
                                )} 3s infinite ease`,
                              }}
                              d={
                                checkResult.success
                                  ? mdiCheckCircle
                                  : mdiCloseCircle
                              }
                            />
                          </span>
                        </Tooltip>
                      </TableCell>
                      <TableCell>
                        {documents[checkResult.docId] && (
                          <MdiLink
                            to={`/document/${checkResult.docId}/parsing`}
                            component={Link}
                          >
                            {`${documents[checkResult.docId].name} (${
                              checkResult.docId
                            })`}
                          </MdiLink>
                        )}
                        {!documents[checkResult.docId] && (
                          <Typography sx={{ fontStyle: 'italic' }}>
                            deleted ({checkResult.docId})
                          </Typography>
                        )}
                      </TableCell>
                      <TableCell>{checkResult.name}</TableCell>
                      <TableCell>
                        <CheckHistory
                          documentId={checkResult.docId}
                          constraintName={checkResult.name}
                        />
                      </TableCell>
                      <TableCell>
                        {checkResult.success
                          ? ''
                          : new Date(checkResult.checkTime).toLocaleString()}
                      </TableCell>
                    </TableRow>
                  ))
                : null}
            </TableBody>
          </Table>
        </TableContainer>
      </CardContent>
    </Card>
  )
}

export default RuntimeChecks
