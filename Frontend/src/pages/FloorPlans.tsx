import { mdiDelete } from '@mdi/js'
import {
  Card,
  CardContent,
  CircularProgress,
  Grid,
  IconButton,
  LinearProgress,
  Link as MdiLink,
  Tooltip,
  Typography,
} from '@mui/material'
import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'

import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import useApi from '@hooks/useAPI'

import { translateError } from '@utils/ErrorTranslator'

import FullWidthAlert from '@components/FullWidthAlert'
import FAIcon from '@components/Icon'
import CreateFloorPlanDialog from '@components/floorplan/CreateFloorPlanDialog'
import DeleteFloorPlanDialog from '@components/floorplan/DeleteFloorPlanDialog'

import { BaseEmptyResponse, FloorPlanDto } from '@generated/openapi'

const FloorPlans = () => {
  const [plans, setPlans] = useState<Array<FloorPlanDto>>([])
  const [error, setError] = useState<string | undefined>(undefined)
  const [deleteTarget, setDeleteTarget] = useState<
    { id: number; name: string; isLoading: boolean } | undefined
  >(undefined)

  const { isLoading, callApi } = useApi<void, Array<FloorPlanDto>>(
    'floor-plan',
    'GET'
  )

  const loadFloorPlans = () => {
    callApi()
      .then((res) => {
        if (res.errorMessage?.length) setError(res.errorMessage.join(', '))
        else if (res.data) setPlans(res.data)
      })
      .catch()
  }

  useEffect(loadFloorPlans, [])

  const deleteFloorPlan = (id?: number, name?: string) => {
    if (!id || !name) return
    setDeleteTarget({ id, name, isLoading: false })
  }

  const onCancelDelete = () => {
    setDeleteTarget(undefined)
  }

  const onConfirmDelete = (
    apiCalls: Array<() => Promise<BaseEmptyResponse>>
  ) => {
    if (!deleteTarget) return

    setDeleteTarget({ ...deleteTarget, isLoading: true })
    Promise.all(apiCalls.map((c) => c())).then((res) => {
      setDeleteTarget(undefined)
      const results = res.flatMap((r) => r.results ?? [])
      const errors = results ? translateError(results).join(', ') : ''
      setError(errors ? errors : undefined)
      loadFloorPlans()
    })
  }

  return (
    <Card>
      <CardContent>
        <DeleteFloorPlanDialog
          open={deleteTarget ? !deleteTarget.isLoading : false}
          target={deleteTarget}
          onCancel={onCancelDelete}
          onConfirmDelete={onConfirmDelete}
        />
        <Grid
          container
          justifyContent="space-between"
          sx={{ marginBottom: '1rem' }}
        >
          <Grid item>
            <Typography gutterBottom variant="h4" component="span">
              Floor Plans
            </Typography>
          </Grid>
          <Grid item>
            <CreateFloorPlanDialog />
          </Grid>
        </Grid>
        <TableContainer component={Paper}>
          <Table aria-label="floor plans">
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell align="right">Floor</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {plans &&
                plans.map((p) => (
                  <TableRow
                    key={p.id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      <MdiLink to={`/floor-plan/${p.id}`} component={Link}>
                        {p.name || '-'}
                      </MdiLink>
                    </TableCell>
                    <TableCell align="right">{p.floor || '-'}</TableCell>
                    <TableCell
                      sx={{ padding: 0, paddingRight: '1rem' }}
                      align="right"
                    >
                      {p.id === deleteTarget?.id && deleteTarget?.isLoading && (
                        <CircularProgress sx={{ marginY: 0 }} />
                      )}
                      {p.id &&
                        (p.id !== deleteTarget?.id ||
                          !deleteTarget?.isLoading) && (
                          <Tooltip title="Delete">
                            <span>
                              <IconButton
                                onClick={() => deleteFloorPlan(p.id, p.name)}
                                size="small"
                                disabled={deleteTarget?.isLoading}
                                sx={{ marginY: 0 }}
                              >
                                <FAIcon sx={{ color: 'red' }} d={mdiDelete} />
                              </IconButton>
                            </span>
                          </Tooltip>
                        )}
                    </TableCell>
                  </TableRow>
                ))}
              {isLoading && (
                <TableRow>
                  <TableCell colSpan={3}>
                    <Box sx={{ width: '100%' }}>
                      <LinearProgress />
                    </Box>
                  </TableCell>
                </TableRow>
              )}
              {error && (
                <TableRow>
                  <TableCell colSpan={3}>
                    <FullWidthAlert severity="error">{error}</FullWidthAlert>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </CardContent>
    </Card>
  )
}

export default FloorPlans
