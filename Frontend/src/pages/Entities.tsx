import {
  Card,
  CardContent,
  LinearProgress,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material'
import { EntityState } from 'generated/openapi'

import React, { useEffect, useState } from 'react'

import useApi from '@hooks/useAPI'

import { entityToIcon } from '@utils/entityIconMapper'

import FullWidthAlert from '@components/FullWidthAlert'
import FAIcon from '@components/Icon'

const Entities = () => {
  const [error, setError] = useState<string | undefined>()
  const [entities, setEntities] = useState<Array<EntityState>>([])

  const { callApi: loadEntities, isLoading: isLoadingEntities } = useApi<
    undefined,
    Array<EntityState>
  >('ha/entities', 'GET')

  useEffect(() => {
    loadEntities(undefined).then(({ data, errorMessage }) => {
      if (errorMessage?.length) setError(errorMessage.join(', '))
      if (data) setEntities(data)
    })
  }, [])

  return (
    <Card>
      <CardContent>
        <Typography gutterBottom variant="h4" component="div">
          Entities
        </Typography>
        <TableContainer component={Paper}>
          <Table aria-label="entities">
            <TableHead>
              <TableRow>
                <TableCell>Type</TableCell>
                <TableCell>Friendly Name</TableCell>
                <TableCell>ID</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {isLoadingEntities && (
                <TableRow>
                  <TableCell colSpan={3}>
                    <LinearProgress />
                  </TableCell>
                </TableRow>
              )}
              {!isLoadingEntities &&
                entities &&
                entities.map((row) => (
                  <TableRow
                    key={row.entityId}
                    sx={{
                      '&:last-child td, &:last-child th': {
                        border: 0,
                      },
                    }}
                  >
                    <TableCell component="th" scope="row">
                      <FAIcon d={entityToIcon(row)} />
                    </TableCell>
                    <TableCell scope="row">
                      {row.attributes.friendlyName || '-'}
                    </TableCell>
                    <TableCell scope="row">{row.entityId}</TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
        {error && (
          <FullWidthAlert variant="filled" severity="error">
            {error}
          </FullWidthAlert>
        )}
      </CardContent>
    </Card>
  )
}

export default Entities
