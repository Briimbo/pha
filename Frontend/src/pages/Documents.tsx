import {
  mdiCheckCircle,
  mdiCloseCircle,
  mdiFastForward,
  mdiHelpCircle,
  mdiPencil,
  mdiPencilOff,
} from '@mdi/js'
import {
  Box,
  Card,
  CardContent,
  Grid,
  IconButton,
  LinearProgress,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from '@mui/material'
import { SxProps } from '@mui/material/styles'

import React, { useEffect, useState } from 'react'

import useEventSubscription from '@hooks/useEventSubscription'

import { defaultFetchError, translateError } from '@utils/ErrorTranslator'

import FullWidthAlert from '@components/FullWidthAlert'
import FAIcon from '@components/Icon'
import CreateDocumentDialog from '@components/constraints/documentOverview/CreateDocumentDialog'
import DocumentRow from '@components/constraints/documentOverview/DocumentRow'

import {
  ConstraintDto,
  StructuredConstraintDocumentDto,
  ValidationControllerApi,
  ValidationState,
  ValidationStateStateEnum,
} from '@generated/openapi'

const getStateIcon = (state: ValidationStateStateEnum, sx?: SxProps) => {
  let data = { title: '', color: '', icon: '' }
  switch (state) {
    case 'SUCCESS':
      data = { title: 'Passed', color: 'green', icon: mdiCheckCircle }
      break
    case 'FAIL':
      data = { title: 'Failed', color: 'red', icon: mdiCloseCircle }
      break
    case 'IN_PROGRESS':
      data = { title: 'In Progress', color: 'lightBlue', icon: mdiHelpCircle }
      break
    case 'UNKNOWN':
      data = { title: 'Unknown', color: 'lightBlue', icon: mdiHelpCircle }
      break
    default:
      data = { title: 'Unknown', color: 'lightBlue', icon: mdiHelpCircle }
  }
  return (
    <Tooltip title={data.title}>
      <span>
        <FAIcon
          sx={{ color: data.color, display: 'block', ...sx }}
          d={data.icon}
        />
      </span>
    </Tooltip>
  )
}

type ValidationStates = {
  [documentId: number]: ValidationState[]
}

const Documents = () => {
  const [error, setError] = useState<string | undefined>(undefined)
  const [documents, setDocuments] = useState<StructuredConstraintDocumentDto[]>(
    []
  )
  const [validationStates, setValidationStates] = useState<ValidationStates>({})
  const [isLoadingDocuments, setLoadingDocuments] = useState(false)
  const [isEditEnabled, setEditEnabled] = useState(false)

  const validationApi = new ValidationControllerApi()

  useEventSubscription((e) => {
    if (e.type === 'VALIDATION_STATE_UPDATE') {
      const newConstraintStates = Object.keys(e.data).map((key) => ({
        constraint: e.data[key],
        dId: Number.parseInt(key.split('#')[0]),
        cName: key.split('#').slice(1).join(''),
      }))

      setValidationStates((prev) => {
        const newState = { ...prev }

        newConstraintStates.forEach((newConstraint) => {
          newState[newConstraint.dId] = [
            ...(newState[newConstraint.dId]
              ? newState[newConstraint.dId].filter(
                  (c) => c.constraint.name !== newConstraint.cName
                )
              : []),
            newConstraint.constraint,
          ]
        })

        return newState
      })
    }
  })

  const pollValidationState = async (constraints: ConstraintDto[]) => {
    validationApi
      .pollUsingPUT({ constraintDto: constraints })
      .then(({ data }) => {
        if (data && data.length) {
          setValidationStates((prevState) => ({
            ...prevState,
            [data[0].constraint.documentId]: data,
          }))
        }
      })
      .catch(() => setError(defaultFetchError))
  }

  useEffect(() => {
    documents.forEach((d) => pollValidationState(d.constraints))
  }, [documents])

  const loadDocuments = () => {
    setLoadingDocuments(true)
    validationApi
      .getAllUsingGET()
      .then(({ data, results }) => {
        const errors = results ? translateError(results).join(', ') : ''
        setError(errors)
        if (data) setDocuments(data)
      })
      .catch(() => setError(defaultFetchError))
      .finally(() => setLoadingDocuments(false))
  }

  const onCheckDocument = (document: StructuredConstraintDocumentDto) => {
    validationApi
      .validateUsingPUT({ constraintDto: document.constraints })
      .then(({ data, results }) => {
        const errors = results ? translateError(results).join(', ') : ''
        setError(errors)
        if (data) {
          setValidationStates((prevState) => ({
            ...prevState,
            [document.id]: data,
          }))
        }
      })
      .catch(() => setError(defaultFetchError))
  }

  const onCheckAll = () => {
    documents.filter((d) => d.enabled).forEach(onCheckDocument)
  }

  useEffect(loadDocuments, [])

  return (
    <Card>
      <CardContent>
        <Grid
          container
          justifyContent="space-between"
          sx={{ marginBottom: '1rem' }}
        >
          <Grid item>
            <Typography gutterBottom variant="h4" component="span">
              Constraint Documents
            </Typography>
          </Grid>
          <Grid item>
            <Tooltip title="Check all">
              <span>
                <IconButton onClick={onCheckAll} size="small" disabled={false}>
                  <FAIcon sx={{ color: 'green' }} d={mdiFastForward} />
                </IconButton>
              </span>
            </Tooltip>
            <Tooltip title="Edit mode">
              <span>
                <IconButton
                  onClick={() => setEditEnabled(!isEditEnabled)}
                  size="small"
                  disabled={false}
                >
                  <FAIcon
                    sx={(theme) =>
                      isEditEnabled
                        ? {
                            backgroundColor: theme.palette.primary.main,
                            color: 'white',
                            borderRadius: 1,
                          }
                        : {}
                    }
                    d={isEditEnabled ? mdiPencil : mdiPencilOff}
                  />
                </IconButton>
              </span>
            </Tooltip>
            <CreateDocumentDialog />
          </Grid>
        </Grid>
        <TableContainer component={Paper}>
          <Table aria-label="documents">
            <colgroup>
              <col style={{ width: '5%' }} />
              <col style={{ width: '5%' }} />
              <col style={{ width: '5%' }} />
              <col style={{ width: '5%' }} />
              <col style={{ width: '60%' }} />
              <col style={{ width: '10%' }} />
              <col style={{ width: '10%' }} />
            </colgroup>
            <TableHead>
              <TableRow>
                <TableCell />
                <TableCell>Enabled</TableCell>
                <TableCell>Result</TableCell>
                <TableCell>ID</TableCell>
                <TableCell>Name</TableCell>
                <TableCell>State</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {documents &&
                documents
                  .sort((a, b) => a.name.localeCompare(b.name))
                  .map((doc) => (
                    <DocumentRow
                      key={doc.id}
                      doc={doc}
                      onRequestFinished={(error) => {
                        if (error) setError(error)
                        else loadDocuments()
                      }}
                      onCheckDocument={() => onCheckDocument(doc)}
                      validationState={validationStates[doc.id]}
                      isEditEnabled={isEditEnabled}
                    />
                  ))}
              {isLoadingDocuments && (
                <TableRow>
                  <TableCell colSpan={7}>
                    <Box sx={{ width: '100%' }}>
                      <LinearProgress />
                    </Box>
                  </TableCell>
                </TableRow>
              )}
              {error ? (
                <TableRow>
                  <TableCell colSpan={7}>
                    <FullWidthAlert severity="error">{error}</FullWidthAlert>
                  </TableCell>
                </TableRow>
              ) : null}
            </TableBody>
          </Table>
        </TableContainer>
      </CardContent>
    </Card>
  )
}

export { getStateIcon }
export default Documents
