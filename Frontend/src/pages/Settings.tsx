import { Grid } from '@mui/material'

import React from 'react'

import HAConfigEdit from '@components/HAConfigEdit'

const Settings = () => {
  return (
    <Grid container direction="column" gap="1rem">
      <Grid item>
        <HAConfigEdit />
      </Grid>
    </Grid>
  )
}

export default Settings
