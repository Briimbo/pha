import { mdiAlert, mdiAxisArrow, mdiCheck, mdiPlus } from '@mdi/js'
import {
  Backdrop,
  Box,
  Button,
  Checkbox,
  CircularProgress,
  Divider,
  Drawer,
  FormControlLabel,
  FormGroup,
  Switch,
  ThemeProvider,
  Toolbar,
  Tooltip,
  Typography,
  useTheme,
} from '@mui/material'
import { withStyles } from '@mui/styles'
import Konva from 'konva'

import React, { useEffect, useReducer, useState } from 'react'
import { Layer, Stage } from 'react-konva'
import { useParams } from 'react-router'

import useApi from '@hooks/useAPI'

import { getGroups } from '@utils/GroupAssignment'
import calculateContentSize from '@utils/contentSizeCalc'

import { useContentWidth } from '@contexts/contentWithContext'
import { useNotifications } from '@contexts/notificationContext'

import {
  FloorPlanState,
  initialState,
  reducer,
} from '@pages/reducers/floorPlanReducer'

import FAIcon from '@components/Icon'
import BackgroundLayer from '@components/floorplan/BackgroundLayer'
import BackgroundUpload from '@components/floorplan/BackgroundOptions'
import EditRoomDialog from '@components/floorplan/EditRoomDialog'
import EntityLayer from '@components/floorplan/EntityLayer'
import EntityList from '@components/floorplan/EntityList'
import { FloorPlanExportPortal } from '@components/floorplan/FloorPlanExportPortal'
import FloorPlanSettingsDialog from '@components/floorplan/FloorPlanSettingsDialog'
import RemoveEntityContextMenu from '@components/floorplan/RemoveEntityContextMenu'
import {
  RoomProps,
  getDefaultPolyline,
  getDefaultRoom,
} from '@components/floorplan/Room'
import RoomLayer from '@components/floorplan/RoomLayer'
import {
  ScaleAxes,
  ScaleAxesCheckbox,
  ScaleButton,
  handleDrawScaleClick,
  handleDrawScaleMouseMove,
} from '@components/floorplan/ScaleAxes'

import { ExportGroupRequest, FloorPlanDto } from '@generated/openapi'

const LimitedBackdrop = withStyles({
  root: {
    position: 'absolute',
    zIndex: 5,
  },
})(Backdrop)

const FloorPlan = () => {
  const { id } = useParams<{ id: string }>()

  const [state, dispatch] = useReducer(reducer, initialState)

  const { state: contentSizeState } = useContentWidth()
  const [canvasSize, setCanvasSize] = useState({ ...contentSizeState })
  const theme = useTheme()

  useEffect(() => {
    const size = calculateContentSize(state, contentSizeState, 350, 60)
    setCanvasSize(size)
  }, [state, contentSizeState])

  const { addNotification } = useNotifications()

  const getRoomAndIndex = () => {
    const { rooms } = state

    // Find the room and its index by *room.shape.current*
    const theRoomIdx = rooms.findIndex(
      (room) => room.shape.type === 'polyline' && room.shape.current
    )
    const theRoom = theRoomIdx > -1 ? rooms[theRoomIdx] : undefined
    return { theRoomIdx, theRoom }
  }

  const handleMouseDown = (event: Konva.KonvaEventObject<MouseEvent>) => {
    if (state.scale.drawScale) return

    const { drawRooms, usePolyline } = state
    // If we want to draw rooms, but no polyline, we opt for a rectangle
    if (drawRooms && !usePolyline) {
      const room = getDefaultRoom()
      if (room.shape.type === 'rectangle') {
        const pointerPos =
          event?.target?.getStage()?.getPointerPosition() || null
        if (pointerPos) {
          room.shape.x = pointerPos.x
          room.shape.y = pointerPos.y
          room.shape.height = 0
          room.shape.width = 0
          dispatch({
            type: 'updateValue',
            updObject: {
              name: 'newRoom',
              value: room,
            },
          })
        }
      }
    }
  }

  const handleMouseUp = (event: Konva.KonvaEventObject<MouseEvent>) => {
    const { drawRooms: drawRooms, usePolyline, newRoom } = state
    // If we want to draw rooms, but no polyline, we opt for a rectangle
    if (drawRooms && !usePolyline && newRoom) {
      const theRoom = { ...newRoom }
      if (theRoom.shape.type === 'rectangle') {
        const sx = theRoom.shape.x
        const sy = theRoom.shape.y
        const pointerPos =
          event?.target?.getStage()?.getPointerPosition() || null
        if (
          pointerPos &&
          pointerPos.x !== theRoom.shape.x &&
          pointerPos.y !== theRoom.shape.y
        ) {
          // if width or height are negative, the starting point is not the top left corner
          const width = pointerPos.x - sx
          const height = pointerPos.y - sy
          if (width < 0) {
            theRoom.shape.x = pointerPos.x
          }
          if (height < 0) {
            theRoom.shape.y = pointerPos.y
          }

          theRoom.shape.width = Math.abs(width)
          theRoom.shape.height = Math.abs(height)

          dispatch({
            type: 'addRoom',
            room: {
              ...theRoom,
              shape: { ...theRoom.shape },
            },
          })
        }
      }
      dispatch({
        type: 'updateValue',
        updObject: {
          name: 'newRoom',
          value: undefined,
        },
      })
    }
  }

  const handleMouseMove = (event: Konva.KonvaEventObject<MouseEvent>) => {
    if (state.scale.drawScale) {
      handleDrawScaleMouseMove(event, state.scale, dispatch)
      return
    }

    const { newRoom } = state
    if (newRoom && newRoom.shape.type === 'rectangle') {
      const sx = newRoom.shape.x
      const sy = newRoom.shape.y
      const pointerPos = event?.target?.getStage()?.getPointerPosition() || null
      if (pointerPos) {
        newRoom.shape.width = pointerPos.x - sx
        newRoom.shape.height = pointerPos.y - sy
        dispatch({
          type: 'updateValue',
          updObject: {
            name: 'newRoom',
            value: {
              ...newRoom,
              shape: { ...newRoom.shape },
            },
          },
        })
      }
    }
  }

  /**
   *
   * @param event the Konva mouse event
   * @returns
   */
  const handleOnClick = (event: Konva.KonvaEventObject<MouseEvent>) => {
    if (state.scale.drawScale) {
      handleDrawScaleClick(event, state.scale, dispatch)
      return
    }

    const { drawRooms, usePolyline } = state
    // decides if a rectangle or polyline should be drawn
    const drawPolyline = drawRooms && usePolyline

    // deselect nodes when an empty area is clicked, that is, if we do not want to draw rooms
    if (event.target === event.target.getStage() && !drawPolyline) {
      dispatch({ type: 'setSelection', selection: { entities: [], rooms: [] } })
    }

    // If the options for drawing a polyline are not checked, we do nothing
    if (!drawPolyline) {
      return
    }

    const { theRoomIdx, theRoom } = getRoomAndIndex()
    const mousePosition = getMousePosition(event)

    // If we know the room and its type is polyline, we can edit it
    if (theRoom && theRoom.shape.type === 'polyline') {
      const theShape = theRoom.shape
      // if the shape is finished, do nothing
      if (theShape.isFinished) {
        return
      }

      // If the mouse is over the starting point of the polygon and we have at least three points
      // for the shape, we can consider the shape as finished
      if (theShape.mouseOverStart && theShape.points?.length >= 3) {
        dispatch({
          type: 'updateRoom',
          idx: theRoomIdx,
          room: {
            ...theRoom,
            shape: { ...theRoom.shape, isFinished: true, current: false },
          },
          shiftx: 0,
          shifty: 0,
        })
      } else if (mousePosition) {
        // If the shape is not finished, we can add the mouse position to the points of the shape
        const points = theShape.points

        dispatch({
          type: 'updateRoom',
          idx: theRoomIdx,
          room: {
            ...theRoom,
            shape: {
              ...theShape,
              points: [...points, mousePosition],
            },
          },
          shiftx: 0,
          shifty: 0,
        })
      }
    } else {
      // If we could not find a room which is currently drawn, we add a new one
      const room = getDefaultPolyline()

      // Typescript is unhappy, if we don't check the type of the shape,
      // even though we know that getDefaultPolyline can only return shapes of type polyline
      if (room.shape.type === 'polyline' && mousePosition) {
        // The first point is now known, so we can set it
        const points = [mousePosition]
        room.shape.points = points

        dispatch({ type: 'addRoom', room })
      }
    }
  }

  /**
   *
   * @param event the Konva mouse event
   * @returns the current mouse position, returns nothing if the state is invalid
   *
   * gets the current mouse position of an event on the stage
   */
  const getMousePosition = (event: Konva.KonvaEventObject<MouseEvent>) => {
    if (!state.drawRooms && !state.usePolyline) {
      return
    }

    const stage = event.target.getStage()
    if (stage && stage.getPointerPosition()) {
      const pointerPos = stage.getPointerPosition()
      if (pointerPos && pointerPos.x && pointerPos.y) {
        return { x: pointerPos.x, y: pointerPos.y }
      }
    }
  }

  const [isFloorPlanExporting, setFloorPlanExporting] = useState(false)
  const onExportFloorPlan = () => {
    setFloorPlanExporting(true)
  }

  const { callApi: exportGroups } = useApi<ExportGroupRequest, undefined>(
    'ha/groups',
    'POST'
  )

  const onExportGroups = () => {
    exportGroups({
      floorId: currentDBVersion?.id || 1,
      floorName: currentDBVersion?.name || 'default',
      groups: getGroups(state.rooms, state.entities).map((g) => ({
        ...g,
        entities: g.entities.map((e) => e.entityId),
      })),
    }).then((res) => {
      if (!res.errorMessage)
        addNotification({
          type: 'success',
          message: 'Successfully exported groups',
        })
    })
  }

  const { callApi: saveFloorPlan } = useApi<FloorPlanDto, FloorPlanDto>(
    'floor-plan',
    'POST'
  )

  const [isLoading, setLoading] = useState(true)
  const [initialLoad, setInitialLoad] = useState(true)
  const [currentDBVersion, setCurrentDBVersion] = useState<
    FloorPlanDto | undefined
  >(undefined)

  const onSave = () => {
    setLoading(true)
    saveFloorPlan({ ...currentDBVersion, floorPlanData: JSON.stringify(state) })
      .then((newVersion) => {
        setCurrentDBVersion(newVersion.data)
        dispatch({ type: 'setUnsavedChanges', unsavedChanges: false })
      })
      .catch()
      .finally(() => setLoading(false))
  }

  const { callApi: loadFloorPlan } = useApi<undefined, FloorPlanDto>(
    `floor-plan/${id}`,
    'GET'
  )

  useEffect(() => {
    setLoading(true)
    loadFloorPlan(undefined)
      .then((res) => {
        if (res.data) {
          setCurrentDBVersion(res.data)
          if (res.data.floorPlanData) {
            const newState = JSON.parse(
              res.data.floorPlanData
            ) as FloorPlanState
            dispatch({ type: 'loadState', state: newState })
          }
        }
      })
      .finally(() => {
        setLoading(false)
        setInitialLoad(false)
      })
  }, [])

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      if (initialLoad) return
      onSave()
    }, 3000)

    return () => clearTimeout(delayDebounceFn)
  }, [state.rooms, state.background, state.entities])

  return (
    <>
      <FloorPlanExportPortal
        floorPlanState={state}
        doExport={isFloorPlanExporting}
        onExportFinished={() => setFloorPlanExporting(false)}
        currentDBVersion={currentDBVersion}
      />
      <LimitedBackdrop open={initialLoad}>
        <CircularProgress color="inherit" />
      </LimitedBackdrop>
      <Drawer
        variant="permanent"
        anchor="right"
        sx={{
          width: 300,
          flexShrink: 0,
          [`& .MuiDrawer-paper`]: { width: 300, boxSizing: 'border-box' },
        }}
      >
        <LimitedBackdrop open={initialLoad} />
        <Toolbar />
        <Box
          sx={{
            overflow: 'hidden',
            gap: '.5rem',
            display: 'flex',
            flexDirection: 'column',
            margin: '.5rem',
          }}
        >
          <Typography variant="h4" gutterBottom component="div">
            {'Floorplan ' + (currentDBVersion?.name ?? 'unknown')}
          </Typography>
          {currentDBVersion && (
            <FloorPlanSettingsDialog
              currentFloorPlan={currentDBVersion}
              save={(f) => setCurrentDBVersion(f)}
            />
          )}
          <ScaleButton
            scale={state.scale}
            dispatch={dispatch}
            icon={mdiAxisArrow}
          />
          <Button onClick={onExportFloorPlan} variant="contained">
            Export Floor Plan
          </Button>
          <Button onClick={onExportGroups} variant="contained">
            Export Groups
          </Button>
          <Button
            onClick={() =>
              dispatch({ type: 'addRoom', room: getDefaultRoom() })
            }
            variant="contained"
          >
            <FAIcon d={mdiPlus} />
            add room
          </Button>
          <BackgroundUpload
            backgroundProps={state.background}
            updateBackground={(bg) =>
              dispatch({
                type: 'updateBackground',
                bg,
              })
            }
          />
          <FormGroup>
            <FormControlLabel
              control={
                <Switch
                  checked={state.drawRooms}
                  onChange={(e) =>
                    dispatch({
                      type: 'updateValue',
                      updObject: {
                        name: 'drawRooms',
                        value: e.target.checked,
                      },
                    })
                  }
                />
              }
              label="draw rooms"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={state.usePolyline}
                  onChange={(e) => {
                    dispatch({
                      type: 'updateValue',
                      updObject: {
                        name: 'usePolyline',
                        value: e.target.checked,
                      },
                    })
                    if (e.target.checked) {
                      dispatch({
                        type: 'updateValue',
                        updObject: {
                          name: 'drawRooms',
                          value: true,
                        },
                      })
                    }
                  }}
                />
              }
              label="draw polyline"
            />
            <ScaleAxesCheckbox scale={state.scale} dispatch={dispatch} />
          </FormGroup>
        </Box>
        <Divider />
        <Box
          sx={{
            overflow: 'auto',
            display: 'flex',
            gap: '1rem',
            flexDirection: 'column',
            flex: 1,
          }}
        >
          <EntityList inputState={state} dispatch={dispatch} />
        </Box>
      </Drawer>
      <div
        style={{
          width: (contentSizeState.width || window.innerWidth) - 330,
          height: (contentSizeState.height || window.innerHeight) - 40,
          overflow: 'scroll',
        }}
      >
        <Stage
          onMouseDown={handleMouseDown}
          onMouseUp={handleMouseUp}
          onMouseMove={handleMouseMove}
          onClick={handleOnClick}
          style={state.scale.drawScale ? { cursor: 'crosshair' } : undefined}
          {...canvasSize}
        >
          <ThemeProvider theme={theme}>
            <Layer>
              {state.background && (
                <BackgroundLayer
                  shape={state.background}
                  updateBackground={(bg) =>
                    dispatch({ type: 'updateBackground', bg })
                  }
                  listening={!state.scale.drawScale}
                />
              )}
            </Layer>
            <Layer>
              <RoomLayer
                rooms={
                  state.newRoom ? [...state.rooms, state.newRoom] : state.rooms
                }
                selectedRooms={state.selectedRooms}
                dispatch={dispatch}
                drawRooms={state.drawRooms}
                usePolyline={state.usePolyline}
                listening={!state.scale.drawScale}
              />
            </Layer>
            <Layer name="entityLayer" listening={!state.scale.drawScale}>
              <EntityLayer
                entities={state.entities}
                selectedEntities={state.selectedEntities}
                dispatch={dispatch}
                drawRooms={state.drawRooms}
                listening={!state.scale.drawScale}
              />
            </Layer>
            <ScaleAxes {...canvasSize} scaleData={state.scale} />
          </ThemeProvider>
        </Stage>
      </div>
      <Box
        sx={{ position: 'fixed', right: '310px', bottom: 0, cursor: 'pointer' }}
      >
        <Tooltip title="click to save">
          <Typography variant="caption" onClick={onSave}>
            {isLoading && (
              <>
                <span>{initialLoad ? 'Loading...' : 'Saving...'}</span>{' '}
                <CircularProgress size="1rem" sx={{ marginLeft: '.2rem' }} />
              </>
            )}
            {!isLoading && (
              <>
                <span>
                  {state.unsavedChanges ? 'Unsaved Changes' : 'Saved'}
                </span>
                <FAIcon
                  sx={{ fontSize: '1rem', marginLeft: '.2rem' }}
                  d={state.unsavedChanges ? mdiAlert : mdiCheck}
                />
              </>
            )}
          </Typography>
        </Tooltip>
      </Box>
      {state.editDialogRoom.room && (
        <EditRoomDialog
          room={state.editDialogRoom.room}
          existingRooms={state.rooms
            .filter((_, idx) => idx !== state.editDialogRoom.idx)
            .map((r) => r.name)}
          onSave={(room: RoomProps) =>
            dispatch({
              type: 'updateRoom',
              idx: state.editDialogRoom.idx,
              room,
              shiftx: 0,
              shifty: 0,
            })
          }
          onDelete={() =>
            dispatch({
              type: 'removeRoom',
              idx: state.editDialogRoom.idx,
            })
          }
          onClose={() => dispatch({ type: 'setEditDialogRoom', id: '' })}
        />
      )}
      {state.removeEntityDialog.entity && (
        <RemoveEntityContextMenu
          entity={state.removeEntityDialog.entity}
          onDelete={(entity) =>
            dispatch({
              type: 'removeEntity',
              idx: state.removeEntityDialog.idx,
              entity,
            })
          }
          onClose={() => dispatch({ type: 'setRemoveEntityDialog', id: '' })}
        />
      )}
    </>
  )
}

export default FloorPlan
