import { FloorPlanState } from '@pages/reducers/floorPlanReducer'

import { EntityProps } from '@components/floorplan/Entity'
import { Point, RoomProps } from '@components/floorplan/Room'

export const update = (
  newState: FloorPlanState,
  stateObjName: 'rooms' | 'entities',
  objToUpdate: RoomProps | EntityProps,
  aIdx: number,
  shiftx: number,
  shifty: number
) => {
  // simple case: we only need to update the object itself
  // that happens, when no shift values are set
  if (!shiftx && !shifty) {
    const newList = [...newState[stateObjName]]
    // update the changed object
    newList[aIdx] = shiftObjectIfNecessary(objToUpdate)
    newState = { ...newState, [stateObjName]: newList, unsavedChanges: true }
    return newState
  }

  // more difficult case: a selected object has been moved, so we need to update all selected entities and rooms
  const newRoomList = updateRooms(newState, objToUpdate, aIdx, shiftx, shifty)
  const newEntityList = updateEntities(
    newState,
    objToUpdate,
    aIdx,
    shiftx,
    shifty
  )

  newState = {
    ...newState,
    entities: newEntityList,
    rooms: newRoomList,
    unsavedChanges: true,
  }
  return newState
}

const updateRooms = (
  newState: FloorPlanState,
  objToUpdate: RoomProps | EntityProps,
  aIdx: number,
  shiftx: number,
  shifty: number
) => {
  // update selectedRooms
  const newRoomList = [...newState.rooms]
  newState.selectedRooms.forEach((id) => {
    // if ids are unequal we have to update this object. Else it has already been updated
    // and we do nothing except for setting the new object
    if (id !== objToUpdate.id) {
      const theRoomIndex = newState.rooms.findIndex((e) => e.id === id)
      if (theRoomIndex > -1) {
        const theRoom = newRoomList[theRoomIndex]
        const { shape, type } = theRoom
        // special case: polyline
        if (type === 'room' && shape.type === 'polyline') {
          let newPoints = shape.points.map(({ x, y }) => {
            return { x: x + shiftx, y: y + shifty }
          })
          newPoints = shiftPolyIfNecessary(newPoints)
          newRoomList[theRoomIndex] = {
            ...theRoom,
            shape: { ...shape, points: newPoints },
          }
        } else if (type === 'room' && shape.type === 'rectangle') {
          newRoomList[theRoomIndex] = {
            ...theRoom,
            shape: {
              ...shape,
              x: shape.x + shiftx < 0 ? 0 : shape.x + shiftx,
              y: shape.y + shifty < 0 ? 0 : shape.y + shifty,
            },
          }
        }
      }
    } else if (id === objToUpdate.id && objToUpdate.type === 'room') {
      newRoomList[aIdx] = shiftObjectIfNecessary(objToUpdate) as RoomProps
    }
  })
  return newRoomList
}

const updateEntities = (
  newState: FloorPlanState,
  objToUpdate: RoomProps | EntityProps,
  aIdx: number,
  shiftx: number,
  shifty: number
) => {
  const newEntityList = [...newState.entities]
  // update selectedEntities
  newState.selectedEntities.forEach((id) => {
    // if ids are unequal we have to update this object. Else it has already been updated
    // and we do nothing except for setting the new object
    if (id !== objToUpdate.id) {
      const theEntityIndex = newState.entities.findIndex((e) => e.id === id)
      if (theEntityIndex > -1) {
        const theEntity = newEntityList[theEntityIndex]
        newEntityList[theEntityIndex] = {
          ...theEntity,
          x: theEntity.x + shiftx < 0 ? 0 : theEntity.x + shiftx,
          y: theEntity.y + shifty < 0 ? 0 : theEntity.y + shifty,
        }
      }
    } else if (id === objToUpdate.id && objToUpdate.type === 'entity') {
      newEntityList[aIdx] = shiftObjectIfNecessary(objToUpdate) as EntityProps
    }
  })
  return newEntityList
}

const getPolylineMin = (points: Point[], coordinate: 'x' | 'y') => {
  const vals: number[] = []
  points.forEach((elem) => vals.push(elem[coordinate]))
  if (vals.length > 0) {
    const minVal = Math.min(...vals)
    return minVal
  }
  return 0
}

const shiftPolylineToEdge = (points: Point[]) => {
  if (points.length === 0) {
    return points
  }

  const xDistToMove = getPolylineMin(points, 'x')
  const yDistToMove = getPolylineMin(points, 'y')
  let newPoints = [...points]
  if (xDistToMove < 0) {
    newPoints = newPoints.map((elem) => {
      const newX = elem.x + Math.abs(xDistToMove)
      return { ...elem, x: newX }
    })
  }
  if (yDistToMove < 0) {
    newPoints = newPoints.map((elem) => {
      const newY = elem.y + Math.abs(yDistToMove)
      return { ...elem, y: newY }
    })
  }

  return newPoints
}

const shiftPolyIfNecessary = (points: Point[]) => {
  let newPoints = [...points]
  if (getPolylineMin(points, 'x') < 0 || getPolylineMin(points, 'y') < 0) {
    newPoints = shiftPolylineToEdge(points)
  }
  return newPoints
}

const shiftObjectIfNecessary = (objToUpdate: RoomProps | EntityProps) => {
  let theObj = { ...objToUpdate }
  if (theObj.type === 'room' && theObj.shape.type === 'polyline') {
    const newPoints = shiftPolyIfNecessary(theObj.shape.points)
    theObj = { ...theObj, shape: { ...theObj.shape, points: newPoints } }
  } else if (theObj.type === 'room' && theObj.shape.type === 'rectangle') {
    const { shape } = theObj
    theObj = {
      ...theObj,
      shape: {
        ...shape,
        x: shape.x <= 0 ? 0 : shape.x,
        y: shape.y <= 0 ? 0 : shape.y,
      },
    }
  } else if (theObj.type === 'entity') {
    const { x, y } = theObj
    theObj = {
      ...theObj,
      x: x <= 0 ? 0 : x,
      y: y <= 0 ? 0 : y,
    }
  }
  return theObj
}

export const remove = (
  newState: FloorPlanState,
  stateObjName: 'rooms' | 'entities',
  aIdx: number
) => {
  newState[stateObjName].splice(aIdx, 1)
  newState = {
    ...newState,
    [stateObjName]: [...newState[stateObjName]],
    unsavedChanges: true,
    ...(stateObjName === 'rooms' ? { selectedRooms: [] } : {}),
  }

  return newState
}

export const toggleSelect = (
  newState: FloorPlanState,
  id: string,
  type: 'selectedRooms' | 'selectedEntities'
) => {
  const isSelected = newState[type].includes(id)
  newState = {
    ...newState,
    [type]: isSelected
      ? newState[type].filter((i) => i !== id)
      : [...newState[type], id],
  }
  return newState
}

type setRoomsDialogType = {
  type: 'rooms'
  objToUpd: 'editDialogRoom'
  objValName: 'room'
}
export const setRoomsDialogObj: setRoomsDialogType = {
  type: 'rooms',
  objToUpd: 'editDialogRoom',
  objValName: 'room',
}

type setEntitiesDialogType = {
  type: 'entities'
  objToUpd: 'removeEntityDialog'
  objValName: 'entity'
}
export const setEntitiesDialogObj: setEntitiesDialogType = {
  type: 'entities',
  objToUpd: 'removeEntityDialog',
  objValName: 'entity',
}

export const setDialog = (
  newState: FloorPlanState,
  id: string,
  typeObj: setRoomsDialogType | setEntitiesDialogType
) => {
  const { type, objToUpd, objValName } = typeObj
  const idx = newState[type].findIndex((r) => r.id === id)
  newState = {
    ...newState,
    ...(type === 'rooms' ? { selectedRooms: [id] } : {}),
    [objToUpd]: {
      [objValName]: idx >= 0 ? newState[type][idx] : undefined,
      idx,
    },
  }
  return newState
}
