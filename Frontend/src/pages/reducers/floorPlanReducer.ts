import { exhaustiveCheck } from '@utils/exhaustiveCheck'

import {
  remove,
  setDialog,
  setEntitiesDialogObj,
  setRoomsDialogObj,
  toggleSelect,
  update,
} from '@pages/reducers/helper/floorPlanReducerHelper'

import { BackgroundProps } from '@components/floorplan/BackgroundLayer'
import { EntityProps } from '@components/floorplan/Entity'
import { RoomProps } from '@components/floorplan/Room'
import { ScaleData } from '@components/floorplan/ScaleAxes'

export type FloorPlanActions =
  | { type: 'addRoom'; room: RoomProps }
  | {
      type: 'updateRoom'
      idx: number
      room: RoomProps
      shiftx: number
      shifty: number
    }
  | { type: 'removeRoom'; idx: number }
  | { type: 'setRooms'; rooms: Array<RoomProps> }
  | { type: 'toggleSelectRoom'; id: string }
  | { type: 'updateBackground'; bg?: BackgroundProps }
  | { type: 'setEditDialogRoom'; id: string }
  | { type: 'updateValue'; updObject: UpdateValueType }
  | { type: 'addEntity'; entity: EntityProps }
  | {
      type: 'updateEntity'
      idx: number
      entity: EntityProps
      shiftx: number
      shifty: number
    }
  | { type: 'removeEntity'; idx: number; entity: EntityProps }
  | { type: 'toggleSelectEntity'; id: string }
  | {
      type: 'setSelection'
      selection: SelectionUpdate
    }
  | { type: 'setRemoveEntityDialog'; id: string }
  | { type: 'loadState'; state: FloorPlanState }
  | { type: 'setUnsavedChanges'; unsavedChanges: boolean }

type UpdateValueType = ValueBoolType | ValueRoomType | ValueScaleType

type ValueBoolType = {
  name: 'usePolyline' | 'drawRooms'
  value: boolean
}

type ValueRoomType = {
  name: 'newRoom'
  value: RoomProps | undefined
}

type ValueScaleType = {
  name: 'scale'
  value: ScaleData
}

export type SelectionUpdate = {
  rooms: Array<string> | undefined
  entities: Array<string> | undefined
}

export type FloorPlanState = {
  rooms: Array<RoomProps>
  selectedRooms: Array<string>
  entities: Array<EntityProps>
  selectedEntities: Array<string>
  background?: BackgroundProps
  editDialogRoom: { room: RoomProps | undefined; idx: number }
  drawRooms: boolean
  usePolyline: boolean
  scale: ScaleData
  newRoom: RoomProps | undefined
  removeEntityDialog: { entity: EntityProps | undefined; idx: number }
  unsavedChanges: boolean
}

export const initialState: FloorPlanState = {
  rooms: [],
  entities: [],
  selectedRooms: [],
  selectedEntities: [],
  background: undefined,
  editDialogRoom: { room: undefined, idx: -1 },
  drawRooms: false,
  usePolyline: false,
  scale: {
    drawScale: false,
    metersPerPixel: 0.02,
    show: false,
  },
  newRoom: undefined,
  removeEntityDialog: { entity: undefined, idx: -1 },
  unsavedChanges: false,
}

export function reducer(state: FloorPlanState, action: FloorPlanActions) {
  let newState = { ...state }

  switch (action.type) {
    case 'addRoom': {
      const { room } = action
      newState = {
        ...newState,
        rooms: [...newState.rooms, room],
        unsavedChanges: true,
      }
      if (room.shape.type === 'rectangle' && !state.drawRooms) {
        newState = reducer(newState, {
          type: 'setEditDialogRoom',
          id: room.id,
        })
      }
      break
    }
    case 'updateRoom':
      newState = update(
        newState,
        'rooms',
        action.room,
        action.idx,
        action.shiftx,
        action.shifty
      )
      break
    case 'removeRoom':
      newState = remove(newState, 'rooms', action.idx)
      break
    case 'setRooms':
      newState = { ...newState, rooms: action.rooms, unsavedChanges: true }
      break
    case 'toggleSelectRoom':
      newState = toggleSelect(newState, action.id, 'selectedRooms')
      break
    case 'addEntity':
      newState = {
        ...newState,
        entities: [...newState.entities, action.entity],
        unsavedChanges: true,
      }
      break
    case 'updateEntity':
      newState = update(
        newState,
        'entities',
        action.entity,
        action.idx,
        action.shiftx,
        action.shifty
      )
      break
    case 'removeEntity':
      newState = remove(newState, 'entities', action.idx)
      break
    case 'toggleSelectEntity':
      newState = toggleSelect(newState, action.id, 'selectedEntities')
      break
    case 'setSelection':
      newState = {
        ...newState,
        selectedEntities:
          action.selection.entities ?? newState.selectedEntities,
        selectedRooms: action.selection.rooms ?? newState.selectedRooms,
      }
      break
    case 'updateBackground':
      newState = { ...newState, background: action.bg, unsavedChanges: true }
      break
    case 'setEditDialogRoom':
      newState = setDialog(newState, action.id, setRoomsDialogObj)
      break
    case 'setRemoveEntityDialog':
      newState = setDialog(newState, action.id, setEntitiesDialogObj)
      break
    case 'updateValue':
      newState = {
        ...newState,
        [action.updObject.name]: action.updObject.value,
      }
      break
    case 'loadState':
      newState = { ...initialState, ...action.state }
      break
    case 'setUnsavedChanges':
      newState = { ...newState, unsavedChanges: action.unsavedChanges }
      break
    default:
      exhaustiveCheck(action, 'reducer action was not matched')
  }

  return newState
}
