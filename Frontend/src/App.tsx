import {
  CssBaseline,
  StyledEngineProvider,
  Theme,
  ThemeProvider,
  createTheme,
} from '@mui/material'
import { grey } from '@mui/material/colors'

import React from 'react'
import { HashRouter } from 'react-router-dom'

import Dashboard from '@pages/Dashboard'

import './App.css'

declare module '@mui/styles/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}

const ColorModeContext = React.createContext({
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  toggleColorMode: (_: 'light' | 'dark') => {
    console.error('Please provide an implementation for toggleColorMode')
  },
})

function App() {
  const [mode, setMode] = React.useState<'light' | 'dark'>('light')

  const colorMode = {
    mode,
    toggleColorMode: (newMode: 'light' | 'dark') => {
      setMode(newMode)
    },
  }

  const myTheme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode,
          ...(mode === 'light'
            ? {
                primary: {
                  main: '#48b7ea',
                },
                secondary: {
                  main: '#000',
                },
                divider: '#0000001f',
                text: {
                  primary: grey[900],
                  secondary: grey[800],
                },
              }
            : {
                primary: {
                  main: '#48b7ea',
                },
                background: {
                  default: grey[900],
                  paper: grey[900],
                },
                secondary: {
                  main: '#000',
                },
                divider: '#000',
                text: {
                  primary: '#fff',
                  secondary: grey[500],
                },
              }),
        },
      }),
    [mode]
  )

  return (
    <HashRouter>
      <StyledEngineProvider injectFirst>
        <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={myTheme}>
            <CssBaseline />
            <Dashboard colorMode={colorMode} />
          </ThemeProvider>
        </ColorModeContext.Provider>
      </StyledEngineProvider>
    </HashRouter>
  )
}

export default App
