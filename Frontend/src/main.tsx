import React from 'react'
import ReactDOM from 'react-dom'

import { ContentWidthProvider } from '@contexts/contentWithContext'
import { NotificationContextProvider } from '@contexts/notificationContext'

import App from './App'

ReactDOM.render(
  <React.StrictMode>
    <ContentWidthProvider>
      <NotificationContextProvider>
        <App />
      </NotificationContextProvider>
    </ContentWidthProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
