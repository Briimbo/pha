import { EntityState } from 'generated/openapi'
import { v4 as uuid } from 'uuid'

import { EntityProps } from '@components/floorplan/Entity'
import { RoomProps } from '@components/floorplan/Room'

export type Group = {
  name: string
  entity_id: string
  entities: EntityState[]
}

function isEntityInRoom(room: RoomProps, entity: EntityProps): boolean {
  const roomShape = room.shape
  if (roomShape.type === 'rectangle') {
    return (
      roomShape.x <= entity.x &&
      roomShape.x + roomShape.width >= entity.x &&
      roomShape.y <= entity.y &&
      roomShape.y + roomShape.height >= entity.y
    )
  } else if (roomShape.type === 'polyline') {
    // The entity is in the polyline room if a line from the point to infinity crosses the polyline room an odd number of times
    let odd = false
    const roomPoints = roomShape.points
    // For each edge can be interpreted as for each point of the polyline room and the previous one
    roomPoints.forEach((point, index, array) => {
      // if index is 0, the previous value is the last element in the array
      const prev = index === 0 ? array[array.length - 1] : array[index - 1]
      //If a line from the point into infinity crosses this edge
      // One point needs to be above, one below our y coordinate
      // ...and the edge doesn't cross our Y corrdinate before our x coordinate (but between our x coordinate and infinity)
      if (
        point.y > entity.y !== prev.y > entity.y &&
        entity.x <
          ((prev.x - point.x) * (entity.y - point.y)) / (prev.y - point.y) +
            point.x
      ) {
        // toggle odd
        odd = !odd
      }
    })

    //If the number of crossings was odd, the point is in the room
    return odd
  }
  return false
}

function getGroups(rooms: RoomProps[], entities: EntityProps[]): Group[] {
  const res: Group[] = []

  rooms.forEach((room) => {
    res.push({
      name: room.name,
      entity_id: room.id,
      entities: getEntitiesInRoom(room, entities).map((e) => e.entity),
    })
  })

  const inRooms = res.flatMap((e) => e.entities)
  const entitiesWithoutRoom = entities
    .map((e) => e.entity)
    .filter((e) => !inRooms.includes(e))

  res.push({
    name: 'outside',
    entity_id: uuid(),
    entities: entitiesWithoutRoom,
  })
  return res
}

function getEntitiesInRoom(
  room: RoomProps,
  entities: EntityProps[]
): EntityProps[] {
  return entities.filter((entity) => isEntityInRoom(room, entity))
}

function getGroupsWithUnplaced(
  rooms: RoomProps[],
  placedEntities: EntityProps[],
  allEntities: EntityState[]
): Array<[string, Array<EntityState>]> {
  const result: Array<[string, Array<EntityState>]> = []
  const groups = getGroups(rooms, placedEntities)
  groups.forEach((g) => result.push([g.name, g.entities]))
  result.push([
    'Not placed',
    allEntities.filter((e) => !placedEntities.some((p) => p.id === e.entityId)),
  ])
  return result
}

export { getGroups, getGroupsWithUnplaced, getEntitiesInRoom }
