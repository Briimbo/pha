import { KonvaEventObject } from 'konva/lib/Node'

import { SelectionUpdate } from '@pages/reducers/floorPlanReducer'

export const handleSelection = (
  drawRooms: boolean,
  event: KonvaEventObject<MouseEvent>,
  oldSelection: Array<string>,
  id: string,
  selected: boolean,
  type: 'room' | 'entity'
): SelectionUpdate => {
  // reason for action parameter: it is possiple that for example first a room and then an entity is clicked
  // without ctrl key being pressed
  // If that is the case, we want to deselect the room and select the entity
  // But in order to do that, we need the information that this case has happended outside of this method
  if (drawRooms) {
    return { entities: undefined, rooms: undefined }
  }
  const keyPressed =
    event.evt.shiftKey || event.evt.ctrlKey || event.evt.metaKey
  let newSelection: Array<string> = []
  let other = undefined
  if (!keyPressed) {
    newSelection = [id]
    other = []
  } else if (keyPressed && selected) {
    newSelection = oldSelection.filter((i) => i !== id)
  } else if (keyPressed && !selected) {
    newSelection = oldSelection.concat(id)
  }

  if (type === 'room') return { rooms: newSelection, entities: other }
  else return { entities: newSelection, rooms: other }
}
