export function fuzzyFilterList<T>(
  searchTerm: string,
  list: Array<T>,
  objectGetters: ((t: T) => string)[]
): Array<T> {
  return list.filter((item: T) => {
    return objectGetters.some(
      (f) => f(item).toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0
    )
  })
}
