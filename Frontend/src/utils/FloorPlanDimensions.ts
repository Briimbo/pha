import { FloorPlanState } from '@pages/reducers/floorPlanReducer'

import { EntityProps, ICON_DIMENSIONS } from '@components/floorplan/Entity'
import { PolylineShape, RoomProps, RoomShape } from '@components/floorplan/Room'

export const getDimensions = (state: FloorPlanState) => {
  const entityBounds = getEntityBounds(state.entities)
  const roomBounds = getRoomBounds(state.rooms)
  const backgroundBounds = state.background
    ? {
        xMin: state.background.x,
        yMin: state.background.y,
        xMax: state.background.x + (state.background.width ?? 0),
        yMax: state.background.y + (state.background.height ?? 0),
      }
    : { xMin: Number.MAX_VALUE, yMin: Number.MAX_VALUE, xMax: 0, yMax: 0 }

  const minMax = {
    xMin: Math.min(entityBounds.xMin, roomBounds.xMin, backgroundBounds.xMin),
    yMin: Math.min(entityBounds.yMin, roomBounds.yMin, backgroundBounds.yMin),
    xMax: Math.max(entityBounds.xMax, roomBounds.xMax, backgroundBounds.xMax),
    yMax: Math.max(entityBounds.yMax, roomBounds.yMax, backgroundBounds.yMax),
  }

  // add a little space around the edges
  return {
    xMin: minMax.xMin,
    yMin: minMax.yMin,
    width: minMax.xMax - minMax.xMin,
    height: minMax.yMax - minMax.yMin,
  }
}

const getRoomBounds = (input: RoomProps[]) => {
  const preprocessed = input.flatMap(
    (r) => (r.shape as PolylineShape)?.points ?? [r.shape as RoomShape]
  )
  return getBounds(preprocessed, (x) =>
    (x as RoomShape).type === 'rectangle'
      ? (x as RoomShape)
      : { width: 0, height: 0 }
  )
}

const getEntityBounds = (input: EntityProps[]) => {
  return getBounds(
    input.map((e) => ({
      ...e,
      x: e.x - ICON_DIMENSIONS.width / 2,
      y: e.y - ICON_DIMENSIONS.height / 2,
    })),
    () => ICON_DIMENSIONS
  )
}

/**
 * Computes boundaries of an array of elements whose top left corner is given by x and y and whose width and height can be computed by the dimensions function
 * @param input elements with their top left corner at x, y
 * @param dimensions function to compute the width and height of an element
 * @returns the minimum and maximum x and y coordinates
 */
const getBounds = <T extends { x: number; y: number }>(
  input: T[],
  dimensions: (t: T) => { width: number; height: number }
) => {
  return input
    .map((e) => ({
      xMin: e.x,
      yMin: e.y,
      xMax: e.x + dimensions(e).width,
      yMax: e.y + dimensions(e).height,
    }))
    .reduce(
      (prev, current) => ({
        xMin: Math.min(prev.xMin, current.xMin),
        yMin: Math.min(prev.yMin, current.yMin),
        xMax: Math.max(prev.xMax, current.xMax),
        yMax: Math.max(prev.yMax, current.yMax),
      }),
      { xMin: Number.MAX_VALUE, yMin: Number.MAX_VALUE, xMax: 0, yMax: 0 }
    )
}

export const shiftToOrigin = (state: FloorPlanState) => {
  const { xMin, yMin } = getDimensions(state)

  const shift = <T extends { x: number; y: number }>(obj: T): T => {
    return { ...obj, x: obj.x - xMin, y: obj.y - yMin }
  }

  const shiftRoom = (
    obj: RoomShape | PolylineShape
  ): RoomShape | PolylineShape => {
    if (obj.type === 'rectangle') {
      return shift(obj as RoomShape)
    } else {
      const polyline = obj as PolylineShape
      return { ...polyline, points: polyline.points.map((p) => shift(p)) }
    }
  }

  return {
    ...state,
    entities: state.entities.map((e) => shift(e)),
    rooms: state.rooms.map((r) => ({ ...r, shape: shiftRoom(r.shape) })),
    background: state.background ? shift(state.background) : state.background,
  }
}
