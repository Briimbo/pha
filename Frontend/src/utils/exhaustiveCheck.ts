export function exhaustiveCheck(param: never, errorString?: string): never {
  throw new Error(errorString)
}
