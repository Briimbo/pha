export function getBase64(file: File): Promise<string> {
  return new Promise((res, rej) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
      res(reader.result as string)
    }
    reader.onerror = function (error) {
      rej(error)
    }
  })
}
