function groupBy<T, K extends string | number | symbol>(
  arr: T[],
  getKey: (item: T) => K
) {
  return arr.reduce((res, curr) => {
    const group = getKey(curr)
    if (!res[group]) res[group] = []
    res[group].push(curr)
    return res
  }, {} as Record<K, T[]>)
}

function findLastIndex<T>(arr: T[], predicate: (obj: T) => boolean) {
  let i = arr.length - 1
  while (i >= 0) {
    if (predicate(arr[i])) return i
    i--
  }
  return -1
}

export { groupBy, findLastIndex }
