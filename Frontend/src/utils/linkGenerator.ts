import type { NotificationDtoLinkedObjectTypeEnum } from 'generated/openapi'

const getLink = (
  type: NotificationDtoLinkedObjectTypeEnum | undefined,
  id: number | undefined
): string | undefined => {
  if (!type || !id) return undefined
  switch (type) {
    case 'FLOOR_PLAN':
      return `/floor-plan/${id}`
    case 'DOCUMENT':
      return `/document/${id}/results`
    case 'RUNTIME_OVERVIEW':
      return `/runtime`
  }
}

export default getLink
