import * as MuiIcons from '@mdi/js'
import {
  mdiAccount,
  mdiBrightness5,
  mdiDoor,
  mdiFlash,
  mdiHelp,
  mdiHome,
  mdiLightbulb,
  mdiMotionSensor,
  mdiSwapHorizontal,
  mdiThermometer,
  mdiUpdate,
  mdiWaterPercent,
  mdiWindowClosed,
} from '@mdi/js'
import { EntityState } from 'generated/openapi'

export const entityToIcon = (entity: EntityState): string => {
  const { icon, deviceKind } = entity.attributes
  if (icon) {
    const key = ('mdi' +
      icon
        .split(/[:-]/)
        .slice(1)
        .map((s) => s.charAt(0).toUpperCase() + s.slice(1))
        .join('')) as unknown as keyof typeof MuiIcons
    return MuiIcons[key]
  }

  switch (deviceKind) {
    case 'binary_sensor':
      return mdiSwapHorizontal
    case 'door':
      return mdiDoor
    case 'humidity':
      return mdiWaterPercent
    case 'light':
      return mdiLightbulb
    case 'person':
      return mdiAccount
    case 'presence':
      return mdiHome
    case 'occupancy':
      return mdiHome
    case 'sensor':
      return mdiMotionSensor
    case 'sun':
      return mdiBrightness5
    case 'switch':
      return mdiFlash
    case 'temperature':
      return mdiThermometer
    case 'update':
      return mdiUpdate
    case 'window':
      return mdiWindowClosed

    default:
      return mdiHelp
  }
}
