import { ResultDetails } from 'generated/openapi'

interface ErrorParams {
  [name: string]: string
}

export interface ErrorDetails {
  code: string
  msg: string
  params?: ErrorParams
}
const codesToMessage = {
  'error/internal': () => `Unexpected problem occurred on the server`,

  'error/state/invalid': (params: ErrorParams) =>
    `State '${translateParams(params['state'])}'' of ${translateParams(
      params['object']
    )} is invalid`,

  'error/value/invalid': (params: ErrorParams) =>
    `Value '${params['value']}'' for ${translateParams(
      params['valueName']
    )} is invalid`,

  'error/parsing/syntax-error': () =>
    `Syntax error occurred while parsing the request`,

  'error/object-mgmt/duplicate-value': (params: ErrorParams) =>
    `${translateParams(params['objectType'])} with '${translateParams(
      params['valueName']
    )}' set to '${translateParams(
      params['value']
    )}' already exists but '${translateParams(
      params['valueName']
    )}' must be unique.`,
}

const params = {
  'ha-instance': 'Home Assistant Instance',
  unconfigured: 'unconfigured',
  url: 'URL',
}

const translateParams = (param?: string): string => {
  return param ? params[param as keyof typeof params] || param : '-'
}

export const translateError = (
  errors: Array<ErrorDetails | ResultDetails>
): Array<string> => {
  return errors
    .filter((i) => i.code !== 'success')
    .map((i): string => {
      const mapper = codesToMessage[i.code as keyof typeof codesToMessage]
      if (mapper) return mapper(i.params || {})
      else {
        console.warn(
          `error code '${i.code}' is not matched. the default message is used`
        )
        return i.msg ?? 'Unknown error occured.'
      }
    })
}

export const defaultFetchError = 'Unable to connect to server'
