import { FloorPlanState } from '@pages/reducers/floorPlanReducer'

import { ICON_DIMENSIONS } from '@components/floorplan/Entity'

const calculateContentSize = (
  floorPlanState: FloorPlanState,
  min: {
    width?: number
    height?: number
  },
  xOffset?: number,
  yOffset?: number
): { width: number; height: number } => {
  let maxWidth = 0
  let maxHeight = 0

  for (const e of floorPlanState.entities) {
    maxWidth = Math.max(maxWidth, e.x + ICON_DIMENSIONS.width)
    maxHeight = Math.max(maxHeight, e.y + ICON_DIMENSIONS.height)
  }

  for (const r of floorPlanState.rooms) {
    switch (r.shape.type) {
      case 'polyline':
        {
          for (const point of r.shape.points) {
            maxWidth = Math.max(maxWidth, point.x + 10)
            maxHeight = Math.max(maxHeight, point.y + 30)
          }
        }
        break
      case 'rectangle': {
        maxWidth = Math.max(maxWidth, r.shape.width + r.shape.x)
        maxHeight = Math.max(maxHeight, r.shape.height + r.shape.y)
      }
    }
  }

  if (floorPlanState.background && floorPlanState.background.img) {
    maxWidth = Math.max(
      maxWidth,
      floorPlanState.background.width || 0 + floorPlanState.background.x
    )
    maxHeight = Math.max(
      maxHeight,
      floorPlanState.background.height || 0 + floorPlanState.background.y
    )
  }

  return {
    width: Math.max(
      min.width ? min.width - (xOffset || 0) : window.innerWidth,
      maxWidth
    ),
    height: Math.max(
      min.height ? min.height - (yOffset || 0) : window.innerHeight,
      maxHeight
    ),
  }
}

export default calculateContentSize
