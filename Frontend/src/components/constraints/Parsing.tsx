import Editor, { useMonaco } from '@monaco-editor/react'
import { Button, Grid, TextField } from '@mui/material'

import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

import useApi from '@hooks/useAPI'

import FullWidthAlert from '@components/FullWidthAlert'

import {
  ConstraintDocumentDto,
  ParseResultDto,
  SyntaxErrorDto,
} from '@generated/openapi'

const Parsing = ({
  onNameChanged,
}: {
  onNameChanged: (name: string) => void
}) => {
  const { id } = useParams<{ id: string }>()

  const [title, setTitle] = useState('')
  const [text, setText] = useState('')

  const [error, setError] = useState<string | undefined>('')
  const [success, setSuccess] = useState<string | undefined>('')
  const [errors, setErrors] = useState<SyntaxErrorDto[]>([])
  const [document, setDocument] = useState<ParseResultDto>()

  const { isLoading: isSaving, callApi: callSave } = useApi<
    ConstraintDocumentDto,
    ParseResultDto
  >(`constraints/document/`, 'POST')

  const monaco = useMonaco()

  const { isLoading: isLoadingDocument, callApi: callLoad } = useApi<
    undefined,
    ParseResultDto
  >(`constraints/document/${id}`, 'GET')

  const submitData = () => {
    setError(undefined)
    setSuccess(undefined)

    if (!isSaving) {
      callSave({ id: +id, name: title, text: text }).then(
        ({ data, errorMessage }) => {
          if (errorMessage?.length) setError(errorMessage.join(', '))
          else {
            if (data?.errors !== undefined) setErrors(data?.errors)
            else setErrors([])
            setSuccess('Saved')
            if (data) setDocument(data)
          }
        }
      )
    } else {
      setError('Please wait for the prior request to finish.')
    }
  }

  useEffect(() => {
    callLoad(undefined).then(({ data, errorMessage }) => {
      if (errorMessage?.length) setError(errorMessage.join(', '))
      if (data) setDocument(data)
    })
  }, [])

  useEffect(() => {
    if (document) {
      onNameChanged(document.document.name)
      setText(document.document.text)
      setTitle(document.document.name)
    }
  }, [document])

  monaco?.editor.setTheme('vs-dark')
  monaco?.languages.register({ id: 'ConstraintLang' })
  monaco?.languages.setMonarchTokensProvider('ConstraintLang', {
    typeKeywords: ['read', 'constraint'],

    keywords: ['true', 'false', 'on', 'off', 'as'],

    operators: ['||', 'OR', 'AND', '&&', '!', 'NOT', ':'],

    // we include these common regular expressions
    symbols: /[=><!~?:&|+\-*/^%]+/,

    // C# style strings
    escapes:
      /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,

    tokenizer: {
      root: [
        [
          /[a-z_$][\w$]*/,
          {
            cases: {
              '@typeKeywords': 'keyword',
              '@keywords': 'keyword',
              '@default': 'identifier',
            },
          },
        ],
        [/[A-Z][\w$]*/, 'type.identifier'],
        // whitespace
        { include: '@whitespace' },
        [/[{}()[\]]/, '@brackets'],
        [/[<>](?!@symbols)/, '@brackets'],
        [/@symbols/, { cases: { '@operators': 'operator', '@default': '' } }],
        [
          /@\s*[a-zA-Z_$][\w$]*/,
          { token: 'annotation', log: 'annotation token: $0' },
        ],
        [/\d*\.\d+([eE][-+]?\d+)?/, 'number.float'],
        [/0[xX][0-9a-fA-F]+/, 'number.hex'],
        [/\d+/, 'number'],
        [/[;,.]/, 'delimiter'],
        [/"([^"\\]|\\.)*$/, 'string.invalid'], // non-teminated string
        [/"/, { token: 'string.quote', bracket: '@open', next: '@string' }],
        [/'[^\\']'/, 'string'],
        [/(')(@escapes)(')/, ['string', 'string.escape', 'string']],
        [/'/, 'string.invalid'],
      ],
      comment: [
        [/[^/*]+/, 'comment'],
        [/\/\*/, 'comment', '@push'], // nested comment
        ['\\*/', 'comment', '@pop'],
        [/[/*]/, 'comment'],
      ],
      string: [
        [/[^\\"]+/, 'string'],
        [/@escapes/, 'string.escape'],
        [/\\./, 'string.escape.invalid'],
        [/"/, { token: 'string.quote', bracket: '@close', next: '@pop' }],
      ],

      whitespace: [[/[ \t\r\n]+/, 'white']],
    },
  })

  return (
    <Grid container spacing={2} style={{ height: '100vh' }}>
      <Grid item xs={12}>
        {error && (
          <FullWidthAlert variant="filled" severity="error">
            {error}
          </FullWidthAlert>
        )}
        {success && (
          <FullWidthAlert variant="filled" severity="success">
            {success}
          </FullWidthAlert>
        )}
      </Grid>

      <Grid item xs={8} style={{ height: '100vh' }}>
        <TextField
          id="standard-basic"
          label="Title"
          variant="outlined"
          value={title}
          sx={{ marginBottom: '1rem' }}
          onChange={(e) => setTitle(e.target.value)}
        />
        <Editor
          height="50vh"
          value={text || ''}
          language="ConstraintLang"
          line={3}
          onChange={(e) => setText(e || '')}
        />
        {errors.map((err) => (
          <FullWidthAlert
            key={err.startLine + ':' + err.endLine}
            variant="outlined"
            severity="error"
          >
            Error in Line {err.startLine}-{err.endLine}: {err.error}
          </FullWidthAlert>
        ))}
        <Grid marginTop="1rem" container justifyContent="end">
          <Button
            disabled={isSaving || isLoadingDocument}
            onClick={() => submitData()}
            variant="contained"
          >
            Save
          </Button>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default Parsing
