import {
  Box,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Divider,
  SxProps,
  Table,
  TableBody,
  TableCell,
  TableRow,
  tableCellClasses,
} from '@mui/material'

import React, { useEffect, useState } from 'react'

import { useErrorHandling } from '@hooks/useAPI'

import { CoverageDto, ValidationControllerApi } from '@generated/openapi'

const getColor = (value: number) => {
  const hue = (value * 120).toString(10)
  return ['hsl(', hue, ',100%,50%)'].join('')
}

const rowSx = { display: 'flex' }
const colSx = { flex: 1 }

const CoverageWidget = ({ sx }: { sx: SxProps }) => {
  const [data, setData] = useState<CoverageDto>()

  const validationApi = new ValidationControllerApi()
  const { call: getCoverage } = useErrorHandling(() =>
    validationApi.getCoverageUsingGET()
  )

  useEffect(() => {
    getCoverage().then(
      ({ success, result }) => success && result?.data && setData(result.data)
    )
  }, [])

  const entityCoverage =
    (data && data.coveredEntities / data.totalSupportedEntities) || 0
  const automationCoverage =
    (data && data.coveredAutomations / data.totalAutomations) || 0

  return (
    <Card
      sx={{
        ...sx,
        padding: '1rem',
        marginBottom: '2rem',
        overflow: 'visible',
      }}
    >
      <CardHeader
        title="Constraint Coverage"
        action={<Box sx={{ height: 100 }} />}
      />
      <CardContent>
        {!data && (
          <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            <CircularProgress />
          </Box>
        )}
        {data && (
          <Table
            sx={{
              [`& .${tableCellClasses.root}`]: {
                paddingBlock: '0.2rem',
              },
            }}
          >
            <TableBody>
              <TableRow sx={rowSx}>
                <TableCell sx={colSx}>Total Entities:</TableCell>
                <TableCell align="right">{data.totalEntities}</TableCell>
              </TableRow>
              <TableRow sx={rowSx}>
                <TableCell sx={colSx}>PHA-Supported Entities:</TableCell>
                <TableCell align="right">
                  {data.totalSupportedEntities}
                </TableCell>
              </TableRow>
              <TableRow sx={rowSx}>
                <TableCell sx={colSx}>Covered Entities:</TableCell>
                <TableCell align="right">{data.coveredEntities}</TableCell>
              </TableRow>
              <TableRow sx={rowSx}>
                <TableCell sx={colSx}>Entity Coverage:</TableCell>
                <TableCell
                  align="right"
                  sx={{
                    padding: 1,
                  }}
                >
                  <Box
                    sx={{
                      backgroundColor: getColor(entityCoverage),
                      borderRadius: 1,
                      paddingInline: 1,
                      color: 'black',
                    }}
                  >
                    {data.totalSupportedEntities == 0
                      ? 'n/a'
                      : `${(entityCoverage * 100).toFixed(2)}\u00A0%`}
                  </Box>
                </TableCell>
              </TableRow>
              <Divider sx={{ borderBottomWidth: 3 }} />
              <TableRow sx={rowSx}>
                <TableCell sx={colSx}>Automations:</TableCell>
                <TableCell align="right">{data.totalAutomations}</TableCell>
              </TableRow>
              <TableRow sx={rowSx}>
                <TableCell sx={colSx}>Covered Automations:</TableCell>
                <TableCell align="right">{data.coveredAutomations}</TableCell>
              </TableRow>
              <TableRow sx={rowSx}>
                <TableCell sx={colSx}>Automation Coverage:</TableCell>
                <TableCell align="right" sx={{ padding: 1 }}>
                  <Box
                    sx={{
                      backgroundColor: getColor(automationCoverage),
                      borderRadius: 1,
                      paddingInline: 1,
                      color: 'black',
                    }}
                  >
                    {data.totalAutomations == 0
                      ? 'n/a'
                      : `${(automationCoverage * 100).toFixed(2)}\u00A0%`}
                  </Box>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        )}
      </CardContent>
    </Card>
  )
}

export default CoverageWidget
