import {
  mdiChevronDown,
  mdiChevronRight,
  mdiCloseBoxOutline,
  mdiDelete,
  mdiPlayCircle,
  mdiTextBoxSearch,
} from '@mdi/js'
import {
  Checkbox,
  CircularProgress,
  IconButton,
  Link as MdiLink,
  TableCell,
  TableRow,
  Tooltip,
} from '@mui/material'

import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import { translateError } from '@utils/ErrorTranslator'

import { getStateIcon } from '@pages/Documents'

import FAIcon from '@components/Icon'
import ConstraintRow from '@components/constraints/documentOverview/ConstraintRow'

import {
  ConstraintDto,
  StructuredConstraintDocumentDto,
  ValidationControllerApi,
  ValidationState,
  ValidationStateStateEnum,
} from '@generated/openapi'

const aggregateDocumentState = (
  states: ValidationState[] | undefined
): ValidationStateStateEnum => {
  if (!states) return ValidationStateStateEnum.Unknown
  let success = true
  let inProgress = false
  for (const state of states) {
    switch (state.state) {
      case 'UNKNOWN':
        return ValidationStateStateEnum.Unknown
      case 'IN_PROGRESS':
        inProgress = true
        break
      case 'FAIL':
        success = false
    }
    if (state.phase === 'SUBMITTED') inProgress = true
  }
  return inProgress
    ? ValidationStateStateEnum.InProgress
    : success
    ? ValidationStateStateEnum.Success
    : ValidationStateStateEnum.Fail
}

const DocumentRow = ({
  doc,
  validationState,
  isEditEnabled,
  onRequestFinished,
  onCheckDocument,
}: {
  doc: StructuredConstraintDocumentDto
  validationState: ValidationState[] | undefined
  isEditEnabled: boolean
  onRequestFinished: (error?: string) => void
  onCheckDocument: () => void
}) => {
  const [open, setOpen] = useState(false)
  const [isDocEnabled, setDocEnabled] = useState(doc.enabled)
  const [documentState, setDocumentState] = useState<ValidationStateStateEnum>(
    ValidationStateStateEnum.Unknown
  )

  const [sortedConstraints, setSortedConstraints] = useState<
    Array<ConstraintDto>
  >([])

  const [isLoadingDelete, setLoadingDelete] = useState(false)
  const validationApi = new ValidationControllerApi()

  const deleteDocument = (id?: number) => {
    if (!id) return
    setLoadingDelete(true)
    validationApi
      .deleteUsingDELETE({ id })
      .then((res) => {
        const errors = res.results ? translateError(res.results).join(', ') : ''
        onRequestFinished(errors ?? undefined)
      })
      .catch((err) => {
        onRequestFinished(err)
      })
  }

  const updateEnabled = (value: boolean) => {
    setDocEnabled(value)
    validationApi
      .updateEnabledUsingPATCH({ id: doc.id, body: value })
      .then((res) => {
        const errors = res.results ? translateError(res.results).join(', ') : ''
        onRequestFinished(errors ?? undefined)
      })
      .catch((err) => {
        onRequestFinished(err)
        setDocEnabled(doc.enabled)
      })
  }

  useEffect(() => {
    if (!validationState && !doc.constraints.length)
      setDocumentState(ValidationStateStateEnum.Success)
    else setDocumentState(aggregateDocumentState(validationState))
  }, [doc, validationState])

  useEffect(() => {
    setSortedConstraints(
      [...doc.constraints].sort((a, b) => {
        return a.name.localeCompare(b.name)
      })
    )
    setDocEnabled(doc.enabled)
  }, [doc])

  const findState = (name: string) => {
    return validationState?.find((i) => i.constraint.name === name)
  }

  return (
    <>
      <TableRow
        key={doc.id}
        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
      >
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            <FAIcon d={open ? mdiChevronDown : mdiChevronRight} />
          </IconButton>
        </TableCell>
        <TableCell>
          <Tooltip title={isEditEnabled ? '' : 'Enable edit mode to change'}>
            <span>
              <Checkbox
                disabled={!isEditEnabled}
                value={isDocEnabled}
                checked={isDocEnabled}
                onChange={(event) => updateEnabled(event.target.checked)}
                icon={<FAIcon d={mdiCloseBoxOutline} />}
              />
            </span>
          </Tooltip>
        </TableCell>
        <TableCell>{getStateIcon(documentState)}</TableCell>
        <TableCell>{doc.id}</TableCell>
        <TableCell>
          <MdiLink to={`/document/${doc.id}/parsing`} component={Link}>
            {doc.name || '-'}
          </MdiLink>
        </TableCell>
        <TableCell></TableCell>
        <TableCell sx={{ padding: 0, paddingRight: '1rem' }} align="right">
          {isLoadingDelete && <CircularProgress sx={{ marginY: 0 }} />}
          {!isLoadingDelete && (
            <Tooltip title="Run checks">
              <span>
                <IconButton onClick={onCheckDocument} size="small">
                  <FAIcon sx={{ color: 'green' }} d={mdiPlayCircle} />
                </IconButton>
              </span>
            </Tooltip>
          )}
          {!isLoadingDelete && (
            <Tooltip title="Inspect details">
              <span>
                <IconButton
                  href={`/#/document/${doc.id}/results`}
                  size="small"
                  disabled={isLoadingDelete}
                  sx={{ marginY: 0 }}
                >
                  <FAIcon
                    sx={(theme) => ({ color: theme.palette.text.primary })}
                    d={mdiTextBoxSearch}
                  />
                </IconButton>
              </span>
            </Tooltip>
          )}
          {!isLoadingDelete && (
            <Tooltip title="Delete">
              <span>
                <IconButton
                  onClick={() => deleteDocument(doc.id)}
                  size="small"
                  disabled={isLoadingDelete}
                  sx={{ marginY: 0 }}
                >
                  <FAIcon sx={{ color: 'red' }} d={mdiDelete} />
                </IconButton>
              </span>
            </Tooltip>
          )}
        </TableCell>
      </TableRow>
      {sortedConstraints.map((c) => (
        <ConstraintRow
          key={c.name}
          constraint={c}
          validationState={validationState && findState(c.name)}
          open={open}
        />
      ))}
    </>
  )
}

export { aggregateDocumentState }
export default DocumentRow
