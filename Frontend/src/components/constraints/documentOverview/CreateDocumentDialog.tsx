import { mdiPlus } from '@mdi/js'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  TextField,
  Tooltip,
} from '@mui/material'

import { useState } from 'react'
import React from 'react'
import { useHistory } from 'react-router-dom'

import { defaultFetchError, translateError } from '@utils/ErrorTranslator'

import FullWidthAlert from '@components/FullWidthAlert'
import FAIcon from '@components/Icon'

import { ValidationControllerApi } from '@generated/openapi'

const CreateDocumentDialog = () => {
  const [open, setOpen] = useState(false)
  const [title, setTitle] = useState('')
  const [error, setError] = useState('')

  const onCancel = () => setOpen(false)

  const history = useHistory()

  const [isLoading, setLoading] = useState(false)
  const validationApi = new ValidationControllerApi()

  const onSave = () => {
    const data = { name: title, text: '' }
    setLoading(true)
    validationApi
      .createOrUpdateUsingPOST({ constraintDocumentDto: data })
      .then((res) => {
        const errors = res.results ? translateError(res.results).join(', ') : ''
        setError(errors)
        setLoading(false)
        if (!errors) {
          setOpen(false)
          history.push(`document/${res.data?.document.id}/parsing`)
        }
      })
      .catch(() => {
        setLoading(false)
        setError(defaultFetchError)
      })
  }

  return (
    <>
      <Tooltip title="Create new document">
        <IconButton onClick={() => setOpen(true)}>
          <FAIcon d={mdiPlus} />
        </IconButton>
      </Tooltip>

      <Dialog open={open} onClose={onCancel}>
        <DialogTitle>Create new constraint document</DialogTitle>
        <DialogContent>
          <Grid
            container
            alignItems="center"
            justifyContent="flex-end"
            columnSpacing={1}
            rowSpacing={3}
            sx={{ paddingTop: '1rem' }}
          >
            <Grid item>
              <TextField
                label="Title"
                onChange={(e) => setTitle(e.target.value)}
                value={title}
                placeholder="Title"
              />
            </Grid>
          </Grid>
          {error && <FullWidthAlert severity="error">{error}</FullWidthAlert>}
        </DialogContent>
        <DialogActions sx={{ justifyContent: 'space-between' }}>
          <Button disabled={isLoading} color="error" onClick={onCancel}>
            cancel
          </Button>
          <Button
            disabled={isLoading || !title}
            variant="contained"
            onClick={onSave}
          >
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default CreateDocumentDialog
