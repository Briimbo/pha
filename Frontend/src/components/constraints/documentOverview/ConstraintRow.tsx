import { Collapse, TableCell, TableRow } from '@mui/material'

import React from 'react'

import { getStateIcon } from '@pages/Documents'

import { ImageDialog } from '@components/ImageDisplay'

import {
  ConstraintDto,
  ValidationState,
  ValidationStatePhaseEnum,
  ValidationStateStateEnum,
} from '@generated/openapi'

const ConstraintRow = ({
  constraint,
  validationState,
  open,
}: {
  constraint: ConstraintDto
  validationState?: ValidationState
  open: boolean
  phase?: ValidationStatePhaseEnum
  svg?: string
}) => {
  if (validationState) {
    constraint = validationState.constraint
  }
  const style = open ? {} : { borderBottom: 'none' }
  return (
    <TableRow>
      <TableCell sx={{ padding: 0 }} style={style}></TableCell>
      <TableCell sx={{ padding: 0 }} style={style}></TableCell>
      <TableCell sx={{ padding: 0, paddingLeft: 5 }} style={style}>
        <Collapse in={open}>
          {getStateIcon(
            validationState?.state ?? ValidationStateStateEnum.Unknown
          )}
        </Collapse>
      </TableCell>
      <TableCell sx={{ padding: 0 }} style={style}></TableCell>
      <TableCell sx={{ padding: 0, paddingLeft: 5 }} style={style}>
        <Collapse in={open}>{constraint.name}</Collapse>
      </TableCell>
      <TableCell sx={{ padding: 0 }} style={style}>
        <Collapse in={open}>{validationState?.phase}</Collapse>
      </TableCell>
      <TableCell
        sx={{ padding: 0, paddingRight: '1rem' }}
        align="right"
        style={style}
      >
        <Collapse in={open}>
          {validationState?.svg && (
            <ImageDialog
              title={`Constraint: ${constraint.name}`}
              type="svgUtf8"
              imgData={validationState.svg}
            />
          )}
        </Collapse>
      </TableCell>
    </TableRow>
  )
}

export default ConstraintRow
