import {
  Box,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  SxProps,
  Table,
  TableBody,
  TableCell,
  TableRow,
  tableCellClasses,
} from '@mui/material'
import { Cell, Pie, PieChart, ResponsiveContainer, Tooltip } from 'recharts'

import React, { useEffect, useState } from 'react'

import { useErrorHandling } from '@hooks/useAPI'

import { ValidationControllerApi, ValidationStats } from '@generated/openapi'

type ChartData = {
  name: 'Successful Checks' | 'Errors' | 'Aborts'
  value: number
}

const COLORS = {
  'Successful Checks': 'green',
  Errors: '#ff0000',
  Aborts: 'grey',
}

const ValidationStatWidget = ({ sx }: { sx: SxProps }) => {
  const [data, setData] = useState<ValidationStats>()
  const [chartData, setChartData] = useState<ChartData[]>([])

  const validationApi = new ValidationControllerApi()
  const { call: getValidationStatistics } = useErrorHandling(() =>
    validationApi.getValidationStatisticsUsingGET()
  )
  useEffect(() => {
    getValidationStatistics().then(
      ({ success, result }) => success && result?.data && setData(result.data)
    )
  }, [])

  useEffect(() => {
    if (data) {
      setChartData([
        {
          name: 'Successful Checks',
          value: data.checkedConstraints - data.errors,
        },
        { name: 'Errors', value: data.errors },
        { name: 'Aborts', value: data.aborts },
      ])
    }
  }, [data])

  return (
    <Card
      sx={{ ...sx, padding: '1rem', marginBottom: '2rem', overflow: 'visible' }}
    >
      <CardHeader
        title="Validation Statistics"
        action={
          <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            <ResponsiveContainer width="99%" height={100}>
              <PieChart width={100} height={100}>
                <Pie dataKey="value" data={chartData} innerRadius="40%">
                  {chartData.map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={COLORS[entry.name]} />
                  ))}
                </Pie>
                <Tooltip
                  allowEscapeViewBox={{ y: true, x: true }}
                  contentStyle={{ paddingBlock: 0, paddingInline: 5 }}
                />
              </PieChart>
            </ResponsiveContainer>
          </Box>
        }
        sx={{
          '.MuiCardHeader-content': { flex: 'none' },
          '.MuiCardHeader-action': { flex: 1 },
        }}
      />
      <CardContent>
        {!data && (
          <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            <CircularProgress />
          </Box>
        )}
        {data && (
          <Table
            sx={{
              [`& .${tableCellClasses.root}`]: {
                paddingBlock: '0.2rem',
              },
            }}
          >
            <TableBody>
              <TableRow>
                <TableCell>Checked Documents:</TableCell>
                <TableCell align="right">{data.checkedDocuments}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Checked Constraints:</TableCell>
                <TableCell align="right">{data.checkedConstraints}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Found Errors:</TableCell>
                <TableCell align="right">{data.errors}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Aborted Checks:</TableCell>
                <TableCell align="right">{data.aborts}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>&empty; Errors Per Document Check:</TableCell>
                <TableCell align="right">
                  {data.checkedDocuments == 0
                    ? 0
                    : (data.errors / data.checkedDocuments).toFixed(2)}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>&empty; Errors Per Constraint Check:</TableCell>
                <TableCell align="right">
                  {data.checkedConstraints == 0
                    ? 0
                    : (data.errors / data.checkedConstraints).toFixed(2)}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        )}
      </CardContent>
    </Card>
  )
}

export default ValidationStatWidget
