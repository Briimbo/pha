import { ExpandMore } from '@mui/icons-material'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Typography,
  lighten,
} from '@mui/material'

import React from 'react'

import { groupBy } from '@utils/arrayUtils'

import {
  Notice,
  NoticePhaseEnum,
  NoticeTypeEnum,
  ValidationStateStateEnum,
} from '@generated/openapi'

import { NoticeGroups, getIcon } from './ConstraintResult'

export const PHASE_DESCRIPTION = {
  [NoticePhaseEnum.Check1]: 'CHECK 1: Do automations lead to unwanted states?',
  [NoticePhaseEnum.Check2]:
    'CHECK 2: Are there automations followed by environment events that lead to unwanted states?',
  [NoticePhaseEnum.Check3]:
    'CHECK 3: Which unwanted states are not automatically fixed by automations?',
  [NoticePhaseEnum.Check4]:
    'CHECK 4: Do automations result in continuous loops of change?',
  General: 'General Results',
}

const DEFAULT_NOTICES = {
  [NoticeTypeEnum.Error]: [],
  [NoticeTypeEnum.Warning]: [],
  [NoticeTypeEnum.Info]: [],
}

const CheckResult = ({
  state,
  phase,
  notices,
}: {
  state: ValidationStateStateEnum
  phase: NoticeGroups
  notices: Notice[]
}) => {
  const {
    [NoticeTypeEnum.Error]: errors,
    [NoticeTypeEnum.Warning]: warnings,
    [NoticeTypeEnum.Info]: infos,
  } = { ...DEFAULT_NOTICES, ...groupBy(notices, (n) => n.type) }

  return (
    <Accordion key={phase}>
      <AccordionSummary
        expandIcon={<ExpandMore />}
        sx={(theme) => ({
          backgroundColor:
            theme.palette.mode === 'dark'
              ? theme.palette.background.default
              : lighten(theme.palette.primary.light, 0.75),
          flexDirection: 'row-reverse',
          '& .MuiAccordionSummary-content': {
            marginLeft: theme.spacing(1),
            marginBlock: 0,
          },
        })}
      >
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
          }}
        >
          {state === ValidationStateStateEnum.Unknown && getIcon('UNKNOWN')}
          {state !== ValidationStateStateEnum.Unknown &&
            ((errors.length > 0 && getIcon(NoticeTypeEnum.Error)) ||
              getIcon('SUCCESS'))}
          <Typography
            sx={{ marginInlineStart: '0.5rem', marginInlineEnd: '1rem' }}
          >
            {PHASE_DESCRIPTION[phase]}
          </Typography>
          {infos.length > 0 && (
            <>
              <Box sx={{ marginInlineEnd: '1rem' }} />
              {getIcon(NoticeTypeEnum.Info)}
              <Typography>{infos.length}</Typography>
            </>
          )}
          {warnings.length > 0 && (
            <>
              <Box sx={{ marginInlineEnd: '1rem' }} />
              {getIcon(NoticeTypeEnum.Warning)}
              <Typography>{warnings.length}</Typography>
            </>
          )}
        </Box>
      </AccordionSummary>
      <Box sx={{ marginTop: '0.5rem' }} />
      {notices.length > 0 &&
        [...errors, ...warnings, ...infos].map((notice, index) => (
          <AccordionDetails
            key={index}
            sx={{ display: 'flex', flexDirection: 'row' }}
          >
            {getIcon(notice.type)}
            <Typography
              style={{ whiteSpace: 'pre-wrap', marginInlineStart: '0.5rem' }}
            >
              {notice.message}
            </Typography>
          </AccordionDetails>
        ))}
      {notices.length == 0 && (
        <AccordionDetails sx={{ display: 'flex', flexDirection: 'row' }}>
          <Typography>No issues found! :)</Typography>
        </AccordionDetails>
      )}
    </Accordion>
  )
}

export default CheckResult
