import {
  mdiAlert,
  mdiCheckCircle,
  mdiCloseCircle,
  mdiHelpCircle,
  mdiInformation,
} from '@mdi/js'
import { Box, Card, Divider, Tooltip, Typography } from '@mui/material'

import React from 'react'

import { groupBy } from '@utils/arrayUtils'

import FAIcon from '@components/Icon'
import { InteractiveSvg } from '@components/ImageDisplay'

import {
  Notice,
  NoticePhaseEnum,
  NoticeTypeEnum,
  ValidationState,
} from '@generated/openapi'

import CheckResult from './CheckResult'

export type NoticeGroups =
  | NoticePhaseEnum.Check1
  | NoticePhaseEnum.Check2
  | NoticePhaseEnum.Check3
  | NoticePhaseEnum.Check4
  | 'General'

const SUPPORTED_CHECKS = [
  NoticePhaseEnum.Check1,
  NoticePhaseEnum.Check2,
  NoticePhaseEnum.Check3,
  NoticePhaseEnum.Check4,
]

const ICONS = {
  SUCCESS: { color: 'green', icon: mdiCheckCircle },
  [NoticeTypeEnum.Error]: { color: 'red', icon: mdiCloseCircle },
  [NoticeTypeEnum.Warning]: { color: 'orange', icon: mdiAlert },
  [NoticeTypeEnum.Info]: { color: '#2480e8', icon: mdiInformation },
  UNKNOWN: { color: 'lightblue', icon: mdiHelpCircle },
}

const getIcon = (type: 'UNKNOWN' | 'SUCCESS' | NoticeTypeEnum) => {
  return (
    <Tooltip title={type}>
      <span>
        <FAIcon
          sx={{
            color: ICONS[type].color,
            marginInline: '.2rem',
            display: 'block',
          }}
          d={ICONS[type].icon}
        />
      </span>
    </Tooltip>
  )
}

const ConstraintResult = ({ constraint }: { constraint: ValidationState }) => {
  const defaultNotices = SUPPORTED_CHECKS.map((p) => [
    p,
    [] as Notice[],
  ]) as Array<[NoticePhaseEnum, Notice[]]>
  const noticesByPhase = {
    ...Object.fromEntries(defaultNotices),
    ...groupBy(constraint.notices, (n) =>
      SUPPORTED_CHECKS.includes(n.phase) ? n.phase : 'General'
    ),
  } as { [key in NoticeGroups]: Notice[] }

  return (
    <>
      <Typography variant="h4">
        Constraint: {constraint.constraint.name}
      </Typography>
      <Box style={{ width: '98%', marginInlineStart: '0.5rem' }}>
        <Typography variant="h4" style={{ marginBlock: '1rem' }}>
          Checks:
        </Typography>
        {Object.entries(noticesByPhase).map(([phase, notices]) => (
          <CheckResult
            key={phase}
            state={constraint.state}
            phase={phase as NoticeGroups}
            notices={notices}
          />
        ))}
        <Divider />
        {constraint.svg && (
          <Card
            sx={{
              padding: '1rem',
              marginBlock: '1rem',
              cursor: 'grab',
            }}
          >
            <InteractiveSvg svg={constraint.svg} />
          </Card>
        )}
      </Box>
    </>
  )
}

export { getIcon }
export default ConstraintResult
