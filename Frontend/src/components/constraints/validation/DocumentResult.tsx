import {
  Box,
  Card,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
} from '@mui/material'

import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

import { getStateIcon } from '@pages/Documents'

import { aggregateDocumentState } from '@components/constraints/documentOverview/DocumentRow'
import ConstraintResult from '@components/constraints/validation/ConstraintResult'

import {
  StructuredConstraintDocumentDto,
  ValidationControllerApi,
  ValidationState,
  ValidationStateStateEnum,
} from '@generated/openapi'

const getSummaryText = (state: ValidationStateStateEnum) => {
  switch (state) {
    case 'FAIL':
      return 'There are issues in your configuration!'
    case 'SUCCESS':
      return 'No issues found!'
    case 'UNKNOWN':
      return 'Run checks to find issues!'
    case 'IN_PROGRESS':
      return 'Waiting for checks to complete...'
  }
}

const getSummary = (states: ValidationState[]) => {
  const aggregatedState = aggregateDocumentState(states)

  return (
    <>
      <Typography variant="h4">Validation Result: </Typography>
      {getStateIcon(aggregatedState, {
        fontSize: '40px',
        marginInline: '1rem',
      })}
      {
        <Typography variant="subtitle1">
          {getSummaryText(aggregatedState)}
        </Typography>
      }
    </>
  )
}

const DocumentResult = () => {
  const { id } = useParams<{ id: string }>()
  const [document, setDocument] = useState<StructuredConstraintDocumentDto>()
  const [states, setStates] = useState<ValidationState[]>([])
  const [selectedConstraintIdx, setSelectedConstraintIdx] = useState(0)
  const api = new ValidationControllerApi()

  useEffect(() => {
    api
      .getUsingGET({ id: Number.parseInt(id) })
      .then((res) => setDocument(res.data?.structured))
  }, [])

  useEffect(() => {
    if (document) {
      api
        .pollUsingPUT({ constraintDto: document.constraints })
        .then((res) => setStates(res.data ?? []))
    }
  }, [document])

  return (
    <>
      <Box style={{ overflow: 'auto', maxHeight: '100%' }}>
        <Box
          style={{
            padding: '0.5rem',
            marginBlockEnd: '0.5rem',
            flexDirection: 'row',
            display: 'flex',
            alignItems: 'center',
          }}
        >
          {getSummary(states)}
        </Box>
        <Card
          sx={{
            flexDirection: 'row',
            display: 'flex',
            marginBlockStart: '2rem',
          }}
        >
          <Card
            style={{
              width: '15%',
              margin: '.1rem',
              marginRight: '0',
              paddingInline: '.5rem',
            }}
          >
            <List>
              {states.map((vs, index) => (
                <ListItemButton
                  key={vs.constraint.name + index}
                  onClick={() => setSelectedConstraintIdx(index)}
                  selected={selectedConstraintIdx === index}
                >
                  <ListItemIcon
                    style={{
                      marginInlineStart: '-0.5rem',
                      marginInlineEnd: '-1rem',
                    }}
                  >
                    {getStateIcon(vs.state)}
                  </ListItemIcon>{' '}
                  <ListItemText
                    style={{ whiteSpace: 'normal', wordWrap: 'break-word' }}
                  >
                    {vs.constraint.name}
                  </ListItemText>
                </ListItemButton>
              ))}
            </List>
          </Card>
          <Card style={{ width: '100%', padding: '1rem' }}>
            {states.length > 0 && (
              <ConstraintResult constraint={states[selectedConstraintIdx]} />
            )}
          </Card>
        </Card>
      </Box>
    </>
  )
}

export default DocumentResult
