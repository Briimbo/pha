import { SvgIcon } from '@mui/material'
import { SxProps, Theme } from '@mui/material/styles'

import React from 'react'

const FAIcon = ({
  d,
  sx,
  onClick,
}: {
  d: string
  sx?: SxProps<Theme>
  onClick?: () => void
}) => {
  return (
    <SvgIcon sx={sx} onClick={() => !!onClick && onClick()}>
      <path d={d} />
    </SvgIcon>
  )
}

export default FAIcon
