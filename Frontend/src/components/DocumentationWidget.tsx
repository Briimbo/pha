import { Card, CardContent, CardHeader, Link, Typography } from '@mui/material'

import React from 'react'

const DocumentationWidget = () => {
  return (
    <Card>
      <CardHeader title="Getting Started" />
      <CardContent>
        <Typography>
          The goal of Painless Home Automation is to assist in configuring your
          smart home and to help you validate your configuration. You can
          <ul>
            <li>
              Create interactive floor plans to get a better overview over your
              smart home
            </li>
            <li>
              Check that your configuration works correctly by definining
              constraints
            </li>
            <li>
              Automatically monitor your smart home to get notified in case of
              unwanted behavior
            </li>
          </ul>
          For further information, see the sections below. You can find the
          further documentation of this project in the READMEs.
        </Typography>
        <Typography variant="h5" sx={{ marginBlockStart: 3 }}>
          Floor Plan Modeling
        </Typography>
        <Typography>
          <p>
            To better understand your current physical setup, it helps to first
            create an overview over devices in your smart home. The floor
            planner will help you create interactive overviews that you can
            later integrate into your HomeAssistant dashboard. Use the toolbox
            to add elements to the floor plan and design it using drag and drop
            or resizing to match your physical setup. The scale allows you to
            define constrants (see next section) based on distance.
          </p>
          <p>
            Once you are finished, you can export the floor plan to your Home
            Assistant dashboard or export the created rooms as{' '}
            <Link href="https://www.home-assistant.io/integrations/group/">
              Home Assistant groups
            </Link>{' '}
            (old-style) containing the entities in that room. You have to
            prepare the export as explained in the frontend documentation, but
            this should by a one-time effort of a couple minutes.
          </p>
        </Typography>
        <Typography variant="h5" sx={{ marginBlockStart: 3 }}>
          Constraint Checking
        </Typography>
        <Typography>
          <p>
            For checking your smart home configuration, you can define
            constraints about entities that should always hold. For example, you
            could specify that the window should not be open while the heater is
            turned on:
          </p>
          <p>
            Constraints are grouped in documents, which allows you to structure
            them, delete multiple at once or disable them together for
            experimental (or permanent) purposes. For example, you could create
            one document for your bathroom and one for your kitchen, or one for
            your heaters and one for your windows, etc. This reference backend
            documentation gives you guidance on how to define constraints.
          </p>
          <p>
            After you ran the checks, we will tell you if there are issues with
            your current configuration the could lead to violation of the
            constraints. The graph tells you the path of events that cause
            problems. You can decide on the actions you take yourself, it might
            also be okay to leave the configuration as is in case you are
            smarter than the system.
          </p>
        </Typography>
        <Typography variant="h5" sx={{ marginBlockStart: 3 }}>
          Monitoring
        </Typography>
        <Typography>
          <p>
            In some cases, it might not be possible to prove absence of errors
            or there may be cases where your smart home behaves unexpectedly,
            causing unforseeable actual violations of constraints. Our
            monitoring regularly checks whether all of your constraints still
            hold during runtime. If there are violations, they can be sent to
            you via a notification. Like for usual{' '}
            <Link href="https://www.home-assistant.io/integrations/notify/">
              Home Assistant notifications
            </Link>
            , you have to provide the information about a notification service
            that we should send the notifications to. You can also view them by
            clicking on the bell in the top right corner.
          </p>
        </Typography>
        <Typography variant="h5" sx={{ marginBlockStart: 3 }}>
          About this project
        </Typography>
        <Typography>
          <p>
            This project was developed by the students of the Projektgruppe 646
            (PG646) at the department of computer science of TU Dortmund
            University at the chair for programming systems. The goal of our
            project was to bring DevOps to the domain of smart homes because
            configuration and validation can be cumbersome and does not scale
            well. At the same time, one&apos;s personal living space should feel
            safe, which is not a given anymore with increasing technology
            brought to everyone&apos;s home. Although our approach cannot, of
            course, provide absolute safety, it still is a step in the right
            direction.
          </p>
          <p>
            For further details about theoretical aspects, background,
            evaluation and full feature presentation, feel free to read the
            accompanying report for this project. Please contact us for the
            report.
          </p>
          <p>
            The full source code of this project can be accessed on request.
          </p>
          <p>
            In case you have any questions, feel free to reach out to us via
            GitLab or contact our supervisors{' '}
            <Link href="mailto:tim.tegeler@tu-dortmund.de">Tim Tegeler</Link> or{' '}
            <Link href="mailto:dominic.wirkner@tu-dortmund.de">
              Dominic Wirkner
            </Link>
            .
          </p>
        </Typography>
        <Typography variant="h5">Authors</Typography>
        <Typography>
          <p>
            Benjamin Kraatz, Benjamin Laumann, Christian Benedict Smit, Daniel
            Ginter, Daniel Stefan Heinz-Eugen Klose, David Schmidt, Etienne
            Palanga, Gerit Maldaner, Julien Leuering, Lisa Salewsky and
            Sebastian Teumert
          </p>
        </Typography>
      </CardContent>
    </Card>
  )
}

export default DocumentationWidget
