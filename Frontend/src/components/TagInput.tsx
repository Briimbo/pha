import { Chip, TextField } from '@mui/material'

import React from 'react'

const TagInput = ({
  tags,
  setTags,
  placeholder,
}: {
  tags: string[]
  setTags: (tags: string[]) => void
  placeholder?: string
}) => {
  const [input, setInput] = React.useState('')

  const deleteTag = (tag: string) => {
    const newTags = [...tags]
    newTags.splice(newTags.indexOf(tag), 1)
    setTags(newTags)
  }

  const onKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    const targetValue = (e.target as HTMLInputElement).value
    if (e.key === 'Enter') {
      if (targetValue && tags.indexOf(targetValue) === -1) {
        setTags([...tags, targetValue])
      }
      setInput('')
    }
  }

  return (
    <TextField
      id="input-with-icon-textfield"
      label={placeholder}
      value={input}
      fullWidth
      helperText="press enter to add the item"
      onChange={(e) => setInput(e.target.value)}
      onKeyDown={onKeyDown}
      onBlur={(e) => {
        if (e.target.value && tags.indexOf(e.target.value) === -1) {
          setTags([...tags, e.target.value])
        }
        setInput('')
      }}
      InputProps={{
        startAdornment: (
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {tags.map((t) => (
              <Chip
                key={t}
                label={t}
                onDelete={() => deleteTag(t)}
                sx={{ marginRight: '.25rem', marginY: '.25rem' }}
              />
            ))}
          </div>
        ),
      }}
      variant="outlined"
    />
  )
}

export default TagInput
