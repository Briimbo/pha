import Konva from 'konva'

import React, { useEffect, useRef } from 'react'
import { Transformer } from 'react-konva'

import { FloorPlanActions } from '@pages/reducers/floorPlanReducer'

import Room, {
  PolylineShape,
  RoomProps,
  RoomShape,
} from '@components/floorplan/Room'

const RoomLayer = (props: {
  rooms: Array<RoomProps>
  selectedRooms: Array<string>
  dispatch: React.Dispatch<FloorPlanActions>
  drawRooms: boolean
  usePolyline: boolean
  exportMode?: boolean
  listening?: boolean
}) => {
  const trRef = useRef(null)
  const { rooms, selectedRooms, dispatch, drawRooms, usePolyline } = props

  useEffect(() => {
    if (drawRooms && usePolyline) {
      return
    }

    // delete removed rooms from transformer
    const transformer = trRef.current as unknown as Konva.Transformer
    if (transformer) {
      const newNodes = transformer
        .nodes()
        .filter((e) => selectedRooms.includes(e.attrs.id))
      transformer.nodes(newNodes)
    }
  }, [selectedRooms, trRef])

  const setShapeInTransformer = (
    selected: boolean,
    shapeId: string,
    shape?: Konva.Node
  ) => {
    const transformer = trRef.current as unknown as Konva.Transformer
    if (transformer) {
      const oldNodes = transformer.nodes()
      let newNodes: Array<Konva.Node> = []

      if (selected) {
        // add node to Transformer's nodes
        if (shape) {
          newNodes = oldNodes.concat(shape)
        }
      } else {
        newNodes = oldNodes.filter((node: Konva.Node) => node.id() !== shapeId)
      }
      transformer.nodes(newNodes)
      transformer.getLayer()?.batchDraw()
    }
  }

  return (
    <>
      {rooms.map((room, idx) => {
        return (
          <Room
            roomProps={room}
            selectedRooms={selectedRooms}
            onSelect={({ entities, rooms }) =>
              dispatch({ type: 'setSelection', selection: { entities, rooms } })
            }
            selected={selectedRooms.includes(room.id)}
            updateShape={(
              newShape: PolylineShape | RoomShape,
              shiftx: number,
              shifty: number
            ) => {
              dispatch({
                type: 'updateRoom',
                idx,
                room: { ...room, shape: newShape },
                shiftx,
                shifty,
              })
            }}
            key={room.id}
            onDblClick={() =>
              dispatch({ type: 'setEditDialogRoom', id: room.id })
            }
            drawRooms={drawRooms}
            usePolyline={usePolyline}
            setShapeInTransformer={setShapeInTransformer}
            exportMode={props.exportMode}
            listening={props.listening}
          />
        )
      })}
      <Transformer
        ref={trRef}
        rotateEnabled={false}
        boundBoxFunc={(oldBox, newBox) => {
          // limit resize
          if (newBox.width < 5 || newBox.height < 5) {
            return oldBox
          }
          return newBox
        }}
      />
    </>
  )
}

export default RoomLayer
