import { mdiClose, mdiMagnify } from '@mdi/js'
import {
  Checkbox,
  FormControl,
  FormControlLabel,
  IconButton,
  InputAdornment,
  InputLabel,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  Tooltip,
  Typography,
} from '@mui/material'
import List from '@mui/material/List'
import { EntityState } from 'generated/openapi'

import React, { useEffect, useState } from 'react'

import useApi from '@hooks/useAPI'

import { getGroupsWithUnplaced } from '@utils/GroupAssignment'
import { entityToIcon } from '@utils/entityIconMapper'
import { fuzzyFilterList } from '@utils/searchUtils'

import {
  FloorPlanActions,
  FloorPlanState,
} from '@pages/reducers/floorPlanReducer'

import FullWidthAlert from '@components/FullWidthAlert'
import FAIcon from '@components/Icon'

import { addEntity } from './Entity'

type SortOption = 'name' | 'type' | ''
const sortOptions = [
  {
    name: 'Name',
    value: 'name',
  },
  {
    name: 'Type',
    value: 'type',
  },
]

const sort = (entities: EntityState[], sortOption: SortOption) => {
  if (sortOption === 'name') {
    return entities.sort((a, b) => {
      const aName = a.attributes.friendlyName || a.entityId
      const bName = b.attributes.friendlyName || b.entityId
      return aName.localeCompare(bName)
    })
  } else if (sortOption === 'type') {
    return entities.sort((a, b) => {
      const aType = a.attributes.deviceKind
      const bType = b.attributes.deviceKind
      return aType.localeCompare(bType)
    })
  } else {
    return entities
  }
}

const EntityList = (props: {
  inputState: FloorPlanState
  dispatch: React.Dispatch<FloorPlanActions>
}) => {
  const [error, setError] = useState<string | undefined>()
  const [entities, setEntities] = useState<Array<EntityState>>([])
  const [sortOption, setSortOption] = useState<SortOption>('')

  const { callApi: loadEntities } = useApi<undefined, Array<EntityState>>(
    'ha/entities',
    'GET'
  )

  useEffect(() => {
    loadEntities(undefined).then(({ data, errorMessage }) => {
      if (errorMessage?.length) {
        setError(errorMessage.join(', '))
      }
      if (data) {
        setEntities(data)
        setDisplayedEntities([['', data]])
      }
    })
  }, [])

  const [searchTerm, setSearchTerm] = useState('')
  const [isGroupEntities, setGroupEntities] = useState(false)
  const [groupedEntities, setGroupedEntities] = useState<
    Array<[string, Array<EntityState>]>
  >([])
  const [displayedEntities, setDisplayedEntities] = useState<
    Array<[string, Array<EntityState>]>
  >([])

  useEffect(() => {
    let result: Array<[string, Array<EntityState>]> = []
    if (isGroupEntities) {
      result = getGroupsWithUnplaced(
        props.inputState.rooms,
        props.inputState.entities,
        entities
      )
    } else {
      result = [['', entities]]
    }
    setGroupedEntities(result)
  }, [
    props.inputState.rooms,
    props.inputState.entities,
    isGroupEntities,
    entities,
  ])

  useEffect(() => {
    let result = groupedEntities
    if (searchTerm) {
      result = result.map(([roomName, roomEntities]) => [
        roomName,
        fuzzyFilterList<EntityState>(searchTerm, roomEntities, [
          (i) => i.entityId,
          (i) => i.attributes.friendlyName ?? '',
        ]),
      ])
    }
    setDisplayedEntities(result)
  }, [searchTerm, groupedEntities])

  const setHover = (hovered: boolean, entity: string) => {
    const currentEntityIdx = props.inputState.entities.findIndex(
      (item) => item.id === entity
    )
    if (currentEntityIdx < 0) {
      return
    }
    props.dispatch({
      type: 'updateEntity',
      idx: currentEntityIdx,
      entity: {
        ...props.inputState.entities[currentEntityIdx],
        hovered: hovered,
      },
      shiftx: 0,
      shifty: 0,
    })
  }

  const renderEntity = (entity: EntityState) => {
    return (
      <ListItem
        disablePadding
        key={entity.entityId}
        onMouseEnter={() => setHover(true, entity.entityId)}
        onMouseLeave={() => setHover(false, entity.entityId)}
      >
        <ListItemButton
          key={entity.entityId}
          disabled={props.inputState.entities.some(
            (item) => item?.id === entity?.entityId
          )}
          onClick={() => {
            if (
              !props.inputState.entities.some(
                (item) => item.id === entity.entityId
              )
            ) {
              props.dispatch({
                type: 'addEntity',
                entity: addEntity(entity),
              })
            }
          }}
          sx={{
            '&:last-child td, &:last-child th': {
              border: 0,
            },
          }}
        >
          <ListItemIcon>
            <FAIcon d={entityToIcon(entity)} />
          </ListItemIcon>
          <Tooltip title={entity.attributes.friendlyName || entity.entityId}>
            <ListItemText
              sx={{ overflow: 'hidden' }}
              primary={entity.attributes.friendlyName}
            />
          </Tooltip>
        </ListItemButton>
      </ListItem>
    )
  }

  const renderRoomGroup = (
    [roomName, roomEntities]: [string, Array<EntityState>],
    idx: number
  ) => {
    roomEntities = sort([...roomEntities], sortOption)
    return (
      <React.Fragment key={`${idx}${roomName}Fragment`}>
        {roomName && (
          <ListItem>
            <ListItemText>
              <Typography variant="h6">{roomName}:</Typography>
            </ListItemText>
          </ListItem>
        )}
        {roomEntities &&
          roomEntities.length > 0 &&
          roomEntities.map(renderEntity)}
        {(!roomEntities || roomEntities.length == 0) && (
          <ListItem style={{ marginTop: -15, paddingLeft: 40 }}>
            <ListItemText>
              <Typography>This group is empty</Typography>
            </ListItemText>
          </ListItem>
        )}
      </React.Fragment>
    )
  }

  return (
    <>
      <List sx={{ width: '100%', maxWidth: 300 }}>
        <ListItem>
          <ListItemText>
            <Typography variant="h5">Entities:</Typography>
          </ListItemText>
        </ListItem>
        {entities && (
          <React.Fragment key="entityFragment">
            <ListItem
              style={{
                display: 'flex',
                flexDirection: 'column',
                marginBottom: 10,
              }}
            >
              <ListItemText>
                <FormControl sx={{ width: '100%' }} variant="outlined">
                  <OutlinedInput
                    size="small"
                    id="entity-search-input"
                    type="text"
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.target.value)}
                    endAdornment={
                      <>
                        {searchTerm && (
                          <InputAdornment position="end">
                            <IconButton onClick={() => setSearchTerm('')}>
                              <FAIcon d={mdiClose} />
                            </IconButton>
                          </InputAdornment>
                        )}
                        <InputAdornment position="end">
                          <FAIcon d={mdiMagnify} />
                        </InputAdornment>
                      </>
                    }
                  />
                </FormControl>
                <FormControl
                  sx={{ width: '100%', marginTop: '1rem' }}
                  variant="outlined"
                >
                  <InputLabel id="sort-filter-label">Sort By</InputLabel>
                  <Select
                    MenuProps={{ disableScrollLock: true }}
                    margin="dense"
                    size="small"
                    labelId="sort-filter-label"
                    value={sortOption}
                    label="sort by"
                    displayEmpty
                    sx={{ width: '100%', marginTop: '0.5rem' }}
                    placeholder="Sort By"
                    onChange={(e) =>
                      setSortOption(e.target.value as SortOption)
                    }
                    endAdornment={
                      sortOption !== '' && (
                        <InputAdornment position="end">
                          <IconButton
                            sx={{ marginRight: '1rem' }}
                            onClick={() => setSortOption('')}
                          >
                            <FAIcon d={mdiClose} />
                          </IconButton>
                        </InputAdornment>
                      )
                    }
                  >
                    {sortOptions.map((i) => (
                      <MenuItem key={i.name} value={i.value}>
                        {i.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </ListItemText>
              <FormControlLabel
                style={{ paddingLeft: 0, alignSelf: 'end' }}
                control={
                  <Checkbox
                    onChange={(e) => setGroupEntities(e.target.checked)}
                  />
                }
                label="Group By Rooms"
              />
            </ListItem>
          </React.Fragment>
        )}
        {displayedEntities.map(renderRoomGroup)}
        {error && (
          <ListItemText>
            <FullWidthAlert severity="error">{error}</FullWidthAlert>
          </ListItemText>
        )}
      </List>
    </>
  )
}

export default EntityList
