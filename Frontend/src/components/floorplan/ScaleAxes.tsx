import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  InputAdornment,
  Paper,
  TextField,
  useTheme,
} from '@mui/material'
import { KonvaEventObject } from 'konva/lib/Node'

import React, { useEffect, useState } from 'react'
import Draggable from 'react-draggable'
import { Layer, Line, Text } from 'react-konva'

import { FloorPlanActions } from '@pages/reducers/floorPlanReducer'

import FAIcon from '@components/Icon'

export type ScaleData = {
  drawScale: boolean
  drawStart?: [number, number]
  drawEnd?: [number, number]
  metersPerPixel: number
  show: boolean
}

const round = (d: number, stepSize: number) => {
  const decimals =
    stepSize.toString().length - Math.floor(stepSize).toString().length
  return +d.toFixed(decimals)
}

export const handleDrawScaleClick = (
  event: KonvaEventObject<MouseEvent>,
  scale: ScaleData,
  dispatch: (action: FloorPlanActions) => void
) => {
  let newScale: ScaleData
  const position = event.target.getStage()?.getPointerPosition()
  if (!scale.drawStart) {
    newScale = {
      ...scale,
      drawStart: position ? [position.x, position.y] : undefined,
    }
  } else {
    newScale = {
      ...scale,
      drawScale: false,
      drawEnd: position ? [position.x, position.y] : undefined,
    }
  }

  dispatch({
    type: 'updateValue',
    updObject: {
      name: 'scale',
      value: {
        ...newScale,
      },
    },
  })
}

export const handleDrawScaleMouseMove = (
  event: KonvaEventObject<MouseEvent>,
  scale: ScaleData,
  dispatch: (action: FloorPlanActions) => void
) => {
  if (!scale.drawScale || !scale.drawStart) {
    return
  }
  const position = event.target.getStage()?.getPointerPosition()
  dispatch({
    type: 'updateValue',
    updObject: {
      name: 'scale',
      value: {
        ...scale,
        drawEnd: position ? [position.x, position.y] : undefined,
      },
    },
  })
}

const AxisTick = ({
  axis,
  distance,
  metersPerPixel,
  stepSizeInMeters,
  color,
}: {
  axis: 'x' | 'y'
  distance: number
  metersPerPixel: number
  stepSizeInMeters: number
  color: string
}) => {
  const value = round(distance * metersPerPixel, stepSizeInMeters)

  if (axis === 'x') {
    const x = distance - value.toString().length * 2 - 6

    return (
      <>
        <Line points={[distance, 0, distance, 5]} stroke={color} />
        <Text x={x} y={9} text={`${value}m`} align="center" fill={color} />
      </>
    )
  } else {
    return (
      <>
        <Line points={[0, distance, 5, distance]} stroke={color} />
        <Text
          x={7}
          y={distance - 5}
          text={`${value}m`}
          align="center"
          fill={color}
        />
      </>
    )
  }
}

const DistanceLine = ({
  start,
  end,
  length,
  color,
}: {
  start: [number, number]
  end: [number, number]
  length: number
  color: string
}) => {
  const slopeX = end[0] - start[0]
  const slopeY = end[1] - start[1]
  const startEndLength = Math.hypot(slopeX, slopeY)
  const orthogonalSlopeX = slopeY / startEndLength
  const orthogonalSlopeY = -slopeX / startEndLength
  const lambda = (0.5 * length) / Math.hypot(orthogonalSlopeX, orthogonalSlopeY)
  const offsetX = lambda * orthogonalSlopeX
  const offsetY = lambda * orthogonalSlopeY

  return (
    <>
      <Line points={[...start, ...end]} stroke={color} />
      <Line
        points={[
          start[0] + offsetX,
          start[1] + offsetY,
          start[0] - offsetX,
          start[1] - offsetY,
        ]}
        stroke={color}
      />
      <Line
        points={[
          end[0] + offsetX,
          end[1] + offsetY,
          end[0] - offsetX,
          end[1] - offsetY,
        ]}
        stroke={color}
      />
    </>
  )
}

const ScaleAxes = ({
  width,
  height,
  scaleData,
}: {
  width?: number
  height?: number
  scaleData: ScaleData
}) => {
  if (!width || !height) return null
  const { metersPerPixel } = scaleData
  let stepSizeInMeters = 1

  // find step size in meters that is nice to display and that leads to
  // a step size in pixels between 60 and 120 pixels (last while condition)
  while (stepSizeInMeters / metersPerPixel > 30) {
    stepSizeInMeters /= 10
  }

  while (stepSizeInMeters / metersPerPixel < 30) {
    stepSizeInMeters *= 10
  }

  while (stepSizeInMeters / metersPerPixel > 20) {
    stepSizeInMeters /= 2
  }

  while (stepSizeInMeters / metersPerPixel < 60) {
    stepSizeInMeters *= 2
  }

  const stepSize = stepSizeInMeters / metersPerPixel
  const xStepCount = Math.floor(width / stepSize)
  const xSteps = Array.from(
    { length: xStepCount },
    (_, i) => (i + 1) * stepSize
  )
  const yStepCount = Math.floor(height / stepSize)
  const ySteps = Array.from(
    { length: yStepCount },
    (_, i) => (i + 1) * stepSize
  )

  const color = useTheme().palette.text.primary

  return (
    <Layer>
      {scaleData.drawStart && scaleData.drawEnd && (
        <DistanceLine
          start={scaleData.drawStart}
          end={scaleData.drawEnd}
          length={20}
          color={color}
        />
      )}
      {scaleData.show && (
        <>
          <Line points={[0, 0, xSteps[xSteps.length - 1], 0]} stroke={color} />
          {xSteps.map((x) => (
            <AxisTick
              key={`x-${x}`}
              distance={x}
              axis="x"
              metersPerPixel={metersPerPixel}
              stepSizeInMeters={stepSizeInMeters}
              color={color}
            />
          ))}
          <Line points={[0, 0, 0, ySteps[ySteps.length - 1]]} stroke={color} />
          {ySteps.map((y) => (
            <AxisTick
              key={`y-${y}`}
              distance={y}
              axis="y"
              metersPerPixel={metersPerPixel}
              stepSizeInMeters={stepSizeInMeters}
              color={color}
            />
          ))}
        </>
      )}
    </Layer>
  )
}

const ScaleButton = ({
  icon,
  scale,
  dispatch,
}: {
  icon: string
  scale: ScaleData
  dispatch: (action: FloorPlanActions) => void
}) => {
  const [distance, setDistance] = useState('')
  const [error, setError] = useState('')
  const dialogOpen =
    scale.drawStart !== undefined &&
    scale.drawEnd !== undefined &&
    !scale.drawScale

  const updateDistance = (value: string) => {
    const noCommas = value.replaceAll(',', '.')
    if (/^[0-9]*([,\\.][0-9]*)?$/.test(noCommas)) {
      setDistance(noCommas)
    }
    setError('')
  }

  useEffect(() => {
    setError('')
    setDistance('')
  }, [dialogOpen])

  const onButtonClick = () => {
    dispatch({
      type: 'updateValue',
      updObject: {
        name: 'scale',
        value: {
          ...scale,
          drawScale: true,
          drawEnd: undefined,
          drawStart: undefined,
        },
      },
    })
  }

  const updateScale = () => {
    const { drawStart, drawEnd } = scale
    if (!drawStart || !drawEnd) return
    if (!distance) {
      setError('Please enter a valid number!')
      return
    }
    const drawnDistanceInMeters = +distance
    if (drawnDistanceInMeters === 0) {
      setError('Distance must be more than 0!')
      return
    }
    const drawnDistanceInPixels = Math.hypot(
      drawEnd[0] - drawStart[0],
      drawEnd[1] - drawStart[1]
    )
    const newMetersPerPixel = drawnDistanceInMeters / drawnDistanceInPixels
    onScaleUpdateFinished(newMetersPerPixel)
  }

  const onScaleUpdateFinished = (newMetersPerPixel?: number) => {
    dispatch({
      type: 'updateValue',
      updObject: {
        name: 'scale',
        value: {
          ...scale,
          drawScale: false,
          drawEnd: undefined,
          drawStart: undefined,
          metersPerPixel: newMetersPerPixel ?? scale.metersPerPixel,
        },
      },
    })
  }

  return (
    <>
      <Button onClick={onButtonClick} variant="contained">
        <FAIcon d={icon} />
        Change Scale
      </Button>
      <Dialog
        open={dialogOpen}
        onClose={() => onScaleUpdateFinished()}
        PaperComponent={(props) => (
          <Draggable
            handle="#draggable-dialog-title"
            cancel={'[class*="MuiDialogContent-root"]'}
          >
            <Paper {...props} />
          </Draggable>
        )}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          Length of drawn line
        </DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            value={distance}
            onChange={(e) => updateDistance(e.target.value)}
            placeholder="e.g. 1.5"
            inputProps={{
              inputMode: 'numeric',
              pattern: '[0-9]*[,\\.][0-9]*',
              style: { textAlign: 'end' },
            }}
            error={!!error}
            helperText={error}
            InputProps={{
              endAdornment: (
                <InputAdornment
                  position="start"
                  sx={{ marginInlineStart: 0.3 }}
                >
                  m
                </InputAdornment>
              ),
            }}
            sx={{ marginBlockStart: 1 }}
          />
        </DialogContent>
        <DialogActions sx={{ justifyContent: 'space-between' }}>
          <Button onClick={() => onScaleUpdateFinished()} color="error">
            Cancel
          </Button>
          <Button onClick={() => updateScale()} variant="contained">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

const ScaleAxesCheckbox = ({
  scale: scale,
  dispatch,
}: {
  scale: ScaleData
  dispatch: (action: FloorPlanActions) => void
}) => {
  return (
    <FormControlLabel
      control={
        <Checkbox
          checked={scale.show}
          onChange={(e) =>
            dispatch({
              type: 'updateValue',
              updObject: {
                name: 'scale',
                value: { ...scale, show: e.target.checked },
              },
            })
          }
        />
      }
      label="show scale"
    />
  )
}

export { ScaleAxes, ScaleButton, ScaleAxesCheckbox }
