import { EntityState } from 'generated/openapi'
import Konva from 'konva'
import { KonvaEventObject } from 'konva/lib/Node'

import React, { useEffect, useRef } from 'react'
import { Group, Path, Rect, Text } from 'react-konva'

import { handleSelection } from '@utils/SelectionHelper'
import { entityToIcon } from '@utils/entityIconMapper'

import { SelectionUpdate } from '@pages/reducers/floorPlanReducer'

export type EntityComponentProps = {
  updateShape: (
    entityProps: EntityProps,
    shiftx: number,
    shifty: number
  ) => void
  entityProps: EntityProps
  onMouseClick: () => void
  onSelect: (selection: SelectionUpdate) => void
  selectedEntities: Array<string>
  drawRooms: boolean
  listening?: boolean
}
export type EntityProps = {
  type: 'entity'
  x: number
  y: number
  id: string
  entity: EntityState
  image: string
  hovered: boolean
}
export function addEntity(entity: EntityState): EntityProps {
  return {
    type: 'entity',
    x: 50 + Math.random() * 10,
    y: 50 + Math.random() * 10,
    id: entity.entityId,
    entity: entity,
    image: entityToIcon(entity),
    hovered: false,
  }
}
export const ICON_DIMENSIONS = { width: 35, height: 35 }
const Entity = (props: EntityComponentProps) => {
  const {
    entityProps,
    updateShape,
    onMouseClick,
    onSelect,
    selectedEntities,
    drawRooms,
    listening,
  } = props
  const { hovered, entity, image } = entityProps

  const selected = selectedEntities.includes(entityProps.id)

  const setHover = (hov: boolean) => {
    updateShape({ ...entityProps, hovered: hov }, 0, 0)
  }

  const group = useRef<Konva.Group>(null)

  useEffect(() => {
    if (hovered) {
      const debounceDelay = setTimeout(() => {
        group.current?.to({
          scaleX: 1.2,
          scaleY: 1.2,
          duration: 0.2,
          onFinish: () =>
            group.current?.to({
              scaleX: 1.0,
              scaleY: 1.0,
              duration: 0.2,
            }),
        })
      }, 400)
      return () => clearTimeout(debounceDelay)
    }
  }, [hovered])

  const handleEntityClick = (event: KonvaEventObject<MouseEvent>) => {
    const newSelection = handleSelection(
      drawRooms,
      event,
      selectedEntities,
      entity.entityId,
      selected,
      'entity'
    )
    onSelect(newSelection)
  }

  const handleDrag = (event: KonvaEventObject<MouseEvent>) => {
    const newX = event.target.x() + ICON_DIMENSIONS.width / 2
    const newY = event.target.y() + ICON_DIMENSIONS.width / 2
    const shiftx = selected ? newX - entityProps.x : 0
    const shifty = selected ? newY - entityProps.y : 0
    updateShape(
      {
        ...entityProps,
        x: newX,
        y: newY,
      },
      shiftx,
      shifty
    )
  }

  return (
    <Group
      _useStrictMode
      ref={group}
      draggable
      listening={listening}
      onDragEnd={handleDrag}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      x={entityProps.x - ICON_DIMENSIONS.width / 2}
      y={entityProps.y - ICON_DIMENSIONS.height / 2}
      onContextMenu={(e) => {
        e.evt.preventDefault()
        onMouseClick()
      }}
      onDblClick={() => onMouseClick()}
      onClick={(event) => handleEntityClick(event)}
    >
      <Rect x={0} y={0} {...ICON_DIMENSIONS}></Rect>
      <Path
        fill={selected ? 'green' : 'black'}
        data={image}
        x={0}
        y={0}
        scale={{ x: 1.5, y: 1.5 }}
      />
      {hovered && (
        <Text
          text={
            props.entityProps.entity.attributes.friendlyName ||
            props.entityProps.id
          }
          y={-15}
        />
      )}
    </Group>
  )
}

export default Entity
