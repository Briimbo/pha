import Konva from 'konva'

import React, { useRef } from 'react'
import { Image, Transformer } from 'react-konva'

export type BackgroundProps = {
  x: number
  y: number
  img: string
  editable: boolean
  width?: number
  height?: number
  opacity: number
}

export const createDefaultBackground = (img: string): BackgroundProps => ({
  editable: false,
  img,
  height: undefined,
  width: undefined,
  x: 0,
  y: 0,
  opacity: 1,
})

const BackgroundLayer = ({
  listening,
  shape,
  updateBackground,
}: {
  listening?: boolean
  shape: BackgroundProps
  updateBackground: (newValue: BackgroundProps) => void
}) => {
  const { editable, x, y, img, width, height, opacity } = shape
  const trRef = useRef<Konva.Transformer>(null)
  const shapeRef = useRef<Konva.Image>(null)

  React.useEffect(() => {
    if (editable && trRef.current && shapeRef.current) {
      trRef.current.nodes([shapeRef.current])
      trRef.current.getLayer()?.batchDraw()
    }
  }, [editable])

  const newImg = new window.Image()
  newImg.src = img

  return (
    <>
      {img && (
        <Image
          width={width}
          height={height}
          ref={shapeRef}
          x={x}
          y={y}
          opacity={opacity}
          draggable={editable}
          listening={listening}
          image={newImg}
          onDragEnd={(e) => {
            updateBackground({
              ...shape,
              x: e.target.x(),
              y: e.target.y(),
            })
          }}
          onTransformEnd={() => {
            const node = shapeRef.current

            if (node) {
              const scaleX = node.scaleX()
              const scaleY = node.scaleY()

              // we will reset it back
              node.scaleX(1)
              node.scaleY(1)
              updateBackground({
                ...shape,
                x: node.x(),
                y: node.y(),
                // set minimal value
                width: Math.max(5, node.width() * scaleX),
                height: Math.max(node.height() * scaleY),
              })
            }
          }}
        />
      )}
      {img && editable && (
        <Transformer
          ref={trRef}
          boundBoxFunc={(oldBox, newBox) => {
            // limit resize
            if (newBox.width < 5 || newBox.height < 5) {
              return oldBox
            }
            return newBox
          }}
        />
      )}
    </>
  )
}

export default BackgroundLayer
