import Konva from 'konva'

import React, { useEffect, useReducer, useRef, useState } from 'react'
import ReactDOM from 'react-dom'
import { Layer, Stage } from 'react-konva'

import { useComponentDidUpdate } from '@hooks/useComponentDidUpdate'

import { defaultFetchError, translateError } from '@utils/ErrorTranslator'
import { getDimensions, shiftToOrigin } from '@utils/FloorPlanDimensions'

import { useNotifications } from '@contexts/notificationContext'

import { FloorPlanState } from '@pages/reducers/floorPlanReducer'

import { FloorPlanDto, HomeAssistantControllerApi } from '@generated/openapi'

import BackgroundLayer from './BackgroundLayer'
import RoomLayer from './RoomLayer'

type FloorPlanExportProps = {
  floorPlanState: FloorPlanState
  doExport: boolean
  currentDBVersion: FloorPlanDto | undefined
  onExportFinished: () => void
}

const FloorPlanExport = (props: FloorPlanExportProps) => {
  const [state, setState] = useState({
    floorPlanState: props.floorPlanState,
    isExporting: false,
  })

  const [, mockDispatch] = useReducer(() => {
    /* do nothing */
  }, undefined)

  const stageRef = useRef<Konva.Stage>(null)
  const haApi = new HomeAssistantControllerApi()

  const { addNotification } = useNotifications()

  const onExportFinished = (error?: string[]) => {
    if (error && error.length > 0) {
      addNotification({
        type: 'error',
        message: `Export failed. ${error.join(' ')}`,
      })
    } else {
      addNotification({
        type: 'success',
        message:
          'Export successful. You should now be able to use this floor plan in your HomeAssistant instance.',
      })
    }
    setState({ ...state, isExporting: false })
    props.onExportFinished()
  }

  useEffect(() => {
    if (props.doExport) {
      setState({
        floorPlanState: shiftToOrigin(props.floorPlanState),
        isExporting: true,
      })
    }
  }, [props.doExport])

  useComponentDidUpdate(() => {
    if (props.doExport && state.isExporting) {
      const stage = stageRef.current
      const name = props.currentDBVersion?.name
      const id = props.currentDBVersion?.id

      if (!name || !id) {
        onExportFinished(['The floorplan needs to have a name and an id'])
        return
      }

      if (!stage) {
        onExportFinished([
          'Unexpted error. "stage" contains a falsy value. Please report this to the developers :)',
        ])
        return
      }

      const dimensions = getDimensions(state.floorPlanState)
      stage.toDataURL({
        width: dimensions.width,
        height: dimensions.height,
        callback: (img) => {
          haApi
            .exportFloorPlanToCardUsingPOST({
              exportFloorPlanDto: {
                ...dimensions,
                id: id,
                name: name,
                backgroundImgBase64: img,
                entities: state.floorPlanState.entities.map((e) => ({
                  ...e,
                  deviceKind: e.entity.attributes.deviceKind,
                })),
              },
            })
            .then((res) => {
              const errors = res.results
                ? translateError(res.results)
                : undefined
              onExportFinished(errors)
            })
            .catch(() => {
              onExportFinished([defaultFetchError])
            })
        },
      })
    }
  }, [state.isExporting])

  // Only include parts that will be used as the background image for HA, e.g. our background image and the drawn rooms
  return (
    <Stage ref={stageRef}>
      <Layer>
        {state.floorPlanState.background && (
          <BackgroundLayer
            shape={{
              ...state.floorPlanState.background,
              editable: false,
            }}
            updateBackground={() => {
              /* do nothing */
            }}
          />
        )}
      </Layer>
      <Layer>
        <RoomLayer
          rooms={state.floorPlanState.rooms}
          selectedRooms={[]}
          dispatch={mockDispatch}
          drawRooms={false}
          usePolyline={false}
          exportMode
        />
      </Layer>
    </Stage>
  )
}

export const FloorPlanExportPortal = (props: FloorPlanExportProps) => {
  const [container] = React.useState(() => {
    return document.createElement('div')
  })

  React.useEffect(() => {
    container.id = 'root-portal'
    document.body.appendChild(container)
    return () => {
      document.body.removeChild(container)
    }
  }, [])

  return ReactDOM.createPortal(<FloorPlanExport {...props} />, container)
}
