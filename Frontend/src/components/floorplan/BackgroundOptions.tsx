import { mdiImageArea } from '@mdi/js'
import { Button, Checkbox, Slider, Typography } from '@mui/material'

import React from 'react'

import { getBase64 } from '@utils/fileUtils'

import FAIcon from '@components/Icon'

import { BackgroundProps, createDefaultBackground } from './BackgroundLayer'

const BackgroundOptions = ({
  backgroundProps,
  updateBackground,
}: {
  updateBackground: (backgroundProps?: BackgroundProps) => void
  backgroundProps?: BackgroundProps
}) => {
  const onFileLoad = async ({
    target,
  }: React.ChangeEvent<HTMLInputElement>) => {
    try {
      if (target.files?.length) {
        const file = target.files[0]
        const imgFile = await getBase64(file)
        const tmpImg = new Image()
        tmpImg.src = imgFile
        tmpImg.onload = () => {
          updateBackground({
            ...(backgroundProps || createDefaultBackground(imgFile)),
            img: imgFile,
            editable: false,
            width: tmpImg.width,
            height: tmpImg.height,
          })
        }
      } else {
        updateBackground(undefined)
      }
    } catch (e) {
      // should i do error handling here ? how could this happen ?
      updateBackground(undefined)
    }
  }

  const remove = (e: React.MouseEvent) => {
    if (backgroundProps?.img) {
      updateBackground(undefined)
      e.preventDefault()
    }
  }

  return (
    <>
      <Button
        variant="contained"
        component="label"
        onClick={(e: React.MouseEvent) => remove(e)}
      >
        <FAIcon d={mdiImageArea} />
        <span>
          {backgroundProps?.img ? 'Remove Background' : 'Use Background'}
        </span>
        {!backgroundProps?.img && (
          <input
            type="file"
            multiple={false}
            hidden
            accept="image/*"
            onChange={(e) => onFileLoad(e)}
          />
        )}
      </Button>
      {backgroundProps && (
        <>
          <div>
            <label>Edit Mode</label>
            <Checkbox
              checked={backgroundProps.editable}
              onChange={(_, checked) =>
                updateBackground({ ...backgroundProps, editable: checked })
              }
            />
          </div>

          {backgroundProps?.editable && (
            <>
              <Typography id="input-slider" gutterBottom>
                Opacity
              </Typography>
              <Slider
                sx={{ width: '80%' }}
                min={0}
                max={1}
                aria-labelledby="input-slider"
                step={0.05}
                value={backgroundProps.opacity}
                onChange={(_, num) =>
                  updateBackground({
                    ...backgroundProps,
                    opacity: typeof num == 'number' ? num : 1,
                  })
                }
              />
            </>
          )}
        </>
      )}
    </>
  )
}

export default BackgroundOptions
