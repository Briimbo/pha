import Konva from 'konva'

import React, { forwardRef } from 'react'
import { Group, Line, Rect, Text } from 'react-konva'

import {
  LabelProps,
  Point,
  PolylineShape,
  RoomProps,
  RoomShape,
} from '@components/floorplan/Room'

type PolylineRoomProps = {
  onClick: (event: Konva.KonvaEventObject<MouseEvent>) => void
  onDblClick: () => void
  updateShape: (
    shape: RoomShape | PolylineShape,
    shiftx: number,
    shifty: number
  ) => void
  room: RoomProps
  drawRooms: boolean
  selected: boolean
  usePolyline: boolean
  exportMode?: boolean
  labelProps: LabelProps
  listening?: boolean
}

const BasePolylineRoom = (
  {
    onClick,
    onDblClick,
    updateShape,
    labelProps,
    room,
    drawRooms,
    usePolyline,
    exportMode,
    selected,
    listening,
  }: PolylineRoomProps,
  shapeRef: React.ForwardedRef<Konva.Line>
) => {
  if (room.shape.type === 'rectangle') {
    throw new Error(
      'Type Error. Cannot process type rectangle in component PolylineRoom'
    )
  }
  const { points, isFinished } = room.shape

  // [ [a, b], [c, d], ... ] to [ a, b, c, d, ...]
  const flattenedPoints = points.flatMap((p) => [p.x, p.y])

  const labelPos =
    points.length > 0
      ? { x: points[0].x + 5, y: points[0].y + 10 }
      : { x: 0, y: 0 }

  /**
   *
   * @param event the Konva mouse event
   * @returns nothing (returns only if the current state is invalid)
   *
   * sets the state if the mouse is over the start point of a polygon
   */
  const handleMouseOverStartPoint = (
    event: Konva.KonvaEventObject<MouseEvent>
  ) => {
    if (!drawRooms && !usePolyline) {
      return
    }

    event.target.scale({ x: 2, y: 2 })
    if (room && room.shape.type === 'polyline') {
      updateShape(
        {
          ...room.shape,
          mouseOverStart: true,
        },
        0,
        0
      )
    }
  }

  /**
   *
   * @param event the Konva mouse event
   * @returns nothing (returns only if the current state is invalid)
   *
   * sets the state if the mouse leaves the start point of a polygon
   */

  const handleMouseOutStartPoint = (
    event: Konva.KonvaEventObject<MouseEvent>
  ) => {
    if (!drawRooms && !usePolyline) {
      return
    }

    event.target.scale({ x: 1, y: 1 })
    if (room && room.shape.type === 'polyline') {
      updateShape(
        {
          ...room.shape,
          mouseOverStart: false,
        },
        0,
        0
      )
    }
  }

  /**
   *
   * @param event the Konva mouse event
   * @returns nothing (returns only if the current state is invalid)
   *
   * allows for moving a point of an existing polygon
   */
  const handleDragMovePoint = (event: Konva.KonvaEventObject<MouseEvent>) => {
    // Get the point and the index of the point
    const index = event.target.index - 1
    const pos: Point = { x: event.target.attrs.x, y: event.target.attrs.y }

    if (room?.shape && room.shape.type === 'polyline') {
      const points = room.shape.points
      updateShape(
        {
          ...room.shape,
          points: [...points.slice(0, index), pos, ...points.slice(index + 1)],
        },
        0,
        0
      )
    }
  }

  const handlePolyDrag = (event: Konva.KonvaEventObject<MouseEvent>) => {
    // check type so that typescript is happy and make sure that the target is no corner rectangle
    if (
      room.shape.type === 'polyline' &&
      !event.target.attrs.height &&
      typeof shapeRef !== 'function' &&
      shapeRef?.current
    ) {
      const absolutePoints = []
      const points = shapeRef.current.points()
      const transform = shapeRef.current.getAbsoluteTransform()

      if (points && transform) {
        let i = 0
        while (i < points.length) {
          const point = {
            x: points[i],
            y: points[i + 1],
          }
          absolutePoints.push(transform.point(point))
          i = i + 2
        }

        // If the Polyline room is selected, compute delta x and delta y to shift other selected rooms and entities as well
        let shiftx = 0
        let shifty = 0
        if (room.shape.points[0] && absolutePoints[0] && selected) {
          shiftx = absolutePoints[0].x - room.shape.points[0].x
          shifty = absolutePoints[0].y - room.shape.points[0].y
        }

        updateShape(
          {
            ...room.shape,
            points: absolutePoints,
          },
          shiftx,
          shifty
        )
        event.target.position({ x: 0, y: 0 })
        event.target.scale({ x: 1, y: 1 })
      }
    }
  }

  return (
    <Group
      _useStrictMode
      listening={listening}
      onDblClick={onDblClick}
      draggable={room.shape.isFinished && !drawRooms}
      onDragEnd={handlePolyDrag}
    >
      <Line
        points={flattenedPoints}
        stroke="black"
        closed={isFinished}
        fill={room.shape.fill}
        onTransformEnd={handlePolyDrag}
        ref={shapeRef}
        onClick={onClick}
        id={room.shape.id}
      />
      {!exportMode &&
        points.map((point: Point, index: number) => {
          const width = 6
          const x = point.x - width / 2
          const y = point.y - width / 2
          const startPointAttr =
            index === 0
              ? {
                  hitStrokeWidth: 12,
                  onMouseOver: (event: Konva.KonvaEventObject<MouseEvent>) =>
                    handleMouseOverStartPoint(event),
                  onMouseOut: (event: Konva.KonvaEventObject<MouseEvent>) =>
                    handleMouseOutStartPoint(event),
                }
              : null
          return (
            <Rect // The Rectangle representing the corner of the polyline
              key={index}
              x={x}
              y={y}
              width={width}
              height={width}
              fill="white"
              stroke="black"
              onDragMove={(event: Konva.KonvaEventObject<MouseEvent>) =>
                handleDragMovePoint(event)
              }
              draggable
              {...startPointAttr}
            />
          )
        })}
      <Text text={room.name} {...labelPos} {...labelProps} />
    </Group>
  )
}

const PolylineRoom = forwardRef(BasePolylineRoom)

export default PolylineRoom
