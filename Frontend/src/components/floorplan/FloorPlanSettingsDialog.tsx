import { mdiCog } from '@mdi/js'
import { Tooltip } from '@mui/material'
import { Button } from '@mui/material'

import React, { useState } from 'react'

import useApi from '@hooks/useAPI'

import FAIcon from '@components/Icon'

import { FloorPlanDto } from '@generated/openapi'

import FloorPlanDialog from './FloorPlanDialog'

const FloorPlanSettingsDialog = (props: {
  currentFloorPlan: FloorPlanDto
  save: (fp: FloorPlanDto) => void
}) => {
  const [open, setOpen] = useState<boolean>(false)
  const [error, setError] = useState<string | undefined>(undefined)

  const { callApi, isLoading } = useApi<FloorPlanDto, FloorPlanDto>(
    'floor-plan',
    'POST'
  )

  const onSave = (data: FloorPlanDto) => {
    setError(undefined)
    callApi(data).then((res) => {
      if (res.errorMessage?.length) setError(res.errorMessage.join(', '))
      else {
        if (res.data) props.save(res.data)
        setOpen(false)
      }
    })
  }

  return (
    <>
      <Tooltip title="Edit Floor Plan">
        <Button variant="contained" onClick={() => setOpen(true)}>
          <FAIcon d={mdiCog} /> Settings
        </Button>
      </Tooltip>
      <FloorPlanDialog
        isLoading={isLoading}
        open={open}
        onSave={onSave}
        error={error}
        floorPlan={props.currentFloorPlan}
        onCancel={() => {
          setOpen(false)
          setError(undefined)
        }}
      />
    </>
  )
}

export default FloorPlanSettingsDialog
