import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  FormLabel,
  Grid,
  TextField,
} from '@mui/material'
import { Color, ColorPicker, createColor } from 'mui-color'

import React, { useState } from 'react'

import { useNotifications } from '@contexts/notificationContext'

import { RoomProps } from '@components/floorplan/Room'

const EditRoomDialog = (props: {
  room: RoomProps
  existingRooms: string[]
  onClose: () => void
  onSave: (room: RoomProps) => void
  onDelete: () => void
}) => {
  const { room, existingRooms, onClose, onSave, onDelete } = props
  const [color, setColor] = useState(createColor(room.shape.fill))
  const [name, setName] = useState(room.name)
  const { addNotification } = useNotifications()

  const onSavePressed = () => {
    if (existingRooms.some((r) => r === name)) {
      addNotification({
        type: 'error',
        message: 'A room with this name already exists.',
      })
      return
    }
    room.shape.fill = typeof color === 'string' ? color : `#${color.hex}`
    room.name = name
    onSave(room)
    onClose()
  }

  const onDeletePressed = () => {
    onDelete()
    onClose()
  }

  return (
    <Dialog open={room !== undefined} onClose={onClose}>
      <DialogTitle>Edit room &quot;{room.name}&quot;</DialogTitle>
      <DialogContent style={{ paddingTop: 10 }}>
        <Grid
          container
          alignItems="center"
          justifyContent="flex-end"
          columnSpacing={1}
          rowSpacing={3}
        >
          <Grid item xs={12} sm={3}>
            <FormLabel>room name</FormLabel>
          </Grid>
          <Grid item xs={12} sm={8}>
            <TextField
              id="room-name"
              label="room name"
              variant="outlined"
              fullWidth
              onChange={(e) => setName(e.target.value)}
              value={name}
            />
          </Grid>
          <Grid item xs={12} sm={3}>
            <FormLabel>color</FormLabel>
          </Grid>
          <Grid item xs={12} sm={8}>
            <ColorPicker
              value={color}
              onChange={(c) => {
                if (c as Color) {
                  setColor(c as Color)
                } else if (c as number | string) {
                  setColor(createColor(c))
                }
              }}
            />
          </Grid>
        </Grid>
        <Divider style={{ marginTop: 50 }} />
        <DialogActions
          style={{ justifyContent: 'space-between', marginTop: 10 }}
        >
          <Button onClick={onClose} color="error">
            Cancel
          </Button>
          <Button onClick={onDeletePressed} variant="contained" color="error">
            Remove
          </Button>
          <Button onClick={onSavePressed} variant="contained">
            Save
          </Button>
        </DialogActions>
      </DialogContent>
    </Dialog>
  )
}

export default EditRoomDialog
