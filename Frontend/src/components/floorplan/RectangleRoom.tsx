import Konva from 'konva'
import { KonvaEventObject } from 'konva/lib/Node'

import React, { forwardRef, useCallback } from 'react'
import { Group, Rect, Text } from 'react-konva'

import {
  LabelProps,
  PolylineShape,
  RoomProps,
  RoomShape,
} from '@components/floorplan/Room'

type RectangleRoomProps = {
  drawRooms: boolean
  room: RoomProps
  shape: RoomShape
  selected: boolean
  onClick: (event: Konva.KonvaEventObject<MouseEvent>) => void
  onDblClick: () => void
  updateShape: (
    shape: RoomShape | PolylineShape,
    shiftx: number,
    shifty: number
  ) => void
  labelProps: LabelProps
  listening?: boolean
}

const BaseRectangleRoom = (
  {
    drawRooms,
    room,
    shape,
    onClick,
    onDblClick,
    selected,
    labelProps,
    updateShape,
    listening,
  }: RectangleRoomProps,
  shapeRef: React.ForwardedRef<Konva.Rect>
) => {
  if (room.shape.type === 'polyline') {
    throw new Error(
      'Type Error. Cannot process type rectangle in component PolylineRoom'
    )
  }

  const handleDrag = useCallback(
    (event: KonvaEventObject<MouseEvent>) => {
      const newX = event.target.x()
      const newY = event.target.y()
      const shiftx = selected ? newX - shape.x : 0
      const shifty = selected ? newY - shape.y : 0

      updateShape(
        {
          ...shape,
          x: newX,
          y: newY,
        },
        shiftx,
        shifty
      )
    },
    [shape, selected, updateShape]
  )

  return (
    <Group
      _useStrictMode
      draggable={!drawRooms}
      listening={listening}
      onDragEnd={handleDrag}
      x={shape.x}
      y={shape.y}
    >
      <Rect
        id={shape.id}
        onClick={onClick}
        onDblClick={onDblClick}
        ref={shapeRef as React.RefObject<Konva.Rect>} // shapeRef is definitely of type  React.RefObject<Konva.Rect> due to its initialisation. Typescript just cannot infer that
        stroke="black"
        width={shape.width}
        height={shape.height}
        fill={shape.fill}
        onTransformEnd={(e) => {
          const node = e.target
          const group = node.parent

          if (group) {
            const scaleX = node.scaleX()
            const scaleY = node.scaleY()
            const x = group.x() + node.x()
            const y = group.y() + node.y()
            // we will reset it back
            node.scaleX(1)
            node.scaleY(1)
            node.x(0)
            node.y(0)

            updateShape(
              {
                ...shape,
                x: x,
                y: y,
                // set minimal value
                width: Math.max(5, node.width() * scaleX),
                height: Math.max(5, node.height() * scaleY),
              },
              0,
              0
            )
          }
        }}
      />
      <Text text={room.name} x={4} y={4} {...labelProps} />
    </Group>
  )
}

const RectangleRoom = forwardRef(BaseRectangleRoom)

export default RectangleRoom
