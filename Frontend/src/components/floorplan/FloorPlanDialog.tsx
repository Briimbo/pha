import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
} from '@mui/material'

import React, { ChangeEvent, useEffect, useState } from 'react'

import FullWidthAlert from '@components/FullWidthAlert'

import { FloorPlanDto } from '@generated/openapi'

const FloorPlanDialog = ({
  open,
  floorPlan,
  create,
  onSave,
  onCancel,
  isLoading,
  error,
}: {
  open: boolean
  floorPlan?: FloorPlanDto
  create?: boolean
  onCancel: () => void
  onSave: (p: FloorPlanDto) => void
  error?: string
  isLoading?: boolean
}) => {
  const [internalFloorPlan, setInternalFloorPlan] = useState({ ...floorPlan })

  const setValue =
    (name: keyof FloorPlanDto) => (event: ChangeEvent<HTMLTextAreaElement>) => {
      setInternalFloorPlan({ ...internalFloorPlan, [name]: event.target.value })
    }

  useEffect(() => {
    if (!open) {
      setInternalFloorPlan({ ...floorPlan })
    }
  }, [open])

  return (
    <Dialog open={open}>
      <DialogTitle>
        {create ? 'Create new floor plan' : 'Edit Floor Plan'}
      </DialogTitle>
      <DialogContent>
        <Grid
          container
          alignItems="center"
          justifyContent="flex-end"
          columnSpacing={1}
          rowSpacing={3}
          sx={{ paddingTop: '1rem' }}
        >
          <Grid item>
            <TextField
              label="Name"
              onChange={setValue('name')}
              value={internalFloorPlan.name}
            />
          </Grid>
          <Grid item>
            <TextField
              label="Floor"
              type="number"
              onChange={setValue('floor')}
              value={internalFloorPlan.floor}
              inputProps={{ inputMode: 'numeric', pattern: '[1-9][0-9]*' }}
            />
          </Grid>
        </Grid>
        {error && <FullWidthAlert severity="error">{error}</FullWidthAlert>}
      </DialogContent>
      <DialogActions sx={{ justifyContent: 'space-between' }}>
        <Button disabled={isLoading} color="error" onClick={() => onCancel()}>
          cancel
        </Button>
        <Button
          disabled={isLoading || !internalFloorPlan.name}
          variant="contained"
          onClick={() => onSave(internalFloorPlan)}
        >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default FloorPlanDialog
