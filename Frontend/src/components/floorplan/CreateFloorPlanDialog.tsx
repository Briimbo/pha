import { mdiPlus } from '@mdi/js'
import { IconButton, Tooltip } from '@mui/material'

import React, { useState } from 'react'
import { useHistory } from 'react-router'

import { defaultFetchError, translateError } from '@utils/ErrorTranslator'

import { FloorPlanControllerApi, FloorPlanDto } from '@generated/openapi'

import FAIcon from '../Icon'
import FloorPlanDialog from './FloorPlanDialog'

const randomName = () => {
  const options = [
    'Living Room',
    'Cellar',
    'First Floor',
    'Second Floor',
    'Heaven',
    'Attic',
  ]
  return options[Math.round(Math.random() * (options.length - 1))]
}

const CreateFloorPlanDialog = () => {
  const [open, setOpen] = useState<boolean>(false)
  const [isLoading, setLoading] = useState(false)
  const [error, setError] = useState<string | undefined>(undefined)

  const floorplanApi = new FloorPlanControllerApi()
  const history = useHistory()

  const onSave = (data: FloorPlanDto) => {
    setLoading(true)
    floorplanApi
      .saveFloorPlanUsingPOST({ floorPlanDto: data })
      .then((res) => {
        const errors = res.results ? translateError(res.results).join(', ') : ''
        setError(errors)
        setLoading(false)
        if (!errors) {
          setOpen(false)
          history.push(`floor-plan/${res.data?.id}`)
        }
      })
      .catch(() => {
        setLoading(false)
        setError(defaultFetchError)
      })
  }

  return (
    <>
      <Tooltip title="Create new floor plan">
        <IconButton onClick={() => setOpen(true)}>
          <FAIcon d={mdiPlus} />
        </IconButton>
      </Tooltip>
      <FloorPlanDialog
        isLoading={isLoading}
        open={open}
        onSave={onSave}
        error={error}
        create
        floorPlan={{ name: randomName(), floor: 1 }}
        onCancel={() => {
          setOpen(false)
          setError(undefined)
        }}
      />
    </>
  )
}

export default CreateFloorPlanDialog
