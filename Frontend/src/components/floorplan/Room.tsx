import Konva from 'konva'
import { v4 as uuid } from 'uuid'

import React, { useEffect } from 'react'

import { handleSelection } from '@utils/SelectionHelper'

import { SelectionUpdate } from '@pages/reducers/floorPlanReducer'

import PolylineRoom from './PolylineRoom'
import RectangleRoom from './RectangleRoom'

export type LabelProps = {
  fontSize: number
  stroke: string
  strokeWidth: number
  fillAfterStrokeEnabled: boolean
}

const defaultLabelProps: LabelProps = {
  fontSize: 18,
  stroke: 'white',
  strokeWidth: 2,
  fillAfterStrokeEnabled: true,
}

export type RoomComponentProps = {
  updateShape: (
    shape: RoomShape | PolylineShape,
    shiftx: number,
    shifty: number
  ) => void
  roomProps: RoomProps
  selected: boolean
  onSelect: (selection: SelectionUpdate) => void
  selectedRooms: Array<string>
  onDblClick: () => void
  drawRooms: boolean
  usePolyline: boolean
  setShapeInTransformer: (
    selected: boolean,
    shapeId: string,
    shape?: Konva.Node
  ) => void
  exportMode?: boolean
  listening?: boolean
}

export type RoomProps = {
  type: 'room'
  id: string
  name: string
  shape: RoomShape | PolylineShape
  removed: boolean
}

export type RoomShape = {
  type: 'rectangle'
  x: number
  y: number
  width: number
  height: number
  id: string
  fill: string
}

export type Point = { x: number; y: number }

export type PolylineShape = {
  type: 'polyline'
  points: Array<Point>
  mouseOverStart: boolean
  isFinished: boolean
  id: string
  current: boolean
  fill: string
}

export function getDefaultRoom(): RoomProps {
  const id = uuid()
  return {
    type: 'room',
    id: id,
    name: `room-${id.substring(id.length - 6)}`,
    shape: {
      type: 'rectangle',
      x: 50 + Math.random() * 10,
      y: 50 + Math.random() * 10,
      width: 75,
      height: 75,
      id: id,
      fill: '#DDDDDD',
    },
    removed: false,
  }
}

export function getDefaultPolyline(): RoomProps {
  const id = uuid()
  return {
    type: 'room',
    id: id,
    name: `room-${id.substring(id.length - 6)}`,
    removed: false,
    shape: {
      type: 'polyline',
      id: id,
      points: [],
      mouseOverStart: false,
      isFinished: false,
      current: true,
      fill: '#DDDDDD',
    },
  }
}

const Room = (props: RoomComponentProps) => {
  const { selected, selectedRooms, drawRooms, usePolyline } = props
  const { shape, id: roomId } = props.roomProps

  const shapeRef =
    shape.type === 'polyline'
      ? React.useRef<Konva.Line>(null)
      : React.useRef<Konva.Rect>(null)

  useEffect(() => {
    if (drawRooms && usePolyline) {
      return
    }

    props.setShapeInTransformer(
      selected,
      shape.id,
      shapeRef?.current ?? undefined
    )
  }, [selected, shape.id])

  const handleRoomClick = (event: Konva.KonvaEventObject<MouseEvent>) => {
    const newSelection = handleSelection(
      drawRooms,
      event,
      selectedRooms,
      roomId,
      selected,
      'room'
    )
    props.onSelect(newSelection)
  }

  if (shape.type === 'polyline') {
    return (
      <PolylineRoom
        room={props.roomProps}
        labelProps={defaultLabelProps}
        onClick={handleRoomClick}
        onDblClick={props.onDblClick}
        updateShape={props.updateShape}
        drawRooms={drawRooms}
        selected={selected}
        usePolyline={usePolyline}
        ref={shapeRef as React.RefObject<Konva.Line>} // shapeRef is definitely of type  React.RefObject<Konva.Line> due to its initialisation. Typescript just cannot infer that
        exportMode={props.exportMode}
        listening={props.listening}
      />
    )
  }

  return (
    <RectangleRoom
      ref={shapeRef as React.RefObject<Konva.Rect>} // shapeRef is definitely of type  React.RefObject<Konva.Line> due to its initialisation. Typescript just cannot infer that
      drawRooms={drawRooms}
      labelProps={defaultLabelProps}
      room={props.roomProps}
      shape={shape as RoomShape}
      selected={selected}
      onClick={handleRoomClick}
      onDblClick={props.onDblClick}
      updateShape={props.updateShape}
      listening={props.listening}
    />
  )
}

export default Room
