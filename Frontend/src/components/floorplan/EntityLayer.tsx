import React from 'react'

import { FloorPlanActions } from '@pages/reducers/floorPlanReducer'

import Entity, { EntityProps } from './Entity'

const EntityLayer = (props: {
  entities: Array<EntityProps>
  selectedEntities: Array<string>
  dispatch: React.Dispatch<FloorPlanActions>
  drawRooms: boolean
  listening?: boolean
}) => {
  const { entities, dispatch } = props
  return (
    <>
      {entities.map((entity, idx) => {
        return (
          <Entity
            updateShape={(newShape, shiftx, shifty) =>
              dispatch({
                type: 'updateEntity',
                idx,
                entity: newShape,
                shiftx,
                shifty,
              })
            }
            entityProps={entity}
            onMouseClick={() =>
              dispatch({ type: 'setRemoveEntityDialog', id: entity.id })
            }
            onSelect={(selection) =>
              dispatch({ type: 'setSelection', selection })
            }
            selectedEntities={props.selectedEntities}
            drawRooms={props.drawRooms}
            listening={props.listening}
            key={entity.id}
          />
        )
      })}
    </>
  )
}
export default EntityLayer
