import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  FormGroup,
} from '@mui/material'

import React, { useEffect, useState } from 'react'

import {
  BaseEmptyResponse,
  FloorPlanControllerApi,
  HomeAssistantControllerApi,
} from '@generated/openapi'

export type FloorPlanDeleteOptions = {
  deleteExportedGroups: boolean
  deleteExportedFloorPlan: boolean
  deletePHAFloorPlan: boolean
}

const defaultState = {
  deleteExportedGroups: false,
  deleteExportedFloorPlan: false,
  deletePHAFloorPlan: true,
} as FloorPlanDeleteOptions

const DeleteFloorPlanDialog = ({
  open,
  target,
  onCancel,
  onConfirmDelete,
}: {
  open: boolean
  target?: {
    id: number
    name: string
  }
  onCancel: () => void
  onConfirmDelete: (calls: Array<() => Promise<BaseEmptyResponse>>) => void
}) => {
  const [state, setState] = useState(defaultState)

  const onDelete = () => {
    const floorPlanApi = new FloorPlanControllerApi()
    const haApi = new HomeAssistantControllerApi()
    if (target) {
      const { id } = target
      const calls = [] as Array<() => Promise<BaseEmptyResponse>>
      if (state.deletePHAFloorPlan)
        calls.push(() => floorPlanApi.deleteFloorPlanUsingDELETE({ id }))
      if (state.deleteExportedFloorPlan)
        calls.push(() => haApi.deleteExportedFloorPlanCardUsingDELETE({ id }))
      if (state.deleteExportedGroups)
        calls.push(() => haApi.deleteExportedGroupsUsingDELETE({ id }))

      onConfirmDelete(calls)
    }
  }

  useEffect(() => {
    if (open) setState(defaultState)
  }, [open])

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    switch (event.target.name) {
      case 'exportGroup':
        setState({ ...state, deleteExportedGroups: event.target.checked })
        break
      case 'exportFloorPlan':
        setState({ ...state, deleteExportedFloorPlan: event.target.checked })
        break
      case 'floorPlan':
        setState({ ...state, deletePHAFloorPlan: event.target.checked })
        break
      default:
        console.error(
          `Unexpected target: ${event.target.name} does not match any known target`
        )
    }
  }

  return (
    <>
      <Dialog open={open}>
        <DialogTitle>Delete floor plan &quot;{target?.name}&quot;?</DialogTitle>
        <DialogContent>
          <FormGroup>
            <FormControlLabel
              control={
                <Checkbox
                  onChange={handleChange}
                  name="exportGroup"
                  checked={state.deleteExportedGroups}
                />
              }
              label="delete exported groups"
            />
            <FormControlLabel
              control={
                <Checkbox
                  onChange={handleChange}
                  name="exportFloorPlan"
                  checked={state.deleteExportedFloorPlan}
                />
              }
              label="delete exported floor plan"
            />
            <FormControlLabel
              control={
                <Checkbox
                  onChange={handleChange}
                  name="floorPlan"
                  checked={state.deletePHAFloorPlan}
                />
              }
              label="delete from current list"
            />
          </FormGroup>
        </DialogContent>
        <DialogActions sx={{ justifyContent: 'space-between' }}>
          <Button variant="contained" onClick={onCancel}>
            Cancel
          </Button>
          <Button color="error" onClick={onDelete}>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default DeleteFloorPlanDialog
