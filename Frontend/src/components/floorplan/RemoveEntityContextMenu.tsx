import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  FormLabel,
  Grid,
} from '@mui/material'

import React from 'react'

import { EntityProps } from './Entity'

const RemoveEntityContextMenu = (props: {
  entity: EntityProps
  onClose: () => void
  onDelete: (entity: EntityProps) => void
}) => {
  const { entity, onClose, onDelete } = props

  const onDeletePressed = () => {
    onDelete(entity)
    onClose()
  }

  return (
    <Dialog open={entity !== undefined} onClose={onClose}>
      <DialogTitle>
        Delete entity {entity.entity.attributes.friendlyName}
      </DialogTitle>
      <DialogContent style={{ paddingTop: 10 }}>
        <Grid
          container
          alignItems="center"
          justifyContent="flex-end"
          columnSpacing={1}
          rowSpacing={3}
        >
          <Grid item xs={12} sm={3}>
            <FormLabel>entity id</FormLabel>
          </Grid>{' '}
          <Grid item xs={12} sm={8}>
            <FormLabel>{props.entity.id}</FormLabel>
          </Grid>
        </Grid>
        <Divider style={{ marginTop: 50 }} />
        <DialogActions
          style={{ justifyContent: 'space-between', marginTop: 10 }}
        >
          <Button onClick={onClose}>Cancel</Button>
          <Button onClick={onDeletePressed} variant="contained" color="error">
            Remove
          </Button>
        </DialogActions>
      </DialogContent>
    </Dialog>
  )
}

export default RemoveEntityContextMenu
