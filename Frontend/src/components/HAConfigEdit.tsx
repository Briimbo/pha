import { mdiClose, mdiEye, mdiEyeOff } from '@mdi/js'
import {
  Button,
  Card,
  CardContent,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Typography,
} from '@mui/material'

import React, { ChangeEvent, useEffect, useState } from 'react'

import useApi from '@hooks/useAPI'

import FullWidthAlert from '@components/FullWidthAlert'

import FAIcon from './Icon'

const HAConfigEdit = () => {
  const [showToken, setShowToken] = useState(false)
  const [haSetting, setHaSettings] = useState({
    url: '',
    token: '',
  })
  const [error, setError] = useState<string | undefined>()

  const { callApi: loadConfig, isLoading: isLoadingConfig } = useApi<
    undefined,
    { url: string; token: string }
  >('ha/config', 'GET')

  useEffect(() => {
    loadConfig(undefined).then(({ data, errorMessage }) => {
      if (errorMessage?.length) setError(errorMessage.join(', '))
      if (data)
        setHaSettings({
          token: data.token || '',
          url: data.url || '',
        })
    })
  }, [])

  const { callApi: setConfig, isLoading: isSetConfigLoading } = useApi<
    { url: string; token: string },
    boolean
  >('ha/config', 'POST')

  const [success, setSuccess] = useState<string | undefined>()

  const handleFormChange =
    (prop: keyof typeof haSetting) =>
    (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
      setHaSettings({ ...haSetting, [prop]: event.target.value })
    }

  const save = () => {
    setSuccess(undefined)
    setError(undefined)
    setConfig(haSetting).then(({ errorMessage }) => {
      if (errorMessage?.length) setError(errorMessage.join(', '))
      else setSuccess('Configuration saved successfully')
    })
  }
  return (
    <Card>
      <CardContent>
        <Typography gutterBottom variant="h4" component="div">
          Home Assistant Settings
        </Typography>
        <FormControl
          disabled={isLoadingConfig || isSetConfigLoading}
          sx={{ m: 1, width: '100%' }}
          variant="outlined"
        >
          <InputLabel htmlFor="outlined-adornment-password">URL</InputLabel>
          <OutlinedInput
            placeholder="http://localhost:8123"
            value={haSetting.url}
            onChange={handleFormChange('url')}
            label="url"
          />
        </FormControl>
        <FormControl
          disabled={isLoadingConfig || isSetConfigLoading}
          sx={{ m: 1, width: '100%' }}
          variant="outlined"
        >
          <InputLabel htmlFor="outlined-adornment-password">Token</InputLabel>
          <OutlinedInput
            type={showToken ? 'text' : 'password'}
            value={haSetting.token}
            onChange={handleFormChange('token')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => setShowToken(!showToken)}
                  onMouseDown={(e) => {
                    e.preventDefault()
                  }}
                  edge="end"
                >
                  <FAIcon d={showToken ? mdiEyeOff : mdiEye} />
                </IconButton>
              </InputAdornment>
            }
            label="Token"
          />
        </FormControl>
        {error && (
          <FullWidthAlert
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setError(undefined)
                }}
              >
                <FAIcon d={mdiClose} />
              </IconButton>
            }
            variant="filled"
            severity="error"
          >
            {error}
          </FullWidthAlert>
        )}
        {success && (
          <FullWidthAlert
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setSuccess(undefined)
                }}
              >
                <FAIcon d={mdiClose} />
              </IconButton>
            }
            variant="filled"
            severity="success"
          >
            {success}
          </FullWidthAlert>
        )}
        <Grid marginTop="1rem" container justifyContent="end">
          <Button
            disabled={isLoadingConfig || isSetConfigLoading}
            onClick={save}
            variant="contained"
          >
            Save
          </Button>
        </Grid>
      </CardContent>
    </Card>
  )
}

export default HAConfigEdit
