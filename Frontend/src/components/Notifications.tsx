import { Alert, Snackbar } from '@mui/material'

import React from 'react'

import { useNotifications } from '@contexts/notificationContext'

const Notifications = () => {
  const { notifications, removeNotification } = useNotifications()
  return (
    <Snackbar open={true}>
      <div>
        {notifications.map((n) => (
          <Alert
            variant="filled"
            key={n.id}
            onClose={() => {
              removeNotification(n.id)
            }}
            severity={n.type}
            sx={{ width: '100%', marginBottom: '1rem' }}
          >
            {n.message}
          </Alert>
        ))}
      </div>
    </Snackbar>
  )
}

export default Notifications
