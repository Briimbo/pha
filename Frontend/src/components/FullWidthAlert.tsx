import { Alert } from '@mui/material'
import { styled } from '@mui/material/styles'

const FullWidthAlert = styled(Alert)`
  width: 100%;
`

export default FullWidthAlert
