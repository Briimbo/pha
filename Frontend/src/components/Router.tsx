import {
  mdiCog,
  mdiFloorPlan,
  mdiHome,
  mdiHomeSearch,
  mdiListStatus,
  mdiTextBoxSearch,
  mdiViewList,
} from '@mdi/js'

import React from 'react'
import { Route, Switch } from 'react-router-dom'

import DocumentDetails from '@pages/DocumentDetails'
import Documents from '@pages/Documents'
import Entities from '@pages/Entities'
import FloorPlan from '@pages/FloorPlan'
import FloorPlans from '@pages/FloorPlans'
import Home from '@pages/Home'
import NotFound from '@pages/NotFound'
import RuntimeChecks from '@pages/RuntimeChecks'
import Settings from '@pages/Settings'

export const routes = [
  {
    path: '/',
    component: <Home />,
    name: 'Home',
    icon: mdiHome,
  },
  {
    path: '/runtime',
    component: <RuntimeChecks />,
    name: 'Monitoring',
    icon: mdiHomeSearch,
  },
  {
    path: '/constraints',
    component: <Documents />,
    name: 'Constraints',
    icon: mdiListStatus,
  },
  {
    path: '/document/:id/:page',
    component: <DocumentDetails />,
    hideInMenu: true,
    icon: mdiTextBoxSearch,
  },
  {
    path: '/entities',
    component: <Entities />,
    name: 'Entities',
    icon: mdiViewList,
  },
  {
    path: '/floor-plans',
    component: <FloorPlans />,
    name: 'Floor Plans',
    icon: mdiFloorPlan,
  },
  {
    path: '/floor-plan/:id',
    component: <FloorPlan />,
    hideInMenu: true,
    name: 'Floor Plan',
    icon: mdiFloorPlan,
  },
  ...(process.env.NODE_ENV !== 'production'
    ? [
        {
          path: '/settings',
          component: <Settings />,
          name: 'Settings',
          icon: mdiCog,
        },
      ]
    : []),
]

const Router = () => {
  return (
    <Switch>
      {routes.map((r) => (
        <Route exact path={r.path} key={r.path}>
          {r.component}
        </Route>
      ))}
      <Route path="*">
        <NotFound />
      </Route>
    </Switch>
  )
}

export default Router
