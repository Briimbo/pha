import { mdiGraphql } from '@mdi/js'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
} from '@mui/material'
import { Buffer } from 'buffer'

import React, { useState } from 'react'
import { TransformComponent, TransformWrapper } from 'react-zoom-pan-pinch'

import FullWidthAlert from '@components/FullWidthAlert'
import FAIcon from '@components/Icon'

export const setSize = (
  svgImgString: string,
  width = '100%',
  height = '100%'
): string => {
  const parser = new DOMParser()
  const svgImg = parser.parseFromString(svgImgString, 'image/svg+xml')
  svgImg.getElementsByTagName('svg')[0].setAttribute('height', height)
  svgImg.getElementsByTagName('svg')[0].setAttribute('width', width)
  const serizalizer = new XMLSerializer()
  return serizalizer.serializeToString(svgImg)
}

const ImageDialog = ({
  imgData,
  type,
  title,
  icon = mdiGraphql,
}: {
  imgData: string
  type: 'pngBase64' | 'svgUtf8'
  title: string
  icon?: string
}) => {
  const [open, setOpen] = useState(false)

  const onClose = () => setOpen(false)

  let img
  switch (type) {
    case 'pngBase64':
      img = `data:image/png;base64,${imgData}`
      break
    case 'svgUtf8': {
      const svgImg = setSize(imgData)
      img = `data:image/svg+xml;base64,${Buffer.from(svgImg, 'utf8').toString(
        'base64'
      )}`
      break
    }
    default:
      break
  }

  return (
    <>
      <IconButton
        aria-label="view image"
        onClick={() => setOpen(true)}
        color="primary"
      >
        <FAIcon d={icon} />
      </IconButton>
      <Dialog open={open} onClose={onClose} maxWidth="lg" fullWidth={true}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          {img && (
            <TransformWrapper>
              <TransformComponent
                wrapperStyle={{ width: '100%', height: '100%' }}
                contentStyle={{ width: '100%', height: '100%' }}
              >
                <img src={img} />
              </TransformComponent>
            </TransformWrapper>
          )}
          {!img && (
            <FullWidthAlert severity="error">
              Cannot display image, invalid image
            </FullWidthAlert>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>Close</Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

const InteractiveSvg = ({ svg }: { svg: string }) => {
  const svgImg = `data:image/svg+xml;base64,${Buffer.from(
    setSize(svg),
    'utf8'
  ).toString('base64')}`

  return (
    <TransformWrapper>
      <TransformComponent
        wrapperStyle={{ width: '100%', height: '100%' }}
        contentStyle={{ width: '100%', height: '100%' }}
      >
        <img src={svgImg} />
      </TransformComponent>
    </TransformWrapper>
  )
}

export { ImageDialog, InteractiveSvg }
