import { mdiBell, mdiDotsCircle, mdiEyeOff, mdiTrashCan } from '@mdi/js'
import { Badge, Button, CircularProgress, ListItem, Paper } from '@mui/material'
import Box from '@mui/material/Box'
import IconButton from '@mui/material/IconButton'
import Menu from '@mui/material/Menu'
import Tooltip from '@mui/material/Tooltip'

import React from 'react'

import useInbox from '@hooks/useInbox'

import PHAIcon from '@components/Icon'

import InboxItem from './InboxItem'
import NotificationChannelDialog from './notifications/NotificationChannelDialog'

const NotificationInbox = () => {
  const {
    notifications,
    unread,
    setReadById,
    deleteById,
    readAll,
    deleteAll,
    existsMore,
    loadMore,
    isLoading,
  } = useInbox()

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <React.Fragment>
      <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center' }}>
        <Tooltip title="Notification Inbox">
          <Badge badgeContent={unread} color="secondary">
            <IconButton
              onClick={handleClick}
              size="small"
              sx={{ ml: 2 }}
              aria-controls={open ? 'notification-inbox' : undefined}
              aria-haspopup="true"
              aria-expanded={open ? 'true' : undefined}
            >
              <PHAIcon d={mdiBell} sx={{ color: open ? 'white' : undefined }} />
            </IconButton>
          </Badge>
        </Tooltip>
      </Box>
      <Menu
        anchorEl={anchorEl}
        id="notification-inbox"
        open={open}
        onClose={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: 'visible',
            zIndex: 1400,
            minWidth: 400,
            maxHeight: 600,
            filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
            mt: 1.5,
            '& .MuiAvatar-root': {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            '&:before': {
              content: '""',
              display: 'block',
              position: 'absolute',
              overflow: '',
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: 'background.paper',
              transform: 'translateY(-50%) rotate(45deg)',
            },
          },
        }}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      >
        <Paper sx={{ overflowY: 'scroll', minWidth: 400, maxHeight: 600 }}>
          <ListItem>
            <Button disabled={isLoading} onClick={readAll}>
              <PHAIcon d={mdiEyeOff} sx={{ marginRight: '1rem' }} />
              read all
            </Button>
            <Button disabled={isLoading} onClick={deleteAll}>
              <PHAIcon d={mdiTrashCan} sx={{ marginRight: '1rem' }} />
              delete all
            </Button>
            <NotificationChannelDialog />
          </ListItem>
          {notifications.map((n) => (
            <InboxItem
              key={n.id}
              notification={n}
              deleteItem={() => deleteById(n.id)}
              setRead={(r: boolean) => setReadById(n.id, r)}
            />
          ))}
          {existsMore && (
            <ListItem>
              <Button disabled={isLoading} onClick={loadMore}>
                <PHAIcon d={mdiDotsCircle} sx={{ marginRight: '1rem' }} />
                load more
              </Button>
            </ListItem>
          )}
          {isLoading && (
            <ListItem alignItems="center">
              <CircularProgress />
            </ListItem>
          )}
        </Paper>
      </Menu>
    </React.Fragment>
  )
}

export default NotificationInbox
