import { TextareaAutosize } from '@material-ui/core'
import { Checkbox, FormControlLabel } from '@mui/material'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import TextField from '@mui/material/TextField'
import {
  CreateNewChannelUsingPOSTRequest,
  NotificationChannelCrudResponse,
  UpdateChannelUsingPUTRequest,
} from 'generated/openapi'

import * as React from 'react'

import { useErrorHandling } from '@hooks/useAPI'

import { useNotifications } from '@contexts/notificationContext'

import TagInput from '@components/TagInput'

import {
  NotificationChannelDto,
  NotificationControllerApi,
} from '@generated/openapi'

const defaultInput = {
  targets: [],
  data: '{\n  \n}',
  service: '',
  name: '',
  active: true,
}

export default function ChannelCreateUpdateDialog({
  create,
  open,
  data,
  onClose,
}: {
  create: boolean
  open: boolean
  data?: NotificationChannelDto
  onClose: () => void
}) {
  const notifyApi = new NotificationControllerApi()
  const { addNotification } = useNotifications()

  const { call: createNew, isLoading: isLoadingCreate } = useErrorHandling(
    (params?: CreateNewChannelUsingPOSTRequest) =>
      notifyApi.createNewChannelUsingPOST(params)
  )

  const { call: update, isLoading: isLoadingUpdate } = useErrorHandling(
    (params?: UpdateChannelUsingPUTRequest) =>
      notifyApi.updateChannelUsingPUT(params)
  )

  const [input, setInput] = React.useState<NotificationChannelDto>(defaultInput)

  const setFieldSimple = (name: keyof NotificationChannelDto) => {
    return (v: unknown) => {
      setInput({ ...input, [name]: v })
    }
  }

  const setField = (name: keyof NotificationChannelDto) => {
    return (v: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
      setInput({ ...input, [name]: v.target.value })
    }
  }

  const createOrUpdate = () => {
    new Promise<{ result?: NotificationChannelCrudResponse; success: boolean }>(
      (res) => {
        if (create) {
          res(createNew({ notificationChannelDto: input }))
        } else {
          res(update({ notificationChannelDto: input }))
        }
      }
    ).then(({ success }) => {
      if (success) {
        addNotification({
          message: create
            ? 'New channel was created successfully'
            : 'Channel was successfully updated',
          type: 'success',
        })

        setInput(defaultInput)

        onClose()
      }
    })
  }

  React.useEffect(() => {
    if (data) setInput(data)
    else setInput(defaultInput)
  }, [data])

  return (
    <div>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>
          {create ? 'Create' : 'Update'} Notification Channel
        </DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            value={input.name}
            onChange={setField('name')}
            margin="dense"
            id="name"
            label="Name"
            fullWidth
            variant="outlined"
            sx={{ marginBottom: '1rem' }}
          />
          <TextField
            autoFocus
            value={input.service}
            onChange={setField('service')}
            margin="dense"
            id="service"
            label="Service"
            fullWidth
            variant="outlined"
            sx={{ marginBottom: '1rem' }}
          />
          <TagInput
            tags={input.targets || []}
            setTags={setFieldSimple('targets')}
            placeholder="Targets"
          />
          <TextareaAutosize
            style={{ width: '100%', marginTop: '1rem' }}
            placeholder="Data"
            minRows={3}
            value={input.data}
            onChange={setField('data')}
          ></TextareaAutosize>
          <FormControlLabel
            control={
              <Checkbox
                onChange={() => setInput({ ...input, active: !input.active })}
                checked={input.active}
              />
            }
            label="Active"
          />
        </DialogContent>
        <DialogActions>
          <Button
            disabled={isLoadingCreate || isLoadingUpdate}
            onClick={onClose}
          >
            Close
          </Button>
          <Button
            onClick={createOrUpdate}
            variant="contained"
            disabled={isLoadingCreate || isLoadingUpdate}
          >
            {create ? 'Create' : 'Update'}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
