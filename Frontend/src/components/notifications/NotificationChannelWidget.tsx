import { mdiCancel, mdiCheck, mdiPencil, mdiPlus, mdiTrashCan } from '@mdi/js'
import {
  Card,
  CardHeader,
  IconButton,
  LinearProgress,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material'
import {
  NotificationChannelCrudResponse,
  NotificationChannelLoadResponse,
} from 'generated/openapi'

import * as React from 'react'

import { translateError } from '@utils/ErrorTranslator'

import { useNotifications } from '@contexts/notificationContext'

import {
  NotificationChannelDto,
  NotificationControllerApi,
} from '@generated/openapi'

import FAIcon from '../Icon'
import ChannelCreateUpdateDialog from './ChannelCreateUpdateDialog'

const NotificationChannelWidget = () => {
  const notifyApi = new NotificationControllerApi()
  const [open, setOpen] = React.useState(false)
  const [error, setError] = React.useState<string | undefined>(undefined)
  const [loading, setLoading] = React.useState(false)
  const [editChannel, setEditChannel] = React.useState<
    NotificationChannelDto | undefined
  >(undefined)
  const [channels, setChannels] = React.useState<Array<NotificationChannelDto>>(
    []
  )

  const { addNotification } = useNotifications()

  const deleteChannel = (id?: number) => {
    if (!id) return
    notifyApi
      .deleteChannelUsingDELETE({ id })
      .then((r: NotificationChannelCrudResponse) => {
        if (!r.results?.every((e) => e.code === 'success') || !r.data) {
          addNotification({
            message: r.results?.length
              ? translateError(r.results).join(', ')
              : 'unknown error occurred',
            type: 'error',
          })

          return
        }
        setChannels(channels.filter((c) => c.id !== id))
      })
  }

  const loadChannel = () => {
    setError(undefined)
    setLoading(true)
    notifyApi
      .loadChannelsUsingGET()
      .then((r: NotificationChannelLoadResponse) => {
        if (!r.results?.every((e) => e.code === 'success') || !r.data) {
          if (r.results) setError(translateError(r.results).join(', '))
          else setError('unknown error occurred')
          return
        }
        setChannels(r.data)
      })
      .finally(() => setLoading(false))
  }

  const startEdit = (dto: NotificationChannelDto) => {
    setEditChannel(dto)
    setOpen(true)
  }

  React.useEffect(loadChannel, [])

  return (
    <Card sx={{ padding: '1rem', marginBottom: '2rem' }}>
      <CardHeader
        title="Notification Channel"
        action={
          <IconButton onClick={() => setOpen(true)}>
            <FAIcon d={mdiPlus} />
          </IconButton>
        }
      ></CardHeader>
      <TableContainer component={Paper}>
        <Table aria-label="notification channel">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="left">Service</TableCell>
              <TableCell align="left">Targets</TableCell>
              <TableCell align="left">Active</TableCell>
              <TableCell align="right">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {channels.map((row: NotificationChannelDto) => (
              <TableRow
                key={row.service}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell align="left">{row.name}</TableCell>
                <TableCell align="left">{row.service}</TableCell>
                <TableCell
                  component="th"
                  scope="row"
                  align="left"
                  sx={{ wordBreak: 'break-all' }}
                >
                  {row.targets?.join(', ')}
                </TableCell>
                <TableCell align="left">
                  <FAIcon
                    d={row.active ? mdiCheck : mdiCancel}
                    sx={{ color: row.active ? 'green' : 'red' }}
                  />
                </TableCell>
                <TableCell align="right">
                  <IconButton onClick={() => deleteChannel(row.id)}>
                    <FAIcon d={mdiTrashCan} sx={{ color: 'red' }} />
                  </IconButton>
                  <IconButton onClick={() => startEdit(row)}>
                    <FAIcon d={mdiPencil} />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
            {channels.length === 0 && (
              <TableRow>
                <TableCell colSpan={5}>
                  <Typography align="center">No channel found</Typography>
                </TableCell>
              </TableRow>
            )}
            {loading && (
              <TableRow>
                <TableCell colSpan={5}>
                  <LinearProgress />
                </TableCell>
              </TableRow>
            )}
            {error && (
              <TableRow>
                <TableCell colSpan={5}>
                  <Typography align="center">{error}</Typography>
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <ChannelCreateUpdateDialog
        create={!editChannel}
        open={open}
        data={editChannel}
        onClose={() => {
          setOpen(false)
          loadChannel()
          setEditChannel(undefined)
        }}
      />
    </Card>
  )
}

export default NotificationChannelWidget
