import { mdiCog } from '@mdi/js'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'

import * as React from 'react'

import FAIcon from '../Icon'
import NotificationChannelWidget from './NotificationChannelWidget'

export default function NotificationChannelDialog() {
  const [open, setOpen] = React.useState(false)

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  return (
    <div>
      <Button onClick={handleClickOpen}>
        <FAIcon d={mdiCog} sx={{ marginRight: '1rem' }} /> Settings
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogContent>
          <NotificationChannelWidget />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
