import { mdiBrightness2, mdiBrightness7 } from '@mdi/js'
import { Switch } from '@mui/material'

import React, { useEffect } from 'react'

import FAIcon from './Icon'

const PREF_THEME_KEY = 'PREF_THEME'

const ThemeSwitch = ({
  colorMode: { mode, toggleColorMode },
}: {
  colorMode: {
    toggleColorMode: (mode: 'light' | 'dark') => void
    mode: 'light' | 'dark'
  }
}) => {
  const toggle = (newMode?: 'light' | 'dark') => {
    const setMode = newMode ? newMode : mode === 'light' ? 'dark' : 'light'

    toggleColorMode(setMode)
    localStorage.setItem(PREF_THEME_KEY, setMode)
  }

  useEffect(() => {
    const savedTheme = localStorage.getItem(PREF_THEME_KEY)

    if (savedTheme === 'light') {
      toggleColorMode('light')
    } else {
      toggleColorMode('dark')
      localStorage.setItem(PREF_THEME_KEY, 'dark')
    }
  }, [])

  return (
    <>
      <FAIcon
        d={mdiBrightness7}
        onClick={() => toggle('light')}
        sx={{ color: 'yellow' }}
      />
      <Switch checked={mode === 'dark'} onChange={() => toggle()} />
      <FAIcon
        d={mdiBrightness2}
        onClick={() => toggle('dark')}
        sx={{ color: 'darkblue' }}
      />
    </>
  )
}

export default ThemeSwitch
