import {
  mdiAlert,
  mdiEye,
  mdiEyeOff,
  mdiGraph,
  mdiInformation,
  mdiLink,
  mdiTrashCan,
} from '@mdi/js'
import {
  Avatar,
  IconButton,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Link as MdiLink,
} from '@mui/material'

import React, { useState } from 'react'
import { Link } from 'react-router-dom'

import getLink from '@utils/linkGenerator'

import PHAIcon from '@components/Icon'

import { NotificationDto } from '@generated/openapi'

const InboxItem = (props: {
  setRead: (read: boolean) => Promise<void>
  deleteItem: () => Promise<void>
  notification: NotificationDto
}) => {
  const { notification: n, deleteItem, setRead } = props
  const [isLoading, setLoading] = useState(false)

  const internRead = () => {
    setLoading(true)
    setRead(!n.read).finally(() => {
      setLoading(false)
    })
  }

  const internDelete = () => {
    setLoading(true)
    deleteItem().finally(() => {
      setLoading(false)
    })
  }

  let icon = mdiInformation
  let color = undefined

  switch (n.type) {
    case 'MODEL_CHECKING_STATUS':
      icon = mdiGraph
      color = 'green'
      break
    case 'RUNTIME_CONSTRAINT_VIOLATION':
      icon = mdiAlert
      color = 'red'
      break
    case 'FLOOR_PLAN':
    default:
      icon = mdiInformation
      color = undefined
      break
  }

  const link = getLink(n.linkedObjectType, n.linkedObjectId)

  return (
    <ListItem selected={!n.read}>
      <ListItemAvatar>
        <Avatar>
          <PHAIcon sx={{ color }} d={icon} />
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        primaryTypographyProps={{ fontWeight: n.read ? '400' : 'bold' }}
        primary={n.title}
        secondary={n.message}
      />
      {link && (
        <MdiLink to={link} component={Link}>
          <IconButton>
            <PHAIcon d={mdiLink} />
          </IconButton>
        </MdiLink>
      )}
      <IconButton disabled={isLoading} onClick={() => internRead()}>
        <PHAIcon d={n.read ? mdiEye : mdiEyeOff} />
      </IconButton>
      <IconButton disabled={isLoading} onClick={() => internDelete()}>
        <PHAIcon d={mdiTrashCan} />
      </IconButton>
    </ListItem>
  )
}

export default InboxItem
