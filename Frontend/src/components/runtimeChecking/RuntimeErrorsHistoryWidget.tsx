import {
  Box,
  Card,
  CardHeader,
  CircularProgress,
  SxProps,
  useTheme,
} from '@mui/material'
import {
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'

import React, { useEffect, useState } from 'react'

import { useErrorHandling } from '@hooks/useAPI'

import { findLastIndex } from '@utils/arrayUtils'

import { RuntimeCheckControllerApi } from '@generated/openapi'

import DateIntervalSlider, { Interval } from './DateIntervalSlider'

type HistoryGraphItem = { date: number; errors: number }

const dayInSeconds = 24 * 60 * 60

const RuntimeErrorsHistoryWidget = ({ sx }: { sx?: SxProps }) => {
  const [data, setData] = useState<HistoryGraphItem[]>([])
  const [displayedData, setDisplayedData] = useState<HistoryGraphItem[]>([])
  const [interval, setInterval] = useState<Interval>([-1, -1])
  const [intervalBounds, setIntervalBounds] = useState<Interval>([0, 0])

  const theme = useTheme()

  const runtimeApi = new RuntimeCheckControllerApi()
  const { call: getErrorCountHistory, isLoading } = useErrorHandling(() =>
    runtimeApi.getErrorCountHistoryUsingGET()
  )
  useEffect(() => {
    getErrorCountHistory().then((res) => {
      if (res.success && res.result?.data) {
        const newData = res.result.data.map((e) => ({
          date: new Date(e.date).getTime(),
          errors: e.errors,
        }))
        setData(newData)
        const range = [
          newData[0].date / 1000,
          newData[newData.length - 1].date / 1000,
        ] as Interval
        setIntervalBounds(range)
        setInterval(range)
      }
    })
  }, [])

  useEffect(() => {
    setDisplayedData(
      data.slice(
        data.findIndex((v) => v.date >= interval[0] * 1000),
        findLastIndex(data, (v) => v.date <= interval[1] * 1000) + 1
      )
    )
  }, [interval, data])

  return (
    <Card sx={{ ...sx, padding: '1rem', marginBottom: '2rem' }}>
      <CardHeader
        title={`Monitoring History (Total: ${displayedData.reduce(
          (prev, current) => prev + current.errors,
          0
        )})`}
      />
      {isLoading && (
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
          <CircularProgress />
        </Box>
      )}
      {!isLoading && (
        <ResponsiveContainer height={350} width="100%">
          <LineChart
            data={displayedData}
            margin={{
              top: 20,
              right: 20,
              bottom: 20,
              left: 20,
            }}
          >
            <XAxis
              dataKey="date"
              type="number"
              scale="time"
              domain={['auto', 'auto']}
              interval="preserveStartEnd"
              minTickGap={20}
              tickMargin={5}
              tickFormatter={(x) => new Date(x).toLocaleDateString()}
              padding={{ left: 30, right: 30 }}
              stroke={theme.palette.text.primary}
            />
            <YAxis
              label={{
                value: 'Errors',
                angle: -90,
                position: 'insideLeft',
                fill: theme.palette.text.primary,
              }}
              interval="preserveStartEnd"
              stroke={theme.palette.text.primary}
            />
            <Tooltip
              labelFormatter={(l: number) => new Date(l).toLocaleDateString()}
            />
            <Line
              type="monotone"
              dataKey="errors"
              dot={false}
              activeDot={{ r: 5 }}
              stroke={theme.palette.primary.main}
              strokeWidth={2}
            />
          </LineChart>
        </ResponsiveContainer>
      )}
      {intervalBounds[0] != intervalBounds[1] && (
        <DateIntervalSlider
          interval={interval}
          setInterval={setInterval}
          intervalBounds={intervalBounds}
          step={dayInSeconds}
          dateOnly
        />
      )}
    </Card>
  )
}

export default RuntimeErrorsHistoryWidget
