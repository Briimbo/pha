import { mdiChartLine } from '@mdi/js'
import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  IconButton,
  useTheme,
} from '@mui/material'
import { Line, LineChart, Tooltip, XAxis, YAxis } from 'recharts'

import React, { useEffect, useState } from 'react'

import { useErrorHandling } from '@hooks/useAPI'

import { findLastIndex } from '@utils/arrayUtils'

import FAIcon from '@components/Icon'

import { RuntimeCheckControllerApi } from '@generated/openapi'

import DateIntervalSlider, { Interval } from './DateIntervalSlider'

type HistoryGraphItem = { timestamp: number; state: boolean }

const CheckHistory = ({
  documentId,
  constraintName,
}: {
  documentId: number
  constraintName: string
}) => {
  const [open, setOpen] = useState(false)
  const [data, setData] = useState<HistoryGraphItem[]>([])
  const [interval, setInterval] = useState<Interval>([-1, -1])
  const [intervalBounds, setIntervalBounds] = useState<Interval>([0, 0])

  const theme = useTheme()

  const runtimeApi = new RuntimeCheckControllerApi()
  const { call: getHistory, isLoading } = useErrorHandling(() =>
    runtimeApi.getHistoryUsingGET({ docId: documentId, constraintName })
  )
  useEffect(() => {
    getHistory().then(({ success, result }) => {
      if (success && result?.data) {
        const res = result.data
        const preprocessed = res.map((c) => ({
          timestamp: new Date(c.checkTime).getTime(),
          state: c.success,
        }))
        setData(preprocessed)

        const range = [
          ~~(new Date(res[0].checkTime).getTime() / 1000),
          ~~(new Date(res[res.length - 1].checkTime).getTime() / 1000),
        ] as [number, number]
        setIntervalBounds(range)

        if (interval[0] < 0) setInterval(range)
      }
    })
  }, [open])

  return (
    <>
      <IconButton onClick={() => setOpen(true)}>
        <FAIcon d={mdiChartLine} />
      </IconButton>
      <Dialog open={open} onClose={() => setOpen(false)} maxWidth={false}>
        <DialogTitle>
          History of &quot;{constraintName}&quot; ({documentId})
        </DialogTitle>
        <DialogContent sx={{ marginInline: '1rem' }}>
          {isLoading && (
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <CircularProgress />
            </Box>
          )}
          {!isLoading && (
            <>
              <LineChart
                width={700}
                height={300}
                data={data.slice(
                  data.findIndex((v) => v.timestamp >= interval[0] * 1000),
                  findLastIndex(
                    data,
                    (v) => v.timestamp <= interval[1] * 1000
                  ) + 1
                )}
                margin={{
                  top: 20,
                  left: 5,
                  bottom: 20,
                }}
              >
                <XAxis
                  dataKey="timestamp"
                  type="number"
                  scale="time"
                  domain={['auto', 'auto']}
                  interval="preserveStartEnd"
                  minTickGap={20}
                  tickMargin={5}
                  tickFormatter={(x) =>
                    interval[1] - interval[0] < 24 * 60 * 60
                      ? new Date(x).toLocaleTimeString()
                      : new Date(x).toLocaleDateString()
                  }
                  padding={{ left: 30, right: 30 }}
                  stroke={theme.palette.text.primary}
                />
                <YAxis
                  interval="preserveStartEnd"
                  tickCount={2}
                  tickFormatter={(v: number) => (v ? 'violated' : 'fulfilled')}
                  padding={{ bottom: 30, top: 30 }}
                  stroke={theme.palette.text.primary}
                />
                <Tooltip
                  labelFormatter={(l: number) => new Date(l).toLocaleString()}
                  formatter={(f: number) =>
                    f ? 'Constraint violated' : 'Constraint fulfilled'
                  }
                  contentStyle={{
                    backgroundColor: theme.palette.background.paper,
                  }}
                />
                <Line
                  dataKey={(d: HistoryGraphItem) => (d.state ? 0 : 1)}
                  dot={false}
                  activeDot={{ r: 5 }}
                  stroke={theme.palette.primary.main}
                />
              </LineChart>
              {intervalBounds[0] != intervalBounds[1] && (
                <>
                  <Divider
                    sx={{ marginBlockStart: '1rem', marginBlockEnd: '2rem' }}
                  />
                  <DateIntervalSlider
                    interval={interval}
                    setInterval={setInterval}
                    intervalBounds={intervalBounds}
                  />
                </>
              )}
            </>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)}>Close</Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default CheckHistory
