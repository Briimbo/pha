import { mdiAlert, mdiArrowRight, mdiCheckCircle } from '@mdi/js'
import {
  Button,
  Card,
  CardHeader,
  LinearProgress,
  Link as MdiLink,
  Paper,
  SxProps,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material'

import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import { useErrorHandling } from '@hooks/useAPI'
import useEventSubscription from '@hooks/useEventSubscription'

import { DocumentIdMap, getActiveConstraints } from '@pages/RuntimeChecks'

import FAIcon from '@components/Icon'

import {
  CheckResult,
  RuntimeCheckControllerApi,
  ValidationControllerApi,
} from '@generated/openapi'

const RuntimeErrorsWidget = ({ sx }: { sx?: SxProps }) => {
  const [displayedErrors, setDisplayedErrors] = useState<CheckResult[]>([])
  const [errors, setErrors] = useState<CheckResult[]>([])
  const [totalErrors, setTotalErrors] = useState(0)
  const [documents, setDocuments] = useState<DocumentIdMap>({})

  const runtimeApi = new RuntimeCheckControllerApi()
  const { call: getCurrentErrors, isLoading: isLoadingErrors } =
    useErrorHandling(() => runtimeApi.getCurrentErrorsUsingGET())
  const validationApi = new ValidationControllerApi()
  const { call: getDocuments, isLoading: isLoadingDocuments } =
    useErrorHandling(() => validationApi.getAllUsingGET())

  const loadData = () => {
    getCurrentErrors().then(
      ({ success, result }) => success && result?.data && setErrors(result.data)
    )
    getDocuments().then(({ result, success }) => {
      if (success && result?.data) {
        const mapping = Object.fromEntries(
          result.data.map((d) => [d.id.toString(), d])
        )
        setDocuments(mapping)
      }
    })
  }

  useEffect(() => {
    loadData()
  }, [])

  useEffect(() => {
    if (errors && documents) {
      const filtered = getActiveConstraints(documents, errors)
      setDisplayedErrors(filtered.slice(0, 5))
      setTotalErrors(filtered.length)
    }
  }, [errors, documents])

  useEventSubscription((event) => {
    if (event.type === 'RUNTIME_CHECK') {
      loadData()
    }
  })

  return (
    <Card sx={{ ...sx, padding: '1rem', marginBottom: '2rem' }}>
      <CardHeader
        title={
          <div style={{ alignItems: 'center', display: 'flex' }}>
            {`Current Errors: ${totalErrors}`}
            {
              <FAIcon
                d={totalErrors > 0 ? mdiAlert : mdiCheckCircle}
                sx={{
                  color: totalErrors > 0 ? 'red' : 'green',
                  marginInlineStart: '1rem',
                }}
              />
            }
          </div>
        }
      />
      <TableContainer component={Paper}>
        <Table aria-label="notification channel">
          <TableHead>
            <TableRow>
              <TableCell>Document</TableCell>
              <TableCell>Constraint</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {!isLoadingErrors &&
              !isLoadingDocuments &&
              displayedErrors.map((row) => (
                <TableRow
                  key={`${row.docId}-${row.name}`}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell>
                    <MdiLink
                      to={`/document/${row.docId}/parsing`}
                      component={Link}
                    >
                      {`${documents[row.docId.toString()].name} (${row.docId})`}
                    </MdiLink>
                  </TableCell>
                  <TableCell>{row.name}</TableCell>
                </TableRow>
              ))}
            {displayedErrors.length === 0 &&
              !isLoadingErrors &&
              !isLoadingDocuments && (
                <TableRow>
                  <TableCell colSpan={2}>
                    <Typography align="center">
                      There are no errors currently!
                    </Typography>
                  </TableCell>
                </TableRow>
              )}
            {totalErrors > 5 && (
              <TableRow>
                <TableCell colSpan={2} align="center" sx={{ padding: '8px' }}>
                  <Button
                    component={Link}
                    sx={{ padding: '8px', paddingInline: '20px' }}
                    startIcon={<FAIcon d={mdiArrowRight} />}
                    to="runtime"
                  >
                    View all errors
                  </Button>
                </TableCell>
              </TableRow>
            )}
            {(isLoadingErrors || isLoadingDocuments) && (
              <TableRow>
                <TableCell colSpan={2}>
                  <LinearProgress />
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </Card>
  )
}

export default RuntimeErrorsWidget
