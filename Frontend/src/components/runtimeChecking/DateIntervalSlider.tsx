import { mdiArrowCollapseLeft, mdiArrowCollapseRight } from '@mdi/js'
import { Box, IconButton, Slider, Tooltip, Typography } from '@mui/material'

import React from 'react'

import FAIcon from '@components/Icon'

export type Interval = [number, number]

const DateIntervalSlider = ({
  interval,
  setInterval,
  intervalBounds,
  step = 1,
  dateOnly = false,
}: {
  interval: Interval
  setInterval: (interval: Interval) => void
  intervalBounds: Interval
  step?: number
  dateOnly?: boolean
}) => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
      }}
    >
      <Typography>Interval: </Typography>
      <Tooltip title="Min">
        <span>
          <IconButton
            color="primary"
            sx={{ marginInline: '1rem' }}
            onClick={() => setInterval([intervalBounds[0], interval[1]])}
            disabled={interval[0] === intervalBounds[0]}
          >
            <FAIcon d={mdiArrowCollapseLeft} />
          </IconButton>
        </span>
      </Tooltip>
      <Slider
        value={interval}
        onChange={(_, v) => {
          if (Array.isArray(v) && v.length == 2)
            setInterval(v as [number, number])
        }}
        min={intervalBounds[0]}
        max={intervalBounds[1]}
        step={step}
        valueLabelDisplay="auto"
        valueLabelFormat={(value) =>
          dateOnly
            ? new Date(value * 1000).toLocaleDateString()
            : new Date(value * 1000).toLocaleString()
        }
      />
      <Tooltip title="Max">
        <span>
          <IconButton
            color="primary"
            sx={{ marginInline: '1rem' }}
            onClick={() => setInterval([interval[0], intervalBounds[1]])}
            disabled={interval[1] === intervalBounds[1]}
          >
            <FAIcon d={mdiArrowCollapseRight} />
          </IconButton>
        </span>
      </Tooltip>
    </Box>
  )
}

export default DateIntervalSlider
