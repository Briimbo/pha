import { v4 as uuid } from 'uuid'

import React, { ReactNode, createContext, useContext, useState } from 'react'

export interface NotificationInstance {
  type: 'success' | 'info' | 'error'
  persistent?: boolean
  message: string
}

interface NotificationInstanceID extends NotificationInstance {
  id: string
}

const NotificationContext = createContext<{
  notifications: Array<NotificationInstanceID>
  addNotification: (i: NotificationInstance) => void
  clearNotifications: () => void
  removeNotification: (id?: string) => void
}>({
  notifications: [],
  addNotification: (i: NotificationInstance) => {
    throw new Error('adding notifications has not been implemented' + i.message)
  },
  clearNotifications: () => {
    return
  },
  removeNotification: (id?: string) => {
    throw new Error('removing notifications has not been implemented' + id)
  },
})

export const NotificationContextProvider = (props: { children: ReactNode }) => {
  const [notifications, setNotifications] = useState<
    Array<NotificationInstanceID>
  >([])

  const addNotification = (n: NotificationInstance) => {
    const id = uuid()
    setNotifications([
      ...notifications,
      {
        id,
        ...n,
      },
    ])

    if (!n.persistent) {
      setTimeout(() => {
        setNotifications((prev) => prev.filter((i) => i.id !== id))
      }, 5000)
    }
  }

  const removeNotification = (id?: string) => {
    setNotifications(notifications.filter((i) => i.id !== id))
  }

  const clearNotifications = () => {
    setNotifications([])
  }

  return (
    <NotificationContext.Provider
      value={{
        notifications,
        addNotification,
        clearNotifications,
        removeNotification,
      }}
    >
      {props.children}
    </NotificationContext.Provider>
  )
}

export const useNotifications = () => {
  const context = useContext(NotificationContext)

  if (context === undefined) {
    throw new Error(
      'useNotifications must be used within a NotificationsContextProvider'
    )
  }

  return context
}
