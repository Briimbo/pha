import React, { ReactNode, createContext, useContext, useState } from 'react'

export const ContendWidthContext = createContext<
  | {
      state: {
        width?: number
        height?: number
      }
      setWidth: (width: number) => void
      setHeight: (height: number) => void
    }
  | undefined
>(undefined)

const ContentWidthProvider = (props: { children: ReactNode }) => {
  const [width, setWidth] = useState<number | undefined>(undefined)
  const [height, setHeight] = useState<number | undefined>(undefined)

  // NOTE: you *might* need to memoize this value
  // Learn more in http://kcd.im/optimize-context
  const value = { state: { width, height }, setWidth, setHeight }
  return (
    <ContendWidthContext.Provider value={value}>
      {props.children}
    </ContendWidthContext.Provider>
  )
}

const useContentWidth = () => {
  const context = useContext(ContendWidthContext)

  if (context === undefined) {
    throw new Error('useUserState must be used within a UserStateProvider')
  }

  return context
}

export { ContentWidthProvider, useContentWidth }
