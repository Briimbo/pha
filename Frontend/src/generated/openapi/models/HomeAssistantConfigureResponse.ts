/* tslint:disable */
/* eslint-disable */
/**
 * Api Documentation
 * Api Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    ConfigureInstanceParams,
    ConfigureInstanceParamsFromJSON,
    ConfigureInstanceParamsFromJSONTyped,
    ConfigureInstanceParamsToJSON,
} from './ConfigureInstanceParams';
import {
    ResultDetails,
    ResultDetailsFromJSON,
    ResultDetailsFromJSONTyped,
    ResultDetailsToJSON,
} from './ResultDetails';

/**
 * 
 * @export
 * @interface HomeAssistantConfigureResponse
 */
export interface HomeAssistantConfigureResponse {
    /**
     * 
     * @type {ConfigureInstanceParams}
     * @memberof HomeAssistantConfigureResponse
     */
    data?: ConfigureInstanceParams;
    /**
     * 
     * @type {Array<ResultDetails>}
     * @memberof HomeAssistantConfigureResponse
     */
    results?: Array<ResultDetails>;
}

export function HomeAssistantConfigureResponseFromJSON(json: any): HomeAssistantConfigureResponse {
    return HomeAssistantConfigureResponseFromJSONTyped(json, false);
}

export function HomeAssistantConfigureResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): HomeAssistantConfigureResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'data': !exists(json, 'data') ? undefined : ConfigureInstanceParamsFromJSON(json['data']),
        'results': !exists(json, 'results') ? undefined : ((json['results'] as Array<any>).map(ResultDetailsFromJSON)),
    };
}

export function HomeAssistantConfigureResponseToJSON(value?: HomeAssistantConfigureResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'data': ConfigureInstanceParamsToJSON(value.data),
        'results': value.results === undefined ? undefined : ((value.results as Array<any>).map(ResultDetailsToJSON)),
    };
}

