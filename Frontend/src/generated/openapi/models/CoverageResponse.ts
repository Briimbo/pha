/* tslint:disable */
/* eslint-disable */
/**
 * Api Documentation
 * Api Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    CoverageDto,
    CoverageDtoFromJSON,
    CoverageDtoFromJSONTyped,
    CoverageDtoToJSON,
} from './CoverageDto';
import {
    ResultDetails,
    ResultDetailsFromJSON,
    ResultDetailsFromJSONTyped,
    ResultDetailsToJSON,
} from './ResultDetails';

/**
 * 
 * @export
 * @interface CoverageResponse
 */
export interface CoverageResponse {
    /**
     * 
     * @type {CoverageDto}
     * @memberof CoverageResponse
     */
    data?: CoverageDto;
    /**
     * 
     * @type {Array<ResultDetails>}
     * @memberof CoverageResponse
     */
    results?: Array<ResultDetails>;
}

export function CoverageResponseFromJSON(json: any): CoverageResponse {
    return CoverageResponseFromJSONTyped(json, false);
}

export function CoverageResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): CoverageResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'data': !exists(json, 'data') ? undefined : CoverageDtoFromJSON(json['data']),
        'results': !exists(json, 'results') ? undefined : ((json['results'] as Array<any>).map(ResultDetailsFromJSON)),
    };
}

export function CoverageResponseToJSON(value?: CoverageResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'data': CoverageDtoToJSON(value.data),
        'results': value.results === undefined ? undefined : ((value.results as Array<any>).map(ResultDetailsToJSON)),
    };
}

