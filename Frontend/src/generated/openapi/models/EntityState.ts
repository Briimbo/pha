/* tslint:disable */
/* eslint-disable */
/**
 * Api Documentation
 * Api Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    EntityAttributes,
    EntityAttributesFromJSON,
    EntityAttributesFromJSONTyped,
    EntityAttributesToJSON,
} from './EntityAttributes';

/**
 * 
 * @export
 * @interface EntityState
 */
export interface EntityState {
    /**
     * 
     * @type {EntityAttributes}
     * @memberof EntityState
     */
    attributes: EntityAttributes;
    /**
     * 
     * @type {string}
     * @memberof EntityState
     */
    domain?: string;
    /**
     * 
     * @type {string}
     * @memberof EntityState
     */
    entityId: string;
    /**
     * 
     * @type {Date}
     * @memberof EntityState
     */
    lastChanged: Date;
    /**
     * 
     * @type {Date}
     * @memberof EntityState
     */
    lastUpdated?: Date;
    /**
     * 
     * @type {string}
     * @memberof EntityState
     */
    state: string;
}

export function EntityStateFromJSON(json: any): EntityState {
    return EntityStateFromJSONTyped(json, false);
}

export function EntityStateFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityState {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'attributes': EntityAttributesFromJSON(json['attributes']),
        'domain': !exists(json, 'domain') ? undefined : json['domain'],
        'entityId': json['entityId'],
        'lastChanged': (new Date(json['lastChanged'])),
        'lastUpdated': !exists(json, 'lastUpdated') ? undefined : (new Date(json['lastUpdated'])),
        'state': json['state'],
    };
}

export function EntityStateToJSON(value?: EntityState | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'attributes': EntityAttributesToJSON(value.attributes),
        'domain': value.domain,
        'entityId': value.entityId,
        'lastChanged': (value.lastChanged.toISOString()),
        'lastUpdated': value.lastUpdated === undefined ? undefined : (value.lastUpdated.toISOString()),
        'state': value.state,
    };
}

