/* tslint:disable */
/* eslint-disable */
/**
 * Api Documentation
 * Api Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    ResultDetails,
    ResultDetailsFromJSON,
    ResultDetailsFromJSONTyped,
    ResultDetailsToJSON,
} from './ResultDetails';
import {
    ValidationState,
    ValidationStateFromJSON,
    ValidationStateFromJSONTyped,
    ValidationStateToJSON,
} from './ValidationState';

/**
 * 
 * @export
 * @interface JsonResponseListValidationState
 */
export interface JsonResponseListValidationState {
    /**
     * 
     * @type {Array<ValidationState>}
     * @memberof JsonResponseListValidationState
     */
    data?: Array<ValidationState>;
    /**
     * 
     * @type {Array<ResultDetails>}
     * @memberof JsonResponseListValidationState
     */
    results?: Array<ResultDetails>;
}

export function JsonResponseListValidationStateFromJSON(json: any): JsonResponseListValidationState {
    return JsonResponseListValidationStateFromJSONTyped(json, false);
}

export function JsonResponseListValidationStateFromJSONTyped(json: any, ignoreDiscriminator: boolean): JsonResponseListValidationState {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'data': !exists(json, 'data') ? undefined : ((json['data'] as Array<any>).map(ValidationStateFromJSON)),
        'results': !exists(json, 'results') ? undefined : ((json['results'] as Array<any>).map(ResultDetailsFromJSON)),
    };
}

export function JsonResponseListValidationStateToJSON(value?: JsonResponseListValidationState | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'data': value.data === undefined ? undefined : ((value.data as Array<any>).map(ValidationStateToJSON)),
        'results': value.results === undefined ? undefined : ((value.results as Array<any>).map(ResultDetailsToJSON)),
    };
}

