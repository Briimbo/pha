/* tslint:disable */
/* eslint-disable */
/**
 * Api Documentation
 * Api Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface CoverageDto
 */
export interface CoverageDto {
    /**
     * 
     * @type {number}
     * @memberof CoverageDto
     */
    coveredAutomations: number;
    /**
     * 
     * @type {number}
     * @memberof CoverageDto
     */
    coveredEntities: number;
    /**
     * 
     * @type {number}
     * @memberof CoverageDto
     */
    totalAutomations: number;
    /**
     * 
     * @type {number}
     * @memberof CoverageDto
     */
    totalEntities: number;
    /**
     * 
     * @type {number}
     * @memberof CoverageDto
     */
    totalSupportedEntities: number;
}

export function CoverageDtoFromJSON(json: any): CoverageDto {
    return CoverageDtoFromJSONTyped(json, false);
}

export function CoverageDtoFromJSONTyped(json: any, ignoreDiscriminator: boolean): CoverageDto {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'coveredAutomations': json['coveredAutomations'],
        'coveredEntities': json['coveredEntities'],
        'totalAutomations': json['totalAutomations'],
        'totalEntities': json['totalEntities'],
        'totalSupportedEntities': json['totalSupportedEntities'],
    };
}

export function CoverageDtoToJSON(value?: CoverageDto | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'coveredAutomations': value.coveredAutomations,
        'coveredEntities': value.coveredEntities,
        'totalAutomations': value.totalAutomations,
        'totalEntities': value.totalEntities,
        'totalSupportedEntities': value.totalSupportedEntities,
    };
}

