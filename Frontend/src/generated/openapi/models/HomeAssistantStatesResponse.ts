/* tslint:disable */
/* eslint-disable */
/**
 * Api Documentation
 * Api Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    EntityState,
    EntityStateFromJSON,
    EntityStateFromJSONTyped,
    EntityStateToJSON,
} from './EntityState';
import {
    ResultDetails,
    ResultDetailsFromJSON,
    ResultDetailsFromJSONTyped,
    ResultDetailsToJSON,
} from './ResultDetails';

/**
 * 
 * @export
 * @interface HomeAssistantStatesResponse
 */
export interface HomeAssistantStatesResponse {
    /**
     * 
     * @type {Array<EntityState>}
     * @memberof HomeAssistantStatesResponse
     */
    data?: Array<EntityState>;
    /**
     * 
     * @type {Array<ResultDetails>}
     * @memberof HomeAssistantStatesResponse
     */
    results?: Array<ResultDetails>;
}

export function HomeAssistantStatesResponseFromJSON(json: any): HomeAssistantStatesResponse {
    return HomeAssistantStatesResponseFromJSONTyped(json, false);
}

export function HomeAssistantStatesResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): HomeAssistantStatesResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'data': !exists(json, 'data') ? undefined : ((json['data'] as Array<any>).map(EntityStateFromJSON)),
        'results': !exists(json, 'results') ? undefined : ((json['results'] as Array<any>).map(ResultDetailsFromJSON)),
    };
}

export function HomeAssistantStatesResponseToJSON(value?: HomeAssistantStatesResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'data': value.data === undefined ? undefined : ((value.data as Array<any>).map(EntityStateToJSON)),
        'results': value.results === undefined ? undefined : ((value.results as Array<any>).map(ResultDetailsToJSON)),
    };
}

