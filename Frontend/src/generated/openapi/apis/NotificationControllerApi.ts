/* tslint:disable */
/* eslint-disable */
/**
 * Api Documentation
 * Api Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import {
    BaseIdResponse,
    BaseIdResponseFromJSON,
    BaseIdResponseToJSON,
    NotificationChannelCrudResponse,
    NotificationChannelCrudResponseFromJSON,
    NotificationChannelCrudResponseToJSON,
    NotificationChannelDto,
    NotificationChannelDtoFromJSON,
    NotificationChannelDtoToJSON,
    NotificationChannelLoadResponse,
    NotificationChannelLoadResponseFromJSON,
    NotificationChannelLoadResponseToJSON,
    NotificationCrudResponse,
    NotificationCrudResponseFromJSON,
    NotificationCrudResponseToJSON,
    NotificationPageResponse,
    NotificationPageResponseFromJSON,
    NotificationPageResponseToJSON,
    SseEmitter,
    SseEmitterFromJSON,
    SseEmitterToJSON,
} from '../models';

export interface CreateNewChannelUsingPOSTRequest {
    notificationChannelDto?: NotificationChannelDto;
}

export interface DeleteByIdUsingDELETERequest {
    id: number;
}

export interface DeleteChannelUsingDELETERequest {
    id: number;
}

export interface LoadNotificationsUsingGETRequest {
    page: number;
}

export interface UpdateChannelUsingPUTRequest {
    notificationChannelDto?: NotificationChannelDto;
}

export interface UpdateReadUsingPUTRequest {
    id: number;
    body?: boolean;
}

/**
 * 
 */
export class NotificationControllerApi extends runtime.BaseAPI {

    /**
     * countUnread
     */
    async countUnreadUsingGETRaw(initOverrides?: RequestInit): Promise<runtime.ApiResponse<BaseIdResponse>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/notification/unread-count`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => BaseIdResponseFromJSON(jsonValue));
    }

    /**
     * countUnread
     */
    async countUnreadUsingGET(initOverrides?: RequestInit): Promise<BaseIdResponse> {
        const response = await this.countUnreadUsingGETRaw(initOverrides);
        return await response.value();
    }

    /**
     * createNewChannel
     */
    async createNewChannelUsingPOSTRaw(requestParameters: CreateNewChannelUsingPOSTRequest, initOverrides?: RequestInit): Promise<runtime.ApiResponse<NotificationChannelCrudResponse>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        const response = await this.request({
            path: `/api/notification/channel/new`,
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
            body: NotificationChannelDtoToJSON(requestParameters.notificationChannelDto),
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => NotificationChannelCrudResponseFromJSON(jsonValue));
    }

    /**
     * createNewChannel
     */
    async createNewChannelUsingPOST(requestParameters: CreateNewChannelUsingPOSTRequest = {}, initOverrides?: RequestInit): Promise<NotificationChannelCrudResponse> {
        const response = await this.createNewChannelUsingPOSTRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * deleteAll
     */
    async deleteAllUsingDELETERaw(initOverrides?: RequestInit): Promise<runtime.ApiResponse<BaseIdResponse>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/notification`,
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => BaseIdResponseFromJSON(jsonValue));
    }

    /**
     * deleteAll
     */
    async deleteAllUsingDELETE(initOverrides?: RequestInit): Promise<BaseIdResponse> {
        const response = await this.deleteAllUsingDELETERaw(initOverrides);
        return await response.value();
    }

    /**
     * deleteById
     */
    async deleteByIdUsingDELETERaw(requestParameters: DeleteByIdUsingDELETERequest, initOverrides?: RequestInit): Promise<runtime.ApiResponse<NotificationCrudResponse>> {
        if (requestParameters.id === null || requestParameters.id === undefined) {
            throw new runtime.RequiredError('id','Required parameter requestParameters.id was null or undefined when calling deleteByIdUsingDELETE.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/notification/{id}`.replace(`{${"id"}}`, encodeURIComponent(String(requestParameters.id))),
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => NotificationCrudResponseFromJSON(jsonValue));
    }

    /**
     * deleteById
     */
    async deleteByIdUsingDELETE(requestParameters: DeleteByIdUsingDELETERequest, initOverrides?: RequestInit): Promise<NotificationCrudResponse> {
        const response = await this.deleteByIdUsingDELETERaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * deleteChannel
     */
    async deleteChannelUsingDELETERaw(requestParameters: DeleteChannelUsingDELETERequest, initOverrides?: RequestInit): Promise<runtime.ApiResponse<NotificationChannelCrudResponse>> {
        if (requestParameters.id === null || requestParameters.id === undefined) {
            throw new runtime.RequiredError('id','Required parameter requestParameters.id was null or undefined when calling deleteChannelUsingDELETE.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/notification/channel/{id}`.replace(`{${"id"}}`, encodeURIComponent(String(requestParameters.id))),
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => NotificationChannelCrudResponseFromJSON(jsonValue));
    }

    /**
     * deleteChannel
     */
    async deleteChannelUsingDELETE(requestParameters: DeleteChannelUsingDELETERequest, initOverrides?: RequestInit): Promise<NotificationChannelCrudResponse> {
        const response = await this.deleteChannelUsingDELETERaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * loadChannels
     */
    async loadChannelsUsingGETRaw(initOverrides?: RequestInit): Promise<runtime.ApiResponse<NotificationChannelLoadResponse>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/notification/channels`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => NotificationChannelLoadResponseFromJSON(jsonValue));
    }

    /**
     * loadChannels
     */
    async loadChannelsUsingGET(initOverrides?: RequestInit): Promise<NotificationChannelLoadResponse> {
        const response = await this.loadChannelsUsingGETRaw(initOverrides);
        return await response.value();
    }

    /**
     * loadNotifications
     */
    async loadNotificationsUsingGETRaw(requestParameters: LoadNotificationsUsingGETRequest, initOverrides?: RequestInit): Promise<runtime.ApiResponse<NotificationPageResponse>> {
        if (requestParameters.page === null || requestParameters.page === undefined) {
            throw new runtime.RequiredError('page','Required parameter requestParameters.page was null or undefined when calling loadNotificationsUsingGET.');
        }

        const queryParameters: any = {};

        if (requestParameters.page !== undefined) {
            queryParameters['page'] = requestParameters.page;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/notification`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => NotificationPageResponseFromJSON(jsonValue));
    }

    /**
     * loadNotifications
     */
    async loadNotificationsUsingGET(requestParameters: LoadNotificationsUsingGETRequest, initOverrides?: RequestInit): Promise<NotificationPageResponse> {
        const response = await this.loadNotificationsUsingGETRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * markAllAsRead
     */
    async markAllAsReadUsingPOSTRaw(initOverrides?: RequestInit): Promise<runtime.ApiResponse<BaseIdResponse>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/notification/read-all`,
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => BaseIdResponseFromJSON(jsonValue));
    }

    /**
     * markAllAsRead
     */
    async markAllAsReadUsingPOST(initOverrides?: RequestInit): Promise<BaseIdResponse> {
        const response = await this.markAllAsReadUsingPOSTRaw(initOverrides);
        return await response.value();
    }

    /**
     * subscribe
     */
    async subscribeUsingGETRaw(initOverrides?: RequestInit): Promise<runtime.ApiResponse<SseEmitter>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/notification/subscribe`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => SseEmitterFromJSON(jsonValue));
    }

    /**
     * subscribe
     */
    async subscribeUsingGET(initOverrides?: RequestInit): Promise<SseEmitter> {
        const response = await this.subscribeUsingGETRaw(initOverrides);
        return await response.value();
    }

    /**
     * updateChannel
     */
    async updateChannelUsingPUTRaw(requestParameters: UpdateChannelUsingPUTRequest, initOverrides?: RequestInit): Promise<runtime.ApiResponse<NotificationChannelCrudResponse>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        const response = await this.request({
            path: `/api/notification/channel/update`,
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
            body: NotificationChannelDtoToJSON(requestParameters.notificationChannelDto),
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => NotificationChannelCrudResponseFromJSON(jsonValue));
    }

    /**
     * updateChannel
     */
    async updateChannelUsingPUT(requestParameters: UpdateChannelUsingPUTRequest = {}, initOverrides?: RequestInit): Promise<NotificationChannelCrudResponse> {
        const response = await this.updateChannelUsingPUTRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * updateRead
     */
    async updateReadUsingPUTRaw(requestParameters: UpdateReadUsingPUTRequest, initOverrides?: RequestInit): Promise<runtime.ApiResponse<NotificationCrudResponse>> {
        if (requestParameters.id === null || requestParameters.id === undefined) {
            throw new runtime.RequiredError('id','Required parameter requestParameters.id was null or undefined when calling updateReadUsingPUT.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        const response = await this.request({
            path: `/api/notification/read/{id}`.replace(`{${"id"}}`, encodeURIComponent(String(requestParameters.id))),
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
            body: requestParameters.body as any,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => NotificationCrudResponseFromJSON(jsonValue));
    }

    /**
     * updateRead
     */
    async updateReadUsingPUT(requestParameters: UpdateReadUsingPUTRequest, initOverrides?: RequestInit): Promise<NotificationCrudResponse> {
        const response = await this.updateReadUsingPUTRaw(requestParameters, initOverrides);
        return await response.value();
    }

}
