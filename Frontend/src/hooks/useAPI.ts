import { useState } from 'react'

import { ErrorDetails, translateError } from '@utils/ErrorTranslator'

import { useNotifications } from '@contexts/notificationContext'

import { ResultDetails } from '@generated/openapi'

export const API_ENDPOINT = import.meta.env.API_ENDPOINT || 'api/'

export interface APIResponse<Data> {
  results: Array<ErrorDetails>
  data?: Data
}

interface APIResult<Data> extends APIResponse<Data> {
  errorMessage?: Array<string>
}

const useApi = <Params, ResponseData>(
  url: string,
  method: 'GET' | 'POST' | 'DELETE' | 'PUT'
) => {
  const [isLoading, setLoading] = useState(false)
  const { addNotification } = useNotifications()

  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: '',
  }

  /**
   *
   * @param params params to use for the api call
   * @returns
   */
  const callApi = (params: Params): Promise<APIResult<ResponseData>> => {
    return new Promise((res) => {
      setLoading(true)
      fetch(API_ENDPOINT + url, {
        body: JSON.stringify(params),
        method,
        headers,
      })
        .then((res) => res.json() as unknown as APIResponse<ResponseData>)
        .then(async (response) => {
          if (
            response.results.length &&
            response.results[0].code === 'success' &&
            response.data !== undefined
          ) {
            res({ ...response, errorMessage: undefined })
          } else {
            res({
              ...response,
              errorMessage: translateError(response.results),
            })
            translateError(response.results).forEach((m) => {
              addNotification({
                message: m,
                type: 'error',
              })
            })
          }
        })
        .catch((error) => {
          res({ results: [], errorMessage: [error] })
        })
        .finally(async () => await setLoading(false))
    })
  }

  return {
    callApi,
    isLoading,
  }
}

export const useErrorHandling = <Params, Res>(
  apiCall: (
    requestParameters?: Params,
    initOverrides?: RequestInit | undefined
  ) => Promise<Res>
): {
  isLoading: boolean
  call: (
    requestParameters?: Params,
    initOverrides?: RequestInit | undefined
  ) => Promise<{ success: boolean; result?: Res }>
} => {
  const { addNotification } = useNotifications()
  const [isLoading, setLoading] = useState(false)

  const call = async (
    requestParameters?: Params,
    initOverrides?: RequestInit | undefined,
    showErrorNotification?: boolean
  ) => {
    setLoading(true)
    try {
      const r = (await apiCall(
        requestParameters,
        initOverrides
      )) as unknown as {
        results?: Array<ResultDetails>
        data?: unknown
      }
      setLoading(false)

      if (!r.results?.every((e) => e.code === 'success') || !r.data) {
        const errors = r.results
          ? translateError(r.results).join(', ')
          : 'unknown error occurred'
        if (showErrorNotification) {
          addNotification({
            message: errors,
            type: 'error',
          })
        }
        return {
          result: r as Res,
          translateErrors: errors,
          success: false,
        }
      }

      return {
        result: r as Res,
        translateErrors: undefined,
        success: true,
      }
    } catch (e) {
      // only happens when the backend is down.
      addNotification({
        message: 'unknown error occurred',
        type: 'error',
      })

      console.error(e)
      setLoading(false)

      return {
        result: undefined,
        success: false,
      }
    }
  }

  return { call, isLoading }
}

export default useApi
