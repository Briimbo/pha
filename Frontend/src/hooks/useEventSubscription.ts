import {
  CheckResult,
  NotificationDto,
  ValidationState,
} from 'generated/openapi'
import { v4 as uuid } from 'uuid'

import { useEffect } from 'react'

import { API_ENDPOINT } from './useAPI'

let eventSource: EventSource | undefined = undefined

export type Event =
  | {
      type: 'NEW_NOTIFICATION' | 'DELETE_NOTIFICATION' | 'UPDATE_NOTIFICATION'
      data: NotificationDto
    }
  | { type: 'CLEAR_NOTIFICATIONS' | 'DELETE_NOTIFICATIONS' }
  | { type: 'VALIDATION_STATE_UPDATE'; data: Record<string, ValidationState> }
  | { type: 'RUNTIME_CHECK'; data: CheckResult }

let subscribers: Array<{
  id: string
  callback: (e: Event) => void
}> = []

const useEventSubscription = (callback: (e: Event) => void) => {
  const id = uuid()
  useEffect(() => {
    subscribers.push({ callback, id })
    if (!eventSource) {
      eventSource = new EventSource(`${API_ENDPOINT}notification/subscribe`)

      eventSource.onmessage = (event) => {
        const e = JSON.parse(event.data) as Event
        subscribers.forEach((s) => {
          s.callback(e)
        })
      }

      eventSource.onerror = () => {
        eventSource?.close()
      }
    }

    return () => {
      subscribers = subscribers.filter((s) => s.id !== id)
    }
  }, [])
}

export default useEventSubscription
