import React, { useEffect, useRef } from 'react'

type ComponentDidUpdateCallBack = React.EffectCallback

/**
 * Accepts a function that contains imperative, possibly effectful code.
 * Executes the passed effect function after rendering of the component
 * see also https://stackoverflow.com/questions/53255951/equivalent-to-componentdidupdate-using-react-hooks
 *
 * @param effect Imperative function that can return a cleanup function
 * @param deps If present, effect will only activate after rerendering caused by a change of the values in the list.
 */
export const useComponentDidUpdate = (
  effect: ComponentDidUpdateCallBack,
  deps: React.DependencyList
) => {
  const hasMounted = useRef(false)

  useEffect(() => {
    if (!hasMounted.current) {
      // potentially do componentDidMount logic here
      hasMounted.current = true
    } else {
      effect()
    }
  }, deps)
}
