import { useEffect, useState } from 'react'

import { useNotifications } from '@contexts/notificationContext'

import { NotificationControllerApi, NotificationDto } from '@generated/openapi'

import useEventSubscription, { Event } from './useEventSubscription'

const useInbox = () => {
  const notifyApi = new NotificationControllerApi()

  const [isLoading, setLoading] = useState(false)
  const [notifications, setNotifications] = useState<Array<NotificationDto>>([])
  const [page, setPage] = useState(0)
  const [unread, setUnread] = useState(0)
  const [existsMore, setExistsMore] = useState(true)
  const { addNotification } = useNotifications()

  const loadMore = () => {
    if (existsMore && !isLoading) {
      setLoading(true)
      setExistsMore(false)
      notifyApi
        .loadNotificationsUsingGET({ page })
        .then((r) => {
          setNotifications([...notifications, ...(r.data?.items || [])])
          setPage(page + 1)
          setExistsMore((r.data?.totalPages || 0) > page + 1)
        })
        .finally(() => {
          setLoading(false)
        })
    }
  }

  const deleteById = (id?: number): Promise<void> => {
    if (id === undefined) return new Promise<void>((res) => res())
    return notifyApi.deleteByIdUsingDELETE({ id }).then((r) => {
      setNotifications(notifications.filter((n) => n.id !== id))
      if (!r.data?.read) {
        setUnread(unread - 1)
      }
    })
  }

  const setReadById = (
    id: number | undefined,
    read: boolean
  ): Promise<void> => {
    if (id === undefined) return new Promise<void>((res) => res())
    return notifyApi.updateReadUsingPUT({ id, body: read }).then((r) => {
      setNotifications(
        notifications.map((n) => (n.id !== id ? n : r.data || { ...n, read }))
      )
      setUnread(unread + (read ? -1 : 1))
    })
  }

  const readAll = () => {
    notifyApi.markAllAsReadUsingPOST()
    setNotifications(notifications.map((n) => ({ ...n, read: true })))
    setUnread(0)
  }

  const deleteAll = () => {
    notifyApi.deleteAllUsingDELETE()
    setNotifications([])
    setExistsMore(false)
    setUnread(0)
  }

  useEventSubscription((event: Event) => {
    if (event.type === 'NEW_NOTIFICATION') {
      const e = event.data
      setUnread((prev) => prev + 1)
      setNotifications((prev) => [e, ...prev])
      addNotification({ message: e.message || '', type: 'info' })
    } else if (event.type === 'DELETE_NOTIFICATION') {
      const e = event.data
      notifyApi.countUnreadUsingGET().then((r) => {
        setUnread(r.data || 0)
      })
      setNotifications((prev) => prev.filter((n) => n.id !== e.id))
    } else if (event.type === 'UPDATE_NOTIFICATION') {
      const e = event.data
      notifyApi.countUnreadUsingGET().then((r) => {
        setUnread(r.data || 0)
      })
      setNotifications((prev) => prev.map((n) => (n.id === e.id ? e : n)))
    } else if (event.type === 'CLEAR_NOTIFICATIONS') {
      setUnread(0)
      setNotifications((prev) => prev.map((n) => ({ ...n, read: true })))
    } else if (event.type === 'DELETE_NOTIFICATIONS') {
      setUnread(0)
      setNotifications([])
    }
  })

  useEffect(() => {
    loadMore()
    notifyApi.countUnreadUsingGET().then((r) => {
      setUnread(r.data || 0)
    })
  }, [])

  return {
    notifications,
    loadMore,
    deleteById,
    setReadById,
    readAll,
    deleteAll,
    isLoading,
    existsMore,
    unread,
  }
}

export default useInbox
