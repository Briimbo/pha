import react from '@vitejs/plugin-react'
import path from 'path'
import { defineConfig } from 'vite'

// https://vitejs.dev/config/
export default defineConfig({
  base: '',
  plugins: [react()],
  resolve: {
    alias: {
      '@hooks': path.resolve(__dirname, './src/hooks'),
      '@components': path.resolve(__dirname, './src/components'),
      '@contexts': path.resolve(__dirname, './src/contexts'),
      '@pages': path.resolve(__dirname, './src/pages'),
      '@utils': path.resolve(__dirname, './src/utils'),
      '@images': path.resolve(__dirname, './src/assets/images'),
      '@generated': path.resolve(__dirname, './src/generated'),
    },
  },
  server: {
    proxy: {
      // string shorthand
      '/api':
        process.env.PROXY_URL == undefined
          ? 'http://localhost:8080'
          : process.env.PROXY_URL,
      '/swagger-ui':
        process.env.PROXY_URL == undefined
          ? 'http://localhost:8080'
          : process.env.PROXY_URL,
    },
    //host: "0.0.0.0"
  },
})
