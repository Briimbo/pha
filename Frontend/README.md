# Painless Home Automation Frontend

Run locally for development:

`npm ci && npm run dev`

Run in container:

- `docker build -t frontend .`
- `docker run -dp 3000:3000 frontend`

# Exporting Groups to HomeAssistant

Your group entry in the `configuration.yaml` needs to look like this:

```yaml
group: !include_dir_merge_named groups/
```

Place all your current group configurations in a `groups` directory.
This plugin will place another file (`pha.groups.yaml`) in that directory.
You need to configure the path to the `groups` directory of your HomeAssistant instance in the properties of the Gateway, see [Gateway README.md](../Gateway/README.md).
Set the property `pha.app.default.ha.groupsDir` relative to the `Gateway` directory.
E.g., if your HA instance is located at `~/hassio.config` and your gateway application is at `~/Implementierung/Gateway`, enter the `../../hassio.config/groups` as the `groupsDir` property

# Exporting the Floor Plan to HomeAssistant

Your `configuration.yaml` needs to contain at least the following:

```yaml
lovelace:
  mode: storage
  dashboards:
    pha-floor-plans:
      mode: yaml
      title: Floor Plan
      icon: mdi:floor-plan
      show_in_sidebar: true
      filename: pha_floorplans_lovelace.yaml
http:
```

You can include other dashboards or configure your http endpoint as you like.
The floor plan needs to host some files, so make sure they are reachable via `/local` (see [HTTP Integration](https://www.home-assistant.io/integrations/http#hosting-files)).
You need to configure the path to the HA config directory of your HomeAssistant instance in the properties of the Gateway, see [Gateway README.md](../Gateway/README.md).
Set the property `pha.app.default.ha.configDir` relative to the `Gateway` directory.
E.g., if your HA instance is located at `~/hassio.config` and your gateway application is at `~/Implementierung/Gateway`, enter `../../hassio.config` as the `configDir` property
