# Painless Home Automation

Painless Home Automation

## Running

There are three ways to run the PHA plugin:

- inside the HA `.devcontainer`
- inside the HA VM
- on your host system as three separate instances (node, spring, nodered)

### Devcontainer

- Install the *Remote Development* extension for VSCode (https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
- Clone the repo via `git clone git@gitlab.com:pg646/implementierung.git`
- Open VSCode in top level: `cd implementierung && code .`
- Allow VSCode to re-open the workspace in the devcontainer when prompted
- When VSCode has finished initializing, run "Task > Start Home Assistant"
- Open `localhost:7123` (sometimes also `8123`)
- Wait for HA to properly set up (should be fast, 2-3 minutes)
- In HA, go to "Settings" > "Add-Ons" > "Add-On Store" > "Local Add-Ons" and install the PHA Plugin (this takes a *long* time the first time around, about 10 minutes depending on your machine, it is a lot faster afterwards \[when doing changes in VSCode and re-installing the plugin, it should only take a minute\])
- Click "Start"
- Check "Show in sidebar" in the plugin configuration
- The plugin should now be available for use in HA!
  - If you open the plugin too fast (before it is started up), you may get a `502: Bad Gateway`. Wait a moment and reload the site.

### VM

- Download and install the HomeAssistant VM like described [here](https://www.home-assistant.io/installation/).
- If using VMware Workstation Player
  - Go to the "Network Adapter" settings and tick "Replicate physical network connection state"
  - Click on "Configure Adapters" and only select your used network adapter
  - Enter the location of the installed VM
  - Edit `Name.vmx`
  - Set `firmware = "efi"`
- Boot the VM and enter the shown ip and port in you webbrowser
- Configure HomeAssistant
- Click on your profile and enable "Advanced Mode"
- Click on "Settings" then "Add-ons, Backups & Supervisor" then "ADD-ON STORE" and install the "Samba share" addon.
- Access the VM's ip through a file explorer (Windows: `\\ip`)
- Enter the `addons` folder and copy this repository into it (the folder `implementierung` not its contents)
- Refresh the HomeAssistant Website
- Go back to the "ADD-ON STORE" and install the now shown Local add-on "PHA"

### As seperate programs

You need Java 17 (exactly), Docker and Node 16+.

- Run `npm ci` inside `/Simulation/nodered.config`
- Run `docker compose up -d` inside `/Simulation`
- Run `npm i && npm run dev` inside `/Frontend`
- Build the maven dependencies
  - Run `mvn generate-sources` inside `Constraint-Language/ls5.pg646.constraints.parent/ls5.pg646.constraints`
  - Run `mvn install` inside `Constraint-Language/ls5.pg646.constraints.parent`
- Run `mvn spring-boot:run` inside `/Gateway`
- Open `localhost:3000`


### Via docker compose (currently broken)

~~Run `$ docker compose up -d` to start all containers.~~

~~If you have changed some files, run `$ docker compose up -d --build` to force
a rebuild of all containers.~~

## Contributing

Google Style Guide for Java (?)

## Technologies

### Frontend
- React version 17

### Backend
- Java Version 17 w/ `--enable-preview`
  - Build with Maven 3.8.3
  - Spring Version 2.5.5

#### XText

- Version 2.26 M2

### Infrastructure as Code
- Docker

### Homeautomation 
- Home Assistant

## Authors and acknowledgment
Authored by Benjamin Kraatz, Benjamin Laumann, Christian Benedict Smit, Daniel Ginter, Daniel Stefan Heinz-Eugen Klose, David Schmidt, Etienne Palanga, Gerit Maldaner, Julien Leuering, Lisa Salewsky and Sebastian Teumert.
Supervised by Tim Tegeler and Dominic Wirkner.
## License
See LICENSE
