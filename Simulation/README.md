# HA Demo

1. Clone the repo via `git clone git@gitlab.com:pg646/Implementierung.git`
1. Open `Simulation/`
1. Run `npm ci` inside `nodered.config` to restore `node_modules`
1. Run `docker compose up` (optionally with `-d`)
1. Open `localhost:8123 ` (HomeAssist), user/pw is `pg/pg`
1. Open `localhost:1880` or `localhost:1880/ui` for NodeRed

## Known Issues

There currently is an issue on startup with the two provided automations, claiming
`ERROR (MainThread) [homeassistant.components.automation.unter_30grad_licht_aus] Unter 30Grad Licht aus: Error executing script. Service not found for call_service at pos 1: Unable to find service light.turn_off` (similar for the second automation).
This might be due to initialization via NodeRED, causing HomeAssistant to try to trigger the automation but lights are not yet initialized. This is a one time issue though, the system and automations will work fine despite the error.
