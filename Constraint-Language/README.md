# Constraint Language

````
.
├── README.md                       You are here
└── ls5.pg646.constraints.parent    
    ├── ls5.pg646.constraints       Language Definition & Grammar
    ├── ls5.pg646.constraints.ide   Language Server
    └── pom.xml
````

## Use in Eclipse

1. Install XText (https://www.eclipse.org/Xtext/)
1. Import constraint language:
	1. Open  <kbd>File / Import</kbd>, choose <kbd>Maven / Existing Maven Projects</kbd>
	1. For *Root Directory*, chose `path/to/implementierung/Constraint-Language`
	1. Import all three projects
1. Generate sources
	1. Right-click `ls5.pg646.constraints/src/main/java/ls5.pg646.constrains/GenerateConstraints.mew2`, choose <kbd>Run As... / MWE2 Workflow</kbd>
1. Create a launch configuration for the language server
	1. Right click <kbd>ls5.pg646.constraints.ide / Run As &hellip; Run Configuration</kbd>
	1. Select <kbd>Java Application</kbd>, press <kbd>New</kbd>
	1. As Main Class, choose `org.eclipse.xtext.ide.server.ServerLauncher`
	1. The configuration should look like this:  
		![Launch configuration](docs/eclipse-launch-config.png)
1. Configure Eclipse to understand the grammar via LSP:
	1. Open <kbd>Window / Preferences</kbd>
	1. Go to <kbd>General / Content-Types</kbd>
	1. Choose <kbdd>Text</kbd>, click <kbd>Add Child &hellip;</kbd>
	1. For *File Associations*, add `*.check`
	1. It should look like this:  
		![Launch configuration](docs/eclipse-content-types.png)
1. Configure Eclipse to start the LSP with our launch config:
	1. Go to <kbd>Window / Preferences / Language Servers</kbd>
	1. Click <kbd>Add</kbd>
	1. On the left, choose <kbd>Text / Constraints</kbd> for the content type
	1. On the right, choose <kbd>Java Application</kbd> and choose the launch config you created in step 4
	1. It should look like this:  
		![Launch configuration](docs/eclipse-lsp-config.png)
1. Test out the language
	1. Create a new plain project
	1. Create a new file that ends with `.check`
	1. Open it. The Console should show the LSP starting and the JSON communication, <kbd>Ctrl + SPACE</kbd> should show code assist
	1. There is no syntax highlighting, as thats not part of the LSP.
