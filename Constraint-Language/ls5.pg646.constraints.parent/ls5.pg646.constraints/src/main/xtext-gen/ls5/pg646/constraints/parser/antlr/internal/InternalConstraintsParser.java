package ls5.pg646.constraints.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import ls5.pg646.constraints.services.ConstraintsGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalConstraintsParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_DOUBLE", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'read'", "'.'", "'as'", "';'", "'runtime'", "'constraint'", "':'", "'|'", "'||'", "'OR'", "'&&'", "'AND'", "'('", "')'", "'not'", "'in'", "'group'", "'range'", "'of'", "'class'", "'is'", "'=='", "'!='", "'>'", "'<'", "'>='", "'<='", "'-'", "'!'", "'true'", "'false'", "'forall'", "'all'", "'exists'", "'any'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=7;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__37=37;
    public static final int RULE_DOUBLE=6;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalConstraintsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalConstraintsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalConstraintsParser.tokenNames; }
    public String getGrammarFileName() { return "InternalConstraints.g"; }



     	private ConstraintsGrammarAccess grammarAccess;

        public InternalConstraintsParser(TokenStream input, ConstraintsGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "ConstraintDocument";
       	}

       	@Override
       	protected ConstraintsGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleConstraintDocument"
    // InternalConstraints.g:65:1: entryRuleConstraintDocument returns [EObject current=null] : iv_ruleConstraintDocument= ruleConstraintDocument EOF ;
    public final EObject entryRuleConstraintDocument() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraintDocument = null;


        try {
            // InternalConstraints.g:65:59: (iv_ruleConstraintDocument= ruleConstraintDocument EOF )
            // InternalConstraints.g:66:2: iv_ruleConstraintDocument= ruleConstraintDocument EOF
            {
             newCompositeNode(grammarAccess.getConstraintDocumentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConstraintDocument=ruleConstraintDocument();

            state._fsp--;

             current =iv_ruleConstraintDocument; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraintDocument"


    // $ANTLR start "ruleConstraintDocument"
    // InternalConstraints.g:72:1: ruleConstraintDocument returns [EObject current=null] : ( ( (lv_readings_0_0= ruleReading ) )* ( (lv_constraints_1_0= ruleConstraint ) )* ) ;
    public final EObject ruleConstraintDocument() throws RecognitionException {
        EObject current = null;

        EObject lv_readings_0_0 = null;

        EObject lv_constraints_1_0 = null;



        	enterRule();

        try {
            // InternalConstraints.g:78:2: ( ( ( (lv_readings_0_0= ruleReading ) )* ( (lv_constraints_1_0= ruleConstraint ) )* ) )
            // InternalConstraints.g:79:2: ( ( (lv_readings_0_0= ruleReading ) )* ( (lv_constraints_1_0= ruleConstraint ) )* )
            {
            // InternalConstraints.g:79:2: ( ( (lv_readings_0_0= ruleReading ) )* ( (lv_constraints_1_0= ruleConstraint ) )* )
            // InternalConstraints.g:80:3: ( (lv_readings_0_0= ruleReading ) )* ( (lv_constraints_1_0= ruleConstraint ) )*
            {
            // InternalConstraints.g:80:3: ( (lv_readings_0_0= ruleReading ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalConstraints.g:81:4: (lv_readings_0_0= ruleReading )
            	    {
            	    // InternalConstraints.g:81:4: (lv_readings_0_0= ruleReading )
            	    // InternalConstraints.g:82:5: lv_readings_0_0= ruleReading
            	    {

            	    					newCompositeNode(grammarAccess.getConstraintDocumentAccess().getReadingsReadingParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_readings_0_0=ruleReading();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getConstraintDocumentRule());
            	    					}
            	    					add(
            	    						current,
            	    						"readings",
            	    						lv_readings_0_0,
            	    						"ls5.pg646.constraints.Constraints.Reading");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalConstraints.g:99:3: ( (lv_constraints_1_0= ruleConstraint ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=16 && LA2_0<=17)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalConstraints.g:100:4: (lv_constraints_1_0= ruleConstraint )
            	    {
            	    // InternalConstraints.g:100:4: (lv_constraints_1_0= ruleConstraint )
            	    // InternalConstraints.g:101:5: lv_constraints_1_0= ruleConstraint
            	    {

            	    					newCompositeNode(grammarAccess.getConstraintDocumentAccess().getConstraintsConstraintParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_constraints_1_0=ruleConstraint();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getConstraintDocumentRule());
            	    					}
            	    					add(
            	    						current,
            	    						"constraints",
            	    						lv_constraints_1_0,
            	    						"ls5.pg646.constraints.Constraints.Constraint");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraintDocument"


    // $ANTLR start "entryRuleReading"
    // InternalConstraints.g:122:1: entryRuleReading returns [EObject current=null] : iv_ruleReading= ruleReading EOF ;
    public final EObject entryRuleReading() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReading = null;


        try {
            // InternalConstraints.g:122:48: (iv_ruleReading= ruleReading EOF )
            // InternalConstraints.g:123:2: iv_ruleReading= ruleReading EOF
            {
             newCompositeNode(grammarAccess.getReadingRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReading=ruleReading();

            state._fsp--;

             current =iv_ruleReading; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReading"


    // $ANTLR start "ruleReading"
    // InternalConstraints.g:129:1: ruleReading returns [EObject current=null] : (otherlv_0= 'read' ( (lv_type_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_identifier_3_0= RULE_ID ) ) otherlv_4= 'as' ( (lv_name_5_0= RULE_ID ) ) otherlv_6= ';' ) ;
    public final EObject ruleReading() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_type_1_0=null;
        Token otherlv_2=null;
        Token lv_identifier_3_0=null;
        Token otherlv_4=null;
        Token lv_name_5_0=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalConstraints.g:135:2: ( (otherlv_0= 'read' ( (lv_type_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_identifier_3_0= RULE_ID ) ) otherlv_4= 'as' ( (lv_name_5_0= RULE_ID ) ) otherlv_6= ';' ) )
            // InternalConstraints.g:136:2: (otherlv_0= 'read' ( (lv_type_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_identifier_3_0= RULE_ID ) ) otherlv_4= 'as' ( (lv_name_5_0= RULE_ID ) ) otherlv_6= ';' )
            {
            // InternalConstraints.g:136:2: (otherlv_0= 'read' ( (lv_type_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_identifier_3_0= RULE_ID ) ) otherlv_4= 'as' ( (lv_name_5_0= RULE_ID ) ) otherlv_6= ';' )
            // InternalConstraints.g:137:3: otherlv_0= 'read' ( (lv_type_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_identifier_3_0= RULE_ID ) ) otherlv_4= 'as' ( (lv_name_5_0= RULE_ID ) ) otherlv_6= ';'
            {
            otherlv_0=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getReadingAccess().getReadKeyword_0());
            		
            // InternalConstraints.g:141:3: ( (lv_type_1_0= RULE_ID ) )
            // InternalConstraints.g:142:4: (lv_type_1_0= RULE_ID )
            {
            // InternalConstraints.g:142:4: (lv_type_1_0= RULE_ID )
            // InternalConstraints.g:143:5: lv_type_1_0= RULE_ID
            {
            lv_type_1_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_type_1_0, grammarAccess.getReadingAccess().getTypeIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getReadingRule());
            					}
            					setWithLastConsumed(
            						current,
            						"type",
            						lv_type_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getReadingAccess().getFullStopKeyword_2());
            		
            // InternalConstraints.g:163:3: ( (lv_identifier_3_0= RULE_ID ) )
            // InternalConstraints.g:164:4: (lv_identifier_3_0= RULE_ID )
            {
            // InternalConstraints.g:164:4: (lv_identifier_3_0= RULE_ID )
            // InternalConstraints.g:165:5: lv_identifier_3_0= RULE_ID
            {
            lv_identifier_3_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            					newLeafNode(lv_identifier_3_0, grammarAccess.getReadingAccess().getIdentifierIDTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getReadingRule());
            					}
            					setWithLastConsumed(
            						current,
            						"identifier",
            						lv_identifier_3_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_4=(Token)match(input,14,FOLLOW_5); 

            			newLeafNode(otherlv_4, grammarAccess.getReadingAccess().getAsKeyword_4());
            		
            // InternalConstraints.g:185:3: ( (lv_name_5_0= RULE_ID ) )
            // InternalConstraints.g:186:4: (lv_name_5_0= RULE_ID )
            {
            // InternalConstraints.g:186:4: (lv_name_5_0= RULE_ID )
            // InternalConstraints.g:187:5: lv_name_5_0= RULE_ID
            {
            lv_name_5_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_name_5_0, grammarAccess.getReadingAccess().getNameIDTerminalRuleCall_5_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getReadingRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_5_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_6=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getReadingAccess().getSemicolonKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReading"


    // $ANTLR start "entryRuleConstraint"
    // InternalConstraints.g:211:1: entryRuleConstraint returns [EObject current=null] : iv_ruleConstraint= ruleConstraint EOF ;
    public final EObject entryRuleConstraint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraint = null;


        try {
            // InternalConstraints.g:211:51: (iv_ruleConstraint= ruleConstraint EOF )
            // InternalConstraints.g:212:2: iv_ruleConstraint= ruleConstraint EOF
            {
             newCompositeNode(grammarAccess.getConstraintRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConstraint=ruleConstraint();

            state._fsp--;

             current =iv_ruleConstraint; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraint"


    // $ANTLR start "ruleConstraint"
    // InternalConstraints.g:218:1: ruleConstraint returns [EObject current=null] : ( ( (lv_runtime_0_0= 'runtime' ) )? otherlv_1= 'constraint' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ':' ( (lv_expression_4_0= ruleExpression ) ) otherlv_5= ';' ) ;
    public final EObject ruleConstraint() throws RecognitionException {
        EObject current = null;

        Token lv_runtime_0_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_expression_4_0 = null;



        	enterRule();

        try {
            // InternalConstraints.g:224:2: ( ( ( (lv_runtime_0_0= 'runtime' ) )? otherlv_1= 'constraint' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ':' ( (lv_expression_4_0= ruleExpression ) ) otherlv_5= ';' ) )
            // InternalConstraints.g:225:2: ( ( (lv_runtime_0_0= 'runtime' ) )? otherlv_1= 'constraint' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ':' ( (lv_expression_4_0= ruleExpression ) ) otherlv_5= ';' )
            {
            // InternalConstraints.g:225:2: ( ( (lv_runtime_0_0= 'runtime' ) )? otherlv_1= 'constraint' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ':' ( (lv_expression_4_0= ruleExpression ) ) otherlv_5= ';' )
            // InternalConstraints.g:226:3: ( (lv_runtime_0_0= 'runtime' ) )? otherlv_1= 'constraint' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ':' ( (lv_expression_4_0= ruleExpression ) ) otherlv_5= ';'
            {
            // InternalConstraints.g:226:3: ( (lv_runtime_0_0= 'runtime' ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==16) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalConstraints.g:227:4: (lv_runtime_0_0= 'runtime' )
                    {
                    // InternalConstraints.g:227:4: (lv_runtime_0_0= 'runtime' )
                    // InternalConstraints.g:228:5: lv_runtime_0_0= 'runtime'
                    {
                    lv_runtime_0_0=(Token)match(input,16,FOLLOW_9); 

                    					newLeafNode(lv_runtime_0_0, grammarAccess.getConstraintAccess().getRuntimeRuntimeKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getConstraintRule());
                    					}
                    					setWithLastConsumed(current, "runtime", lv_runtime_0_0 != null, "runtime");
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,17,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getConstraintAccess().getConstraintKeyword_1());
            		
            // InternalConstraints.g:244:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalConstraints.g:245:4: (lv_name_2_0= RULE_ID )
            {
            // InternalConstraints.g:245:4: (lv_name_2_0= RULE_ID )
            // InternalConstraints.g:246:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_10); 

            					newLeafNode(lv_name_2_0, grammarAccess.getConstraintAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConstraintRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,18,FOLLOW_11); 

            			newLeafNode(otherlv_3, grammarAccess.getConstraintAccess().getColonKeyword_3());
            		
            // InternalConstraints.g:266:3: ( (lv_expression_4_0= ruleExpression ) )
            // InternalConstraints.g:267:4: (lv_expression_4_0= ruleExpression )
            {
            // InternalConstraints.g:267:4: (lv_expression_4_0= ruleExpression )
            // InternalConstraints.g:268:5: lv_expression_4_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getConstraintAccess().getExpressionExpressionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_8);
            lv_expression_4_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConstraintRule());
            					}
            					set(
            						current,
            						"expression",
            						lv_expression_4_0,
            						"ls5.pg646.constraints.Constraints.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getConstraintAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraint"


    // $ANTLR start "entryRuleDeclaredArgument"
    // InternalConstraints.g:293:1: entryRuleDeclaredArgument returns [EObject current=null] : iv_ruleDeclaredArgument= ruleDeclaredArgument EOF ;
    public final EObject entryRuleDeclaredArgument() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclaredArgument = null;


        try {
            // InternalConstraints.g:293:57: (iv_ruleDeclaredArgument= ruleDeclaredArgument EOF )
            // InternalConstraints.g:294:2: iv_ruleDeclaredArgument= ruleDeclaredArgument EOF
            {
             newCompositeNode(grammarAccess.getDeclaredArgumentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDeclaredArgument=ruleDeclaredArgument();

            state._fsp--;

             current =iv_ruleDeclaredArgument; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclaredArgument"


    // $ANTLR start "ruleDeclaredArgument"
    // InternalConstraints.g:300:1: ruleDeclaredArgument returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleDeclaredArgument() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalConstraints.g:306:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalConstraints.g:307:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalConstraints.g:307:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalConstraints.g:308:3: (lv_name_0_0= RULE_ID )
            {
            // InternalConstraints.g:308:3: (lv_name_0_0= RULE_ID )
            // InternalConstraints.g:309:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getDeclaredArgumentAccess().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getDeclaredArgumentRule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclaredArgument"


    // $ANTLR start "entryRuleExpression"
    // InternalConstraints.g:328:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalConstraints.g:328:51: (iv_ruleExpression= ruleExpression EOF )
            // InternalConstraints.g:329:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalConstraints.g:335:1: ruleExpression returns [EObject current=null] : (this_QuantifiedExpression_0= ruleQuantifiedExpression | this_LogicalOr_1= ruleLogicalOr ) ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_QuantifiedExpression_0 = null;

        EObject this_LogicalOr_1 = null;



        	enterRule();

        try {
            // InternalConstraints.g:341:2: ( (this_QuantifiedExpression_0= ruleQuantifiedExpression | this_LogicalOr_1= ruleLogicalOr ) )
            // InternalConstraints.g:342:2: (this_QuantifiedExpression_0= ruleQuantifiedExpression | this_LogicalOr_1= ruleLogicalOr )
            {
            // InternalConstraints.g:342:2: (this_QuantifiedExpression_0= ruleQuantifiedExpression | this_LogicalOr_1= ruleLogicalOr )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( ((LA4_0>=43 && LA4_0<=46)) ) {
                alt4=1;
            }
            else if ( ((LA4_0>=RULE_ID && LA4_0<=RULE_INT)||LA4_0==24||(LA4_0>=39 && LA4_0<=42)) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalConstraints.g:343:3: this_QuantifiedExpression_0= ruleQuantifiedExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionAccess().getQuantifiedExpressionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_QuantifiedExpression_0=ruleQuantifiedExpression();

                    state._fsp--;


                    			current = this_QuantifiedExpression_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalConstraints.g:352:3: this_LogicalOr_1= ruleLogicalOr
                    {

                    			newCompositeNode(grammarAccess.getExpressionAccess().getLogicalOrParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_LogicalOr_1=ruleLogicalOr();

                    state._fsp--;


                    			current = this_LogicalOr_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleQuantifiedExpression"
    // InternalConstraints.g:364:1: entryRuleQuantifiedExpression returns [EObject current=null] : iv_ruleQuantifiedExpression= ruleQuantifiedExpression EOF ;
    public final EObject entryRuleQuantifiedExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuantifiedExpression = null;


        try {
            // InternalConstraints.g:364:61: (iv_ruleQuantifiedExpression= ruleQuantifiedExpression EOF )
            // InternalConstraints.g:365:2: iv_ruleQuantifiedExpression= ruleQuantifiedExpression EOF
            {
             newCompositeNode(grammarAccess.getQuantifiedExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuantifiedExpression=ruleQuantifiedExpression();

            state._fsp--;

             current =iv_ruleQuantifiedExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuantifiedExpression"


    // $ANTLR start "ruleQuantifiedExpression"
    // InternalConstraints.g:371:1: ruleQuantifiedExpression returns [EObject current=null] : ( () ( (lv_quantifier_1_0= ruleQuantifier ) ) ( (lv_argument_2_0= ruleDeclaredArgument ) ) (otherlv_3= '|' ( (lv_query_4_0= ruleQueryExpression ) ) )? otherlv_5= '.' ( (lv_expression_6_0= ruleExpression ) ) ) ;
    public final EObject ruleQuantifiedExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_5=null;
        Enumerator lv_quantifier_1_0 = null;

        EObject lv_argument_2_0 = null;

        EObject lv_query_4_0 = null;

        EObject lv_expression_6_0 = null;



        	enterRule();

        try {
            // InternalConstraints.g:377:2: ( ( () ( (lv_quantifier_1_0= ruleQuantifier ) ) ( (lv_argument_2_0= ruleDeclaredArgument ) ) (otherlv_3= '|' ( (lv_query_4_0= ruleQueryExpression ) ) )? otherlv_5= '.' ( (lv_expression_6_0= ruleExpression ) ) ) )
            // InternalConstraints.g:378:2: ( () ( (lv_quantifier_1_0= ruleQuantifier ) ) ( (lv_argument_2_0= ruleDeclaredArgument ) ) (otherlv_3= '|' ( (lv_query_4_0= ruleQueryExpression ) ) )? otherlv_5= '.' ( (lv_expression_6_0= ruleExpression ) ) )
            {
            // InternalConstraints.g:378:2: ( () ( (lv_quantifier_1_0= ruleQuantifier ) ) ( (lv_argument_2_0= ruleDeclaredArgument ) ) (otherlv_3= '|' ( (lv_query_4_0= ruleQueryExpression ) ) )? otherlv_5= '.' ( (lv_expression_6_0= ruleExpression ) ) )
            // InternalConstraints.g:379:3: () ( (lv_quantifier_1_0= ruleQuantifier ) ) ( (lv_argument_2_0= ruleDeclaredArgument ) ) (otherlv_3= '|' ( (lv_query_4_0= ruleQueryExpression ) ) )? otherlv_5= '.' ( (lv_expression_6_0= ruleExpression ) )
            {
            // InternalConstraints.g:379:3: ()
            // InternalConstraints.g:380:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getQuantifiedExpressionAccess().getQuantifiedExpressionAction_0(),
            					current);
            			

            }

            // InternalConstraints.g:386:3: ( (lv_quantifier_1_0= ruleQuantifier ) )
            // InternalConstraints.g:387:4: (lv_quantifier_1_0= ruleQuantifier )
            {
            // InternalConstraints.g:387:4: (lv_quantifier_1_0= ruleQuantifier )
            // InternalConstraints.g:388:5: lv_quantifier_1_0= ruleQuantifier
            {

            					newCompositeNode(grammarAccess.getQuantifiedExpressionAccess().getQuantifierQuantifierEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_5);
            lv_quantifier_1_0=ruleQuantifier();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQuantifiedExpressionRule());
            					}
            					set(
            						current,
            						"quantifier",
            						lv_quantifier_1_0,
            						"ls5.pg646.constraints.Constraints.Quantifier");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalConstraints.g:405:3: ( (lv_argument_2_0= ruleDeclaredArgument ) )
            // InternalConstraints.g:406:4: (lv_argument_2_0= ruleDeclaredArgument )
            {
            // InternalConstraints.g:406:4: (lv_argument_2_0= ruleDeclaredArgument )
            // InternalConstraints.g:407:5: lv_argument_2_0= ruleDeclaredArgument
            {

            					newCompositeNode(grammarAccess.getQuantifiedExpressionAccess().getArgumentDeclaredArgumentParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_12);
            lv_argument_2_0=ruleDeclaredArgument();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQuantifiedExpressionRule());
            					}
            					set(
            						current,
            						"argument",
            						lv_argument_2_0,
            						"ls5.pg646.constraints.Constraints.DeclaredArgument");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalConstraints.g:424:3: (otherlv_3= '|' ( (lv_query_4_0= ruleQueryExpression ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==19) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalConstraints.g:425:4: otherlv_3= '|' ( (lv_query_4_0= ruleQueryExpression ) )
                    {
                    otherlv_3=(Token)match(input,19,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getQuantifiedExpressionAccess().getVerticalLineKeyword_3_0());
                    			
                    // InternalConstraints.g:429:4: ( (lv_query_4_0= ruleQueryExpression ) )
                    // InternalConstraints.g:430:5: (lv_query_4_0= ruleQueryExpression )
                    {
                    // InternalConstraints.g:430:5: (lv_query_4_0= ruleQueryExpression )
                    // InternalConstraints.g:431:6: lv_query_4_0= ruleQueryExpression
                    {

                    						newCompositeNode(grammarAccess.getQuantifiedExpressionAccess().getQueryQueryExpressionParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_query_4_0=ruleQueryExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getQuantifiedExpressionRule());
                    						}
                    						set(
                    							current,
                    							"query",
                    							lv_query_4_0,
                    							"ls5.pg646.constraints.Constraints.QueryExpression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,13,FOLLOW_11); 

            			newLeafNode(otherlv_5, grammarAccess.getQuantifiedExpressionAccess().getFullStopKeyword_4());
            		
            // InternalConstraints.g:453:3: ( (lv_expression_6_0= ruleExpression ) )
            // InternalConstraints.g:454:4: (lv_expression_6_0= ruleExpression )
            {
            // InternalConstraints.g:454:4: (lv_expression_6_0= ruleExpression )
            // InternalConstraints.g:455:5: lv_expression_6_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getQuantifiedExpressionAccess().getExpressionExpressionParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_2);
            lv_expression_6_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQuantifiedExpressionRule());
            					}
            					set(
            						current,
            						"expression",
            						lv_expression_6_0,
            						"ls5.pg646.constraints.Constraints.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuantifiedExpression"


    // $ANTLR start "entryRuleQueryExpression"
    // InternalConstraints.g:476:1: entryRuleQueryExpression returns [EObject current=null] : iv_ruleQueryExpression= ruleQueryExpression EOF ;
    public final EObject entryRuleQueryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQueryExpression = null;


        try {
            // InternalConstraints.g:476:56: (iv_ruleQueryExpression= ruleQueryExpression EOF )
            // InternalConstraints.g:477:2: iv_ruleQueryExpression= ruleQueryExpression EOF
            {
             newCompositeNode(grammarAccess.getQueryExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQueryExpression=ruleQueryExpression();

            state._fsp--;

             current =iv_ruleQueryExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQueryExpression"


    // $ANTLR start "ruleQueryExpression"
    // InternalConstraints.g:483:1: ruleQueryExpression returns [EObject current=null] : this_QueryUnion_0= ruleQueryUnion ;
    public final EObject ruleQueryExpression() throws RecognitionException {
        EObject current = null;

        EObject this_QueryUnion_0 = null;



        	enterRule();

        try {
            // InternalConstraints.g:489:2: (this_QueryUnion_0= ruleQueryUnion )
            // InternalConstraints.g:490:2: this_QueryUnion_0= ruleQueryUnion
            {

            		newCompositeNode(grammarAccess.getQueryExpressionAccess().getQueryUnionParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_QueryUnion_0=ruleQueryUnion();

            state._fsp--;


            		current = this_QueryUnion_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQueryExpression"


    // $ANTLR start "entryRuleQueryUnion"
    // InternalConstraints.g:501:1: entryRuleQueryUnion returns [EObject current=null] : iv_ruleQueryUnion= ruleQueryUnion EOF ;
    public final EObject entryRuleQueryUnion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQueryUnion = null;


        try {
            // InternalConstraints.g:501:51: (iv_ruleQueryUnion= ruleQueryUnion EOF )
            // InternalConstraints.g:502:2: iv_ruleQueryUnion= ruleQueryUnion EOF
            {
             newCompositeNode(grammarAccess.getQueryUnionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQueryUnion=ruleQueryUnion();

            state._fsp--;

             current =iv_ruleQueryUnion; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQueryUnion"


    // $ANTLR start "ruleQueryUnion"
    // InternalConstraints.g:508:1: ruleQueryUnion returns [EObject current=null] : (this_QueryIntersection_0= ruleQueryIntersection ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleQueryIntersection ) ) )* ) ;
    public final EObject ruleQueryUnion() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        EObject this_QueryIntersection_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalConstraints.g:514:2: ( (this_QueryIntersection_0= ruleQueryIntersection ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleQueryIntersection ) ) )* ) )
            // InternalConstraints.g:515:2: (this_QueryIntersection_0= ruleQueryIntersection ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleQueryIntersection ) ) )* )
            {
            // InternalConstraints.g:515:2: (this_QueryIntersection_0= ruleQueryIntersection ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleQueryIntersection ) ) )* )
            // InternalConstraints.g:516:3: this_QueryIntersection_0= ruleQueryIntersection ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleQueryIntersection ) ) )*
            {

            			newCompositeNode(grammarAccess.getQueryUnionAccess().getQueryIntersectionParserRuleCall_0());
            		
            pushFollow(FOLLOW_14);
            this_QueryIntersection_0=ruleQueryIntersection();

            state._fsp--;


            			current = this_QueryIntersection_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalConstraints.g:524:3: ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleQueryIntersection ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>=20 && LA7_0<=21)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalConstraints.g:525:4: () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleQueryIntersection ) )
            	    {
            	    // InternalConstraints.g:525:4: ()
            	    // InternalConstraints.g:526:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getQueryUnionAccess().getQueryUnionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalConstraints.g:532:4: ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) )
            	    // InternalConstraints.g:533:5: ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) )
            	    {
            	    // InternalConstraints.g:533:5: ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) )
            	    // InternalConstraints.g:534:6: (lv_op_2_1= '||' | lv_op_2_2= 'OR' )
            	    {
            	    // InternalConstraints.g:534:6: (lv_op_2_1= '||' | lv_op_2_2= 'OR' )
            	    int alt6=2;
            	    int LA6_0 = input.LA(1);

            	    if ( (LA6_0==20) ) {
            	        alt6=1;
            	    }
            	    else if ( (LA6_0==21) ) {
            	        alt6=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 6, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt6) {
            	        case 1 :
            	            // InternalConstraints.g:535:7: lv_op_2_1= '||'
            	            {
            	            lv_op_2_1=(Token)match(input,20,FOLLOW_13); 

            	            							newLeafNode(lv_op_2_1, grammarAccess.getQueryUnionAccess().getOpVerticalLineVerticalLineKeyword_1_1_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getQueryUnionRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalConstraints.g:546:7: lv_op_2_2= 'OR'
            	            {
            	            lv_op_2_2=(Token)match(input,21,FOLLOW_13); 

            	            							newLeafNode(lv_op_2_2, grammarAccess.getQueryUnionAccess().getOpORKeyword_1_1_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getQueryUnionRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalConstraints.g:559:4: ( (lv_right_3_0= ruleQueryIntersection ) )
            	    // InternalConstraints.g:560:5: (lv_right_3_0= ruleQueryIntersection )
            	    {
            	    // InternalConstraints.g:560:5: (lv_right_3_0= ruleQueryIntersection )
            	    // InternalConstraints.g:561:6: lv_right_3_0= ruleQueryIntersection
            	    {

            	    						newCompositeNode(grammarAccess.getQueryUnionAccess().getRightQueryIntersectionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_14);
            	    lv_right_3_0=ruleQueryIntersection();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getQueryUnionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ls5.pg646.constraints.Constraints.QueryIntersection");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQueryUnion"


    // $ANTLR start "entryRuleQueryIntersection"
    // InternalConstraints.g:583:1: entryRuleQueryIntersection returns [EObject current=null] : iv_ruleQueryIntersection= ruleQueryIntersection EOF ;
    public final EObject entryRuleQueryIntersection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQueryIntersection = null;


        try {
            // InternalConstraints.g:583:58: (iv_ruleQueryIntersection= ruleQueryIntersection EOF )
            // InternalConstraints.g:584:2: iv_ruleQueryIntersection= ruleQueryIntersection EOF
            {
             newCompositeNode(grammarAccess.getQueryIntersectionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQueryIntersection=ruleQueryIntersection();

            state._fsp--;

             current =iv_ruleQueryIntersection; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQueryIntersection"


    // $ANTLR start "ruleQueryIntersection"
    // InternalConstraints.g:590:1: ruleQueryIntersection returns [EObject current=null] : (this_AtomicQueryExpression_0= ruleAtomicQueryExpression ( () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleAtomicQueryExpression ) ) )* ) ;
    public final EObject ruleQueryIntersection() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        EObject this_AtomicQueryExpression_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalConstraints.g:596:2: ( (this_AtomicQueryExpression_0= ruleAtomicQueryExpression ( () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleAtomicQueryExpression ) ) )* ) )
            // InternalConstraints.g:597:2: (this_AtomicQueryExpression_0= ruleAtomicQueryExpression ( () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleAtomicQueryExpression ) ) )* )
            {
            // InternalConstraints.g:597:2: (this_AtomicQueryExpression_0= ruleAtomicQueryExpression ( () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleAtomicQueryExpression ) ) )* )
            // InternalConstraints.g:598:3: this_AtomicQueryExpression_0= ruleAtomicQueryExpression ( () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleAtomicQueryExpression ) ) )*
            {

            			newCompositeNode(grammarAccess.getQueryIntersectionAccess().getAtomicQueryExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_15);
            this_AtomicQueryExpression_0=ruleAtomicQueryExpression();

            state._fsp--;


            			current = this_AtomicQueryExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalConstraints.g:606:3: ( () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleAtomicQueryExpression ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>=22 && LA9_0<=23)) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalConstraints.g:607:4: () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleAtomicQueryExpression ) )
            	    {
            	    // InternalConstraints.g:607:4: ()
            	    // InternalConstraints.g:608:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getQueryIntersectionAccess().getQueryIntersectionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalConstraints.g:614:4: ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) )
            	    // InternalConstraints.g:615:5: ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) )
            	    {
            	    // InternalConstraints.g:615:5: ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) )
            	    // InternalConstraints.g:616:6: (lv_op_2_1= '&&' | lv_op_2_2= 'AND' )
            	    {
            	    // InternalConstraints.g:616:6: (lv_op_2_1= '&&' | lv_op_2_2= 'AND' )
            	    int alt8=2;
            	    int LA8_0 = input.LA(1);

            	    if ( (LA8_0==22) ) {
            	        alt8=1;
            	    }
            	    else if ( (LA8_0==23) ) {
            	        alt8=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 8, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt8) {
            	        case 1 :
            	            // InternalConstraints.g:617:7: lv_op_2_1= '&&'
            	            {
            	            lv_op_2_1=(Token)match(input,22,FOLLOW_13); 

            	            							newLeafNode(lv_op_2_1, grammarAccess.getQueryIntersectionAccess().getOpAmpersandAmpersandKeyword_1_1_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getQueryIntersectionRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalConstraints.g:628:7: lv_op_2_2= 'AND'
            	            {
            	            lv_op_2_2=(Token)match(input,23,FOLLOW_13); 

            	            							newLeafNode(lv_op_2_2, grammarAccess.getQueryIntersectionAccess().getOpANDKeyword_1_1_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getQueryIntersectionRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalConstraints.g:641:4: ( (lv_right_3_0= ruleAtomicQueryExpression ) )
            	    // InternalConstraints.g:642:5: (lv_right_3_0= ruleAtomicQueryExpression )
            	    {
            	    // InternalConstraints.g:642:5: (lv_right_3_0= ruleAtomicQueryExpression )
            	    // InternalConstraints.g:643:6: lv_right_3_0= ruleAtomicQueryExpression
            	    {

            	    						newCompositeNode(grammarAccess.getQueryIntersectionAccess().getRightAtomicQueryExpressionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_15);
            	    lv_right_3_0=ruleAtomicQueryExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getQueryIntersectionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ls5.pg646.constraints.Constraints.AtomicQueryExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQueryIntersection"


    // $ANTLR start "entryRuleAtomicQueryExpression"
    // InternalConstraints.g:665:1: entryRuleAtomicQueryExpression returns [EObject current=null] : iv_ruleAtomicQueryExpression= ruleAtomicQueryExpression EOF ;
    public final EObject entryRuleAtomicQueryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtomicQueryExpression = null;


        try {
            // InternalConstraints.g:665:62: (iv_ruleAtomicQueryExpression= ruleAtomicQueryExpression EOF )
            // InternalConstraints.g:666:2: iv_ruleAtomicQueryExpression= ruleAtomicQueryExpression EOF
            {
             newCompositeNode(grammarAccess.getAtomicQueryExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAtomicQueryExpression=ruleAtomicQueryExpression();

            state._fsp--;

             current =iv_ruleAtomicQueryExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtomicQueryExpression"


    // $ANTLR start "ruleAtomicQueryExpression"
    // InternalConstraints.g:672:1: ruleAtomicQueryExpression returns [EObject current=null] : ( (otherlv_0= '(' this_QueryExpression_1= ruleQueryExpression otherlv_2= ')' ) | ( () ( (otherlv_4= RULE_ID ) ) ( (lv_not_5_0= 'not' ) )? otherlv_6= 'in' otherlv_7= 'group' ( (lv_group_8_0= RULE_STRING ) ) ) | ( () ( (otherlv_10= RULE_ID ) ) ( (lv_not_11_0= 'not' ) )? otherlv_12= 'in' otherlv_13= 'range' ( (lv_range_14_0= RULE_DOUBLE ) ) otherlv_15= 'of' ( (otherlv_16= RULE_ID ) ) ) | ( () ( (otherlv_18= RULE_ID ) ) ( (lv_not_19_0= 'not' ) )? otherlv_20= 'in' otherlv_21= 'class' ( (lv_clazz_22_0= RULE_ID ) ) ) | ( () ( (otherlv_24= RULE_ID ) ) otherlv_25= 'is' ( (lv_not_26_0= 'not' ) )? ( (lv_clazz_27_0= RULE_ID ) ) ) ) ;
    public final EObject ruleAtomicQueryExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_not_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_group_8_0=null;
        Token otherlv_10=null;
        Token lv_not_11_0=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token lv_range_14_0=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token lv_not_19_0=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token lv_clazz_22_0=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        Token lv_not_26_0=null;
        Token lv_clazz_27_0=null;
        EObject this_QueryExpression_1 = null;



        	enterRule();

        try {
            // InternalConstraints.g:678:2: ( ( (otherlv_0= '(' this_QueryExpression_1= ruleQueryExpression otherlv_2= ')' ) | ( () ( (otherlv_4= RULE_ID ) ) ( (lv_not_5_0= 'not' ) )? otherlv_6= 'in' otherlv_7= 'group' ( (lv_group_8_0= RULE_STRING ) ) ) | ( () ( (otherlv_10= RULE_ID ) ) ( (lv_not_11_0= 'not' ) )? otherlv_12= 'in' otherlv_13= 'range' ( (lv_range_14_0= RULE_DOUBLE ) ) otherlv_15= 'of' ( (otherlv_16= RULE_ID ) ) ) | ( () ( (otherlv_18= RULE_ID ) ) ( (lv_not_19_0= 'not' ) )? otherlv_20= 'in' otherlv_21= 'class' ( (lv_clazz_22_0= RULE_ID ) ) ) | ( () ( (otherlv_24= RULE_ID ) ) otherlv_25= 'is' ( (lv_not_26_0= 'not' ) )? ( (lv_clazz_27_0= RULE_ID ) ) ) ) )
            // InternalConstraints.g:679:2: ( (otherlv_0= '(' this_QueryExpression_1= ruleQueryExpression otherlv_2= ')' ) | ( () ( (otherlv_4= RULE_ID ) ) ( (lv_not_5_0= 'not' ) )? otherlv_6= 'in' otherlv_7= 'group' ( (lv_group_8_0= RULE_STRING ) ) ) | ( () ( (otherlv_10= RULE_ID ) ) ( (lv_not_11_0= 'not' ) )? otherlv_12= 'in' otherlv_13= 'range' ( (lv_range_14_0= RULE_DOUBLE ) ) otherlv_15= 'of' ( (otherlv_16= RULE_ID ) ) ) | ( () ( (otherlv_18= RULE_ID ) ) ( (lv_not_19_0= 'not' ) )? otherlv_20= 'in' otherlv_21= 'class' ( (lv_clazz_22_0= RULE_ID ) ) ) | ( () ( (otherlv_24= RULE_ID ) ) otherlv_25= 'is' ( (lv_not_26_0= 'not' ) )? ( (lv_clazz_27_0= RULE_ID ) ) ) )
            {
            // InternalConstraints.g:679:2: ( (otherlv_0= '(' this_QueryExpression_1= ruleQueryExpression otherlv_2= ')' ) | ( () ( (otherlv_4= RULE_ID ) ) ( (lv_not_5_0= 'not' ) )? otherlv_6= 'in' otherlv_7= 'group' ( (lv_group_8_0= RULE_STRING ) ) ) | ( () ( (otherlv_10= RULE_ID ) ) ( (lv_not_11_0= 'not' ) )? otherlv_12= 'in' otherlv_13= 'range' ( (lv_range_14_0= RULE_DOUBLE ) ) otherlv_15= 'of' ( (otherlv_16= RULE_ID ) ) ) | ( () ( (otherlv_18= RULE_ID ) ) ( (lv_not_19_0= 'not' ) )? otherlv_20= 'in' otherlv_21= 'class' ( (lv_clazz_22_0= RULE_ID ) ) ) | ( () ( (otherlv_24= RULE_ID ) ) otherlv_25= 'is' ( (lv_not_26_0= 'not' ) )? ( (lv_clazz_27_0= RULE_ID ) ) ) )
            int alt14=5;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==24) ) {
                alt14=1;
            }
            else if ( (LA14_0==RULE_ID) ) {
                switch ( input.LA(2) ) {
                case 26:
                    {
                    int LA14_3 = input.LA(3);

                    if ( (LA14_3==27) ) {
                        switch ( input.LA(4) ) {
                        case 29:
                            {
                            alt14=3;
                            }
                            break;
                        case 31:
                            {
                            alt14=4;
                            }
                            break;
                        case 28:
                            {
                            alt14=2;
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 14, 4, input);

                            throw nvae;
                        }

                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 3, input);

                        throw nvae;
                    }
                    }
                    break;
                case 27:
                    {
                    switch ( input.LA(3) ) {
                    case 29:
                        {
                        alt14=3;
                        }
                        break;
                    case 31:
                        {
                        alt14=4;
                        }
                        break;
                    case 28:
                        {
                        alt14=2;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 4, input);

                        throw nvae;
                    }

                    }
                    break;
                case 32:
                    {
                    alt14=5;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 14, 2, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalConstraints.g:680:3: (otherlv_0= '(' this_QueryExpression_1= ruleQueryExpression otherlv_2= ')' )
                    {
                    // InternalConstraints.g:680:3: (otherlv_0= '(' this_QueryExpression_1= ruleQueryExpression otherlv_2= ')' )
                    // InternalConstraints.g:681:4: otherlv_0= '(' this_QueryExpression_1= ruleQueryExpression otherlv_2= ')'
                    {
                    otherlv_0=(Token)match(input,24,FOLLOW_13); 

                    				newLeafNode(otherlv_0, grammarAccess.getAtomicQueryExpressionAccess().getLeftParenthesisKeyword_0_0());
                    			

                    				newCompositeNode(grammarAccess.getAtomicQueryExpressionAccess().getQueryExpressionParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_16);
                    this_QueryExpression_1=ruleQueryExpression();

                    state._fsp--;


                    				current = this_QueryExpression_1;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_2=(Token)match(input,25,FOLLOW_2); 

                    				newLeafNode(otherlv_2, grammarAccess.getAtomicQueryExpressionAccess().getRightParenthesisKeyword_0_2());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:699:3: ( () ( (otherlv_4= RULE_ID ) ) ( (lv_not_5_0= 'not' ) )? otherlv_6= 'in' otherlv_7= 'group' ( (lv_group_8_0= RULE_STRING ) ) )
                    {
                    // InternalConstraints.g:699:3: ( () ( (otherlv_4= RULE_ID ) ) ( (lv_not_5_0= 'not' ) )? otherlv_6= 'in' otherlv_7= 'group' ( (lv_group_8_0= RULE_STRING ) ) )
                    // InternalConstraints.g:700:4: () ( (otherlv_4= RULE_ID ) ) ( (lv_not_5_0= 'not' ) )? otherlv_6= 'in' otherlv_7= 'group' ( (lv_group_8_0= RULE_STRING ) )
                    {
                    // InternalConstraints.g:700:4: ()
                    // InternalConstraints.g:701:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicQueryExpressionAccess().getGroupQueryAction_1_0(),
                    						current);
                    				

                    }

                    // InternalConstraints.g:707:4: ( (otherlv_4= RULE_ID ) )
                    // InternalConstraints.g:708:5: (otherlv_4= RULE_ID )
                    {
                    // InternalConstraints.g:708:5: (otherlv_4= RULE_ID )
                    // InternalConstraints.g:709:6: otherlv_4= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                    						}
                    					
                    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_17); 

                    						newLeafNode(otherlv_4, grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentCrossReference_1_1_0());
                    					

                    }


                    }

                    // InternalConstraints.g:720:4: ( (lv_not_5_0= 'not' ) )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0==26) ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // InternalConstraints.g:721:5: (lv_not_5_0= 'not' )
                            {
                            // InternalConstraints.g:721:5: (lv_not_5_0= 'not' )
                            // InternalConstraints.g:722:6: lv_not_5_0= 'not'
                            {
                            lv_not_5_0=(Token)match(input,26,FOLLOW_18); 

                            						newLeafNode(lv_not_5_0, grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_1_2_0());
                            					

                            						if (current==null) {
                            							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                            						}
                            						setWithLastConsumed(current, "not", lv_not_5_0 != null, "not");
                            					

                            }


                            }
                            break;

                    }

                    otherlv_6=(Token)match(input,27,FOLLOW_19); 

                    				newLeafNode(otherlv_6, grammarAccess.getAtomicQueryExpressionAccess().getInKeyword_1_3());
                    			
                    otherlv_7=(Token)match(input,28,FOLLOW_20); 

                    				newLeafNode(otherlv_7, grammarAccess.getAtomicQueryExpressionAccess().getGroupKeyword_1_4());
                    			
                    // InternalConstraints.g:742:4: ( (lv_group_8_0= RULE_STRING ) )
                    // InternalConstraints.g:743:5: (lv_group_8_0= RULE_STRING )
                    {
                    // InternalConstraints.g:743:5: (lv_group_8_0= RULE_STRING )
                    // InternalConstraints.g:744:6: lv_group_8_0= RULE_STRING
                    {
                    lv_group_8_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_group_8_0, grammarAccess.getAtomicQueryExpressionAccess().getGroupSTRINGTerminalRuleCall_1_5_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"group",
                    							lv_group_8_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalConstraints.g:762:3: ( () ( (otherlv_10= RULE_ID ) ) ( (lv_not_11_0= 'not' ) )? otherlv_12= 'in' otherlv_13= 'range' ( (lv_range_14_0= RULE_DOUBLE ) ) otherlv_15= 'of' ( (otherlv_16= RULE_ID ) ) )
                    {
                    // InternalConstraints.g:762:3: ( () ( (otherlv_10= RULE_ID ) ) ( (lv_not_11_0= 'not' ) )? otherlv_12= 'in' otherlv_13= 'range' ( (lv_range_14_0= RULE_DOUBLE ) ) otherlv_15= 'of' ( (otherlv_16= RULE_ID ) ) )
                    // InternalConstraints.g:763:4: () ( (otherlv_10= RULE_ID ) ) ( (lv_not_11_0= 'not' ) )? otherlv_12= 'in' otherlv_13= 'range' ( (lv_range_14_0= RULE_DOUBLE ) ) otherlv_15= 'of' ( (otherlv_16= RULE_ID ) )
                    {
                    // InternalConstraints.g:763:4: ()
                    // InternalConstraints.g:764:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicQueryExpressionAccess().getRangeQueryAction_2_0(),
                    						current);
                    				

                    }

                    // InternalConstraints.g:770:4: ( (otherlv_10= RULE_ID ) )
                    // InternalConstraints.g:771:5: (otherlv_10= RULE_ID )
                    {
                    // InternalConstraints.g:771:5: (otherlv_10= RULE_ID )
                    // InternalConstraints.g:772:6: otherlv_10= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                    						}
                    					
                    otherlv_10=(Token)match(input,RULE_ID,FOLLOW_17); 

                    						newLeafNode(otherlv_10, grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentCrossReference_2_1_0());
                    					

                    }


                    }

                    // InternalConstraints.g:783:4: ( (lv_not_11_0= 'not' ) )?
                    int alt11=2;
                    int LA11_0 = input.LA(1);

                    if ( (LA11_0==26) ) {
                        alt11=1;
                    }
                    switch (alt11) {
                        case 1 :
                            // InternalConstraints.g:784:5: (lv_not_11_0= 'not' )
                            {
                            // InternalConstraints.g:784:5: (lv_not_11_0= 'not' )
                            // InternalConstraints.g:785:6: lv_not_11_0= 'not'
                            {
                            lv_not_11_0=(Token)match(input,26,FOLLOW_18); 

                            						newLeafNode(lv_not_11_0, grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_2_2_0());
                            					

                            						if (current==null) {
                            							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                            						}
                            						setWithLastConsumed(current, "not", lv_not_11_0 != null, "not");
                            					

                            }


                            }
                            break;

                    }

                    otherlv_12=(Token)match(input,27,FOLLOW_21); 

                    				newLeafNode(otherlv_12, grammarAccess.getAtomicQueryExpressionAccess().getInKeyword_2_3());
                    			
                    otherlv_13=(Token)match(input,29,FOLLOW_22); 

                    				newLeafNode(otherlv_13, grammarAccess.getAtomicQueryExpressionAccess().getRangeKeyword_2_4());
                    			
                    // InternalConstraints.g:805:4: ( (lv_range_14_0= RULE_DOUBLE ) )
                    // InternalConstraints.g:806:5: (lv_range_14_0= RULE_DOUBLE )
                    {
                    // InternalConstraints.g:806:5: (lv_range_14_0= RULE_DOUBLE )
                    // InternalConstraints.g:807:6: lv_range_14_0= RULE_DOUBLE
                    {
                    lv_range_14_0=(Token)match(input,RULE_DOUBLE,FOLLOW_23); 

                    						newLeafNode(lv_range_14_0, grammarAccess.getAtomicQueryExpressionAccess().getRangeDOUBLETerminalRuleCall_2_5_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"range",
                    							lv_range_14_0,
                    							"ls5.pg646.constraints.Constraints.DOUBLE");
                    					

                    }


                    }

                    otherlv_15=(Token)match(input,30,FOLLOW_5); 

                    				newLeafNode(otherlv_15, grammarAccess.getAtomicQueryExpressionAccess().getOfKeyword_2_6());
                    			
                    // InternalConstraints.g:827:4: ( (otherlv_16= RULE_ID ) )
                    // InternalConstraints.g:828:5: (otherlv_16= RULE_ID )
                    {
                    // InternalConstraints.g:828:5: (otherlv_16= RULE_ID )
                    // InternalConstraints.g:829:6: otherlv_16= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                    						}
                    					
                    otherlv_16=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(otherlv_16, grammarAccess.getAtomicQueryExpressionAccess().getReferenceReferenceCrossReference_2_7_0());
                    					

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalConstraints.g:842:3: ( () ( (otherlv_18= RULE_ID ) ) ( (lv_not_19_0= 'not' ) )? otherlv_20= 'in' otherlv_21= 'class' ( (lv_clazz_22_0= RULE_ID ) ) )
                    {
                    // InternalConstraints.g:842:3: ( () ( (otherlv_18= RULE_ID ) ) ( (lv_not_19_0= 'not' ) )? otherlv_20= 'in' otherlv_21= 'class' ( (lv_clazz_22_0= RULE_ID ) ) )
                    // InternalConstraints.g:843:4: () ( (otherlv_18= RULE_ID ) ) ( (lv_not_19_0= 'not' ) )? otherlv_20= 'in' otherlv_21= 'class' ( (lv_clazz_22_0= RULE_ID ) )
                    {
                    // InternalConstraints.g:843:4: ()
                    // InternalConstraints.g:844:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicQueryExpressionAccess().getClassQueryAction_3_0(),
                    						current);
                    				

                    }

                    // InternalConstraints.g:850:4: ( (otherlv_18= RULE_ID ) )
                    // InternalConstraints.g:851:5: (otherlv_18= RULE_ID )
                    {
                    // InternalConstraints.g:851:5: (otherlv_18= RULE_ID )
                    // InternalConstraints.g:852:6: otherlv_18= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                    						}
                    					
                    otherlv_18=(Token)match(input,RULE_ID,FOLLOW_17); 

                    						newLeafNode(otherlv_18, grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentCrossReference_3_1_0());
                    					

                    }


                    }

                    // InternalConstraints.g:863:4: ( (lv_not_19_0= 'not' ) )?
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0==26) ) {
                        alt12=1;
                    }
                    switch (alt12) {
                        case 1 :
                            // InternalConstraints.g:864:5: (lv_not_19_0= 'not' )
                            {
                            // InternalConstraints.g:864:5: (lv_not_19_0= 'not' )
                            // InternalConstraints.g:865:6: lv_not_19_0= 'not'
                            {
                            lv_not_19_0=(Token)match(input,26,FOLLOW_18); 

                            						newLeafNode(lv_not_19_0, grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_3_2_0());
                            					

                            						if (current==null) {
                            							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                            						}
                            						setWithLastConsumed(current, "not", lv_not_19_0 != null, "not");
                            					

                            }


                            }
                            break;

                    }

                    otherlv_20=(Token)match(input,27,FOLLOW_24); 

                    				newLeafNode(otherlv_20, grammarAccess.getAtomicQueryExpressionAccess().getInKeyword_3_3());
                    			
                    otherlv_21=(Token)match(input,31,FOLLOW_5); 

                    				newLeafNode(otherlv_21, grammarAccess.getAtomicQueryExpressionAccess().getClassKeyword_3_4());
                    			
                    // InternalConstraints.g:885:4: ( (lv_clazz_22_0= RULE_ID ) )
                    // InternalConstraints.g:886:5: (lv_clazz_22_0= RULE_ID )
                    {
                    // InternalConstraints.g:886:5: (lv_clazz_22_0= RULE_ID )
                    // InternalConstraints.g:887:6: lv_clazz_22_0= RULE_ID
                    {
                    lv_clazz_22_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(lv_clazz_22_0, grammarAccess.getAtomicQueryExpressionAccess().getClazzIDTerminalRuleCall_3_5_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"clazz",
                    							lv_clazz_22_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalConstraints.g:905:3: ( () ( (otherlv_24= RULE_ID ) ) otherlv_25= 'is' ( (lv_not_26_0= 'not' ) )? ( (lv_clazz_27_0= RULE_ID ) ) )
                    {
                    // InternalConstraints.g:905:3: ( () ( (otherlv_24= RULE_ID ) ) otherlv_25= 'is' ( (lv_not_26_0= 'not' ) )? ( (lv_clazz_27_0= RULE_ID ) ) )
                    // InternalConstraints.g:906:4: () ( (otherlv_24= RULE_ID ) ) otherlv_25= 'is' ( (lv_not_26_0= 'not' ) )? ( (lv_clazz_27_0= RULE_ID ) )
                    {
                    // InternalConstraints.g:906:4: ()
                    // InternalConstraints.g:907:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicQueryExpressionAccess().getClassQueryAction_4_0(),
                    						current);
                    				

                    }

                    // InternalConstraints.g:913:4: ( (otherlv_24= RULE_ID ) )
                    // InternalConstraints.g:914:5: (otherlv_24= RULE_ID )
                    {
                    // InternalConstraints.g:914:5: (otherlv_24= RULE_ID )
                    // InternalConstraints.g:915:6: otherlv_24= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                    						}
                    					
                    otherlv_24=(Token)match(input,RULE_ID,FOLLOW_25); 

                    						newLeafNode(otherlv_24, grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentCrossReference_4_1_0());
                    					

                    }


                    }

                    otherlv_25=(Token)match(input,32,FOLLOW_26); 

                    				newLeafNode(otherlv_25, grammarAccess.getAtomicQueryExpressionAccess().getIsKeyword_4_2());
                    			
                    // InternalConstraints.g:930:4: ( (lv_not_26_0= 'not' ) )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==26) ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // InternalConstraints.g:931:5: (lv_not_26_0= 'not' )
                            {
                            // InternalConstraints.g:931:5: (lv_not_26_0= 'not' )
                            // InternalConstraints.g:932:6: lv_not_26_0= 'not'
                            {
                            lv_not_26_0=(Token)match(input,26,FOLLOW_5); 

                            						newLeafNode(lv_not_26_0, grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_4_3_0());
                            					

                            						if (current==null) {
                            							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                            						}
                            						setWithLastConsumed(current, "not", lv_not_26_0 != null, "not");
                            					

                            }


                            }
                            break;

                    }

                    // InternalConstraints.g:944:4: ( (lv_clazz_27_0= RULE_ID ) )
                    // InternalConstraints.g:945:5: (lv_clazz_27_0= RULE_ID )
                    {
                    // InternalConstraints.g:945:5: (lv_clazz_27_0= RULE_ID )
                    // InternalConstraints.g:946:6: lv_clazz_27_0= RULE_ID
                    {
                    lv_clazz_27_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(lv_clazz_27_0, grammarAccess.getAtomicQueryExpressionAccess().getClazzIDTerminalRuleCall_4_4_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicQueryExpressionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"clazz",
                    							lv_clazz_27_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtomicQueryExpression"


    // $ANTLR start "entryRuleLogicalOr"
    // InternalConstraints.g:967:1: entryRuleLogicalOr returns [EObject current=null] : iv_ruleLogicalOr= ruleLogicalOr EOF ;
    public final EObject entryRuleLogicalOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogicalOr = null;


        try {
            // InternalConstraints.g:967:50: (iv_ruleLogicalOr= ruleLogicalOr EOF )
            // InternalConstraints.g:968:2: iv_ruleLogicalOr= ruleLogicalOr EOF
            {
             newCompositeNode(grammarAccess.getLogicalOrRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLogicalOr=ruleLogicalOr();

            state._fsp--;

             current =iv_ruleLogicalOr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogicalOr"


    // $ANTLR start "ruleLogicalOr"
    // InternalConstraints.g:974:1: ruleLogicalOr returns [EObject current=null] : (this_LogicalAnd_0= ruleLogicalAnd ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleLogicalAnd ) ) )* ) ;
    public final EObject ruleLogicalOr() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        EObject this_LogicalAnd_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalConstraints.g:980:2: ( (this_LogicalAnd_0= ruleLogicalAnd ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleLogicalAnd ) ) )* ) )
            // InternalConstraints.g:981:2: (this_LogicalAnd_0= ruleLogicalAnd ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleLogicalAnd ) ) )* )
            {
            // InternalConstraints.g:981:2: (this_LogicalAnd_0= ruleLogicalAnd ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleLogicalAnd ) ) )* )
            // InternalConstraints.g:982:3: this_LogicalAnd_0= ruleLogicalAnd ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleLogicalAnd ) ) )*
            {

            			newCompositeNode(grammarAccess.getLogicalOrAccess().getLogicalAndParserRuleCall_0());
            		
            pushFollow(FOLLOW_14);
            this_LogicalAnd_0=ruleLogicalAnd();

            state._fsp--;


            			current = this_LogicalAnd_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalConstraints.g:990:3: ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleLogicalAnd ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( ((LA16_0>=20 && LA16_0<=21)) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalConstraints.g:991:4: () ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) ) ( (lv_right_3_0= ruleLogicalAnd ) )
            	    {
            	    // InternalConstraints.g:991:4: ()
            	    // InternalConstraints.g:992:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getLogicalOrAccess().getLogicalOrLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalConstraints.g:998:4: ( ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) ) )
            	    // InternalConstraints.g:999:5: ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) )
            	    {
            	    // InternalConstraints.g:999:5: ( (lv_op_2_1= '||' | lv_op_2_2= 'OR' ) )
            	    // InternalConstraints.g:1000:6: (lv_op_2_1= '||' | lv_op_2_2= 'OR' )
            	    {
            	    // InternalConstraints.g:1000:6: (lv_op_2_1= '||' | lv_op_2_2= 'OR' )
            	    int alt15=2;
            	    int LA15_0 = input.LA(1);

            	    if ( (LA15_0==20) ) {
            	        alt15=1;
            	    }
            	    else if ( (LA15_0==21) ) {
            	        alt15=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 15, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt15) {
            	        case 1 :
            	            // InternalConstraints.g:1001:7: lv_op_2_1= '||'
            	            {
            	            lv_op_2_1=(Token)match(input,20,FOLLOW_11); 

            	            							newLeafNode(lv_op_2_1, grammarAccess.getLogicalOrAccess().getOpVerticalLineVerticalLineKeyword_1_1_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getLogicalOrRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalConstraints.g:1012:7: lv_op_2_2= 'OR'
            	            {
            	            lv_op_2_2=(Token)match(input,21,FOLLOW_11); 

            	            							newLeafNode(lv_op_2_2, grammarAccess.getLogicalOrAccess().getOpORKeyword_1_1_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getLogicalOrRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalConstraints.g:1025:4: ( (lv_right_3_0= ruleLogicalAnd ) )
            	    // InternalConstraints.g:1026:5: (lv_right_3_0= ruleLogicalAnd )
            	    {
            	    // InternalConstraints.g:1026:5: (lv_right_3_0= ruleLogicalAnd )
            	    // InternalConstraints.g:1027:6: lv_right_3_0= ruleLogicalAnd
            	    {

            	    						newCompositeNode(grammarAccess.getLogicalOrAccess().getRightLogicalAndParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_14);
            	    lv_right_3_0=ruleLogicalAnd();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getLogicalOrRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ls5.pg646.constraints.Constraints.LogicalAnd");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogicalOr"


    // $ANTLR start "entryRuleLogicalAnd"
    // InternalConstraints.g:1049:1: entryRuleLogicalAnd returns [EObject current=null] : iv_ruleLogicalAnd= ruleLogicalAnd EOF ;
    public final EObject entryRuleLogicalAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogicalAnd = null;


        try {
            // InternalConstraints.g:1049:51: (iv_ruleLogicalAnd= ruleLogicalAnd EOF )
            // InternalConstraints.g:1050:2: iv_ruleLogicalAnd= ruleLogicalAnd EOF
            {
             newCompositeNode(grammarAccess.getLogicalAndRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLogicalAnd=ruleLogicalAnd();

            state._fsp--;

             current =iv_ruleLogicalAnd; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogicalAnd"


    // $ANTLR start "ruleLogicalAnd"
    // InternalConstraints.g:1056:1: ruleLogicalAnd returns [EObject current=null] : (this_Equality_0= ruleEquality ( () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleEquality ) ) )* ) ;
    public final EObject ruleLogicalAnd() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        EObject this_Equality_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalConstraints.g:1062:2: ( (this_Equality_0= ruleEquality ( () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleEquality ) ) )* ) )
            // InternalConstraints.g:1063:2: (this_Equality_0= ruleEquality ( () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleEquality ) ) )* )
            {
            // InternalConstraints.g:1063:2: (this_Equality_0= ruleEquality ( () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleEquality ) ) )* )
            // InternalConstraints.g:1064:3: this_Equality_0= ruleEquality ( () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleEquality ) ) )*
            {

            			newCompositeNode(grammarAccess.getLogicalAndAccess().getEqualityParserRuleCall_0());
            		
            pushFollow(FOLLOW_15);
            this_Equality_0=ruleEquality();

            state._fsp--;


            			current = this_Equality_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalConstraints.g:1072:3: ( () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleEquality ) ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>=22 && LA18_0<=23)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalConstraints.g:1073:4: () ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleEquality ) )
            	    {
            	    // InternalConstraints.g:1073:4: ()
            	    // InternalConstraints.g:1074:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getLogicalAndAccess().getLogicalAndLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalConstraints.g:1080:4: ( ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) ) )
            	    // InternalConstraints.g:1081:5: ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) )
            	    {
            	    // InternalConstraints.g:1081:5: ( (lv_op_2_1= '&&' | lv_op_2_2= 'AND' ) )
            	    // InternalConstraints.g:1082:6: (lv_op_2_1= '&&' | lv_op_2_2= 'AND' )
            	    {
            	    // InternalConstraints.g:1082:6: (lv_op_2_1= '&&' | lv_op_2_2= 'AND' )
            	    int alt17=2;
            	    int LA17_0 = input.LA(1);

            	    if ( (LA17_0==22) ) {
            	        alt17=1;
            	    }
            	    else if ( (LA17_0==23) ) {
            	        alt17=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 17, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt17) {
            	        case 1 :
            	            // InternalConstraints.g:1083:7: lv_op_2_1= '&&'
            	            {
            	            lv_op_2_1=(Token)match(input,22,FOLLOW_11); 

            	            							newLeafNode(lv_op_2_1, grammarAccess.getLogicalAndAccess().getOpAmpersandAmpersandKeyword_1_1_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getLogicalAndRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalConstraints.g:1094:7: lv_op_2_2= 'AND'
            	            {
            	            lv_op_2_2=(Token)match(input,23,FOLLOW_11); 

            	            							newLeafNode(lv_op_2_2, grammarAccess.getLogicalAndAccess().getOpANDKeyword_1_1_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getLogicalAndRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalConstraints.g:1107:4: ( (lv_right_3_0= ruleEquality ) )
            	    // InternalConstraints.g:1108:5: (lv_right_3_0= ruleEquality )
            	    {
            	    // InternalConstraints.g:1108:5: (lv_right_3_0= ruleEquality )
            	    // InternalConstraints.g:1109:6: lv_right_3_0= ruleEquality
            	    {

            	    						newCompositeNode(grammarAccess.getLogicalAndAccess().getRightEqualityParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_15);
            	    lv_right_3_0=ruleEquality();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getLogicalAndRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ls5.pg646.constraints.Constraints.Equality");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogicalAnd"


    // $ANTLR start "entryRuleEquality"
    // InternalConstraints.g:1131:1: entryRuleEquality returns [EObject current=null] : iv_ruleEquality= ruleEquality EOF ;
    public final EObject entryRuleEquality() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEquality = null;


        try {
            // InternalConstraints.g:1131:49: (iv_ruleEquality= ruleEquality EOF )
            // InternalConstraints.g:1132:2: iv_ruleEquality= ruleEquality EOF
            {
             newCompositeNode(grammarAccess.getEqualityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEquality=ruleEquality();

            state._fsp--;

             current =iv_ruleEquality; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEquality"


    // $ANTLR start "ruleEquality"
    // InternalConstraints.g:1138:1: ruleEquality returns [EObject current=null] : (this_Relational_0= ruleRelational ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ) ( (lv_right_3_0= ruleRelational ) ) )* ) ;
    public final EObject ruleEquality() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        EObject this_Relational_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalConstraints.g:1144:2: ( (this_Relational_0= ruleRelational ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ) ( (lv_right_3_0= ruleRelational ) ) )* ) )
            // InternalConstraints.g:1145:2: (this_Relational_0= ruleRelational ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ) ( (lv_right_3_0= ruleRelational ) ) )* )
            {
            // InternalConstraints.g:1145:2: (this_Relational_0= ruleRelational ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ) ( (lv_right_3_0= ruleRelational ) ) )* )
            // InternalConstraints.g:1146:3: this_Relational_0= ruleRelational ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ) ( (lv_right_3_0= ruleRelational ) ) )*
            {

            			newCompositeNode(grammarAccess.getEqualityAccess().getRelationalParserRuleCall_0());
            		
            pushFollow(FOLLOW_27);
            this_Relational_0=ruleRelational();

            state._fsp--;


            			current = this_Relational_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalConstraints.g:1154:3: ( ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ) ( (lv_right_3_0= ruleRelational ) ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( ((LA20_0>=33 && LA20_0<=34)) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalConstraints.g:1155:4: ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ) ( (lv_right_3_0= ruleRelational ) )
            	    {
            	    // InternalConstraints.g:1155:4: ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) )
            	    // InternalConstraints.g:1156:5: () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) )
            	    {
            	    // InternalConstraints.g:1156:5: ()
            	    // InternalConstraints.g:1157:6: 
            	    {

            	    						current = forceCreateModelElementAndSet(
            	    							grammarAccess.getEqualityAccess().getEqualityLeftAction_1_0_0(),
            	    							current);
            	    					

            	    }

            	    // InternalConstraints.g:1163:5: ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) )
            	    // InternalConstraints.g:1164:6: ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) )
            	    {
            	    // InternalConstraints.g:1164:6: ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) )
            	    // InternalConstraints.g:1165:7: (lv_op_2_1= '==' | lv_op_2_2= '!=' )
            	    {
            	    // InternalConstraints.g:1165:7: (lv_op_2_1= '==' | lv_op_2_2= '!=' )
            	    int alt19=2;
            	    int LA19_0 = input.LA(1);

            	    if ( (LA19_0==33) ) {
            	        alt19=1;
            	    }
            	    else if ( (LA19_0==34) ) {
            	        alt19=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 19, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt19) {
            	        case 1 :
            	            // InternalConstraints.g:1166:8: lv_op_2_1= '=='
            	            {
            	            lv_op_2_1=(Token)match(input,33,FOLLOW_11); 

            	            								newLeafNode(lv_op_2_1, grammarAccess.getEqualityAccess().getOpEqualsSignEqualsSignKeyword_1_0_1_0_0());
            	            							

            	            								if (current==null) {
            	            									current = createModelElement(grammarAccess.getEqualityRule());
            	            								}
            	            								setWithLastConsumed(current, "op", lv_op_2_1, null);
            	            							

            	            }
            	            break;
            	        case 2 :
            	            // InternalConstraints.g:1177:8: lv_op_2_2= '!='
            	            {
            	            lv_op_2_2=(Token)match(input,34,FOLLOW_11); 

            	            								newLeafNode(lv_op_2_2, grammarAccess.getEqualityAccess().getOpExclamationMarkEqualsSignKeyword_1_0_1_0_1());
            	            							

            	            								if (current==null) {
            	            									current = createModelElement(grammarAccess.getEqualityRule());
            	            								}
            	            								setWithLastConsumed(current, "op", lv_op_2_2, null);
            	            							

            	            }
            	            break;

            	    }


            	    }


            	    }


            	    }

            	    // InternalConstraints.g:1191:4: ( (lv_right_3_0= ruleRelational ) )
            	    // InternalConstraints.g:1192:5: (lv_right_3_0= ruleRelational )
            	    {
            	    // InternalConstraints.g:1192:5: (lv_right_3_0= ruleRelational )
            	    // InternalConstraints.g:1193:6: lv_right_3_0= ruleRelational
            	    {

            	    						newCompositeNode(grammarAccess.getEqualityAccess().getRightRelationalParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_27);
            	    lv_right_3_0=ruleRelational();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getEqualityRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ls5.pg646.constraints.Constraints.Relational");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEquality"


    // $ANTLR start "entryRuleRelational"
    // InternalConstraints.g:1215:1: entryRuleRelational returns [EObject current=null] : iv_ruleRelational= ruleRelational EOF ;
    public final EObject entryRuleRelational() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelational = null;


        try {
            // InternalConstraints.g:1215:51: (iv_ruleRelational= ruleRelational EOF )
            // InternalConstraints.g:1216:2: iv_ruleRelational= ruleRelational EOF
            {
             newCompositeNode(grammarAccess.getRelationalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRelational=ruleRelational();

            state._fsp--;

             current =iv_ruleRelational; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelational"


    // $ANTLR start "ruleRelational"
    // InternalConstraints.g:1222:1: ruleRelational returns [EObject current=null] : (this_UnaryPrefix_0= ruleUnaryPrefix ( ( ( () otherlv_2= '>' ) | ( () otherlv_4= '<' ) | ( () otherlv_6= '>=' ) | ( () otherlv_8= '<=' ) ) ( (lv_right_9_0= ruleUnaryPrefix ) ) )* ) ;
    public final EObject ruleRelational() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject this_UnaryPrefix_0 = null;

        EObject lv_right_9_0 = null;



        	enterRule();

        try {
            // InternalConstraints.g:1228:2: ( (this_UnaryPrefix_0= ruleUnaryPrefix ( ( ( () otherlv_2= '>' ) | ( () otherlv_4= '<' ) | ( () otherlv_6= '>=' ) | ( () otherlv_8= '<=' ) ) ( (lv_right_9_0= ruleUnaryPrefix ) ) )* ) )
            // InternalConstraints.g:1229:2: (this_UnaryPrefix_0= ruleUnaryPrefix ( ( ( () otherlv_2= '>' ) | ( () otherlv_4= '<' ) | ( () otherlv_6= '>=' ) | ( () otherlv_8= '<=' ) ) ( (lv_right_9_0= ruleUnaryPrefix ) ) )* )
            {
            // InternalConstraints.g:1229:2: (this_UnaryPrefix_0= ruleUnaryPrefix ( ( ( () otherlv_2= '>' ) | ( () otherlv_4= '<' ) | ( () otherlv_6= '>=' ) | ( () otherlv_8= '<=' ) ) ( (lv_right_9_0= ruleUnaryPrefix ) ) )* )
            // InternalConstraints.g:1230:3: this_UnaryPrefix_0= ruleUnaryPrefix ( ( ( () otherlv_2= '>' ) | ( () otherlv_4= '<' ) | ( () otherlv_6= '>=' ) | ( () otherlv_8= '<=' ) ) ( (lv_right_9_0= ruleUnaryPrefix ) ) )*
            {

            			newCompositeNode(grammarAccess.getRelationalAccess().getUnaryPrefixParserRuleCall_0());
            		
            pushFollow(FOLLOW_28);
            this_UnaryPrefix_0=ruleUnaryPrefix();

            state._fsp--;


            			current = this_UnaryPrefix_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalConstraints.g:1238:3: ( ( ( () otherlv_2= '>' ) | ( () otherlv_4= '<' ) | ( () otherlv_6= '>=' ) | ( () otherlv_8= '<=' ) ) ( (lv_right_9_0= ruleUnaryPrefix ) ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( ((LA22_0>=35 && LA22_0<=38)) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalConstraints.g:1239:4: ( ( () otherlv_2= '>' ) | ( () otherlv_4= '<' ) | ( () otherlv_6= '>=' ) | ( () otherlv_8= '<=' ) ) ( (lv_right_9_0= ruleUnaryPrefix ) )
            	    {
            	    // InternalConstraints.g:1239:4: ( ( () otherlv_2= '>' ) | ( () otherlv_4= '<' ) | ( () otherlv_6= '>=' ) | ( () otherlv_8= '<=' ) )
            	    int alt21=4;
            	    switch ( input.LA(1) ) {
            	    case 35:
            	        {
            	        alt21=1;
            	        }
            	        break;
            	    case 36:
            	        {
            	        alt21=2;
            	        }
            	        break;
            	    case 37:
            	        {
            	        alt21=3;
            	        }
            	        break;
            	    case 38:
            	        {
            	        alt21=4;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 21, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt21) {
            	        case 1 :
            	            // InternalConstraints.g:1240:5: ( () otherlv_2= '>' )
            	            {
            	            // InternalConstraints.g:1240:5: ( () otherlv_2= '>' )
            	            // InternalConstraints.g:1241:6: () otherlv_2= '>'
            	            {
            	            // InternalConstraints.g:1241:6: ()
            	            // InternalConstraints.g:1242:7: 
            	            {

            	            							current = forceCreateModelElementAndSet(
            	            								grammarAccess.getRelationalAccess().getGreaterLeftAction_1_0_0_0(),
            	            								current);
            	            						

            	            }

            	            otherlv_2=(Token)match(input,35,FOLLOW_11); 

            	            						newLeafNode(otherlv_2, grammarAccess.getRelationalAccess().getGreaterThanSignKeyword_1_0_0_1());
            	            					

            	            }


            	            }
            	            break;
            	        case 2 :
            	            // InternalConstraints.g:1254:5: ( () otherlv_4= '<' )
            	            {
            	            // InternalConstraints.g:1254:5: ( () otherlv_4= '<' )
            	            // InternalConstraints.g:1255:6: () otherlv_4= '<'
            	            {
            	            // InternalConstraints.g:1255:6: ()
            	            // InternalConstraints.g:1256:7: 
            	            {

            	            							current = forceCreateModelElementAndSet(
            	            								grammarAccess.getRelationalAccess().getSmallerLeftAction_1_0_1_0(),
            	            								current);
            	            						

            	            }

            	            otherlv_4=(Token)match(input,36,FOLLOW_11); 

            	            						newLeafNode(otherlv_4, grammarAccess.getRelationalAccess().getLessThanSignKeyword_1_0_1_1());
            	            					

            	            }


            	            }
            	            break;
            	        case 3 :
            	            // InternalConstraints.g:1268:5: ( () otherlv_6= '>=' )
            	            {
            	            // InternalConstraints.g:1268:5: ( () otherlv_6= '>=' )
            	            // InternalConstraints.g:1269:6: () otherlv_6= '>='
            	            {
            	            // InternalConstraints.g:1269:6: ()
            	            // InternalConstraints.g:1270:7: 
            	            {

            	            							current = forceCreateModelElementAndSet(
            	            								grammarAccess.getRelationalAccess().getGreaterOrEqualLeftAction_1_0_2_0(),
            	            								current);
            	            						

            	            }

            	            otherlv_6=(Token)match(input,37,FOLLOW_11); 

            	            						newLeafNode(otherlv_6, grammarAccess.getRelationalAccess().getGreaterThanSignEqualsSignKeyword_1_0_2_1());
            	            					

            	            }


            	            }
            	            break;
            	        case 4 :
            	            // InternalConstraints.g:1282:5: ( () otherlv_8= '<=' )
            	            {
            	            // InternalConstraints.g:1282:5: ( () otherlv_8= '<=' )
            	            // InternalConstraints.g:1283:6: () otherlv_8= '<='
            	            {
            	            // InternalConstraints.g:1283:6: ()
            	            // InternalConstraints.g:1284:7: 
            	            {

            	            							current = forceCreateModelElementAndSet(
            	            								grammarAccess.getRelationalAccess().getSmallerOrEqualLeftAction_1_0_3_0(),
            	            								current);
            	            						

            	            }

            	            otherlv_8=(Token)match(input,38,FOLLOW_11); 

            	            						newLeafNode(otherlv_8, grammarAccess.getRelationalAccess().getLessThanSignEqualsSignKeyword_1_0_3_1());
            	            					

            	            }


            	            }
            	            break;

            	    }

            	    // InternalConstraints.g:1296:4: ( (lv_right_9_0= ruleUnaryPrefix ) )
            	    // InternalConstraints.g:1297:5: (lv_right_9_0= ruleUnaryPrefix )
            	    {
            	    // InternalConstraints.g:1297:5: (lv_right_9_0= ruleUnaryPrefix )
            	    // InternalConstraints.g:1298:6: lv_right_9_0= ruleUnaryPrefix
            	    {

            	    						newCompositeNode(grammarAccess.getRelationalAccess().getRightUnaryPrefixParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_28);
            	    lv_right_9_0=ruleUnaryPrefix();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getRelationalRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_9_0,
            	    							"ls5.pg646.constraints.Constraints.UnaryPrefix");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelational"


    // $ANTLR start "entryRuleUnaryPrefix"
    // InternalConstraints.g:1320:1: entryRuleUnaryPrefix returns [EObject current=null] : iv_ruleUnaryPrefix= ruleUnaryPrefix EOF ;
    public final EObject entryRuleUnaryPrefix() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryPrefix = null;


        try {
            // InternalConstraints.g:1320:52: (iv_ruleUnaryPrefix= ruleUnaryPrefix EOF )
            // InternalConstraints.g:1321:2: iv_ruleUnaryPrefix= ruleUnaryPrefix EOF
            {
             newCompositeNode(grammarAccess.getUnaryPrefixRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUnaryPrefix=ruleUnaryPrefix();

            state._fsp--;

             current =iv_ruleUnaryPrefix; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryPrefix"


    // $ANTLR start "ruleUnaryPrefix"
    // InternalConstraints.g:1327:1: ruleUnaryPrefix returns [EObject current=null] : (this_Primary_0= rulePrimary | ( () otherlv_2= '-' ( (lv_expression_3_0= ruleUnaryPrefix ) ) ) | ( () otherlv_5= '!' ( (lv_expression_6_0= ruleUnaryPrefix ) ) ) ) ;
    public final EObject ruleUnaryPrefix() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject this_Primary_0 = null;

        EObject lv_expression_3_0 = null;

        EObject lv_expression_6_0 = null;



        	enterRule();

        try {
            // InternalConstraints.g:1333:2: ( (this_Primary_0= rulePrimary | ( () otherlv_2= '-' ( (lv_expression_3_0= ruleUnaryPrefix ) ) ) | ( () otherlv_5= '!' ( (lv_expression_6_0= ruleUnaryPrefix ) ) ) ) )
            // InternalConstraints.g:1334:2: (this_Primary_0= rulePrimary | ( () otherlv_2= '-' ( (lv_expression_3_0= ruleUnaryPrefix ) ) ) | ( () otherlv_5= '!' ( (lv_expression_6_0= ruleUnaryPrefix ) ) ) )
            {
            // InternalConstraints.g:1334:2: (this_Primary_0= rulePrimary | ( () otherlv_2= '-' ( (lv_expression_3_0= ruleUnaryPrefix ) ) ) | ( () otherlv_5= '!' ( (lv_expression_6_0= ruleUnaryPrefix ) ) ) )
            int alt23=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
            case RULE_STRING:
            case RULE_DOUBLE:
            case RULE_INT:
            case 24:
            case 41:
            case 42:
                {
                alt23=1;
                }
                break;
            case 39:
                {
                alt23=2;
                }
                break;
            case 40:
                {
                alt23=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // InternalConstraints.g:1335:3: this_Primary_0= rulePrimary
                    {

                    			newCompositeNode(grammarAccess.getUnaryPrefixAccess().getPrimaryParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Primary_0=rulePrimary();

                    state._fsp--;


                    			current = this_Primary_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalConstraints.g:1344:3: ( () otherlv_2= '-' ( (lv_expression_3_0= ruleUnaryPrefix ) ) )
                    {
                    // InternalConstraints.g:1344:3: ( () otherlv_2= '-' ( (lv_expression_3_0= ruleUnaryPrefix ) ) )
                    // InternalConstraints.g:1345:4: () otherlv_2= '-' ( (lv_expression_3_0= ruleUnaryPrefix ) )
                    {
                    // InternalConstraints.g:1345:4: ()
                    // InternalConstraints.g:1346:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getUnaryPrefixAccess().getUnitaryMinusAction_1_0(),
                    						current);
                    				

                    }

                    otherlv_2=(Token)match(input,39,FOLLOW_11); 

                    				newLeafNode(otherlv_2, grammarAccess.getUnaryPrefixAccess().getHyphenMinusKeyword_1_1());
                    			
                    // InternalConstraints.g:1356:4: ( (lv_expression_3_0= ruleUnaryPrefix ) )
                    // InternalConstraints.g:1357:5: (lv_expression_3_0= ruleUnaryPrefix )
                    {
                    // InternalConstraints.g:1357:5: (lv_expression_3_0= ruleUnaryPrefix )
                    // InternalConstraints.g:1358:6: lv_expression_3_0= ruleUnaryPrefix
                    {

                    						newCompositeNode(grammarAccess.getUnaryPrefixAccess().getExpressionUnaryPrefixParserRuleCall_1_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_expression_3_0=ruleUnaryPrefix();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getUnaryPrefixRule());
                    						}
                    						set(
                    							current,
                    							"expression",
                    							lv_expression_3_0,
                    							"ls5.pg646.constraints.Constraints.UnaryPrefix");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalConstraints.g:1377:3: ( () otherlv_5= '!' ( (lv_expression_6_0= ruleUnaryPrefix ) ) )
                    {
                    // InternalConstraints.g:1377:3: ( () otherlv_5= '!' ( (lv_expression_6_0= ruleUnaryPrefix ) ) )
                    // InternalConstraints.g:1378:4: () otherlv_5= '!' ( (lv_expression_6_0= ruleUnaryPrefix ) )
                    {
                    // InternalConstraints.g:1378:4: ()
                    // InternalConstraints.g:1379:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getUnaryPrefixAccess().getUnitaryNotAction_2_0(),
                    						current);
                    				

                    }

                    otherlv_5=(Token)match(input,40,FOLLOW_11); 

                    				newLeafNode(otherlv_5, grammarAccess.getUnaryPrefixAccess().getExclamationMarkKeyword_2_1());
                    			
                    // InternalConstraints.g:1389:4: ( (lv_expression_6_0= ruleUnaryPrefix ) )
                    // InternalConstraints.g:1390:5: (lv_expression_6_0= ruleUnaryPrefix )
                    {
                    // InternalConstraints.g:1390:5: (lv_expression_6_0= ruleUnaryPrefix )
                    // InternalConstraints.g:1391:6: lv_expression_6_0= ruleUnaryPrefix
                    {

                    						newCompositeNode(grammarAccess.getUnaryPrefixAccess().getExpressionUnaryPrefixParserRuleCall_2_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_expression_6_0=ruleUnaryPrefix();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getUnaryPrefixRule());
                    						}
                    						set(
                    							current,
                    							"expression",
                    							lv_expression_6_0,
                    							"ls5.pg646.constraints.Constraints.UnaryPrefix");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryPrefix"


    // $ANTLR start "entryRulePrimary"
    // InternalConstraints.g:1413:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalConstraints.g:1413:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalConstraints.g:1414:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalConstraints.g:1420:1: rulePrimary returns [EObject current=null] : ( (otherlv_0= '(' this_Expression_1= ruleExpression otherlv_2= ')' ) | this_Constant_3= ruleConstant | ( () ( (otherlv_5= RULE_ID ) ) ) ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject this_Expression_1 = null;

        EObject this_Constant_3 = null;



        	enterRule();

        try {
            // InternalConstraints.g:1426:2: ( ( (otherlv_0= '(' this_Expression_1= ruleExpression otherlv_2= ')' ) | this_Constant_3= ruleConstant | ( () ( (otherlv_5= RULE_ID ) ) ) ) )
            // InternalConstraints.g:1427:2: ( (otherlv_0= '(' this_Expression_1= ruleExpression otherlv_2= ')' ) | this_Constant_3= ruleConstant | ( () ( (otherlv_5= RULE_ID ) ) ) )
            {
            // InternalConstraints.g:1427:2: ( (otherlv_0= '(' this_Expression_1= ruleExpression otherlv_2= ')' ) | this_Constant_3= ruleConstant | ( () ( (otherlv_5= RULE_ID ) ) ) )
            int alt24=3;
            switch ( input.LA(1) ) {
            case 24:
                {
                alt24=1;
                }
                break;
            case RULE_STRING:
            case RULE_DOUBLE:
            case RULE_INT:
            case 41:
            case 42:
                {
                alt24=2;
                }
                break;
            case RULE_ID:
                {
                alt24=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // InternalConstraints.g:1428:3: (otherlv_0= '(' this_Expression_1= ruleExpression otherlv_2= ')' )
                    {
                    // InternalConstraints.g:1428:3: (otherlv_0= '(' this_Expression_1= ruleExpression otherlv_2= ')' )
                    // InternalConstraints.g:1429:4: otherlv_0= '(' this_Expression_1= ruleExpression otherlv_2= ')'
                    {
                    otherlv_0=(Token)match(input,24,FOLLOW_11); 

                    				newLeafNode(otherlv_0, grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0());
                    			

                    				newCompositeNode(grammarAccess.getPrimaryAccess().getExpressionParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_16);
                    this_Expression_1=ruleExpression();

                    state._fsp--;


                    				current = this_Expression_1;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_2=(Token)match(input,25,FOLLOW_2); 

                    				newLeafNode(otherlv_2, grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:1447:3: this_Constant_3= ruleConstant
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getConstantParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Constant_3=ruleConstant();

                    state._fsp--;


                    			current = this_Constant_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalConstraints.g:1456:3: ( () ( (otherlv_5= RULE_ID ) ) )
                    {
                    // InternalConstraints.g:1456:3: ( () ( (otherlv_5= RULE_ID ) ) )
                    // InternalConstraints.g:1457:4: () ( (otherlv_5= RULE_ID ) )
                    {
                    // InternalConstraints.g:1457:4: ()
                    // InternalConstraints.g:1458:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getPrimaryAccess().getReferenceAction_2_0(),
                    						current);
                    				

                    }

                    // InternalConstraints.g:1464:4: ( (otherlv_5= RULE_ID ) )
                    // InternalConstraints.g:1465:5: (otherlv_5= RULE_ID )
                    {
                    // InternalConstraints.g:1465:5: (otherlv_5= RULE_ID )
                    // InternalConstraints.g:1466:6: otherlv_5= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPrimaryRule());
                    						}
                    					
                    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(otherlv_5, grammarAccess.getPrimaryAccess().getReferenceReferenceCrossReference_2_1_0());
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleConstant"
    // InternalConstraints.g:1482:1: entryRuleConstant returns [EObject current=null] : iv_ruleConstant= ruleConstant EOF ;
    public final EObject entryRuleConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstant = null;


        try {
            // InternalConstraints.g:1482:49: (iv_ruleConstant= ruleConstant EOF )
            // InternalConstraints.g:1483:2: iv_ruleConstant= ruleConstant EOF
            {
             newCompositeNode(grammarAccess.getConstantRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConstant=ruleConstant();

            state._fsp--;

             current =iv_ruleConstant; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // InternalConstraints.g:1489:1: ruleConstant returns [EObject current=null] : ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_DOUBLE ) ) ) | ( () ( (lv_value_5_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_7_1= 'true' | lv_value_7_2= 'false' ) ) ) ) ) ;
    public final EObject ruleConstant() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token lv_value_3_0=null;
        Token lv_value_5_0=null;
        Token lv_value_7_1=null;
        Token lv_value_7_2=null;


        	enterRule();

        try {
            // InternalConstraints.g:1495:2: ( ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_DOUBLE ) ) ) | ( () ( (lv_value_5_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_7_1= 'true' | lv_value_7_2= 'false' ) ) ) ) ) )
            // InternalConstraints.g:1496:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_DOUBLE ) ) ) | ( () ( (lv_value_5_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_7_1= 'true' | lv_value_7_2= 'false' ) ) ) ) )
            {
            // InternalConstraints.g:1496:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_DOUBLE ) ) ) | ( () ( (lv_value_5_0= RULE_STRING ) ) ) | ( () ( ( (lv_value_7_1= 'true' | lv_value_7_2= 'false' ) ) ) ) )
            int alt26=4;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                alt26=1;
                }
                break;
            case RULE_DOUBLE:
                {
                alt26=2;
                }
                break;
            case RULE_STRING:
                {
                alt26=3;
                }
                break;
            case 41:
            case 42:
                {
                alt26=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }

            switch (alt26) {
                case 1 :
                    // InternalConstraints.g:1497:3: ( () ( (lv_value_1_0= RULE_INT ) ) )
                    {
                    // InternalConstraints.g:1497:3: ( () ( (lv_value_1_0= RULE_INT ) ) )
                    // InternalConstraints.g:1498:4: () ( (lv_value_1_0= RULE_INT ) )
                    {
                    // InternalConstraints.g:1498:4: ()
                    // InternalConstraints.g:1499:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getConstantAccess().getIntConstantAction_0_0(),
                    						current);
                    				

                    }

                    // InternalConstraints.g:1505:4: ( (lv_value_1_0= RULE_INT ) )
                    // InternalConstraints.g:1506:5: (lv_value_1_0= RULE_INT )
                    {
                    // InternalConstraints.g:1506:5: (lv_value_1_0= RULE_INT )
                    // InternalConstraints.g:1507:6: lv_value_1_0= RULE_INT
                    {
                    lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    						newLeafNode(lv_value_1_0, grammarAccess.getConstantAccess().getValueINTTerminalRuleCall_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getConstantRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_1_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:1525:3: ( () ( (lv_value_3_0= RULE_DOUBLE ) ) )
                    {
                    // InternalConstraints.g:1525:3: ( () ( (lv_value_3_0= RULE_DOUBLE ) ) )
                    // InternalConstraints.g:1526:4: () ( (lv_value_3_0= RULE_DOUBLE ) )
                    {
                    // InternalConstraints.g:1526:4: ()
                    // InternalConstraints.g:1527:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getConstantAccess().getRealConstantAction_1_0(),
                    						current);
                    				

                    }

                    // InternalConstraints.g:1533:4: ( (lv_value_3_0= RULE_DOUBLE ) )
                    // InternalConstraints.g:1534:5: (lv_value_3_0= RULE_DOUBLE )
                    {
                    // InternalConstraints.g:1534:5: (lv_value_3_0= RULE_DOUBLE )
                    // InternalConstraints.g:1535:6: lv_value_3_0= RULE_DOUBLE
                    {
                    lv_value_3_0=(Token)match(input,RULE_DOUBLE,FOLLOW_2); 

                    						newLeafNode(lv_value_3_0, grammarAccess.getConstantAccess().getValueDOUBLETerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getConstantRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_3_0,
                    							"ls5.pg646.constraints.Constraints.DOUBLE");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalConstraints.g:1553:3: ( () ( (lv_value_5_0= RULE_STRING ) ) )
                    {
                    // InternalConstraints.g:1553:3: ( () ( (lv_value_5_0= RULE_STRING ) ) )
                    // InternalConstraints.g:1554:4: () ( (lv_value_5_0= RULE_STRING ) )
                    {
                    // InternalConstraints.g:1554:4: ()
                    // InternalConstraints.g:1555:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getConstantAccess().getStringConstantAction_2_0(),
                    						current);
                    				

                    }

                    // InternalConstraints.g:1561:4: ( (lv_value_5_0= RULE_STRING ) )
                    // InternalConstraints.g:1562:5: (lv_value_5_0= RULE_STRING )
                    {
                    // InternalConstraints.g:1562:5: (lv_value_5_0= RULE_STRING )
                    // InternalConstraints.g:1563:6: lv_value_5_0= RULE_STRING
                    {
                    lv_value_5_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_5_0, grammarAccess.getConstantAccess().getValueSTRINGTerminalRuleCall_2_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getConstantRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_5_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalConstraints.g:1581:3: ( () ( ( (lv_value_7_1= 'true' | lv_value_7_2= 'false' ) ) ) )
                    {
                    // InternalConstraints.g:1581:3: ( () ( ( (lv_value_7_1= 'true' | lv_value_7_2= 'false' ) ) ) )
                    // InternalConstraints.g:1582:4: () ( ( (lv_value_7_1= 'true' | lv_value_7_2= 'false' ) ) )
                    {
                    // InternalConstraints.g:1582:4: ()
                    // InternalConstraints.g:1583:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getConstantAccess().getBooleanConstantAction_3_0(),
                    						current);
                    				

                    }

                    // InternalConstraints.g:1589:4: ( ( (lv_value_7_1= 'true' | lv_value_7_2= 'false' ) ) )
                    // InternalConstraints.g:1590:5: ( (lv_value_7_1= 'true' | lv_value_7_2= 'false' ) )
                    {
                    // InternalConstraints.g:1590:5: ( (lv_value_7_1= 'true' | lv_value_7_2= 'false' ) )
                    // InternalConstraints.g:1591:6: (lv_value_7_1= 'true' | lv_value_7_2= 'false' )
                    {
                    // InternalConstraints.g:1591:6: (lv_value_7_1= 'true' | lv_value_7_2= 'false' )
                    int alt25=2;
                    int LA25_0 = input.LA(1);

                    if ( (LA25_0==41) ) {
                        alt25=1;
                    }
                    else if ( (LA25_0==42) ) {
                        alt25=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 25, 0, input);

                        throw nvae;
                    }
                    switch (alt25) {
                        case 1 :
                            // InternalConstraints.g:1592:7: lv_value_7_1= 'true'
                            {
                            lv_value_7_1=(Token)match(input,41,FOLLOW_2); 

                            							newLeafNode(lv_value_7_1, grammarAccess.getConstantAccess().getValueTrueKeyword_3_1_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getConstantRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_7_1, null);
                            						

                            }
                            break;
                        case 2 :
                            // InternalConstraints.g:1603:7: lv_value_7_2= 'false'
                            {
                            lv_value_7_2=(Token)match(input,42,FOLLOW_2); 

                            							newLeafNode(lv_value_7_2, grammarAccess.getConstantAccess().getValueFalseKeyword_3_1_0_1());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getConstantRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_7_2, null);
                            						

                            }
                            break;

                    }


                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "ruleQuantifier"
    // InternalConstraints.g:1621:1: ruleQuantifier returns [Enumerator current=null] : ( (enumLiteral_0= 'forall' ) | (enumLiteral_1= 'all' ) | (enumLiteral_2= 'exists' ) | (enumLiteral_3= 'any' ) ) ;
    public final Enumerator ruleQuantifier() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalConstraints.g:1627:2: ( ( (enumLiteral_0= 'forall' ) | (enumLiteral_1= 'all' ) | (enumLiteral_2= 'exists' ) | (enumLiteral_3= 'any' ) ) )
            // InternalConstraints.g:1628:2: ( (enumLiteral_0= 'forall' ) | (enumLiteral_1= 'all' ) | (enumLiteral_2= 'exists' ) | (enumLiteral_3= 'any' ) )
            {
            // InternalConstraints.g:1628:2: ( (enumLiteral_0= 'forall' ) | (enumLiteral_1= 'all' ) | (enumLiteral_2= 'exists' ) | (enumLiteral_3= 'any' ) )
            int alt27=4;
            switch ( input.LA(1) ) {
            case 43:
                {
                alt27=1;
                }
                break;
            case 44:
                {
                alt27=2;
                }
                break;
            case 45:
                {
                alt27=3;
                }
                break;
            case 46:
                {
                alt27=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }

            switch (alt27) {
                case 1 :
                    // InternalConstraints.g:1629:3: (enumLiteral_0= 'forall' )
                    {
                    // InternalConstraints.g:1629:3: (enumLiteral_0= 'forall' )
                    // InternalConstraints.g:1630:4: enumLiteral_0= 'forall'
                    {
                    enumLiteral_0=(Token)match(input,43,FOLLOW_2); 

                    				current = grammarAccess.getQuantifierAccess().getALLEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getQuantifierAccess().getALLEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:1637:3: (enumLiteral_1= 'all' )
                    {
                    // InternalConstraints.g:1637:3: (enumLiteral_1= 'all' )
                    // InternalConstraints.g:1638:4: enumLiteral_1= 'all'
                    {
                    enumLiteral_1=(Token)match(input,44,FOLLOW_2); 

                    				current = grammarAccess.getQuantifierAccess().getALLEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getQuantifierAccess().getALLEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalConstraints.g:1645:3: (enumLiteral_2= 'exists' )
                    {
                    // InternalConstraints.g:1645:3: (enumLiteral_2= 'exists' )
                    // InternalConstraints.g:1646:4: enumLiteral_2= 'exists'
                    {
                    enumLiteral_2=(Token)match(input,45,FOLLOW_2); 

                    				current = grammarAccess.getQuantifierAccess().getANYEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getQuantifierAccess().getANYEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalConstraints.g:1653:3: (enumLiteral_3= 'any' )
                    {
                    // InternalConstraints.g:1653:3: (enumLiteral_3= 'any' )
                    // InternalConstraints.g:1654:4: enumLiteral_3= 'any'
                    {
                    enumLiteral_3=(Token)match(input,46,FOLLOW_2); 

                    				current = grammarAccess.getQuantifierAccess().getANYEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getQuantifierAccess().getANYEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuantifier"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000031002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000030002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00007F80010000F0L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000082000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000001000010L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000300002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000C00002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x000000000C000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000004000010L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000600000002L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000007800000002L});

}