/**
 * generated by Xtext 2.26.0.M2
 */
package ls5.pg646.constraints.constraints;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range Query</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ls5.pg646.constraints.constraints.RangeQuery#getName <em>Name</em>}</li>
 *   <li>{@link ls5.pg646.constraints.constraints.RangeQuery#isNot <em>Not</em>}</li>
 *   <li>{@link ls5.pg646.constraints.constraints.RangeQuery#getRange <em>Range</em>}</li>
 *   <li>{@link ls5.pg646.constraints.constraints.RangeQuery#getReference <em>Reference</em>}</li>
 * </ul>
 *
 * @see ls5.pg646.constraints.constraints.ConstraintsPackage#getRangeQuery()
 * @model
 * @generated
 */
public interface RangeQuery extends QueryExpression
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' reference.
   * @see #setName(DeclaredArgument)
   * @see ls5.pg646.constraints.constraints.ConstraintsPackage#getRangeQuery_Name()
   * @model
   * @generated
   */
  DeclaredArgument getName();

  /**
   * Sets the value of the '{@link ls5.pg646.constraints.constraints.RangeQuery#getName <em>Name</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' reference.
   * @see #getName()
   * @generated
   */
  void setName(DeclaredArgument value);

  /**
   * Returns the value of the '<em><b>Not</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Not</em>' attribute.
   * @see #setNot(boolean)
   * @see ls5.pg646.constraints.constraints.ConstraintsPackage#getRangeQuery_Not()
   * @model
   * @generated
   */
  boolean isNot();

  /**
   * Sets the value of the '{@link ls5.pg646.constraints.constraints.RangeQuery#isNot <em>Not</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Not</em>' attribute.
   * @see #isNot()
   * @generated
   */
  void setNot(boolean value);

  /**
   * Returns the value of the '<em><b>Range</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Range</em>' attribute.
   * @see #setRange(double)
   * @see ls5.pg646.constraints.constraints.ConstraintsPackage#getRangeQuery_Range()
   * @model
   * @generated
   */
  double getRange();

  /**
   * Sets the value of the '{@link ls5.pg646.constraints.constraints.RangeQuery#getRange <em>Range</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Range</em>' attribute.
   * @see #getRange()
   * @generated
   */
  void setRange(double value);

  /**
   * Returns the value of the '<em><b>Reference</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reference</em>' reference.
   * @see #setReference(Reference)
   * @see ls5.pg646.constraints.constraints.ConstraintsPackage#getRangeQuery_Reference()
   * @model
   * @generated
   */
  Reference getReference();

  /**
   * Sets the value of the '{@link ls5.pg646.constraints.constraints.RangeQuery#getReference <em>Reference</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reference</em>' reference.
   * @see #getReference()
   * @generated
   */
  void setReference(Reference value);

} // RangeQuery
