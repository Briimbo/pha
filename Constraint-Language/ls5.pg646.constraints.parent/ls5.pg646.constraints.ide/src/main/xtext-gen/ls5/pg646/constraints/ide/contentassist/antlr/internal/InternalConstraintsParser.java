package ls5.pg646.constraints.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import ls5.pg646.constraints.services.ConstraintsGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalConstraintsParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_DOUBLE", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'||'", "'OR'", "'&&'", "'AND'", "'=='", "'!='", "'true'", "'false'", "'forall'", "'all'", "'exists'", "'any'", "'read'", "'.'", "'as'", "';'", "'constraint'", "':'", "'|'", "'('", "')'", "'in'", "'group'", "'range'", "'of'", "'class'", "'is'", "'>'", "'<'", "'>='", "'<='", "'-'", "'!'", "'runtime'", "'not'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=7;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__37=37;
    public static final int RULE_DOUBLE=6;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalConstraintsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalConstraintsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalConstraintsParser.tokenNames; }
    public String getGrammarFileName() { return "InternalConstraints.g"; }


    	private ConstraintsGrammarAccess grammarAccess;

    	public void setGrammarAccess(ConstraintsGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleConstraintDocument"
    // InternalConstraints.g:53:1: entryRuleConstraintDocument : ruleConstraintDocument EOF ;
    public final void entryRuleConstraintDocument() throws RecognitionException {
        try {
            // InternalConstraints.g:54:1: ( ruleConstraintDocument EOF )
            // InternalConstraints.g:55:1: ruleConstraintDocument EOF
            {
             before(grammarAccess.getConstraintDocumentRule()); 
            pushFollow(FOLLOW_1);
            ruleConstraintDocument();

            state._fsp--;

             after(grammarAccess.getConstraintDocumentRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstraintDocument"


    // $ANTLR start "ruleConstraintDocument"
    // InternalConstraints.g:62:1: ruleConstraintDocument : ( ( rule__ConstraintDocument__Group__0 ) ) ;
    public final void ruleConstraintDocument() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:66:2: ( ( ( rule__ConstraintDocument__Group__0 ) ) )
            // InternalConstraints.g:67:2: ( ( rule__ConstraintDocument__Group__0 ) )
            {
            // InternalConstraints.g:67:2: ( ( rule__ConstraintDocument__Group__0 ) )
            // InternalConstraints.g:68:3: ( rule__ConstraintDocument__Group__0 )
            {
             before(grammarAccess.getConstraintDocumentAccess().getGroup()); 
            // InternalConstraints.g:69:3: ( rule__ConstraintDocument__Group__0 )
            // InternalConstraints.g:69:4: rule__ConstraintDocument__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ConstraintDocument__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConstraintDocumentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstraintDocument"


    // $ANTLR start "entryRuleReading"
    // InternalConstraints.g:78:1: entryRuleReading : ruleReading EOF ;
    public final void entryRuleReading() throws RecognitionException {
        try {
            // InternalConstraints.g:79:1: ( ruleReading EOF )
            // InternalConstraints.g:80:1: ruleReading EOF
            {
             before(grammarAccess.getReadingRule()); 
            pushFollow(FOLLOW_1);
            ruleReading();

            state._fsp--;

             after(grammarAccess.getReadingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReading"


    // $ANTLR start "ruleReading"
    // InternalConstraints.g:87:1: ruleReading : ( ( rule__Reading__Group__0 ) ) ;
    public final void ruleReading() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:91:2: ( ( ( rule__Reading__Group__0 ) ) )
            // InternalConstraints.g:92:2: ( ( rule__Reading__Group__0 ) )
            {
            // InternalConstraints.g:92:2: ( ( rule__Reading__Group__0 ) )
            // InternalConstraints.g:93:3: ( rule__Reading__Group__0 )
            {
             before(grammarAccess.getReadingAccess().getGroup()); 
            // InternalConstraints.g:94:3: ( rule__Reading__Group__0 )
            // InternalConstraints.g:94:4: rule__Reading__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Reading__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReadingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReading"


    // $ANTLR start "entryRuleConstraint"
    // InternalConstraints.g:103:1: entryRuleConstraint : ruleConstraint EOF ;
    public final void entryRuleConstraint() throws RecognitionException {
        try {
            // InternalConstraints.g:104:1: ( ruleConstraint EOF )
            // InternalConstraints.g:105:1: ruleConstraint EOF
            {
             before(grammarAccess.getConstraintRule()); 
            pushFollow(FOLLOW_1);
            ruleConstraint();

            state._fsp--;

             after(grammarAccess.getConstraintRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstraint"


    // $ANTLR start "ruleConstraint"
    // InternalConstraints.g:112:1: ruleConstraint : ( ( rule__Constraint__Group__0 ) ) ;
    public final void ruleConstraint() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:116:2: ( ( ( rule__Constraint__Group__0 ) ) )
            // InternalConstraints.g:117:2: ( ( rule__Constraint__Group__0 ) )
            {
            // InternalConstraints.g:117:2: ( ( rule__Constraint__Group__0 ) )
            // InternalConstraints.g:118:3: ( rule__Constraint__Group__0 )
            {
             before(grammarAccess.getConstraintAccess().getGroup()); 
            // InternalConstraints.g:119:3: ( rule__Constraint__Group__0 )
            // InternalConstraints.g:119:4: rule__Constraint__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstraint"


    // $ANTLR start "entryRuleDeclaredArgument"
    // InternalConstraints.g:128:1: entryRuleDeclaredArgument : ruleDeclaredArgument EOF ;
    public final void entryRuleDeclaredArgument() throws RecognitionException {
        try {
            // InternalConstraints.g:129:1: ( ruleDeclaredArgument EOF )
            // InternalConstraints.g:130:1: ruleDeclaredArgument EOF
            {
             before(grammarAccess.getDeclaredArgumentRule()); 
            pushFollow(FOLLOW_1);
            ruleDeclaredArgument();

            state._fsp--;

             after(grammarAccess.getDeclaredArgumentRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDeclaredArgument"


    // $ANTLR start "ruleDeclaredArgument"
    // InternalConstraints.g:137:1: ruleDeclaredArgument : ( ( rule__DeclaredArgument__NameAssignment ) ) ;
    public final void ruleDeclaredArgument() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:141:2: ( ( ( rule__DeclaredArgument__NameAssignment ) ) )
            // InternalConstraints.g:142:2: ( ( rule__DeclaredArgument__NameAssignment ) )
            {
            // InternalConstraints.g:142:2: ( ( rule__DeclaredArgument__NameAssignment ) )
            // InternalConstraints.g:143:3: ( rule__DeclaredArgument__NameAssignment )
            {
             before(grammarAccess.getDeclaredArgumentAccess().getNameAssignment()); 
            // InternalConstraints.g:144:3: ( rule__DeclaredArgument__NameAssignment )
            // InternalConstraints.g:144:4: rule__DeclaredArgument__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__DeclaredArgument__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getDeclaredArgumentAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDeclaredArgument"


    // $ANTLR start "entryRuleExpression"
    // InternalConstraints.g:153:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // InternalConstraints.g:154:1: ( ruleExpression EOF )
            // InternalConstraints.g:155:1: ruleExpression EOF
            {
             before(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalConstraints.g:162:1: ruleExpression : ( ( rule__Expression__Alternatives ) ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:166:2: ( ( ( rule__Expression__Alternatives ) ) )
            // InternalConstraints.g:167:2: ( ( rule__Expression__Alternatives ) )
            {
            // InternalConstraints.g:167:2: ( ( rule__Expression__Alternatives ) )
            // InternalConstraints.g:168:3: ( rule__Expression__Alternatives )
            {
             before(grammarAccess.getExpressionAccess().getAlternatives()); 
            // InternalConstraints.g:169:3: ( rule__Expression__Alternatives )
            // InternalConstraints.g:169:4: rule__Expression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Expression__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getExpressionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleQuantifiedExpression"
    // InternalConstraints.g:178:1: entryRuleQuantifiedExpression : ruleQuantifiedExpression EOF ;
    public final void entryRuleQuantifiedExpression() throws RecognitionException {
        try {
            // InternalConstraints.g:179:1: ( ruleQuantifiedExpression EOF )
            // InternalConstraints.g:180:1: ruleQuantifiedExpression EOF
            {
             before(grammarAccess.getQuantifiedExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleQuantifiedExpression();

            state._fsp--;

             after(grammarAccess.getQuantifiedExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuantifiedExpression"


    // $ANTLR start "ruleQuantifiedExpression"
    // InternalConstraints.g:187:1: ruleQuantifiedExpression : ( ( rule__QuantifiedExpression__Group__0 ) ) ;
    public final void ruleQuantifiedExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:191:2: ( ( ( rule__QuantifiedExpression__Group__0 ) ) )
            // InternalConstraints.g:192:2: ( ( rule__QuantifiedExpression__Group__0 ) )
            {
            // InternalConstraints.g:192:2: ( ( rule__QuantifiedExpression__Group__0 ) )
            // InternalConstraints.g:193:3: ( rule__QuantifiedExpression__Group__0 )
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getGroup()); 
            // InternalConstraints.g:194:3: ( rule__QuantifiedExpression__Group__0 )
            // InternalConstraints.g:194:4: rule__QuantifiedExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuantifiedExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuantifiedExpression"


    // $ANTLR start "entryRuleQueryExpression"
    // InternalConstraints.g:203:1: entryRuleQueryExpression : ruleQueryExpression EOF ;
    public final void entryRuleQueryExpression() throws RecognitionException {
        try {
            // InternalConstraints.g:204:1: ( ruleQueryExpression EOF )
            // InternalConstraints.g:205:1: ruleQueryExpression EOF
            {
             before(grammarAccess.getQueryExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleQueryExpression();

            state._fsp--;

             after(grammarAccess.getQueryExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQueryExpression"


    // $ANTLR start "ruleQueryExpression"
    // InternalConstraints.g:212:1: ruleQueryExpression : ( ruleQueryUnion ) ;
    public final void ruleQueryExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:216:2: ( ( ruleQueryUnion ) )
            // InternalConstraints.g:217:2: ( ruleQueryUnion )
            {
            // InternalConstraints.g:217:2: ( ruleQueryUnion )
            // InternalConstraints.g:218:3: ruleQueryUnion
            {
             before(grammarAccess.getQueryExpressionAccess().getQueryUnionParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleQueryUnion();

            state._fsp--;

             after(grammarAccess.getQueryExpressionAccess().getQueryUnionParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQueryExpression"


    // $ANTLR start "entryRuleQueryUnion"
    // InternalConstraints.g:228:1: entryRuleQueryUnion : ruleQueryUnion EOF ;
    public final void entryRuleQueryUnion() throws RecognitionException {
        try {
            // InternalConstraints.g:229:1: ( ruleQueryUnion EOF )
            // InternalConstraints.g:230:1: ruleQueryUnion EOF
            {
             before(grammarAccess.getQueryUnionRule()); 
            pushFollow(FOLLOW_1);
            ruleQueryUnion();

            state._fsp--;

             after(grammarAccess.getQueryUnionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQueryUnion"


    // $ANTLR start "ruleQueryUnion"
    // InternalConstraints.g:237:1: ruleQueryUnion : ( ( rule__QueryUnion__Group__0 ) ) ;
    public final void ruleQueryUnion() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:241:2: ( ( ( rule__QueryUnion__Group__0 ) ) )
            // InternalConstraints.g:242:2: ( ( rule__QueryUnion__Group__0 ) )
            {
            // InternalConstraints.g:242:2: ( ( rule__QueryUnion__Group__0 ) )
            // InternalConstraints.g:243:3: ( rule__QueryUnion__Group__0 )
            {
             before(grammarAccess.getQueryUnionAccess().getGroup()); 
            // InternalConstraints.g:244:3: ( rule__QueryUnion__Group__0 )
            // InternalConstraints.g:244:4: rule__QueryUnion__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QueryUnion__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQueryUnionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQueryUnion"


    // $ANTLR start "entryRuleQueryIntersection"
    // InternalConstraints.g:253:1: entryRuleQueryIntersection : ruleQueryIntersection EOF ;
    public final void entryRuleQueryIntersection() throws RecognitionException {
        try {
            // InternalConstraints.g:254:1: ( ruleQueryIntersection EOF )
            // InternalConstraints.g:255:1: ruleQueryIntersection EOF
            {
             before(grammarAccess.getQueryIntersectionRule()); 
            pushFollow(FOLLOW_1);
            ruleQueryIntersection();

            state._fsp--;

             after(grammarAccess.getQueryIntersectionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQueryIntersection"


    // $ANTLR start "ruleQueryIntersection"
    // InternalConstraints.g:262:1: ruleQueryIntersection : ( ( rule__QueryIntersection__Group__0 ) ) ;
    public final void ruleQueryIntersection() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:266:2: ( ( ( rule__QueryIntersection__Group__0 ) ) )
            // InternalConstraints.g:267:2: ( ( rule__QueryIntersection__Group__0 ) )
            {
            // InternalConstraints.g:267:2: ( ( rule__QueryIntersection__Group__0 ) )
            // InternalConstraints.g:268:3: ( rule__QueryIntersection__Group__0 )
            {
             before(grammarAccess.getQueryIntersectionAccess().getGroup()); 
            // InternalConstraints.g:269:3: ( rule__QueryIntersection__Group__0 )
            // InternalConstraints.g:269:4: rule__QueryIntersection__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QueryIntersection__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQueryIntersectionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQueryIntersection"


    // $ANTLR start "entryRuleAtomicQueryExpression"
    // InternalConstraints.g:278:1: entryRuleAtomicQueryExpression : ruleAtomicQueryExpression EOF ;
    public final void entryRuleAtomicQueryExpression() throws RecognitionException {
        try {
            // InternalConstraints.g:279:1: ( ruleAtomicQueryExpression EOF )
            // InternalConstraints.g:280:1: ruleAtomicQueryExpression EOF
            {
             before(grammarAccess.getAtomicQueryExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleAtomicQueryExpression();

            state._fsp--;

             after(grammarAccess.getAtomicQueryExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAtomicQueryExpression"


    // $ANTLR start "ruleAtomicQueryExpression"
    // InternalConstraints.g:287:1: ruleAtomicQueryExpression : ( ( rule__AtomicQueryExpression__Alternatives ) ) ;
    public final void ruleAtomicQueryExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:291:2: ( ( ( rule__AtomicQueryExpression__Alternatives ) ) )
            // InternalConstraints.g:292:2: ( ( rule__AtomicQueryExpression__Alternatives ) )
            {
            // InternalConstraints.g:292:2: ( ( rule__AtomicQueryExpression__Alternatives ) )
            // InternalConstraints.g:293:3: ( rule__AtomicQueryExpression__Alternatives )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getAlternatives()); 
            // InternalConstraints.g:294:3: ( rule__AtomicQueryExpression__Alternatives )
            // InternalConstraints.g:294:4: rule__AtomicQueryExpression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtomicQueryExpression"


    // $ANTLR start "entryRuleLogicalOr"
    // InternalConstraints.g:303:1: entryRuleLogicalOr : ruleLogicalOr EOF ;
    public final void entryRuleLogicalOr() throws RecognitionException {
        try {
            // InternalConstraints.g:304:1: ( ruleLogicalOr EOF )
            // InternalConstraints.g:305:1: ruleLogicalOr EOF
            {
             before(grammarAccess.getLogicalOrRule()); 
            pushFollow(FOLLOW_1);
            ruleLogicalOr();

            state._fsp--;

             after(grammarAccess.getLogicalOrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLogicalOr"


    // $ANTLR start "ruleLogicalOr"
    // InternalConstraints.g:312:1: ruleLogicalOr : ( ( rule__LogicalOr__Group__0 ) ) ;
    public final void ruleLogicalOr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:316:2: ( ( ( rule__LogicalOr__Group__0 ) ) )
            // InternalConstraints.g:317:2: ( ( rule__LogicalOr__Group__0 ) )
            {
            // InternalConstraints.g:317:2: ( ( rule__LogicalOr__Group__0 ) )
            // InternalConstraints.g:318:3: ( rule__LogicalOr__Group__0 )
            {
             before(grammarAccess.getLogicalOrAccess().getGroup()); 
            // InternalConstraints.g:319:3: ( rule__LogicalOr__Group__0 )
            // InternalConstraints.g:319:4: rule__LogicalOr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LogicalOr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLogicalOrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLogicalOr"


    // $ANTLR start "entryRuleLogicalAnd"
    // InternalConstraints.g:328:1: entryRuleLogicalAnd : ruleLogicalAnd EOF ;
    public final void entryRuleLogicalAnd() throws RecognitionException {
        try {
            // InternalConstraints.g:329:1: ( ruleLogicalAnd EOF )
            // InternalConstraints.g:330:1: ruleLogicalAnd EOF
            {
             before(grammarAccess.getLogicalAndRule()); 
            pushFollow(FOLLOW_1);
            ruleLogicalAnd();

            state._fsp--;

             after(grammarAccess.getLogicalAndRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLogicalAnd"


    // $ANTLR start "ruleLogicalAnd"
    // InternalConstraints.g:337:1: ruleLogicalAnd : ( ( rule__LogicalAnd__Group__0 ) ) ;
    public final void ruleLogicalAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:341:2: ( ( ( rule__LogicalAnd__Group__0 ) ) )
            // InternalConstraints.g:342:2: ( ( rule__LogicalAnd__Group__0 ) )
            {
            // InternalConstraints.g:342:2: ( ( rule__LogicalAnd__Group__0 ) )
            // InternalConstraints.g:343:3: ( rule__LogicalAnd__Group__0 )
            {
             before(grammarAccess.getLogicalAndAccess().getGroup()); 
            // InternalConstraints.g:344:3: ( rule__LogicalAnd__Group__0 )
            // InternalConstraints.g:344:4: rule__LogicalAnd__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LogicalAnd__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLogicalAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLogicalAnd"


    // $ANTLR start "entryRuleEquality"
    // InternalConstraints.g:353:1: entryRuleEquality : ruleEquality EOF ;
    public final void entryRuleEquality() throws RecognitionException {
        try {
            // InternalConstraints.g:354:1: ( ruleEquality EOF )
            // InternalConstraints.g:355:1: ruleEquality EOF
            {
             before(grammarAccess.getEqualityRule()); 
            pushFollow(FOLLOW_1);
            ruleEquality();

            state._fsp--;

             after(grammarAccess.getEqualityRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEquality"


    // $ANTLR start "ruleEquality"
    // InternalConstraints.g:362:1: ruleEquality : ( ( rule__Equality__Group__0 ) ) ;
    public final void ruleEquality() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:366:2: ( ( ( rule__Equality__Group__0 ) ) )
            // InternalConstraints.g:367:2: ( ( rule__Equality__Group__0 ) )
            {
            // InternalConstraints.g:367:2: ( ( rule__Equality__Group__0 ) )
            // InternalConstraints.g:368:3: ( rule__Equality__Group__0 )
            {
             before(grammarAccess.getEqualityAccess().getGroup()); 
            // InternalConstraints.g:369:3: ( rule__Equality__Group__0 )
            // InternalConstraints.g:369:4: rule__Equality__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Equality__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEqualityAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEquality"


    // $ANTLR start "entryRuleRelational"
    // InternalConstraints.g:378:1: entryRuleRelational : ruleRelational EOF ;
    public final void entryRuleRelational() throws RecognitionException {
        try {
            // InternalConstraints.g:379:1: ( ruleRelational EOF )
            // InternalConstraints.g:380:1: ruleRelational EOF
            {
             before(grammarAccess.getRelationalRule()); 
            pushFollow(FOLLOW_1);
            ruleRelational();

            state._fsp--;

             after(grammarAccess.getRelationalRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRelational"


    // $ANTLR start "ruleRelational"
    // InternalConstraints.g:387:1: ruleRelational : ( ( rule__Relational__Group__0 ) ) ;
    public final void ruleRelational() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:391:2: ( ( ( rule__Relational__Group__0 ) ) )
            // InternalConstraints.g:392:2: ( ( rule__Relational__Group__0 ) )
            {
            // InternalConstraints.g:392:2: ( ( rule__Relational__Group__0 ) )
            // InternalConstraints.g:393:3: ( rule__Relational__Group__0 )
            {
             before(grammarAccess.getRelationalAccess().getGroup()); 
            // InternalConstraints.g:394:3: ( rule__Relational__Group__0 )
            // InternalConstraints.g:394:4: rule__Relational__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Relational__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRelationalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRelational"


    // $ANTLR start "entryRuleUnaryPrefix"
    // InternalConstraints.g:403:1: entryRuleUnaryPrefix : ruleUnaryPrefix EOF ;
    public final void entryRuleUnaryPrefix() throws RecognitionException {
        try {
            // InternalConstraints.g:404:1: ( ruleUnaryPrefix EOF )
            // InternalConstraints.g:405:1: ruleUnaryPrefix EOF
            {
             before(grammarAccess.getUnaryPrefixRule()); 
            pushFollow(FOLLOW_1);
            ruleUnaryPrefix();

            state._fsp--;

             after(grammarAccess.getUnaryPrefixRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnaryPrefix"


    // $ANTLR start "ruleUnaryPrefix"
    // InternalConstraints.g:412:1: ruleUnaryPrefix : ( ( rule__UnaryPrefix__Alternatives ) ) ;
    public final void ruleUnaryPrefix() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:416:2: ( ( ( rule__UnaryPrefix__Alternatives ) ) )
            // InternalConstraints.g:417:2: ( ( rule__UnaryPrefix__Alternatives ) )
            {
            // InternalConstraints.g:417:2: ( ( rule__UnaryPrefix__Alternatives ) )
            // InternalConstraints.g:418:3: ( rule__UnaryPrefix__Alternatives )
            {
             before(grammarAccess.getUnaryPrefixAccess().getAlternatives()); 
            // InternalConstraints.g:419:3: ( rule__UnaryPrefix__Alternatives )
            // InternalConstraints.g:419:4: rule__UnaryPrefix__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__UnaryPrefix__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUnaryPrefixAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnaryPrefix"


    // $ANTLR start "entryRulePrimary"
    // InternalConstraints.g:428:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalConstraints.g:429:1: ( rulePrimary EOF )
            // InternalConstraints.g:430:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalConstraints.g:437:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:441:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalConstraints.g:442:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalConstraints.g:442:2: ( ( rule__Primary__Alternatives ) )
            // InternalConstraints.g:443:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalConstraints.g:444:3: ( rule__Primary__Alternatives )
            // InternalConstraints.g:444:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleConstant"
    // InternalConstraints.g:453:1: entryRuleConstant : ruleConstant EOF ;
    public final void entryRuleConstant() throws RecognitionException {
        try {
            // InternalConstraints.g:454:1: ( ruleConstant EOF )
            // InternalConstraints.g:455:1: ruleConstant EOF
            {
             before(grammarAccess.getConstantRule()); 
            pushFollow(FOLLOW_1);
            ruleConstant();

            state._fsp--;

             after(grammarAccess.getConstantRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // InternalConstraints.g:462:1: ruleConstant : ( ( rule__Constant__Alternatives ) ) ;
    public final void ruleConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:466:2: ( ( ( rule__Constant__Alternatives ) ) )
            // InternalConstraints.g:467:2: ( ( rule__Constant__Alternatives ) )
            {
            // InternalConstraints.g:467:2: ( ( rule__Constant__Alternatives ) )
            // InternalConstraints.g:468:3: ( rule__Constant__Alternatives )
            {
             before(grammarAccess.getConstantAccess().getAlternatives()); 
            // InternalConstraints.g:469:3: ( rule__Constant__Alternatives )
            // InternalConstraints.g:469:4: rule__Constant__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Constant__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getConstantAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "ruleQuantifier"
    // InternalConstraints.g:478:1: ruleQuantifier : ( ( rule__Quantifier__Alternatives ) ) ;
    public final void ruleQuantifier() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:482:1: ( ( ( rule__Quantifier__Alternatives ) ) )
            // InternalConstraints.g:483:2: ( ( rule__Quantifier__Alternatives ) )
            {
            // InternalConstraints.g:483:2: ( ( rule__Quantifier__Alternatives ) )
            // InternalConstraints.g:484:3: ( rule__Quantifier__Alternatives )
            {
             before(grammarAccess.getQuantifierAccess().getAlternatives()); 
            // InternalConstraints.g:485:3: ( rule__Quantifier__Alternatives )
            // InternalConstraints.g:485:4: rule__Quantifier__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Quantifier__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getQuantifierAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuantifier"


    // $ANTLR start "rule__Expression__Alternatives"
    // InternalConstraints.g:493:1: rule__Expression__Alternatives : ( ( ruleQuantifiedExpression ) | ( ruleLogicalOr ) );
    public final void rule__Expression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:497:1: ( ( ruleQuantifiedExpression ) | ( ruleLogicalOr ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( ((LA1_0>=20 && LA1_0<=23)) ) {
                alt1=1;
            }
            else if ( ((LA1_0>=RULE_ID && LA1_0<=RULE_INT)||(LA1_0>=18 && LA1_0<=19)||LA1_0==31||(LA1_0>=43 && LA1_0<=44)) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalConstraints.g:498:2: ( ruleQuantifiedExpression )
                    {
                    // InternalConstraints.g:498:2: ( ruleQuantifiedExpression )
                    // InternalConstraints.g:499:3: ruleQuantifiedExpression
                    {
                     before(grammarAccess.getExpressionAccess().getQuantifiedExpressionParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleQuantifiedExpression();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getQuantifiedExpressionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:504:2: ( ruleLogicalOr )
                    {
                    // InternalConstraints.g:504:2: ( ruleLogicalOr )
                    // InternalConstraints.g:505:3: ruleLogicalOr
                    {
                     before(grammarAccess.getExpressionAccess().getLogicalOrParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleLogicalOr();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getLogicalOrParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Alternatives"


    // $ANTLR start "rule__QueryUnion__OpAlternatives_1_1_0"
    // InternalConstraints.g:514:1: rule__QueryUnion__OpAlternatives_1_1_0 : ( ( '||' ) | ( 'OR' ) );
    public final void rule__QueryUnion__OpAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:518:1: ( ( '||' ) | ( 'OR' ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                alt2=1;
            }
            else if ( (LA2_0==13) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalConstraints.g:519:2: ( '||' )
                    {
                    // InternalConstraints.g:519:2: ( '||' )
                    // InternalConstraints.g:520:3: '||'
                    {
                     before(grammarAccess.getQueryUnionAccess().getOpVerticalLineVerticalLineKeyword_1_1_0_0()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getQueryUnionAccess().getOpVerticalLineVerticalLineKeyword_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:525:2: ( 'OR' )
                    {
                    // InternalConstraints.g:525:2: ( 'OR' )
                    // InternalConstraints.g:526:3: 'OR'
                    {
                     before(grammarAccess.getQueryUnionAccess().getOpORKeyword_1_1_0_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getQueryUnionAccess().getOpORKeyword_1_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__OpAlternatives_1_1_0"


    // $ANTLR start "rule__QueryIntersection__OpAlternatives_1_1_0"
    // InternalConstraints.g:535:1: rule__QueryIntersection__OpAlternatives_1_1_0 : ( ( '&&' ) | ( 'AND' ) );
    public final void rule__QueryIntersection__OpAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:539:1: ( ( '&&' ) | ( 'AND' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14) ) {
                alt3=1;
            }
            else if ( (LA3_0==15) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalConstraints.g:540:2: ( '&&' )
                    {
                    // InternalConstraints.g:540:2: ( '&&' )
                    // InternalConstraints.g:541:3: '&&'
                    {
                     before(grammarAccess.getQueryIntersectionAccess().getOpAmpersandAmpersandKeyword_1_1_0_0()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getQueryIntersectionAccess().getOpAmpersandAmpersandKeyword_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:546:2: ( 'AND' )
                    {
                    // InternalConstraints.g:546:2: ( 'AND' )
                    // InternalConstraints.g:547:3: 'AND'
                    {
                     before(grammarAccess.getQueryIntersectionAccess().getOpANDKeyword_1_1_0_1()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getQueryIntersectionAccess().getOpANDKeyword_1_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__OpAlternatives_1_1_0"


    // $ANTLR start "rule__AtomicQueryExpression__Alternatives"
    // InternalConstraints.g:556:1: rule__AtomicQueryExpression__Alternatives : ( ( ( rule__AtomicQueryExpression__Group_0__0 ) ) | ( ( rule__AtomicQueryExpression__Group_1__0 ) ) | ( ( rule__AtomicQueryExpression__Group_2__0 ) ) | ( ( rule__AtomicQueryExpression__Group_3__0 ) ) | ( ( rule__AtomicQueryExpression__Group_4__0 ) ) );
    public final void rule__AtomicQueryExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:560:1: ( ( ( rule__AtomicQueryExpression__Group_0__0 ) ) | ( ( rule__AtomicQueryExpression__Group_1__0 ) ) | ( ( rule__AtomicQueryExpression__Group_2__0 ) ) | ( ( rule__AtomicQueryExpression__Group_3__0 ) ) | ( ( rule__AtomicQueryExpression__Group_4__0 ) ) )
            int alt4=5;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==31) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_ID) ) {
                switch ( input.LA(2) ) {
                case 46:
                    {
                    int LA4_3 = input.LA(3);

                    if ( (LA4_3==33) ) {
                        switch ( input.LA(4) ) {
                        case 34:
                            {
                            alt4=2;
                            }
                            break;
                        case 35:
                            {
                            alt4=3;
                            }
                            break;
                        case 37:
                            {
                            alt4=4;
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 4, input);

                            throw nvae;
                        }

                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 3, input);

                        throw nvae;
                    }
                    }
                    break;
                case 33:
                    {
                    switch ( input.LA(3) ) {
                    case 34:
                        {
                        alt4=2;
                        }
                        break;
                    case 35:
                        {
                        alt4=3;
                        }
                        break;
                    case 37:
                        {
                        alt4=4;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 4, input);

                        throw nvae;
                    }

                    }
                    break;
                case 38:
                    {
                    alt4=5;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 2, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalConstraints.g:561:2: ( ( rule__AtomicQueryExpression__Group_0__0 ) )
                    {
                    // InternalConstraints.g:561:2: ( ( rule__AtomicQueryExpression__Group_0__0 ) )
                    // InternalConstraints.g:562:3: ( rule__AtomicQueryExpression__Group_0__0 )
                    {
                     before(grammarAccess.getAtomicQueryExpressionAccess().getGroup_0()); 
                    // InternalConstraints.g:563:3: ( rule__AtomicQueryExpression__Group_0__0 )
                    // InternalConstraints.g:563:4: rule__AtomicQueryExpression__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtomicQueryExpression__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicQueryExpressionAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:567:2: ( ( rule__AtomicQueryExpression__Group_1__0 ) )
                    {
                    // InternalConstraints.g:567:2: ( ( rule__AtomicQueryExpression__Group_1__0 ) )
                    // InternalConstraints.g:568:3: ( rule__AtomicQueryExpression__Group_1__0 )
                    {
                     before(grammarAccess.getAtomicQueryExpressionAccess().getGroup_1()); 
                    // InternalConstraints.g:569:3: ( rule__AtomicQueryExpression__Group_1__0 )
                    // InternalConstraints.g:569:4: rule__AtomicQueryExpression__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtomicQueryExpression__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicQueryExpressionAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalConstraints.g:573:2: ( ( rule__AtomicQueryExpression__Group_2__0 ) )
                    {
                    // InternalConstraints.g:573:2: ( ( rule__AtomicQueryExpression__Group_2__0 ) )
                    // InternalConstraints.g:574:3: ( rule__AtomicQueryExpression__Group_2__0 )
                    {
                     before(grammarAccess.getAtomicQueryExpressionAccess().getGroup_2()); 
                    // InternalConstraints.g:575:3: ( rule__AtomicQueryExpression__Group_2__0 )
                    // InternalConstraints.g:575:4: rule__AtomicQueryExpression__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtomicQueryExpression__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicQueryExpressionAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalConstraints.g:579:2: ( ( rule__AtomicQueryExpression__Group_3__0 ) )
                    {
                    // InternalConstraints.g:579:2: ( ( rule__AtomicQueryExpression__Group_3__0 ) )
                    // InternalConstraints.g:580:3: ( rule__AtomicQueryExpression__Group_3__0 )
                    {
                     before(grammarAccess.getAtomicQueryExpressionAccess().getGroup_3()); 
                    // InternalConstraints.g:581:3: ( rule__AtomicQueryExpression__Group_3__0 )
                    // InternalConstraints.g:581:4: rule__AtomicQueryExpression__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtomicQueryExpression__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicQueryExpressionAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalConstraints.g:585:2: ( ( rule__AtomicQueryExpression__Group_4__0 ) )
                    {
                    // InternalConstraints.g:585:2: ( ( rule__AtomicQueryExpression__Group_4__0 ) )
                    // InternalConstraints.g:586:3: ( rule__AtomicQueryExpression__Group_4__0 )
                    {
                     before(grammarAccess.getAtomicQueryExpressionAccess().getGroup_4()); 
                    // InternalConstraints.g:587:3: ( rule__AtomicQueryExpression__Group_4__0 )
                    // InternalConstraints.g:587:4: rule__AtomicQueryExpression__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtomicQueryExpression__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicQueryExpressionAccess().getGroup_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Alternatives"


    // $ANTLR start "rule__LogicalOr__OpAlternatives_1_1_0"
    // InternalConstraints.g:595:1: rule__LogicalOr__OpAlternatives_1_1_0 : ( ( '||' ) | ( 'OR' ) );
    public final void rule__LogicalOr__OpAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:599:1: ( ( '||' ) | ( 'OR' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==12) ) {
                alt5=1;
            }
            else if ( (LA5_0==13) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalConstraints.g:600:2: ( '||' )
                    {
                    // InternalConstraints.g:600:2: ( '||' )
                    // InternalConstraints.g:601:3: '||'
                    {
                     before(grammarAccess.getLogicalOrAccess().getOpVerticalLineVerticalLineKeyword_1_1_0_0()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getLogicalOrAccess().getOpVerticalLineVerticalLineKeyword_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:606:2: ( 'OR' )
                    {
                    // InternalConstraints.g:606:2: ( 'OR' )
                    // InternalConstraints.g:607:3: 'OR'
                    {
                     before(grammarAccess.getLogicalOrAccess().getOpORKeyword_1_1_0_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getLogicalOrAccess().getOpORKeyword_1_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__OpAlternatives_1_1_0"


    // $ANTLR start "rule__LogicalAnd__OpAlternatives_1_1_0"
    // InternalConstraints.g:616:1: rule__LogicalAnd__OpAlternatives_1_1_0 : ( ( '&&' ) | ( 'AND' ) );
    public final void rule__LogicalAnd__OpAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:620:1: ( ( '&&' ) | ( 'AND' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==14) ) {
                alt6=1;
            }
            else if ( (LA6_0==15) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalConstraints.g:621:2: ( '&&' )
                    {
                    // InternalConstraints.g:621:2: ( '&&' )
                    // InternalConstraints.g:622:3: '&&'
                    {
                     before(grammarAccess.getLogicalAndAccess().getOpAmpersandAmpersandKeyword_1_1_0_0()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getLogicalAndAccess().getOpAmpersandAmpersandKeyword_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:627:2: ( 'AND' )
                    {
                    // InternalConstraints.g:627:2: ( 'AND' )
                    // InternalConstraints.g:628:3: 'AND'
                    {
                     before(grammarAccess.getLogicalAndAccess().getOpANDKeyword_1_1_0_1()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getLogicalAndAccess().getOpANDKeyword_1_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__OpAlternatives_1_1_0"


    // $ANTLR start "rule__Equality__OpAlternatives_1_0_1_0"
    // InternalConstraints.g:637:1: rule__Equality__OpAlternatives_1_0_1_0 : ( ( '==' ) | ( '!=' ) );
    public final void rule__Equality__OpAlternatives_1_0_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:641:1: ( ( '==' ) | ( '!=' ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==16) ) {
                alt7=1;
            }
            else if ( (LA7_0==17) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalConstraints.g:642:2: ( '==' )
                    {
                    // InternalConstraints.g:642:2: ( '==' )
                    // InternalConstraints.g:643:3: '=='
                    {
                     before(grammarAccess.getEqualityAccess().getOpEqualsSignEqualsSignKeyword_1_0_1_0_0()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getEqualityAccess().getOpEqualsSignEqualsSignKeyword_1_0_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:648:2: ( '!=' )
                    {
                    // InternalConstraints.g:648:2: ( '!=' )
                    // InternalConstraints.g:649:3: '!='
                    {
                     before(grammarAccess.getEqualityAccess().getOpExclamationMarkEqualsSignKeyword_1_0_1_0_1()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getEqualityAccess().getOpExclamationMarkEqualsSignKeyword_1_0_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__OpAlternatives_1_0_1_0"


    // $ANTLR start "rule__Relational__Alternatives_1_0"
    // InternalConstraints.g:658:1: rule__Relational__Alternatives_1_0 : ( ( ( rule__Relational__Group_1_0_0__0 ) ) | ( ( rule__Relational__Group_1_0_1__0 ) ) | ( ( rule__Relational__Group_1_0_2__0 ) ) | ( ( rule__Relational__Group_1_0_3__0 ) ) );
    public final void rule__Relational__Alternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:662:1: ( ( ( rule__Relational__Group_1_0_0__0 ) ) | ( ( rule__Relational__Group_1_0_1__0 ) ) | ( ( rule__Relational__Group_1_0_2__0 ) ) | ( ( rule__Relational__Group_1_0_3__0 ) ) )
            int alt8=4;
            switch ( input.LA(1) ) {
            case 39:
                {
                alt8=1;
                }
                break;
            case 40:
                {
                alt8=2;
                }
                break;
            case 41:
                {
                alt8=3;
                }
                break;
            case 42:
                {
                alt8=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalConstraints.g:663:2: ( ( rule__Relational__Group_1_0_0__0 ) )
                    {
                    // InternalConstraints.g:663:2: ( ( rule__Relational__Group_1_0_0__0 ) )
                    // InternalConstraints.g:664:3: ( rule__Relational__Group_1_0_0__0 )
                    {
                     before(grammarAccess.getRelationalAccess().getGroup_1_0_0()); 
                    // InternalConstraints.g:665:3: ( rule__Relational__Group_1_0_0__0 )
                    // InternalConstraints.g:665:4: rule__Relational__Group_1_0_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Relational__Group_1_0_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelationalAccess().getGroup_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:669:2: ( ( rule__Relational__Group_1_0_1__0 ) )
                    {
                    // InternalConstraints.g:669:2: ( ( rule__Relational__Group_1_0_1__0 ) )
                    // InternalConstraints.g:670:3: ( rule__Relational__Group_1_0_1__0 )
                    {
                     before(grammarAccess.getRelationalAccess().getGroup_1_0_1()); 
                    // InternalConstraints.g:671:3: ( rule__Relational__Group_1_0_1__0 )
                    // InternalConstraints.g:671:4: rule__Relational__Group_1_0_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Relational__Group_1_0_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelationalAccess().getGroup_1_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalConstraints.g:675:2: ( ( rule__Relational__Group_1_0_2__0 ) )
                    {
                    // InternalConstraints.g:675:2: ( ( rule__Relational__Group_1_0_2__0 ) )
                    // InternalConstraints.g:676:3: ( rule__Relational__Group_1_0_2__0 )
                    {
                     before(grammarAccess.getRelationalAccess().getGroup_1_0_2()); 
                    // InternalConstraints.g:677:3: ( rule__Relational__Group_1_0_2__0 )
                    // InternalConstraints.g:677:4: rule__Relational__Group_1_0_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Relational__Group_1_0_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelationalAccess().getGroup_1_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalConstraints.g:681:2: ( ( rule__Relational__Group_1_0_3__0 ) )
                    {
                    // InternalConstraints.g:681:2: ( ( rule__Relational__Group_1_0_3__0 ) )
                    // InternalConstraints.g:682:3: ( rule__Relational__Group_1_0_3__0 )
                    {
                     before(grammarAccess.getRelationalAccess().getGroup_1_0_3()); 
                    // InternalConstraints.g:683:3: ( rule__Relational__Group_1_0_3__0 )
                    // InternalConstraints.g:683:4: rule__Relational__Group_1_0_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Relational__Group_1_0_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelationalAccess().getGroup_1_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Alternatives_1_0"


    // $ANTLR start "rule__UnaryPrefix__Alternatives"
    // InternalConstraints.g:691:1: rule__UnaryPrefix__Alternatives : ( ( rulePrimary ) | ( ( rule__UnaryPrefix__Group_1__0 ) ) | ( ( rule__UnaryPrefix__Group_2__0 ) ) );
    public final void rule__UnaryPrefix__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:695:1: ( ( rulePrimary ) | ( ( rule__UnaryPrefix__Group_1__0 ) ) | ( ( rule__UnaryPrefix__Group_2__0 ) ) )
            int alt9=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
            case RULE_STRING:
            case RULE_DOUBLE:
            case RULE_INT:
            case 18:
            case 19:
            case 31:
                {
                alt9=1;
                }
                break;
            case 43:
                {
                alt9=2;
                }
                break;
            case 44:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalConstraints.g:696:2: ( rulePrimary )
                    {
                    // InternalConstraints.g:696:2: ( rulePrimary )
                    // InternalConstraints.g:697:3: rulePrimary
                    {
                     before(grammarAccess.getUnaryPrefixAccess().getPrimaryParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    rulePrimary();

                    state._fsp--;

                     after(grammarAccess.getUnaryPrefixAccess().getPrimaryParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:702:2: ( ( rule__UnaryPrefix__Group_1__0 ) )
                    {
                    // InternalConstraints.g:702:2: ( ( rule__UnaryPrefix__Group_1__0 ) )
                    // InternalConstraints.g:703:3: ( rule__UnaryPrefix__Group_1__0 )
                    {
                     before(grammarAccess.getUnaryPrefixAccess().getGroup_1()); 
                    // InternalConstraints.g:704:3: ( rule__UnaryPrefix__Group_1__0 )
                    // InternalConstraints.g:704:4: rule__UnaryPrefix__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__UnaryPrefix__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnaryPrefixAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalConstraints.g:708:2: ( ( rule__UnaryPrefix__Group_2__0 ) )
                    {
                    // InternalConstraints.g:708:2: ( ( rule__UnaryPrefix__Group_2__0 ) )
                    // InternalConstraints.g:709:3: ( rule__UnaryPrefix__Group_2__0 )
                    {
                     before(grammarAccess.getUnaryPrefixAccess().getGroup_2()); 
                    // InternalConstraints.g:710:3: ( rule__UnaryPrefix__Group_2__0 )
                    // InternalConstraints.g:710:4: rule__UnaryPrefix__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__UnaryPrefix__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnaryPrefixAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Alternatives"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalConstraints.g:718:1: rule__Primary__Alternatives : ( ( ( rule__Primary__Group_0__0 ) ) | ( ruleConstant ) | ( ( rule__Primary__Group_2__0 ) ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:722:1: ( ( ( rule__Primary__Group_0__0 ) ) | ( ruleConstant ) | ( ( rule__Primary__Group_2__0 ) ) )
            int alt10=3;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt10=1;
                }
                break;
            case RULE_STRING:
            case RULE_DOUBLE:
            case RULE_INT:
            case 18:
            case 19:
                {
                alt10=2;
                }
                break;
            case RULE_ID:
                {
                alt10=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalConstraints.g:723:2: ( ( rule__Primary__Group_0__0 ) )
                    {
                    // InternalConstraints.g:723:2: ( ( rule__Primary__Group_0__0 ) )
                    // InternalConstraints.g:724:3: ( rule__Primary__Group_0__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_0()); 
                    // InternalConstraints.g:725:3: ( rule__Primary__Group_0__0 )
                    // InternalConstraints.g:725:4: rule__Primary__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:729:2: ( ruleConstant )
                    {
                    // InternalConstraints.g:729:2: ( ruleConstant )
                    // InternalConstraints.g:730:3: ruleConstant
                    {
                     before(grammarAccess.getPrimaryAccess().getConstantParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleConstant();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getConstantParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalConstraints.g:735:2: ( ( rule__Primary__Group_2__0 ) )
                    {
                    // InternalConstraints.g:735:2: ( ( rule__Primary__Group_2__0 ) )
                    // InternalConstraints.g:736:3: ( rule__Primary__Group_2__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_2()); 
                    // InternalConstraints.g:737:3: ( rule__Primary__Group_2__0 )
                    // InternalConstraints.g:737:4: rule__Primary__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Constant__Alternatives"
    // InternalConstraints.g:745:1: rule__Constant__Alternatives : ( ( ( rule__Constant__Group_0__0 ) ) | ( ( rule__Constant__Group_1__0 ) ) | ( ( rule__Constant__Group_2__0 ) ) | ( ( rule__Constant__Group_3__0 ) ) );
    public final void rule__Constant__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:749:1: ( ( ( rule__Constant__Group_0__0 ) ) | ( ( rule__Constant__Group_1__0 ) ) | ( ( rule__Constant__Group_2__0 ) ) | ( ( rule__Constant__Group_3__0 ) ) )
            int alt11=4;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                alt11=1;
                }
                break;
            case RULE_DOUBLE:
                {
                alt11=2;
                }
                break;
            case RULE_STRING:
                {
                alt11=3;
                }
                break;
            case 18:
            case 19:
                {
                alt11=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalConstraints.g:750:2: ( ( rule__Constant__Group_0__0 ) )
                    {
                    // InternalConstraints.g:750:2: ( ( rule__Constant__Group_0__0 ) )
                    // InternalConstraints.g:751:3: ( rule__Constant__Group_0__0 )
                    {
                     before(grammarAccess.getConstantAccess().getGroup_0()); 
                    // InternalConstraints.g:752:3: ( rule__Constant__Group_0__0 )
                    // InternalConstraints.g:752:4: rule__Constant__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Constant__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getConstantAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:756:2: ( ( rule__Constant__Group_1__0 ) )
                    {
                    // InternalConstraints.g:756:2: ( ( rule__Constant__Group_1__0 ) )
                    // InternalConstraints.g:757:3: ( rule__Constant__Group_1__0 )
                    {
                     before(grammarAccess.getConstantAccess().getGroup_1()); 
                    // InternalConstraints.g:758:3: ( rule__Constant__Group_1__0 )
                    // InternalConstraints.g:758:4: rule__Constant__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Constant__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getConstantAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalConstraints.g:762:2: ( ( rule__Constant__Group_2__0 ) )
                    {
                    // InternalConstraints.g:762:2: ( ( rule__Constant__Group_2__0 ) )
                    // InternalConstraints.g:763:3: ( rule__Constant__Group_2__0 )
                    {
                     before(grammarAccess.getConstantAccess().getGroup_2()); 
                    // InternalConstraints.g:764:3: ( rule__Constant__Group_2__0 )
                    // InternalConstraints.g:764:4: rule__Constant__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Constant__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getConstantAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalConstraints.g:768:2: ( ( rule__Constant__Group_3__0 ) )
                    {
                    // InternalConstraints.g:768:2: ( ( rule__Constant__Group_3__0 ) )
                    // InternalConstraints.g:769:3: ( rule__Constant__Group_3__0 )
                    {
                     before(grammarAccess.getConstantAccess().getGroup_3()); 
                    // InternalConstraints.g:770:3: ( rule__Constant__Group_3__0 )
                    // InternalConstraints.g:770:4: rule__Constant__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Constant__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getConstantAccess().getGroup_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Alternatives"


    // $ANTLR start "rule__Constant__ValueAlternatives_3_1_0"
    // InternalConstraints.g:778:1: rule__Constant__ValueAlternatives_3_1_0 : ( ( 'true' ) | ( 'false' ) );
    public final void rule__Constant__ValueAlternatives_3_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:782:1: ( ( 'true' ) | ( 'false' ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==18) ) {
                alt12=1;
            }
            else if ( (LA12_0==19) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalConstraints.g:783:2: ( 'true' )
                    {
                    // InternalConstraints.g:783:2: ( 'true' )
                    // InternalConstraints.g:784:3: 'true'
                    {
                     before(grammarAccess.getConstantAccess().getValueTrueKeyword_3_1_0_0()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getConstantAccess().getValueTrueKeyword_3_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:789:2: ( 'false' )
                    {
                    // InternalConstraints.g:789:2: ( 'false' )
                    // InternalConstraints.g:790:3: 'false'
                    {
                     before(grammarAccess.getConstantAccess().getValueFalseKeyword_3_1_0_1()); 
                    match(input,19,FOLLOW_2); 
                     after(grammarAccess.getConstantAccess().getValueFalseKeyword_3_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__ValueAlternatives_3_1_0"


    // $ANTLR start "rule__Quantifier__Alternatives"
    // InternalConstraints.g:799:1: rule__Quantifier__Alternatives : ( ( ( 'forall' ) ) | ( ( 'all' ) ) | ( ( 'exists' ) ) | ( ( 'any' ) ) );
    public final void rule__Quantifier__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:803:1: ( ( ( 'forall' ) ) | ( ( 'all' ) ) | ( ( 'exists' ) ) | ( ( 'any' ) ) )
            int alt13=4;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt13=1;
                }
                break;
            case 21:
                {
                alt13=2;
                }
                break;
            case 22:
                {
                alt13=3;
                }
                break;
            case 23:
                {
                alt13=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // InternalConstraints.g:804:2: ( ( 'forall' ) )
                    {
                    // InternalConstraints.g:804:2: ( ( 'forall' ) )
                    // InternalConstraints.g:805:3: ( 'forall' )
                    {
                     before(grammarAccess.getQuantifierAccess().getALLEnumLiteralDeclaration_0()); 
                    // InternalConstraints.g:806:3: ( 'forall' )
                    // InternalConstraints.g:806:4: 'forall'
                    {
                    match(input,20,FOLLOW_2); 

                    }

                     after(grammarAccess.getQuantifierAccess().getALLEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConstraints.g:810:2: ( ( 'all' ) )
                    {
                    // InternalConstraints.g:810:2: ( ( 'all' ) )
                    // InternalConstraints.g:811:3: ( 'all' )
                    {
                     before(grammarAccess.getQuantifierAccess().getALLEnumLiteralDeclaration_1()); 
                    // InternalConstraints.g:812:3: ( 'all' )
                    // InternalConstraints.g:812:4: 'all'
                    {
                    match(input,21,FOLLOW_2); 

                    }

                     after(grammarAccess.getQuantifierAccess().getALLEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalConstraints.g:816:2: ( ( 'exists' ) )
                    {
                    // InternalConstraints.g:816:2: ( ( 'exists' ) )
                    // InternalConstraints.g:817:3: ( 'exists' )
                    {
                     before(grammarAccess.getQuantifierAccess().getANYEnumLiteralDeclaration_2()); 
                    // InternalConstraints.g:818:3: ( 'exists' )
                    // InternalConstraints.g:818:4: 'exists'
                    {
                    match(input,22,FOLLOW_2); 

                    }

                     after(grammarAccess.getQuantifierAccess().getANYEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalConstraints.g:822:2: ( ( 'any' ) )
                    {
                    // InternalConstraints.g:822:2: ( ( 'any' ) )
                    // InternalConstraints.g:823:3: ( 'any' )
                    {
                     before(grammarAccess.getQuantifierAccess().getANYEnumLiteralDeclaration_3()); 
                    // InternalConstraints.g:824:3: ( 'any' )
                    // InternalConstraints.g:824:4: 'any'
                    {
                    match(input,23,FOLLOW_2); 

                    }

                     after(grammarAccess.getQuantifierAccess().getANYEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quantifier__Alternatives"


    // $ANTLR start "rule__ConstraintDocument__Group__0"
    // InternalConstraints.g:832:1: rule__ConstraintDocument__Group__0 : rule__ConstraintDocument__Group__0__Impl rule__ConstraintDocument__Group__1 ;
    public final void rule__ConstraintDocument__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:836:1: ( rule__ConstraintDocument__Group__0__Impl rule__ConstraintDocument__Group__1 )
            // InternalConstraints.g:837:2: rule__ConstraintDocument__Group__0__Impl rule__ConstraintDocument__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__ConstraintDocument__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConstraintDocument__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintDocument__Group__0"


    // $ANTLR start "rule__ConstraintDocument__Group__0__Impl"
    // InternalConstraints.g:844:1: rule__ConstraintDocument__Group__0__Impl : ( ( rule__ConstraintDocument__ReadingsAssignment_0 )* ) ;
    public final void rule__ConstraintDocument__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:848:1: ( ( ( rule__ConstraintDocument__ReadingsAssignment_0 )* ) )
            // InternalConstraints.g:849:1: ( ( rule__ConstraintDocument__ReadingsAssignment_0 )* )
            {
            // InternalConstraints.g:849:1: ( ( rule__ConstraintDocument__ReadingsAssignment_0 )* )
            // InternalConstraints.g:850:2: ( rule__ConstraintDocument__ReadingsAssignment_0 )*
            {
             before(grammarAccess.getConstraintDocumentAccess().getReadingsAssignment_0()); 
            // InternalConstraints.g:851:2: ( rule__ConstraintDocument__ReadingsAssignment_0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==24) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalConstraints.g:851:3: rule__ConstraintDocument__ReadingsAssignment_0
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__ConstraintDocument__ReadingsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getConstraintDocumentAccess().getReadingsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintDocument__Group__0__Impl"


    // $ANTLR start "rule__ConstraintDocument__Group__1"
    // InternalConstraints.g:859:1: rule__ConstraintDocument__Group__1 : rule__ConstraintDocument__Group__1__Impl ;
    public final void rule__ConstraintDocument__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:863:1: ( rule__ConstraintDocument__Group__1__Impl )
            // InternalConstraints.g:864:2: rule__ConstraintDocument__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ConstraintDocument__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintDocument__Group__1"


    // $ANTLR start "rule__ConstraintDocument__Group__1__Impl"
    // InternalConstraints.g:870:1: rule__ConstraintDocument__Group__1__Impl : ( ( rule__ConstraintDocument__ConstraintsAssignment_1 )* ) ;
    public final void rule__ConstraintDocument__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:874:1: ( ( ( rule__ConstraintDocument__ConstraintsAssignment_1 )* ) )
            // InternalConstraints.g:875:1: ( ( rule__ConstraintDocument__ConstraintsAssignment_1 )* )
            {
            // InternalConstraints.g:875:1: ( ( rule__ConstraintDocument__ConstraintsAssignment_1 )* )
            // InternalConstraints.g:876:2: ( rule__ConstraintDocument__ConstraintsAssignment_1 )*
            {
             before(grammarAccess.getConstraintDocumentAccess().getConstraintsAssignment_1()); 
            // InternalConstraints.g:877:2: ( rule__ConstraintDocument__ConstraintsAssignment_1 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==28||LA15_0==45) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalConstraints.g:877:3: rule__ConstraintDocument__ConstraintsAssignment_1
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__ConstraintDocument__ConstraintsAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getConstraintDocumentAccess().getConstraintsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintDocument__Group__1__Impl"


    // $ANTLR start "rule__Reading__Group__0"
    // InternalConstraints.g:886:1: rule__Reading__Group__0 : rule__Reading__Group__0__Impl rule__Reading__Group__1 ;
    public final void rule__Reading__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:890:1: ( rule__Reading__Group__0__Impl rule__Reading__Group__1 )
            // InternalConstraints.g:891:2: rule__Reading__Group__0__Impl rule__Reading__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Reading__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reading__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__0"


    // $ANTLR start "rule__Reading__Group__0__Impl"
    // InternalConstraints.g:898:1: rule__Reading__Group__0__Impl : ( 'read' ) ;
    public final void rule__Reading__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:902:1: ( ( 'read' ) )
            // InternalConstraints.g:903:1: ( 'read' )
            {
            // InternalConstraints.g:903:1: ( 'read' )
            // InternalConstraints.g:904:2: 'read'
            {
             before(grammarAccess.getReadingAccess().getReadKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getReadingAccess().getReadKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__0__Impl"


    // $ANTLR start "rule__Reading__Group__1"
    // InternalConstraints.g:913:1: rule__Reading__Group__1 : rule__Reading__Group__1__Impl rule__Reading__Group__2 ;
    public final void rule__Reading__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:917:1: ( rule__Reading__Group__1__Impl rule__Reading__Group__2 )
            // InternalConstraints.g:918:2: rule__Reading__Group__1__Impl rule__Reading__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Reading__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reading__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__1"


    // $ANTLR start "rule__Reading__Group__1__Impl"
    // InternalConstraints.g:925:1: rule__Reading__Group__1__Impl : ( ( rule__Reading__TypeAssignment_1 ) ) ;
    public final void rule__Reading__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:929:1: ( ( ( rule__Reading__TypeAssignment_1 ) ) )
            // InternalConstraints.g:930:1: ( ( rule__Reading__TypeAssignment_1 ) )
            {
            // InternalConstraints.g:930:1: ( ( rule__Reading__TypeAssignment_1 ) )
            // InternalConstraints.g:931:2: ( rule__Reading__TypeAssignment_1 )
            {
             before(grammarAccess.getReadingAccess().getTypeAssignment_1()); 
            // InternalConstraints.g:932:2: ( rule__Reading__TypeAssignment_1 )
            // InternalConstraints.g:932:3: rule__Reading__TypeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Reading__TypeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getReadingAccess().getTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__1__Impl"


    // $ANTLR start "rule__Reading__Group__2"
    // InternalConstraints.g:940:1: rule__Reading__Group__2 : rule__Reading__Group__2__Impl rule__Reading__Group__3 ;
    public final void rule__Reading__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:944:1: ( rule__Reading__Group__2__Impl rule__Reading__Group__3 )
            // InternalConstraints.g:945:2: rule__Reading__Group__2__Impl rule__Reading__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Reading__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reading__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__2"


    // $ANTLR start "rule__Reading__Group__2__Impl"
    // InternalConstraints.g:952:1: rule__Reading__Group__2__Impl : ( '.' ) ;
    public final void rule__Reading__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:956:1: ( ( '.' ) )
            // InternalConstraints.g:957:1: ( '.' )
            {
            // InternalConstraints.g:957:1: ( '.' )
            // InternalConstraints.g:958:2: '.'
            {
             before(grammarAccess.getReadingAccess().getFullStopKeyword_2()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getReadingAccess().getFullStopKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__2__Impl"


    // $ANTLR start "rule__Reading__Group__3"
    // InternalConstraints.g:967:1: rule__Reading__Group__3 : rule__Reading__Group__3__Impl rule__Reading__Group__4 ;
    public final void rule__Reading__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:971:1: ( rule__Reading__Group__3__Impl rule__Reading__Group__4 )
            // InternalConstraints.g:972:2: rule__Reading__Group__3__Impl rule__Reading__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__Reading__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reading__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__3"


    // $ANTLR start "rule__Reading__Group__3__Impl"
    // InternalConstraints.g:979:1: rule__Reading__Group__3__Impl : ( ( rule__Reading__IdentifierAssignment_3 ) ) ;
    public final void rule__Reading__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:983:1: ( ( ( rule__Reading__IdentifierAssignment_3 ) ) )
            // InternalConstraints.g:984:1: ( ( rule__Reading__IdentifierAssignment_3 ) )
            {
            // InternalConstraints.g:984:1: ( ( rule__Reading__IdentifierAssignment_3 ) )
            // InternalConstraints.g:985:2: ( rule__Reading__IdentifierAssignment_3 )
            {
             before(grammarAccess.getReadingAccess().getIdentifierAssignment_3()); 
            // InternalConstraints.g:986:2: ( rule__Reading__IdentifierAssignment_3 )
            // InternalConstraints.g:986:3: rule__Reading__IdentifierAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Reading__IdentifierAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getReadingAccess().getIdentifierAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__3__Impl"


    // $ANTLR start "rule__Reading__Group__4"
    // InternalConstraints.g:994:1: rule__Reading__Group__4 : rule__Reading__Group__4__Impl rule__Reading__Group__5 ;
    public final void rule__Reading__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:998:1: ( rule__Reading__Group__4__Impl rule__Reading__Group__5 )
            // InternalConstraints.g:999:2: rule__Reading__Group__4__Impl rule__Reading__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__Reading__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reading__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__4"


    // $ANTLR start "rule__Reading__Group__4__Impl"
    // InternalConstraints.g:1006:1: rule__Reading__Group__4__Impl : ( 'as' ) ;
    public final void rule__Reading__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1010:1: ( ( 'as' ) )
            // InternalConstraints.g:1011:1: ( 'as' )
            {
            // InternalConstraints.g:1011:1: ( 'as' )
            // InternalConstraints.g:1012:2: 'as'
            {
             before(grammarAccess.getReadingAccess().getAsKeyword_4()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getReadingAccess().getAsKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__4__Impl"


    // $ANTLR start "rule__Reading__Group__5"
    // InternalConstraints.g:1021:1: rule__Reading__Group__5 : rule__Reading__Group__5__Impl rule__Reading__Group__6 ;
    public final void rule__Reading__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1025:1: ( rule__Reading__Group__5__Impl rule__Reading__Group__6 )
            // InternalConstraints.g:1026:2: rule__Reading__Group__5__Impl rule__Reading__Group__6
            {
            pushFollow(FOLLOW_9);
            rule__Reading__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reading__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__5"


    // $ANTLR start "rule__Reading__Group__5__Impl"
    // InternalConstraints.g:1033:1: rule__Reading__Group__5__Impl : ( ( rule__Reading__NameAssignment_5 ) ) ;
    public final void rule__Reading__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1037:1: ( ( ( rule__Reading__NameAssignment_5 ) ) )
            // InternalConstraints.g:1038:1: ( ( rule__Reading__NameAssignment_5 ) )
            {
            // InternalConstraints.g:1038:1: ( ( rule__Reading__NameAssignment_5 ) )
            // InternalConstraints.g:1039:2: ( rule__Reading__NameAssignment_5 )
            {
             before(grammarAccess.getReadingAccess().getNameAssignment_5()); 
            // InternalConstraints.g:1040:2: ( rule__Reading__NameAssignment_5 )
            // InternalConstraints.g:1040:3: rule__Reading__NameAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Reading__NameAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getReadingAccess().getNameAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__5__Impl"


    // $ANTLR start "rule__Reading__Group__6"
    // InternalConstraints.g:1048:1: rule__Reading__Group__6 : rule__Reading__Group__6__Impl ;
    public final void rule__Reading__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1052:1: ( rule__Reading__Group__6__Impl )
            // InternalConstraints.g:1053:2: rule__Reading__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Reading__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__6"


    // $ANTLR start "rule__Reading__Group__6__Impl"
    // InternalConstraints.g:1059:1: rule__Reading__Group__6__Impl : ( ';' ) ;
    public final void rule__Reading__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1063:1: ( ( ';' ) )
            // InternalConstraints.g:1064:1: ( ';' )
            {
            // InternalConstraints.g:1064:1: ( ';' )
            // InternalConstraints.g:1065:2: ';'
            {
             before(grammarAccess.getReadingAccess().getSemicolonKeyword_6()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getReadingAccess().getSemicolonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__Group__6__Impl"


    // $ANTLR start "rule__Constraint__Group__0"
    // InternalConstraints.g:1075:1: rule__Constraint__Group__0 : rule__Constraint__Group__0__Impl rule__Constraint__Group__1 ;
    public final void rule__Constraint__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1079:1: ( rule__Constraint__Group__0__Impl rule__Constraint__Group__1 )
            // InternalConstraints.g:1080:2: rule__Constraint__Group__0__Impl rule__Constraint__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Constraint__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constraint__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__0"


    // $ANTLR start "rule__Constraint__Group__0__Impl"
    // InternalConstraints.g:1087:1: rule__Constraint__Group__0__Impl : ( ( rule__Constraint__RuntimeAssignment_0 )? ) ;
    public final void rule__Constraint__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1091:1: ( ( ( rule__Constraint__RuntimeAssignment_0 )? ) )
            // InternalConstraints.g:1092:1: ( ( rule__Constraint__RuntimeAssignment_0 )? )
            {
            // InternalConstraints.g:1092:1: ( ( rule__Constraint__RuntimeAssignment_0 )? )
            // InternalConstraints.g:1093:2: ( rule__Constraint__RuntimeAssignment_0 )?
            {
             before(grammarAccess.getConstraintAccess().getRuntimeAssignment_0()); 
            // InternalConstraints.g:1094:2: ( rule__Constraint__RuntimeAssignment_0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==45) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalConstraints.g:1094:3: rule__Constraint__RuntimeAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Constraint__RuntimeAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConstraintAccess().getRuntimeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__0__Impl"


    // $ANTLR start "rule__Constraint__Group__1"
    // InternalConstraints.g:1102:1: rule__Constraint__Group__1 : rule__Constraint__Group__1__Impl rule__Constraint__Group__2 ;
    public final void rule__Constraint__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1106:1: ( rule__Constraint__Group__1__Impl rule__Constraint__Group__2 )
            // InternalConstraints.g:1107:2: rule__Constraint__Group__1__Impl rule__Constraint__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Constraint__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constraint__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__1"


    // $ANTLR start "rule__Constraint__Group__1__Impl"
    // InternalConstraints.g:1114:1: rule__Constraint__Group__1__Impl : ( 'constraint' ) ;
    public final void rule__Constraint__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1118:1: ( ( 'constraint' ) )
            // InternalConstraints.g:1119:1: ( 'constraint' )
            {
            // InternalConstraints.g:1119:1: ( 'constraint' )
            // InternalConstraints.g:1120:2: 'constraint'
            {
             before(grammarAccess.getConstraintAccess().getConstraintKeyword_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getConstraintAccess().getConstraintKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__1__Impl"


    // $ANTLR start "rule__Constraint__Group__2"
    // InternalConstraints.g:1129:1: rule__Constraint__Group__2 : rule__Constraint__Group__2__Impl rule__Constraint__Group__3 ;
    public final void rule__Constraint__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1133:1: ( rule__Constraint__Group__2__Impl rule__Constraint__Group__3 )
            // InternalConstraints.g:1134:2: rule__Constraint__Group__2__Impl rule__Constraint__Group__3
            {
            pushFollow(FOLLOW_10);
            rule__Constraint__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constraint__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__2"


    // $ANTLR start "rule__Constraint__Group__2__Impl"
    // InternalConstraints.g:1141:1: rule__Constraint__Group__2__Impl : ( ( rule__Constraint__NameAssignment_2 ) ) ;
    public final void rule__Constraint__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1145:1: ( ( ( rule__Constraint__NameAssignment_2 ) ) )
            // InternalConstraints.g:1146:1: ( ( rule__Constraint__NameAssignment_2 ) )
            {
            // InternalConstraints.g:1146:1: ( ( rule__Constraint__NameAssignment_2 ) )
            // InternalConstraints.g:1147:2: ( rule__Constraint__NameAssignment_2 )
            {
             before(grammarAccess.getConstraintAccess().getNameAssignment_2()); 
            // InternalConstraints.g:1148:2: ( rule__Constraint__NameAssignment_2 )
            // InternalConstraints.g:1148:3: rule__Constraint__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__2__Impl"


    // $ANTLR start "rule__Constraint__Group__3"
    // InternalConstraints.g:1156:1: rule__Constraint__Group__3 : rule__Constraint__Group__3__Impl rule__Constraint__Group__4 ;
    public final void rule__Constraint__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1160:1: ( rule__Constraint__Group__3__Impl rule__Constraint__Group__4 )
            // InternalConstraints.g:1161:2: rule__Constraint__Group__3__Impl rule__Constraint__Group__4
            {
            pushFollow(FOLLOW_11);
            rule__Constraint__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constraint__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__3"


    // $ANTLR start "rule__Constraint__Group__3__Impl"
    // InternalConstraints.g:1168:1: rule__Constraint__Group__3__Impl : ( ':' ) ;
    public final void rule__Constraint__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1172:1: ( ( ':' ) )
            // InternalConstraints.g:1173:1: ( ':' )
            {
            // InternalConstraints.g:1173:1: ( ':' )
            // InternalConstraints.g:1174:2: ':'
            {
             before(grammarAccess.getConstraintAccess().getColonKeyword_3()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getConstraintAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__3__Impl"


    // $ANTLR start "rule__Constraint__Group__4"
    // InternalConstraints.g:1183:1: rule__Constraint__Group__4 : rule__Constraint__Group__4__Impl rule__Constraint__Group__5 ;
    public final void rule__Constraint__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1187:1: ( rule__Constraint__Group__4__Impl rule__Constraint__Group__5 )
            // InternalConstraints.g:1188:2: rule__Constraint__Group__4__Impl rule__Constraint__Group__5
            {
            pushFollow(FOLLOW_9);
            rule__Constraint__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constraint__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__4"


    // $ANTLR start "rule__Constraint__Group__4__Impl"
    // InternalConstraints.g:1195:1: rule__Constraint__Group__4__Impl : ( ( rule__Constraint__ExpressionAssignment_4 ) ) ;
    public final void rule__Constraint__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1199:1: ( ( ( rule__Constraint__ExpressionAssignment_4 ) ) )
            // InternalConstraints.g:1200:1: ( ( rule__Constraint__ExpressionAssignment_4 ) )
            {
            // InternalConstraints.g:1200:1: ( ( rule__Constraint__ExpressionAssignment_4 ) )
            // InternalConstraints.g:1201:2: ( rule__Constraint__ExpressionAssignment_4 )
            {
             before(grammarAccess.getConstraintAccess().getExpressionAssignment_4()); 
            // InternalConstraints.g:1202:2: ( rule__Constraint__ExpressionAssignment_4 )
            // InternalConstraints.g:1202:3: rule__Constraint__ExpressionAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__ExpressionAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getExpressionAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__4__Impl"


    // $ANTLR start "rule__Constraint__Group__5"
    // InternalConstraints.g:1210:1: rule__Constraint__Group__5 : rule__Constraint__Group__5__Impl ;
    public final void rule__Constraint__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1214:1: ( rule__Constraint__Group__5__Impl )
            // InternalConstraints.g:1215:2: rule__Constraint__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__5"


    // $ANTLR start "rule__Constraint__Group__5__Impl"
    // InternalConstraints.g:1221:1: rule__Constraint__Group__5__Impl : ( ';' ) ;
    public final void rule__Constraint__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1225:1: ( ( ';' ) )
            // InternalConstraints.g:1226:1: ( ';' )
            {
            // InternalConstraints.g:1226:1: ( ';' )
            // InternalConstraints.g:1227:2: ';'
            {
             before(grammarAccess.getConstraintAccess().getSemicolonKeyword_5()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getConstraintAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__5__Impl"


    // $ANTLR start "rule__QuantifiedExpression__Group__0"
    // InternalConstraints.g:1237:1: rule__QuantifiedExpression__Group__0 : rule__QuantifiedExpression__Group__0__Impl rule__QuantifiedExpression__Group__1 ;
    public final void rule__QuantifiedExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1241:1: ( rule__QuantifiedExpression__Group__0__Impl rule__QuantifiedExpression__Group__1 )
            // InternalConstraints.g:1242:2: rule__QuantifiedExpression__Group__0__Impl rule__QuantifiedExpression__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__QuantifiedExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group__0"


    // $ANTLR start "rule__QuantifiedExpression__Group__0__Impl"
    // InternalConstraints.g:1249:1: rule__QuantifiedExpression__Group__0__Impl : ( () ) ;
    public final void rule__QuantifiedExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1253:1: ( ( () ) )
            // InternalConstraints.g:1254:1: ( () )
            {
            // InternalConstraints.g:1254:1: ( () )
            // InternalConstraints.g:1255:2: ()
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getQuantifiedExpressionAction_0()); 
            // InternalConstraints.g:1256:2: ()
            // InternalConstraints.g:1256:3: 
            {
            }

             after(grammarAccess.getQuantifiedExpressionAccess().getQuantifiedExpressionAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group__0__Impl"


    // $ANTLR start "rule__QuantifiedExpression__Group__1"
    // InternalConstraints.g:1264:1: rule__QuantifiedExpression__Group__1 : rule__QuantifiedExpression__Group__1__Impl rule__QuantifiedExpression__Group__2 ;
    public final void rule__QuantifiedExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1268:1: ( rule__QuantifiedExpression__Group__1__Impl rule__QuantifiedExpression__Group__2 )
            // InternalConstraints.g:1269:2: rule__QuantifiedExpression__Group__1__Impl rule__QuantifiedExpression__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__QuantifiedExpression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group__1"


    // $ANTLR start "rule__QuantifiedExpression__Group__1__Impl"
    // InternalConstraints.g:1276:1: rule__QuantifiedExpression__Group__1__Impl : ( ( rule__QuantifiedExpression__QuantifierAssignment_1 ) ) ;
    public final void rule__QuantifiedExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1280:1: ( ( ( rule__QuantifiedExpression__QuantifierAssignment_1 ) ) )
            // InternalConstraints.g:1281:1: ( ( rule__QuantifiedExpression__QuantifierAssignment_1 ) )
            {
            // InternalConstraints.g:1281:1: ( ( rule__QuantifiedExpression__QuantifierAssignment_1 ) )
            // InternalConstraints.g:1282:2: ( rule__QuantifiedExpression__QuantifierAssignment_1 )
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getQuantifierAssignment_1()); 
            // InternalConstraints.g:1283:2: ( rule__QuantifiedExpression__QuantifierAssignment_1 )
            // InternalConstraints.g:1283:3: rule__QuantifiedExpression__QuantifierAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__QuantifierAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQuantifiedExpressionAccess().getQuantifierAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group__1__Impl"


    // $ANTLR start "rule__QuantifiedExpression__Group__2"
    // InternalConstraints.g:1291:1: rule__QuantifiedExpression__Group__2 : rule__QuantifiedExpression__Group__2__Impl rule__QuantifiedExpression__Group__3 ;
    public final void rule__QuantifiedExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1295:1: ( rule__QuantifiedExpression__Group__2__Impl rule__QuantifiedExpression__Group__3 )
            // InternalConstraints.g:1296:2: rule__QuantifiedExpression__Group__2__Impl rule__QuantifiedExpression__Group__3
            {
            pushFollow(FOLLOW_13);
            rule__QuantifiedExpression__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group__2"


    // $ANTLR start "rule__QuantifiedExpression__Group__2__Impl"
    // InternalConstraints.g:1303:1: rule__QuantifiedExpression__Group__2__Impl : ( ( rule__QuantifiedExpression__ArgumentAssignment_2 ) ) ;
    public final void rule__QuantifiedExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1307:1: ( ( ( rule__QuantifiedExpression__ArgumentAssignment_2 ) ) )
            // InternalConstraints.g:1308:1: ( ( rule__QuantifiedExpression__ArgumentAssignment_2 ) )
            {
            // InternalConstraints.g:1308:1: ( ( rule__QuantifiedExpression__ArgumentAssignment_2 ) )
            // InternalConstraints.g:1309:2: ( rule__QuantifiedExpression__ArgumentAssignment_2 )
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getArgumentAssignment_2()); 
            // InternalConstraints.g:1310:2: ( rule__QuantifiedExpression__ArgumentAssignment_2 )
            // InternalConstraints.g:1310:3: rule__QuantifiedExpression__ArgumentAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__ArgumentAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getQuantifiedExpressionAccess().getArgumentAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group__2__Impl"


    // $ANTLR start "rule__QuantifiedExpression__Group__3"
    // InternalConstraints.g:1318:1: rule__QuantifiedExpression__Group__3 : rule__QuantifiedExpression__Group__3__Impl rule__QuantifiedExpression__Group__4 ;
    public final void rule__QuantifiedExpression__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1322:1: ( rule__QuantifiedExpression__Group__3__Impl rule__QuantifiedExpression__Group__4 )
            // InternalConstraints.g:1323:2: rule__QuantifiedExpression__Group__3__Impl rule__QuantifiedExpression__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__QuantifiedExpression__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group__3"


    // $ANTLR start "rule__QuantifiedExpression__Group__3__Impl"
    // InternalConstraints.g:1330:1: rule__QuantifiedExpression__Group__3__Impl : ( ( rule__QuantifiedExpression__Group_3__0 )? ) ;
    public final void rule__QuantifiedExpression__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1334:1: ( ( ( rule__QuantifiedExpression__Group_3__0 )? ) )
            // InternalConstraints.g:1335:1: ( ( rule__QuantifiedExpression__Group_3__0 )? )
            {
            // InternalConstraints.g:1335:1: ( ( rule__QuantifiedExpression__Group_3__0 )? )
            // InternalConstraints.g:1336:2: ( rule__QuantifiedExpression__Group_3__0 )?
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getGroup_3()); 
            // InternalConstraints.g:1337:2: ( rule__QuantifiedExpression__Group_3__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==30) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalConstraints.g:1337:3: rule__QuantifiedExpression__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__QuantifiedExpression__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQuantifiedExpressionAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group__3__Impl"


    // $ANTLR start "rule__QuantifiedExpression__Group__4"
    // InternalConstraints.g:1345:1: rule__QuantifiedExpression__Group__4 : rule__QuantifiedExpression__Group__4__Impl rule__QuantifiedExpression__Group__5 ;
    public final void rule__QuantifiedExpression__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1349:1: ( rule__QuantifiedExpression__Group__4__Impl rule__QuantifiedExpression__Group__5 )
            // InternalConstraints.g:1350:2: rule__QuantifiedExpression__Group__4__Impl rule__QuantifiedExpression__Group__5
            {
            pushFollow(FOLLOW_11);
            rule__QuantifiedExpression__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group__4"


    // $ANTLR start "rule__QuantifiedExpression__Group__4__Impl"
    // InternalConstraints.g:1357:1: rule__QuantifiedExpression__Group__4__Impl : ( '.' ) ;
    public final void rule__QuantifiedExpression__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1361:1: ( ( '.' ) )
            // InternalConstraints.g:1362:1: ( '.' )
            {
            // InternalConstraints.g:1362:1: ( '.' )
            // InternalConstraints.g:1363:2: '.'
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getFullStopKeyword_4()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getQuantifiedExpressionAccess().getFullStopKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group__4__Impl"


    // $ANTLR start "rule__QuantifiedExpression__Group__5"
    // InternalConstraints.g:1372:1: rule__QuantifiedExpression__Group__5 : rule__QuantifiedExpression__Group__5__Impl ;
    public final void rule__QuantifiedExpression__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1376:1: ( rule__QuantifiedExpression__Group__5__Impl )
            // InternalConstraints.g:1377:2: rule__QuantifiedExpression__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group__5"


    // $ANTLR start "rule__QuantifiedExpression__Group__5__Impl"
    // InternalConstraints.g:1383:1: rule__QuantifiedExpression__Group__5__Impl : ( ( rule__QuantifiedExpression__ExpressionAssignment_5 ) ) ;
    public final void rule__QuantifiedExpression__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1387:1: ( ( ( rule__QuantifiedExpression__ExpressionAssignment_5 ) ) )
            // InternalConstraints.g:1388:1: ( ( rule__QuantifiedExpression__ExpressionAssignment_5 ) )
            {
            // InternalConstraints.g:1388:1: ( ( rule__QuantifiedExpression__ExpressionAssignment_5 ) )
            // InternalConstraints.g:1389:2: ( rule__QuantifiedExpression__ExpressionAssignment_5 )
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getExpressionAssignment_5()); 
            // InternalConstraints.g:1390:2: ( rule__QuantifiedExpression__ExpressionAssignment_5 )
            // InternalConstraints.g:1390:3: rule__QuantifiedExpression__ExpressionAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__ExpressionAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getQuantifiedExpressionAccess().getExpressionAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group__5__Impl"


    // $ANTLR start "rule__QuantifiedExpression__Group_3__0"
    // InternalConstraints.g:1399:1: rule__QuantifiedExpression__Group_3__0 : rule__QuantifiedExpression__Group_3__0__Impl rule__QuantifiedExpression__Group_3__1 ;
    public final void rule__QuantifiedExpression__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1403:1: ( rule__QuantifiedExpression__Group_3__0__Impl rule__QuantifiedExpression__Group_3__1 )
            // InternalConstraints.g:1404:2: rule__QuantifiedExpression__Group_3__0__Impl rule__QuantifiedExpression__Group_3__1
            {
            pushFollow(FOLLOW_14);
            rule__QuantifiedExpression__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group_3__0"


    // $ANTLR start "rule__QuantifiedExpression__Group_3__0__Impl"
    // InternalConstraints.g:1411:1: rule__QuantifiedExpression__Group_3__0__Impl : ( '|' ) ;
    public final void rule__QuantifiedExpression__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1415:1: ( ( '|' ) )
            // InternalConstraints.g:1416:1: ( '|' )
            {
            // InternalConstraints.g:1416:1: ( '|' )
            // InternalConstraints.g:1417:2: '|'
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getVerticalLineKeyword_3_0()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getQuantifiedExpressionAccess().getVerticalLineKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group_3__0__Impl"


    // $ANTLR start "rule__QuantifiedExpression__Group_3__1"
    // InternalConstraints.g:1426:1: rule__QuantifiedExpression__Group_3__1 : rule__QuantifiedExpression__Group_3__1__Impl ;
    public final void rule__QuantifiedExpression__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1430:1: ( rule__QuantifiedExpression__Group_3__1__Impl )
            // InternalConstraints.g:1431:2: rule__QuantifiedExpression__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group_3__1"


    // $ANTLR start "rule__QuantifiedExpression__Group_3__1__Impl"
    // InternalConstraints.g:1437:1: rule__QuantifiedExpression__Group_3__1__Impl : ( ( rule__QuantifiedExpression__QueryAssignment_3_1 ) ) ;
    public final void rule__QuantifiedExpression__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1441:1: ( ( ( rule__QuantifiedExpression__QueryAssignment_3_1 ) ) )
            // InternalConstraints.g:1442:1: ( ( rule__QuantifiedExpression__QueryAssignment_3_1 ) )
            {
            // InternalConstraints.g:1442:1: ( ( rule__QuantifiedExpression__QueryAssignment_3_1 ) )
            // InternalConstraints.g:1443:2: ( rule__QuantifiedExpression__QueryAssignment_3_1 )
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getQueryAssignment_3_1()); 
            // InternalConstraints.g:1444:2: ( rule__QuantifiedExpression__QueryAssignment_3_1 )
            // InternalConstraints.g:1444:3: rule__QuantifiedExpression__QueryAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpression__QueryAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getQuantifiedExpressionAccess().getQueryAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__Group_3__1__Impl"


    // $ANTLR start "rule__QueryUnion__Group__0"
    // InternalConstraints.g:1453:1: rule__QueryUnion__Group__0 : rule__QueryUnion__Group__0__Impl rule__QueryUnion__Group__1 ;
    public final void rule__QueryUnion__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1457:1: ( rule__QueryUnion__Group__0__Impl rule__QueryUnion__Group__1 )
            // InternalConstraints.g:1458:2: rule__QueryUnion__Group__0__Impl rule__QueryUnion__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__QueryUnion__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryUnion__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__Group__0"


    // $ANTLR start "rule__QueryUnion__Group__0__Impl"
    // InternalConstraints.g:1465:1: rule__QueryUnion__Group__0__Impl : ( ruleQueryIntersection ) ;
    public final void rule__QueryUnion__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1469:1: ( ( ruleQueryIntersection ) )
            // InternalConstraints.g:1470:1: ( ruleQueryIntersection )
            {
            // InternalConstraints.g:1470:1: ( ruleQueryIntersection )
            // InternalConstraints.g:1471:2: ruleQueryIntersection
            {
             before(grammarAccess.getQueryUnionAccess().getQueryIntersectionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleQueryIntersection();

            state._fsp--;

             after(grammarAccess.getQueryUnionAccess().getQueryIntersectionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__Group__0__Impl"


    // $ANTLR start "rule__QueryUnion__Group__1"
    // InternalConstraints.g:1480:1: rule__QueryUnion__Group__1 : rule__QueryUnion__Group__1__Impl ;
    public final void rule__QueryUnion__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1484:1: ( rule__QueryUnion__Group__1__Impl )
            // InternalConstraints.g:1485:2: rule__QueryUnion__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryUnion__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__Group__1"


    // $ANTLR start "rule__QueryUnion__Group__1__Impl"
    // InternalConstraints.g:1491:1: rule__QueryUnion__Group__1__Impl : ( ( rule__QueryUnion__Group_1__0 )* ) ;
    public final void rule__QueryUnion__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1495:1: ( ( ( rule__QueryUnion__Group_1__0 )* ) )
            // InternalConstraints.g:1496:1: ( ( rule__QueryUnion__Group_1__0 )* )
            {
            // InternalConstraints.g:1496:1: ( ( rule__QueryUnion__Group_1__0 )* )
            // InternalConstraints.g:1497:2: ( rule__QueryUnion__Group_1__0 )*
            {
             before(grammarAccess.getQueryUnionAccess().getGroup_1()); 
            // InternalConstraints.g:1498:2: ( rule__QueryUnion__Group_1__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>=12 && LA18_0<=13)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalConstraints.g:1498:3: rule__QueryUnion__Group_1__0
            	    {
            	    pushFollow(FOLLOW_16);
            	    rule__QueryUnion__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getQueryUnionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__Group__1__Impl"


    // $ANTLR start "rule__QueryUnion__Group_1__0"
    // InternalConstraints.g:1507:1: rule__QueryUnion__Group_1__0 : rule__QueryUnion__Group_1__0__Impl rule__QueryUnion__Group_1__1 ;
    public final void rule__QueryUnion__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1511:1: ( rule__QueryUnion__Group_1__0__Impl rule__QueryUnion__Group_1__1 )
            // InternalConstraints.g:1512:2: rule__QueryUnion__Group_1__0__Impl rule__QueryUnion__Group_1__1
            {
            pushFollow(FOLLOW_15);
            rule__QueryUnion__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryUnion__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__Group_1__0"


    // $ANTLR start "rule__QueryUnion__Group_1__0__Impl"
    // InternalConstraints.g:1519:1: rule__QueryUnion__Group_1__0__Impl : ( () ) ;
    public final void rule__QueryUnion__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1523:1: ( ( () ) )
            // InternalConstraints.g:1524:1: ( () )
            {
            // InternalConstraints.g:1524:1: ( () )
            // InternalConstraints.g:1525:2: ()
            {
             before(grammarAccess.getQueryUnionAccess().getQueryUnionLeftAction_1_0()); 
            // InternalConstraints.g:1526:2: ()
            // InternalConstraints.g:1526:3: 
            {
            }

             after(grammarAccess.getQueryUnionAccess().getQueryUnionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__Group_1__0__Impl"


    // $ANTLR start "rule__QueryUnion__Group_1__1"
    // InternalConstraints.g:1534:1: rule__QueryUnion__Group_1__1 : rule__QueryUnion__Group_1__1__Impl rule__QueryUnion__Group_1__2 ;
    public final void rule__QueryUnion__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1538:1: ( rule__QueryUnion__Group_1__1__Impl rule__QueryUnion__Group_1__2 )
            // InternalConstraints.g:1539:2: rule__QueryUnion__Group_1__1__Impl rule__QueryUnion__Group_1__2
            {
            pushFollow(FOLLOW_14);
            rule__QueryUnion__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryUnion__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__Group_1__1"


    // $ANTLR start "rule__QueryUnion__Group_1__1__Impl"
    // InternalConstraints.g:1546:1: rule__QueryUnion__Group_1__1__Impl : ( ( rule__QueryUnion__OpAssignment_1_1 ) ) ;
    public final void rule__QueryUnion__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1550:1: ( ( ( rule__QueryUnion__OpAssignment_1_1 ) ) )
            // InternalConstraints.g:1551:1: ( ( rule__QueryUnion__OpAssignment_1_1 ) )
            {
            // InternalConstraints.g:1551:1: ( ( rule__QueryUnion__OpAssignment_1_1 ) )
            // InternalConstraints.g:1552:2: ( rule__QueryUnion__OpAssignment_1_1 )
            {
             before(grammarAccess.getQueryUnionAccess().getOpAssignment_1_1()); 
            // InternalConstraints.g:1553:2: ( rule__QueryUnion__OpAssignment_1_1 )
            // InternalConstraints.g:1553:3: rule__QueryUnion__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__QueryUnion__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getQueryUnionAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__Group_1__1__Impl"


    // $ANTLR start "rule__QueryUnion__Group_1__2"
    // InternalConstraints.g:1561:1: rule__QueryUnion__Group_1__2 : rule__QueryUnion__Group_1__2__Impl ;
    public final void rule__QueryUnion__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1565:1: ( rule__QueryUnion__Group_1__2__Impl )
            // InternalConstraints.g:1566:2: rule__QueryUnion__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryUnion__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__Group_1__2"


    // $ANTLR start "rule__QueryUnion__Group_1__2__Impl"
    // InternalConstraints.g:1572:1: rule__QueryUnion__Group_1__2__Impl : ( ( rule__QueryUnion__RightAssignment_1_2 ) ) ;
    public final void rule__QueryUnion__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1576:1: ( ( ( rule__QueryUnion__RightAssignment_1_2 ) ) )
            // InternalConstraints.g:1577:1: ( ( rule__QueryUnion__RightAssignment_1_2 ) )
            {
            // InternalConstraints.g:1577:1: ( ( rule__QueryUnion__RightAssignment_1_2 ) )
            // InternalConstraints.g:1578:2: ( rule__QueryUnion__RightAssignment_1_2 )
            {
             before(grammarAccess.getQueryUnionAccess().getRightAssignment_1_2()); 
            // InternalConstraints.g:1579:2: ( rule__QueryUnion__RightAssignment_1_2 )
            // InternalConstraints.g:1579:3: rule__QueryUnion__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__QueryUnion__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getQueryUnionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__Group_1__2__Impl"


    // $ANTLR start "rule__QueryIntersection__Group__0"
    // InternalConstraints.g:1588:1: rule__QueryIntersection__Group__0 : rule__QueryIntersection__Group__0__Impl rule__QueryIntersection__Group__1 ;
    public final void rule__QueryIntersection__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1592:1: ( rule__QueryIntersection__Group__0__Impl rule__QueryIntersection__Group__1 )
            // InternalConstraints.g:1593:2: rule__QueryIntersection__Group__0__Impl rule__QueryIntersection__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__QueryIntersection__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryIntersection__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__Group__0"


    // $ANTLR start "rule__QueryIntersection__Group__0__Impl"
    // InternalConstraints.g:1600:1: rule__QueryIntersection__Group__0__Impl : ( ruleAtomicQueryExpression ) ;
    public final void rule__QueryIntersection__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1604:1: ( ( ruleAtomicQueryExpression ) )
            // InternalConstraints.g:1605:1: ( ruleAtomicQueryExpression )
            {
            // InternalConstraints.g:1605:1: ( ruleAtomicQueryExpression )
            // InternalConstraints.g:1606:2: ruleAtomicQueryExpression
            {
             before(grammarAccess.getQueryIntersectionAccess().getAtomicQueryExpressionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAtomicQueryExpression();

            state._fsp--;

             after(grammarAccess.getQueryIntersectionAccess().getAtomicQueryExpressionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__Group__0__Impl"


    // $ANTLR start "rule__QueryIntersection__Group__1"
    // InternalConstraints.g:1615:1: rule__QueryIntersection__Group__1 : rule__QueryIntersection__Group__1__Impl ;
    public final void rule__QueryIntersection__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1619:1: ( rule__QueryIntersection__Group__1__Impl )
            // InternalConstraints.g:1620:2: rule__QueryIntersection__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryIntersection__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__Group__1"


    // $ANTLR start "rule__QueryIntersection__Group__1__Impl"
    // InternalConstraints.g:1626:1: rule__QueryIntersection__Group__1__Impl : ( ( rule__QueryIntersection__Group_1__0 )* ) ;
    public final void rule__QueryIntersection__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1630:1: ( ( ( rule__QueryIntersection__Group_1__0 )* ) )
            // InternalConstraints.g:1631:1: ( ( rule__QueryIntersection__Group_1__0 )* )
            {
            // InternalConstraints.g:1631:1: ( ( rule__QueryIntersection__Group_1__0 )* )
            // InternalConstraints.g:1632:2: ( rule__QueryIntersection__Group_1__0 )*
            {
             before(grammarAccess.getQueryIntersectionAccess().getGroup_1()); 
            // InternalConstraints.g:1633:2: ( rule__QueryIntersection__Group_1__0 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>=14 && LA19_0<=15)) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalConstraints.g:1633:3: rule__QueryIntersection__Group_1__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__QueryIntersection__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getQueryIntersectionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__Group__1__Impl"


    // $ANTLR start "rule__QueryIntersection__Group_1__0"
    // InternalConstraints.g:1642:1: rule__QueryIntersection__Group_1__0 : rule__QueryIntersection__Group_1__0__Impl rule__QueryIntersection__Group_1__1 ;
    public final void rule__QueryIntersection__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1646:1: ( rule__QueryIntersection__Group_1__0__Impl rule__QueryIntersection__Group_1__1 )
            // InternalConstraints.g:1647:2: rule__QueryIntersection__Group_1__0__Impl rule__QueryIntersection__Group_1__1
            {
            pushFollow(FOLLOW_17);
            rule__QueryIntersection__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryIntersection__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__Group_1__0"


    // $ANTLR start "rule__QueryIntersection__Group_1__0__Impl"
    // InternalConstraints.g:1654:1: rule__QueryIntersection__Group_1__0__Impl : ( () ) ;
    public final void rule__QueryIntersection__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1658:1: ( ( () ) )
            // InternalConstraints.g:1659:1: ( () )
            {
            // InternalConstraints.g:1659:1: ( () )
            // InternalConstraints.g:1660:2: ()
            {
             before(grammarAccess.getQueryIntersectionAccess().getQueryIntersectionLeftAction_1_0()); 
            // InternalConstraints.g:1661:2: ()
            // InternalConstraints.g:1661:3: 
            {
            }

             after(grammarAccess.getQueryIntersectionAccess().getQueryIntersectionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__Group_1__0__Impl"


    // $ANTLR start "rule__QueryIntersection__Group_1__1"
    // InternalConstraints.g:1669:1: rule__QueryIntersection__Group_1__1 : rule__QueryIntersection__Group_1__1__Impl rule__QueryIntersection__Group_1__2 ;
    public final void rule__QueryIntersection__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1673:1: ( rule__QueryIntersection__Group_1__1__Impl rule__QueryIntersection__Group_1__2 )
            // InternalConstraints.g:1674:2: rule__QueryIntersection__Group_1__1__Impl rule__QueryIntersection__Group_1__2
            {
            pushFollow(FOLLOW_14);
            rule__QueryIntersection__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryIntersection__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__Group_1__1"


    // $ANTLR start "rule__QueryIntersection__Group_1__1__Impl"
    // InternalConstraints.g:1681:1: rule__QueryIntersection__Group_1__1__Impl : ( ( rule__QueryIntersection__OpAssignment_1_1 ) ) ;
    public final void rule__QueryIntersection__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1685:1: ( ( ( rule__QueryIntersection__OpAssignment_1_1 ) ) )
            // InternalConstraints.g:1686:1: ( ( rule__QueryIntersection__OpAssignment_1_1 ) )
            {
            // InternalConstraints.g:1686:1: ( ( rule__QueryIntersection__OpAssignment_1_1 ) )
            // InternalConstraints.g:1687:2: ( rule__QueryIntersection__OpAssignment_1_1 )
            {
             before(grammarAccess.getQueryIntersectionAccess().getOpAssignment_1_1()); 
            // InternalConstraints.g:1688:2: ( rule__QueryIntersection__OpAssignment_1_1 )
            // InternalConstraints.g:1688:3: rule__QueryIntersection__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__QueryIntersection__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getQueryIntersectionAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__Group_1__1__Impl"


    // $ANTLR start "rule__QueryIntersection__Group_1__2"
    // InternalConstraints.g:1696:1: rule__QueryIntersection__Group_1__2 : rule__QueryIntersection__Group_1__2__Impl ;
    public final void rule__QueryIntersection__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1700:1: ( rule__QueryIntersection__Group_1__2__Impl )
            // InternalConstraints.g:1701:2: rule__QueryIntersection__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryIntersection__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__Group_1__2"


    // $ANTLR start "rule__QueryIntersection__Group_1__2__Impl"
    // InternalConstraints.g:1707:1: rule__QueryIntersection__Group_1__2__Impl : ( ( rule__QueryIntersection__RightAssignment_1_2 ) ) ;
    public final void rule__QueryIntersection__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1711:1: ( ( ( rule__QueryIntersection__RightAssignment_1_2 ) ) )
            // InternalConstraints.g:1712:1: ( ( rule__QueryIntersection__RightAssignment_1_2 ) )
            {
            // InternalConstraints.g:1712:1: ( ( rule__QueryIntersection__RightAssignment_1_2 ) )
            // InternalConstraints.g:1713:2: ( rule__QueryIntersection__RightAssignment_1_2 )
            {
             before(grammarAccess.getQueryIntersectionAccess().getRightAssignment_1_2()); 
            // InternalConstraints.g:1714:2: ( rule__QueryIntersection__RightAssignment_1_2 )
            // InternalConstraints.g:1714:3: rule__QueryIntersection__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__QueryIntersection__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getQueryIntersectionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__Group_1__2__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_0__0"
    // InternalConstraints.g:1723:1: rule__AtomicQueryExpression__Group_0__0 : rule__AtomicQueryExpression__Group_0__0__Impl rule__AtomicQueryExpression__Group_0__1 ;
    public final void rule__AtomicQueryExpression__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1727:1: ( rule__AtomicQueryExpression__Group_0__0__Impl rule__AtomicQueryExpression__Group_0__1 )
            // InternalConstraints.g:1728:2: rule__AtomicQueryExpression__Group_0__0__Impl rule__AtomicQueryExpression__Group_0__1
            {
            pushFollow(FOLLOW_14);
            rule__AtomicQueryExpression__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_0__0"


    // $ANTLR start "rule__AtomicQueryExpression__Group_0__0__Impl"
    // InternalConstraints.g:1735:1: rule__AtomicQueryExpression__Group_0__0__Impl : ( '(' ) ;
    public final void rule__AtomicQueryExpression__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1739:1: ( ( '(' ) )
            // InternalConstraints.g:1740:1: ( '(' )
            {
            // InternalConstraints.g:1740:1: ( '(' )
            // InternalConstraints.g:1741:2: '('
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getLeftParenthesisKeyword_0_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getLeftParenthesisKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_0__0__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_0__1"
    // InternalConstraints.g:1750:1: rule__AtomicQueryExpression__Group_0__1 : rule__AtomicQueryExpression__Group_0__1__Impl rule__AtomicQueryExpression__Group_0__2 ;
    public final void rule__AtomicQueryExpression__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1754:1: ( rule__AtomicQueryExpression__Group_0__1__Impl rule__AtomicQueryExpression__Group_0__2 )
            // InternalConstraints.g:1755:2: rule__AtomicQueryExpression__Group_0__1__Impl rule__AtomicQueryExpression__Group_0__2
            {
            pushFollow(FOLLOW_19);
            rule__AtomicQueryExpression__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_0__1"


    // $ANTLR start "rule__AtomicQueryExpression__Group_0__1__Impl"
    // InternalConstraints.g:1762:1: rule__AtomicQueryExpression__Group_0__1__Impl : ( ruleQueryExpression ) ;
    public final void rule__AtomicQueryExpression__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1766:1: ( ( ruleQueryExpression ) )
            // InternalConstraints.g:1767:1: ( ruleQueryExpression )
            {
            // InternalConstraints.g:1767:1: ( ruleQueryExpression )
            // InternalConstraints.g:1768:2: ruleQueryExpression
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getQueryExpressionParserRuleCall_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQueryExpression();

            state._fsp--;

             after(grammarAccess.getAtomicQueryExpressionAccess().getQueryExpressionParserRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_0__1__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_0__2"
    // InternalConstraints.g:1777:1: rule__AtomicQueryExpression__Group_0__2 : rule__AtomicQueryExpression__Group_0__2__Impl ;
    public final void rule__AtomicQueryExpression__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1781:1: ( rule__AtomicQueryExpression__Group_0__2__Impl )
            // InternalConstraints.g:1782:2: rule__AtomicQueryExpression__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_0__2"


    // $ANTLR start "rule__AtomicQueryExpression__Group_0__2__Impl"
    // InternalConstraints.g:1788:1: rule__AtomicQueryExpression__Group_0__2__Impl : ( ')' ) ;
    public final void rule__AtomicQueryExpression__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1792:1: ( ( ')' ) )
            // InternalConstraints.g:1793:1: ( ')' )
            {
            // InternalConstraints.g:1793:1: ( ')' )
            // InternalConstraints.g:1794:2: ')'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getRightParenthesisKeyword_0_2()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getRightParenthesisKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_0__2__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_1__0"
    // InternalConstraints.g:1804:1: rule__AtomicQueryExpression__Group_1__0 : rule__AtomicQueryExpression__Group_1__0__Impl rule__AtomicQueryExpression__Group_1__1 ;
    public final void rule__AtomicQueryExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1808:1: ( rule__AtomicQueryExpression__Group_1__0__Impl rule__AtomicQueryExpression__Group_1__1 )
            // InternalConstraints.g:1809:2: rule__AtomicQueryExpression__Group_1__0__Impl rule__AtomicQueryExpression__Group_1__1
            {
            pushFollow(FOLLOW_6);
            rule__AtomicQueryExpression__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_1__0"


    // $ANTLR start "rule__AtomicQueryExpression__Group_1__0__Impl"
    // InternalConstraints.g:1816:1: rule__AtomicQueryExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__AtomicQueryExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1820:1: ( ( () ) )
            // InternalConstraints.g:1821:1: ( () )
            {
            // InternalConstraints.g:1821:1: ( () )
            // InternalConstraints.g:1822:2: ()
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getGroupQueryAction_1_0()); 
            // InternalConstraints.g:1823:2: ()
            // InternalConstraints.g:1823:3: 
            {
            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getGroupQueryAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_1__0__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_1__1"
    // InternalConstraints.g:1831:1: rule__AtomicQueryExpression__Group_1__1 : rule__AtomicQueryExpression__Group_1__1__Impl rule__AtomicQueryExpression__Group_1__2 ;
    public final void rule__AtomicQueryExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1835:1: ( rule__AtomicQueryExpression__Group_1__1__Impl rule__AtomicQueryExpression__Group_1__2 )
            // InternalConstraints.g:1836:2: rule__AtomicQueryExpression__Group_1__1__Impl rule__AtomicQueryExpression__Group_1__2
            {
            pushFollow(FOLLOW_20);
            rule__AtomicQueryExpression__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_1__1"


    // $ANTLR start "rule__AtomicQueryExpression__Group_1__1__Impl"
    // InternalConstraints.g:1843:1: rule__AtomicQueryExpression__Group_1__1__Impl : ( ( rule__AtomicQueryExpression__NameAssignment_1_1 ) ) ;
    public final void rule__AtomicQueryExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1847:1: ( ( ( rule__AtomicQueryExpression__NameAssignment_1_1 ) ) )
            // InternalConstraints.g:1848:1: ( ( rule__AtomicQueryExpression__NameAssignment_1_1 ) )
            {
            // InternalConstraints.g:1848:1: ( ( rule__AtomicQueryExpression__NameAssignment_1_1 ) )
            // InternalConstraints.g:1849:2: ( rule__AtomicQueryExpression__NameAssignment_1_1 )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNameAssignment_1_1()); 
            // InternalConstraints.g:1850:2: ( rule__AtomicQueryExpression__NameAssignment_1_1 )
            // InternalConstraints.g:1850:3: rule__AtomicQueryExpression__NameAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__NameAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNameAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_1__1__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_1__2"
    // InternalConstraints.g:1858:1: rule__AtomicQueryExpression__Group_1__2 : rule__AtomicQueryExpression__Group_1__2__Impl rule__AtomicQueryExpression__Group_1__3 ;
    public final void rule__AtomicQueryExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1862:1: ( rule__AtomicQueryExpression__Group_1__2__Impl rule__AtomicQueryExpression__Group_1__3 )
            // InternalConstraints.g:1863:2: rule__AtomicQueryExpression__Group_1__2__Impl rule__AtomicQueryExpression__Group_1__3
            {
            pushFollow(FOLLOW_20);
            rule__AtomicQueryExpression__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_1__2"


    // $ANTLR start "rule__AtomicQueryExpression__Group_1__2__Impl"
    // InternalConstraints.g:1870:1: rule__AtomicQueryExpression__Group_1__2__Impl : ( ( rule__AtomicQueryExpression__NotAssignment_1_2 )? ) ;
    public final void rule__AtomicQueryExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1874:1: ( ( ( rule__AtomicQueryExpression__NotAssignment_1_2 )? ) )
            // InternalConstraints.g:1875:1: ( ( rule__AtomicQueryExpression__NotAssignment_1_2 )? )
            {
            // InternalConstraints.g:1875:1: ( ( rule__AtomicQueryExpression__NotAssignment_1_2 )? )
            // InternalConstraints.g:1876:2: ( rule__AtomicQueryExpression__NotAssignment_1_2 )?
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNotAssignment_1_2()); 
            // InternalConstraints.g:1877:2: ( rule__AtomicQueryExpression__NotAssignment_1_2 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==46) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalConstraints.g:1877:3: rule__AtomicQueryExpression__NotAssignment_1_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtomicQueryExpression__NotAssignment_1_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNotAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_1__2__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_1__3"
    // InternalConstraints.g:1885:1: rule__AtomicQueryExpression__Group_1__3 : rule__AtomicQueryExpression__Group_1__3__Impl rule__AtomicQueryExpression__Group_1__4 ;
    public final void rule__AtomicQueryExpression__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1889:1: ( rule__AtomicQueryExpression__Group_1__3__Impl rule__AtomicQueryExpression__Group_1__4 )
            // InternalConstraints.g:1890:2: rule__AtomicQueryExpression__Group_1__3__Impl rule__AtomicQueryExpression__Group_1__4
            {
            pushFollow(FOLLOW_21);
            rule__AtomicQueryExpression__Group_1__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_1__3"


    // $ANTLR start "rule__AtomicQueryExpression__Group_1__3__Impl"
    // InternalConstraints.g:1897:1: rule__AtomicQueryExpression__Group_1__3__Impl : ( 'in' ) ;
    public final void rule__AtomicQueryExpression__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1901:1: ( ( 'in' ) )
            // InternalConstraints.g:1902:1: ( 'in' )
            {
            // InternalConstraints.g:1902:1: ( 'in' )
            // InternalConstraints.g:1903:2: 'in'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getInKeyword_1_3()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getInKeyword_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_1__3__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_1__4"
    // InternalConstraints.g:1912:1: rule__AtomicQueryExpression__Group_1__4 : rule__AtomicQueryExpression__Group_1__4__Impl rule__AtomicQueryExpression__Group_1__5 ;
    public final void rule__AtomicQueryExpression__Group_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1916:1: ( rule__AtomicQueryExpression__Group_1__4__Impl rule__AtomicQueryExpression__Group_1__5 )
            // InternalConstraints.g:1917:2: rule__AtomicQueryExpression__Group_1__4__Impl rule__AtomicQueryExpression__Group_1__5
            {
            pushFollow(FOLLOW_22);
            rule__AtomicQueryExpression__Group_1__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_1__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_1__4"


    // $ANTLR start "rule__AtomicQueryExpression__Group_1__4__Impl"
    // InternalConstraints.g:1924:1: rule__AtomicQueryExpression__Group_1__4__Impl : ( 'group' ) ;
    public final void rule__AtomicQueryExpression__Group_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1928:1: ( ( 'group' ) )
            // InternalConstraints.g:1929:1: ( 'group' )
            {
            // InternalConstraints.g:1929:1: ( 'group' )
            // InternalConstraints.g:1930:2: 'group'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getGroupKeyword_1_4()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getGroupKeyword_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_1__4__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_1__5"
    // InternalConstraints.g:1939:1: rule__AtomicQueryExpression__Group_1__5 : rule__AtomicQueryExpression__Group_1__5__Impl ;
    public final void rule__AtomicQueryExpression__Group_1__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1943:1: ( rule__AtomicQueryExpression__Group_1__5__Impl )
            // InternalConstraints.g:1944:2: rule__AtomicQueryExpression__Group_1__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_1__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_1__5"


    // $ANTLR start "rule__AtomicQueryExpression__Group_1__5__Impl"
    // InternalConstraints.g:1950:1: rule__AtomicQueryExpression__Group_1__5__Impl : ( ( rule__AtomicQueryExpression__GroupAssignment_1_5 ) ) ;
    public final void rule__AtomicQueryExpression__Group_1__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1954:1: ( ( ( rule__AtomicQueryExpression__GroupAssignment_1_5 ) ) )
            // InternalConstraints.g:1955:1: ( ( rule__AtomicQueryExpression__GroupAssignment_1_5 ) )
            {
            // InternalConstraints.g:1955:1: ( ( rule__AtomicQueryExpression__GroupAssignment_1_5 ) )
            // InternalConstraints.g:1956:2: ( rule__AtomicQueryExpression__GroupAssignment_1_5 )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getGroupAssignment_1_5()); 
            // InternalConstraints.g:1957:2: ( rule__AtomicQueryExpression__GroupAssignment_1_5 )
            // InternalConstraints.g:1957:3: rule__AtomicQueryExpression__GroupAssignment_1_5
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__GroupAssignment_1_5();

            state._fsp--;


            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getGroupAssignment_1_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_1__5__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__0"
    // InternalConstraints.g:1966:1: rule__AtomicQueryExpression__Group_2__0 : rule__AtomicQueryExpression__Group_2__0__Impl rule__AtomicQueryExpression__Group_2__1 ;
    public final void rule__AtomicQueryExpression__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1970:1: ( rule__AtomicQueryExpression__Group_2__0__Impl rule__AtomicQueryExpression__Group_2__1 )
            // InternalConstraints.g:1971:2: rule__AtomicQueryExpression__Group_2__0__Impl rule__AtomicQueryExpression__Group_2__1
            {
            pushFollow(FOLLOW_6);
            rule__AtomicQueryExpression__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__0"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__0__Impl"
    // InternalConstraints.g:1978:1: rule__AtomicQueryExpression__Group_2__0__Impl : ( () ) ;
    public final void rule__AtomicQueryExpression__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1982:1: ( ( () ) )
            // InternalConstraints.g:1983:1: ( () )
            {
            // InternalConstraints.g:1983:1: ( () )
            // InternalConstraints.g:1984:2: ()
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getRangeQueryAction_2_0()); 
            // InternalConstraints.g:1985:2: ()
            // InternalConstraints.g:1985:3: 
            {
            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getRangeQueryAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__0__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__1"
    // InternalConstraints.g:1993:1: rule__AtomicQueryExpression__Group_2__1 : rule__AtomicQueryExpression__Group_2__1__Impl rule__AtomicQueryExpression__Group_2__2 ;
    public final void rule__AtomicQueryExpression__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:1997:1: ( rule__AtomicQueryExpression__Group_2__1__Impl rule__AtomicQueryExpression__Group_2__2 )
            // InternalConstraints.g:1998:2: rule__AtomicQueryExpression__Group_2__1__Impl rule__AtomicQueryExpression__Group_2__2
            {
            pushFollow(FOLLOW_20);
            rule__AtomicQueryExpression__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__1"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__1__Impl"
    // InternalConstraints.g:2005:1: rule__AtomicQueryExpression__Group_2__1__Impl : ( ( rule__AtomicQueryExpression__NameAssignment_2_1 ) ) ;
    public final void rule__AtomicQueryExpression__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2009:1: ( ( ( rule__AtomicQueryExpression__NameAssignment_2_1 ) ) )
            // InternalConstraints.g:2010:1: ( ( rule__AtomicQueryExpression__NameAssignment_2_1 ) )
            {
            // InternalConstraints.g:2010:1: ( ( rule__AtomicQueryExpression__NameAssignment_2_1 ) )
            // InternalConstraints.g:2011:2: ( rule__AtomicQueryExpression__NameAssignment_2_1 )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNameAssignment_2_1()); 
            // InternalConstraints.g:2012:2: ( rule__AtomicQueryExpression__NameAssignment_2_1 )
            // InternalConstraints.g:2012:3: rule__AtomicQueryExpression__NameAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__NameAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNameAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__1__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__2"
    // InternalConstraints.g:2020:1: rule__AtomicQueryExpression__Group_2__2 : rule__AtomicQueryExpression__Group_2__2__Impl rule__AtomicQueryExpression__Group_2__3 ;
    public final void rule__AtomicQueryExpression__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2024:1: ( rule__AtomicQueryExpression__Group_2__2__Impl rule__AtomicQueryExpression__Group_2__3 )
            // InternalConstraints.g:2025:2: rule__AtomicQueryExpression__Group_2__2__Impl rule__AtomicQueryExpression__Group_2__3
            {
            pushFollow(FOLLOW_20);
            rule__AtomicQueryExpression__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__2"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__2__Impl"
    // InternalConstraints.g:2032:1: rule__AtomicQueryExpression__Group_2__2__Impl : ( ( rule__AtomicQueryExpression__NotAssignment_2_2 )? ) ;
    public final void rule__AtomicQueryExpression__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2036:1: ( ( ( rule__AtomicQueryExpression__NotAssignment_2_2 )? ) )
            // InternalConstraints.g:2037:1: ( ( rule__AtomicQueryExpression__NotAssignment_2_2 )? )
            {
            // InternalConstraints.g:2037:1: ( ( rule__AtomicQueryExpression__NotAssignment_2_2 )? )
            // InternalConstraints.g:2038:2: ( rule__AtomicQueryExpression__NotAssignment_2_2 )?
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNotAssignment_2_2()); 
            // InternalConstraints.g:2039:2: ( rule__AtomicQueryExpression__NotAssignment_2_2 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==46) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalConstraints.g:2039:3: rule__AtomicQueryExpression__NotAssignment_2_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtomicQueryExpression__NotAssignment_2_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNotAssignment_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__2__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__3"
    // InternalConstraints.g:2047:1: rule__AtomicQueryExpression__Group_2__3 : rule__AtomicQueryExpression__Group_2__3__Impl rule__AtomicQueryExpression__Group_2__4 ;
    public final void rule__AtomicQueryExpression__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2051:1: ( rule__AtomicQueryExpression__Group_2__3__Impl rule__AtomicQueryExpression__Group_2__4 )
            // InternalConstraints.g:2052:2: rule__AtomicQueryExpression__Group_2__3__Impl rule__AtomicQueryExpression__Group_2__4
            {
            pushFollow(FOLLOW_23);
            rule__AtomicQueryExpression__Group_2__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_2__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__3"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__3__Impl"
    // InternalConstraints.g:2059:1: rule__AtomicQueryExpression__Group_2__3__Impl : ( 'in' ) ;
    public final void rule__AtomicQueryExpression__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2063:1: ( ( 'in' ) )
            // InternalConstraints.g:2064:1: ( 'in' )
            {
            // InternalConstraints.g:2064:1: ( 'in' )
            // InternalConstraints.g:2065:2: 'in'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getInKeyword_2_3()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getInKeyword_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__3__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__4"
    // InternalConstraints.g:2074:1: rule__AtomicQueryExpression__Group_2__4 : rule__AtomicQueryExpression__Group_2__4__Impl rule__AtomicQueryExpression__Group_2__5 ;
    public final void rule__AtomicQueryExpression__Group_2__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2078:1: ( rule__AtomicQueryExpression__Group_2__4__Impl rule__AtomicQueryExpression__Group_2__5 )
            // InternalConstraints.g:2079:2: rule__AtomicQueryExpression__Group_2__4__Impl rule__AtomicQueryExpression__Group_2__5
            {
            pushFollow(FOLLOW_24);
            rule__AtomicQueryExpression__Group_2__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_2__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__4"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__4__Impl"
    // InternalConstraints.g:2086:1: rule__AtomicQueryExpression__Group_2__4__Impl : ( 'range' ) ;
    public final void rule__AtomicQueryExpression__Group_2__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2090:1: ( ( 'range' ) )
            // InternalConstraints.g:2091:1: ( 'range' )
            {
            // InternalConstraints.g:2091:1: ( 'range' )
            // InternalConstraints.g:2092:2: 'range'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getRangeKeyword_2_4()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getRangeKeyword_2_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__4__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__5"
    // InternalConstraints.g:2101:1: rule__AtomicQueryExpression__Group_2__5 : rule__AtomicQueryExpression__Group_2__5__Impl rule__AtomicQueryExpression__Group_2__6 ;
    public final void rule__AtomicQueryExpression__Group_2__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2105:1: ( rule__AtomicQueryExpression__Group_2__5__Impl rule__AtomicQueryExpression__Group_2__6 )
            // InternalConstraints.g:2106:2: rule__AtomicQueryExpression__Group_2__5__Impl rule__AtomicQueryExpression__Group_2__6
            {
            pushFollow(FOLLOW_25);
            rule__AtomicQueryExpression__Group_2__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_2__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__5"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__5__Impl"
    // InternalConstraints.g:2113:1: rule__AtomicQueryExpression__Group_2__5__Impl : ( ( rule__AtomicQueryExpression__RangeAssignment_2_5 ) ) ;
    public final void rule__AtomicQueryExpression__Group_2__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2117:1: ( ( ( rule__AtomicQueryExpression__RangeAssignment_2_5 ) ) )
            // InternalConstraints.g:2118:1: ( ( rule__AtomicQueryExpression__RangeAssignment_2_5 ) )
            {
            // InternalConstraints.g:2118:1: ( ( rule__AtomicQueryExpression__RangeAssignment_2_5 ) )
            // InternalConstraints.g:2119:2: ( rule__AtomicQueryExpression__RangeAssignment_2_5 )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getRangeAssignment_2_5()); 
            // InternalConstraints.g:2120:2: ( rule__AtomicQueryExpression__RangeAssignment_2_5 )
            // InternalConstraints.g:2120:3: rule__AtomicQueryExpression__RangeAssignment_2_5
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__RangeAssignment_2_5();

            state._fsp--;


            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getRangeAssignment_2_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__5__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__6"
    // InternalConstraints.g:2128:1: rule__AtomicQueryExpression__Group_2__6 : rule__AtomicQueryExpression__Group_2__6__Impl rule__AtomicQueryExpression__Group_2__7 ;
    public final void rule__AtomicQueryExpression__Group_2__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2132:1: ( rule__AtomicQueryExpression__Group_2__6__Impl rule__AtomicQueryExpression__Group_2__7 )
            // InternalConstraints.g:2133:2: rule__AtomicQueryExpression__Group_2__6__Impl rule__AtomicQueryExpression__Group_2__7
            {
            pushFollow(FOLLOW_6);
            rule__AtomicQueryExpression__Group_2__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_2__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__6"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__6__Impl"
    // InternalConstraints.g:2140:1: rule__AtomicQueryExpression__Group_2__6__Impl : ( 'of' ) ;
    public final void rule__AtomicQueryExpression__Group_2__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2144:1: ( ( 'of' ) )
            // InternalConstraints.g:2145:1: ( 'of' )
            {
            // InternalConstraints.g:2145:1: ( 'of' )
            // InternalConstraints.g:2146:2: 'of'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getOfKeyword_2_6()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getOfKeyword_2_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__6__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__7"
    // InternalConstraints.g:2155:1: rule__AtomicQueryExpression__Group_2__7 : rule__AtomicQueryExpression__Group_2__7__Impl ;
    public final void rule__AtomicQueryExpression__Group_2__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2159:1: ( rule__AtomicQueryExpression__Group_2__7__Impl )
            // InternalConstraints.g:2160:2: rule__AtomicQueryExpression__Group_2__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_2__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__7"


    // $ANTLR start "rule__AtomicQueryExpression__Group_2__7__Impl"
    // InternalConstraints.g:2166:1: rule__AtomicQueryExpression__Group_2__7__Impl : ( ( rule__AtomicQueryExpression__ReferenceAssignment_2_7 ) ) ;
    public final void rule__AtomicQueryExpression__Group_2__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2170:1: ( ( ( rule__AtomicQueryExpression__ReferenceAssignment_2_7 ) ) )
            // InternalConstraints.g:2171:1: ( ( rule__AtomicQueryExpression__ReferenceAssignment_2_7 ) )
            {
            // InternalConstraints.g:2171:1: ( ( rule__AtomicQueryExpression__ReferenceAssignment_2_7 ) )
            // InternalConstraints.g:2172:2: ( rule__AtomicQueryExpression__ReferenceAssignment_2_7 )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getReferenceAssignment_2_7()); 
            // InternalConstraints.g:2173:2: ( rule__AtomicQueryExpression__ReferenceAssignment_2_7 )
            // InternalConstraints.g:2173:3: rule__AtomicQueryExpression__ReferenceAssignment_2_7
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__ReferenceAssignment_2_7();

            state._fsp--;


            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getReferenceAssignment_2_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_2__7__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_3__0"
    // InternalConstraints.g:2182:1: rule__AtomicQueryExpression__Group_3__0 : rule__AtomicQueryExpression__Group_3__0__Impl rule__AtomicQueryExpression__Group_3__1 ;
    public final void rule__AtomicQueryExpression__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2186:1: ( rule__AtomicQueryExpression__Group_3__0__Impl rule__AtomicQueryExpression__Group_3__1 )
            // InternalConstraints.g:2187:2: rule__AtomicQueryExpression__Group_3__0__Impl rule__AtomicQueryExpression__Group_3__1
            {
            pushFollow(FOLLOW_6);
            rule__AtomicQueryExpression__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_3__0"


    // $ANTLR start "rule__AtomicQueryExpression__Group_3__0__Impl"
    // InternalConstraints.g:2194:1: rule__AtomicQueryExpression__Group_3__0__Impl : ( () ) ;
    public final void rule__AtomicQueryExpression__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2198:1: ( ( () ) )
            // InternalConstraints.g:2199:1: ( () )
            {
            // InternalConstraints.g:2199:1: ( () )
            // InternalConstraints.g:2200:2: ()
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getClassQueryAction_3_0()); 
            // InternalConstraints.g:2201:2: ()
            // InternalConstraints.g:2201:3: 
            {
            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getClassQueryAction_3_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_3__0__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_3__1"
    // InternalConstraints.g:2209:1: rule__AtomicQueryExpression__Group_3__1 : rule__AtomicQueryExpression__Group_3__1__Impl rule__AtomicQueryExpression__Group_3__2 ;
    public final void rule__AtomicQueryExpression__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2213:1: ( rule__AtomicQueryExpression__Group_3__1__Impl rule__AtomicQueryExpression__Group_3__2 )
            // InternalConstraints.g:2214:2: rule__AtomicQueryExpression__Group_3__1__Impl rule__AtomicQueryExpression__Group_3__2
            {
            pushFollow(FOLLOW_20);
            rule__AtomicQueryExpression__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_3__1"


    // $ANTLR start "rule__AtomicQueryExpression__Group_3__1__Impl"
    // InternalConstraints.g:2221:1: rule__AtomicQueryExpression__Group_3__1__Impl : ( ( rule__AtomicQueryExpression__NameAssignment_3_1 ) ) ;
    public final void rule__AtomicQueryExpression__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2225:1: ( ( ( rule__AtomicQueryExpression__NameAssignment_3_1 ) ) )
            // InternalConstraints.g:2226:1: ( ( rule__AtomicQueryExpression__NameAssignment_3_1 ) )
            {
            // InternalConstraints.g:2226:1: ( ( rule__AtomicQueryExpression__NameAssignment_3_1 ) )
            // InternalConstraints.g:2227:2: ( rule__AtomicQueryExpression__NameAssignment_3_1 )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNameAssignment_3_1()); 
            // InternalConstraints.g:2228:2: ( rule__AtomicQueryExpression__NameAssignment_3_1 )
            // InternalConstraints.g:2228:3: rule__AtomicQueryExpression__NameAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__NameAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNameAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_3__1__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_3__2"
    // InternalConstraints.g:2236:1: rule__AtomicQueryExpression__Group_3__2 : rule__AtomicQueryExpression__Group_3__2__Impl rule__AtomicQueryExpression__Group_3__3 ;
    public final void rule__AtomicQueryExpression__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2240:1: ( rule__AtomicQueryExpression__Group_3__2__Impl rule__AtomicQueryExpression__Group_3__3 )
            // InternalConstraints.g:2241:2: rule__AtomicQueryExpression__Group_3__2__Impl rule__AtomicQueryExpression__Group_3__3
            {
            pushFollow(FOLLOW_20);
            rule__AtomicQueryExpression__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_3__2"


    // $ANTLR start "rule__AtomicQueryExpression__Group_3__2__Impl"
    // InternalConstraints.g:2248:1: rule__AtomicQueryExpression__Group_3__2__Impl : ( ( rule__AtomicQueryExpression__NotAssignment_3_2 )? ) ;
    public final void rule__AtomicQueryExpression__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2252:1: ( ( ( rule__AtomicQueryExpression__NotAssignment_3_2 )? ) )
            // InternalConstraints.g:2253:1: ( ( rule__AtomicQueryExpression__NotAssignment_3_2 )? )
            {
            // InternalConstraints.g:2253:1: ( ( rule__AtomicQueryExpression__NotAssignment_3_2 )? )
            // InternalConstraints.g:2254:2: ( rule__AtomicQueryExpression__NotAssignment_3_2 )?
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNotAssignment_3_2()); 
            // InternalConstraints.g:2255:2: ( rule__AtomicQueryExpression__NotAssignment_3_2 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==46) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalConstraints.g:2255:3: rule__AtomicQueryExpression__NotAssignment_3_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtomicQueryExpression__NotAssignment_3_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNotAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_3__2__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_3__3"
    // InternalConstraints.g:2263:1: rule__AtomicQueryExpression__Group_3__3 : rule__AtomicQueryExpression__Group_3__3__Impl rule__AtomicQueryExpression__Group_3__4 ;
    public final void rule__AtomicQueryExpression__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2267:1: ( rule__AtomicQueryExpression__Group_3__3__Impl rule__AtomicQueryExpression__Group_3__4 )
            // InternalConstraints.g:2268:2: rule__AtomicQueryExpression__Group_3__3__Impl rule__AtomicQueryExpression__Group_3__4
            {
            pushFollow(FOLLOW_26);
            rule__AtomicQueryExpression__Group_3__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_3__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_3__3"


    // $ANTLR start "rule__AtomicQueryExpression__Group_3__3__Impl"
    // InternalConstraints.g:2275:1: rule__AtomicQueryExpression__Group_3__3__Impl : ( 'in' ) ;
    public final void rule__AtomicQueryExpression__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2279:1: ( ( 'in' ) )
            // InternalConstraints.g:2280:1: ( 'in' )
            {
            // InternalConstraints.g:2280:1: ( 'in' )
            // InternalConstraints.g:2281:2: 'in'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getInKeyword_3_3()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getInKeyword_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_3__3__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_3__4"
    // InternalConstraints.g:2290:1: rule__AtomicQueryExpression__Group_3__4 : rule__AtomicQueryExpression__Group_3__4__Impl rule__AtomicQueryExpression__Group_3__5 ;
    public final void rule__AtomicQueryExpression__Group_3__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2294:1: ( rule__AtomicQueryExpression__Group_3__4__Impl rule__AtomicQueryExpression__Group_3__5 )
            // InternalConstraints.g:2295:2: rule__AtomicQueryExpression__Group_3__4__Impl rule__AtomicQueryExpression__Group_3__5
            {
            pushFollow(FOLLOW_6);
            rule__AtomicQueryExpression__Group_3__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_3__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_3__4"


    // $ANTLR start "rule__AtomicQueryExpression__Group_3__4__Impl"
    // InternalConstraints.g:2302:1: rule__AtomicQueryExpression__Group_3__4__Impl : ( 'class' ) ;
    public final void rule__AtomicQueryExpression__Group_3__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2306:1: ( ( 'class' ) )
            // InternalConstraints.g:2307:1: ( 'class' )
            {
            // InternalConstraints.g:2307:1: ( 'class' )
            // InternalConstraints.g:2308:2: 'class'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getClassKeyword_3_4()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getClassKeyword_3_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_3__4__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_3__5"
    // InternalConstraints.g:2317:1: rule__AtomicQueryExpression__Group_3__5 : rule__AtomicQueryExpression__Group_3__5__Impl ;
    public final void rule__AtomicQueryExpression__Group_3__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2321:1: ( rule__AtomicQueryExpression__Group_3__5__Impl )
            // InternalConstraints.g:2322:2: rule__AtomicQueryExpression__Group_3__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_3__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_3__5"


    // $ANTLR start "rule__AtomicQueryExpression__Group_3__5__Impl"
    // InternalConstraints.g:2328:1: rule__AtomicQueryExpression__Group_3__5__Impl : ( ( rule__AtomicQueryExpression__ClazzAssignment_3_5 ) ) ;
    public final void rule__AtomicQueryExpression__Group_3__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2332:1: ( ( ( rule__AtomicQueryExpression__ClazzAssignment_3_5 ) ) )
            // InternalConstraints.g:2333:1: ( ( rule__AtomicQueryExpression__ClazzAssignment_3_5 ) )
            {
            // InternalConstraints.g:2333:1: ( ( rule__AtomicQueryExpression__ClazzAssignment_3_5 ) )
            // InternalConstraints.g:2334:2: ( rule__AtomicQueryExpression__ClazzAssignment_3_5 )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getClazzAssignment_3_5()); 
            // InternalConstraints.g:2335:2: ( rule__AtomicQueryExpression__ClazzAssignment_3_5 )
            // InternalConstraints.g:2335:3: rule__AtomicQueryExpression__ClazzAssignment_3_5
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__ClazzAssignment_3_5();

            state._fsp--;


            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getClazzAssignment_3_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_3__5__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_4__0"
    // InternalConstraints.g:2344:1: rule__AtomicQueryExpression__Group_4__0 : rule__AtomicQueryExpression__Group_4__0__Impl rule__AtomicQueryExpression__Group_4__1 ;
    public final void rule__AtomicQueryExpression__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2348:1: ( rule__AtomicQueryExpression__Group_4__0__Impl rule__AtomicQueryExpression__Group_4__1 )
            // InternalConstraints.g:2349:2: rule__AtomicQueryExpression__Group_4__0__Impl rule__AtomicQueryExpression__Group_4__1
            {
            pushFollow(FOLLOW_14);
            rule__AtomicQueryExpression__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_4__0"


    // $ANTLR start "rule__AtomicQueryExpression__Group_4__0__Impl"
    // InternalConstraints.g:2356:1: rule__AtomicQueryExpression__Group_4__0__Impl : ( () ) ;
    public final void rule__AtomicQueryExpression__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2360:1: ( ( () ) )
            // InternalConstraints.g:2361:1: ( () )
            {
            // InternalConstraints.g:2361:1: ( () )
            // InternalConstraints.g:2362:2: ()
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getClassQueryAction_4_0()); 
            // InternalConstraints.g:2363:2: ()
            // InternalConstraints.g:2363:3: 
            {
            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getClassQueryAction_4_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_4__0__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_4__1"
    // InternalConstraints.g:2371:1: rule__AtomicQueryExpression__Group_4__1 : rule__AtomicQueryExpression__Group_4__1__Impl rule__AtomicQueryExpression__Group_4__2 ;
    public final void rule__AtomicQueryExpression__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2375:1: ( rule__AtomicQueryExpression__Group_4__1__Impl rule__AtomicQueryExpression__Group_4__2 )
            // InternalConstraints.g:2376:2: rule__AtomicQueryExpression__Group_4__1__Impl rule__AtomicQueryExpression__Group_4__2
            {
            pushFollow(FOLLOW_27);
            rule__AtomicQueryExpression__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_4__1"


    // $ANTLR start "rule__AtomicQueryExpression__Group_4__1__Impl"
    // InternalConstraints.g:2383:1: rule__AtomicQueryExpression__Group_4__1__Impl : ( ( rule__AtomicQueryExpression__NameAssignment_4_1 ) ) ;
    public final void rule__AtomicQueryExpression__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2387:1: ( ( ( rule__AtomicQueryExpression__NameAssignment_4_1 ) ) )
            // InternalConstraints.g:2388:1: ( ( rule__AtomicQueryExpression__NameAssignment_4_1 ) )
            {
            // InternalConstraints.g:2388:1: ( ( rule__AtomicQueryExpression__NameAssignment_4_1 ) )
            // InternalConstraints.g:2389:2: ( rule__AtomicQueryExpression__NameAssignment_4_1 )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNameAssignment_4_1()); 
            // InternalConstraints.g:2390:2: ( rule__AtomicQueryExpression__NameAssignment_4_1 )
            // InternalConstraints.g:2390:3: rule__AtomicQueryExpression__NameAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__NameAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNameAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_4__1__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_4__2"
    // InternalConstraints.g:2398:1: rule__AtomicQueryExpression__Group_4__2 : rule__AtomicQueryExpression__Group_4__2__Impl rule__AtomicQueryExpression__Group_4__3 ;
    public final void rule__AtomicQueryExpression__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2402:1: ( rule__AtomicQueryExpression__Group_4__2__Impl rule__AtomicQueryExpression__Group_4__3 )
            // InternalConstraints.g:2403:2: rule__AtomicQueryExpression__Group_4__2__Impl rule__AtomicQueryExpression__Group_4__3
            {
            pushFollow(FOLLOW_28);
            rule__AtomicQueryExpression__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_4__2"


    // $ANTLR start "rule__AtomicQueryExpression__Group_4__2__Impl"
    // InternalConstraints.g:2410:1: rule__AtomicQueryExpression__Group_4__2__Impl : ( 'is' ) ;
    public final void rule__AtomicQueryExpression__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2414:1: ( ( 'is' ) )
            // InternalConstraints.g:2415:1: ( 'is' )
            {
            // InternalConstraints.g:2415:1: ( 'is' )
            // InternalConstraints.g:2416:2: 'is'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getIsKeyword_4_2()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getIsKeyword_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_4__2__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_4__3"
    // InternalConstraints.g:2425:1: rule__AtomicQueryExpression__Group_4__3 : rule__AtomicQueryExpression__Group_4__3__Impl rule__AtomicQueryExpression__Group_4__4 ;
    public final void rule__AtomicQueryExpression__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2429:1: ( rule__AtomicQueryExpression__Group_4__3__Impl rule__AtomicQueryExpression__Group_4__4 )
            // InternalConstraints.g:2430:2: rule__AtomicQueryExpression__Group_4__3__Impl rule__AtomicQueryExpression__Group_4__4
            {
            pushFollow(FOLLOW_28);
            rule__AtomicQueryExpression__Group_4__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_4__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_4__3"


    // $ANTLR start "rule__AtomicQueryExpression__Group_4__3__Impl"
    // InternalConstraints.g:2437:1: rule__AtomicQueryExpression__Group_4__3__Impl : ( ( rule__AtomicQueryExpression__NotAssignment_4_3 )? ) ;
    public final void rule__AtomicQueryExpression__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2441:1: ( ( ( rule__AtomicQueryExpression__NotAssignment_4_3 )? ) )
            // InternalConstraints.g:2442:1: ( ( rule__AtomicQueryExpression__NotAssignment_4_3 )? )
            {
            // InternalConstraints.g:2442:1: ( ( rule__AtomicQueryExpression__NotAssignment_4_3 )? )
            // InternalConstraints.g:2443:2: ( rule__AtomicQueryExpression__NotAssignment_4_3 )?
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNotAssignment_4_3()); 
            // InternalConstraints.g:2444:2: ( rule__AtomicQueryExpression__NotAssignment_4_3 )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==46) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalConstraints.g:2444:3: rule__AtomicQueryExpression__NotAssignment_4_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__AtomicQueryExpression__NotAssignment_4_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNotAssignment_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_4__3__Impl"


    // $ANTLR start "rule__AtomicQueryExpression__Group_4__4"
    // InternalConstraints.g:2452:1: rule__AtomicQueryExpression__Group_4__4 : rule__AtomicQueryExpression__Group_4__4__Impl ;
    public final void rule__AtomicQueryExpression__Group_4__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2456:1: ( rule__AtomicQueryExpression__Group_4__4__Impl )
            // InternalConstraints.g:2457:2: rule__AtomicQueryExpression__Group_4__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__Group_4__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_4__4"


    // $ANTLR start "rule__AtomicQueryExpression__Group_4__4__Impl"
    // InternalConstraints.g:2463:1: rule__AtomicQueryExpression__Group_4__4__Impl : ( ( rule__AtomicQueryExpression__ClazzAssignment_4_4 ) ) ;
    public final void rule__AtomicQueryExpression__Group_4__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2467:1: ( ( ( rule__AtomicQueryExpression__ClazzAssignment_4_4 ) ) )
            // InternalConstraints.g:2468:1: ( ( rule__AtomicQueryExpression__ClazzAssignment_4_4 ) )
            {
            // InternalConstraints.g:2468:1: ( ( rule__AtomicQueryExpression__ClazzAssignment_4_4 ) )
            // InternalConstraints.g:2469:2: ( rule__AtomicQueryExpression__ClazzAssignment_4_4 )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getClazzAssignment_4_4()); 
            // InternalConstraints.g:2470:2: ( rule__AtomicQueryExpression__ClazzAssignment_4_4 )
            // InternalConstraints.g:2470:3: rule__AtomicQueryExpression__ClazzAssignment_4_4
            {
            pushFollow(FOLLOW_2);
            rule__AtomicQueryExpression__ClazzAssignment_4_4();

            state._fsp--;


            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getClazzAssignment_4_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__Group_4__4__Impl"


    // $ANTLR start "rule__LogicalOr__Group__0"
    // InternalConstraints.g:2479:1: rule__LogicalOr__Group__0 : rule__LogicalOr__Group__0__Impl rule__LogicalOr__Group__1 ;
    public final void rule__LogicalOr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2483:1: ( rule__LogicalOr__Group__0__Impl rule__LogicalOr__Group__1 )
            // InternalConstraints.g:2484:2: rule__LogicalOr__Group__0__Impl rule__LogicalOr__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__LogicalOr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalOr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__Group__0"


    // $ANTLR start "rule__LogicalOr__Group__0__Impl"
    // InternalConstraints.g:2491:1: rule__LogicalOr__Group__0__Impl : ( ruleLogicalAnd ) ;
    public final void rule__LogicalOr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2495:1: ( ( ruleLogicalAnd ) )
            // InternalConstraints.g:2496:1: ( ruleLogicalAnd )
            {
            // InternalConstraints.g:2496:1: ( ruleLogicalAnd )
            // InternalConstraints.g:2497:2: ruleLogicalAnd
            {
             before(grammarAccess.getLogicalOrAccess().getLogicalAndParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalAnd();

            state._fsp--;

             after(grammarAccess.getLogicalOrAccess().getLogicalAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__Group__0__Impl"


    // $ANTLR start "rule__LogicalOr__Group__1"
    // InternalConstraints.g:2506:1: rule__LogicalOr__Group__1 : rule__LogicalOr__Group__1__Impl ;
    public final void rule__LogicalOr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2510:1: ( rule__LogicalOr__Group__1__Impl )
            // InternalConstraints.g:2511:2: rule__LogicalOr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalOr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__Group__1"


    // $ANTLR start "rule__LogicalOr__Group__1__Impl"
    // InternalConstraints.g:2517:1: rule__LogicalOr__Group__1__Impl : ( ( rule__LogicalOr__Group_1__0 )* ) ;
    public final void rule__LogicalOr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2521:1: ( ( ( rule__LogicalOr__Group_1__0 )* ) )
            // InternalConstraints.g:2522:1: ( ( rule__LogicalOr__Group_1__0 )* )
            {
            // InternalConstraints.g:2522:1: ( ( rule__LogicalOr__Group_1__0 )* )
            // InternalConstraints.g:2523:2: ( rule__LogicalOr__Group_1__0 )*
            {
             before(grammarAccess.getLogicalOrAccess().getGroup_1()); 
            // InternalConstraints.g:2524:2: ( rule__LogicalOr__Group_1__0 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( ((LA24_0>=12 && LA24_0<=13)) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalConstraints.g:2524:3: rule__LogicalOr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_16);
            	    rule__LogicalOr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getLogicalOrAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__Group__1__Impl"


    // $ANTLR start "rule__LogicalOr__Group_1__0"
    // InternalConstraints.g:2533:1: rule__LogicalOr__Group_1__0 : rule__LogicalOr__Group_1__0__Impl rule__LogicalOr__Group_1__1 ;
    public final void rule__LogicalOr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2537:1: ( rule__LogicalOr__Group_1__0__Impl rule__LogicalOr__Group_1__1 )
            // InternalConstraints.g:2538:2: rule__LogicalOr__Group_1__0__Impl rule__LogicalOr__Group_1__1
            {
            pushFollow(FOLLOW_15);
            rule__LogicalOr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalOr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__Group_1__0"


    // $ANTLR start "rule__LogicalOr__Group_1__0__Impl"
    // InternalConstraints.g:2545:1: rule__LogicalOr__Group_1__0__Impl : ( () ) ;
    public final void rule__LogicalOr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2549:1: ( ( () ) )
            // InternalConstraints.g:2550:1: ( () )
            {
            // InternalConstraints.g:2550:1: ( () )
            // InternalConstraints.g:2551:2: ()
            {
             before(grammarAccess.getLogicalOrAccess().getLogicalOrLeftAction_1_0()); 
            // InternalConstraints.g:2552:2: ()
            // InternalConstraints.g:2552:3: 
            {
            }

             after(grammarAccess.getLogicalOrAccess().getLogicalOrLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__Group_1__0__Impl"


    // $ANTLR start "rule__LogicalOr__Group_1__1"
    // InternalConstraints.g:2560:1: rule__LogicalOr__Group_1__1 : rule__LogicalOr__Group_1__1__Impl rule__LogicalOr__Group_1__2 ;
    public final void rule__LogicalOr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2564:1: ( rule__LogicalOr__Group_1__1__Impl rule__LogicalOr__Group_1__2 )
            // InternalConstraints.g:2565:2: rule__LogicalOr__Group_1__1__Impl rule__LogicalOr__Group_1__2
            {
            pushFollow(FOLLOW_11);
            rule__LogicalOr__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalOr__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__Group_1__1"


    // $ANTLR start "rule__LogicalOr__Group_1__1__Impl"
    // InternalConstraints.g:2572:1: rule__LogicalOr__Group_1__1__Impl : ( ( rule__LogicalOr__OpAssignment_1_1 ) ) ;
    public final void rule__LogicalOr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2576:1: ( ( ( rule__LogicalOr__OpAssignment_1_1 ) ) )
            // InternalConstraints.g:2577:1: ( ( rule__LogicalOr__OpAssignment_1_1 ) )
            {
            // InternalConstraints.g:2577:1: ( ( rule__LogicalOr__OpAssignment_1_1 ) )
            // InternalConstraints.g:2578:2: ( rule__LogicalOr__OpAssignment_1_1 )
            {
             before(grammarAccess.getLogicalOrAccess().getOpAssignment_1_1()); 
            // InternalConstraints.g:2579:2: ( rule__LogicalOr__OpAssignment_1_1 )
            // InternalConstraints.g:2579:3: rule__LogicalOr__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__LogicalOr__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getLogicalOrAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__Group_1__1__Impl"


    // $ANTLR start "rule__LogicalOr__Group_1__2"
    // InternalConstraints.g:2587:1: rule__LogicalOr__Group_1__2 : rule__LogicalOr__Group_1__2__Impl ;
    public final void rule__LogicalOr__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2591:1: ( rule__LogicalOr__Group_1__2__Impl )
            // InternalConstraints.g:2592:2: rule__LogicalOr__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalOr__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__Group_1__2"


    // $ANTLR start "rule__LogicalOr__Group_1__2__Impl"
    // InternalConstraints.g:2598:1: rule__LogicalOr__Group_1__2__Impl : ( ( rule__LogicalOr__RightAssignment_1_2 ) ) ;
    public final void rule__LogicalOr__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2602:1: ( ( ( rule__LogicalOr__RightAssignment_1_2 ) ) )
            // InternalConstraints.g:2603:1: ( ( rule__LogicalOr__RightAssignment_1_2 ) )
            {
            // InternalConstraints.g:2603:1: ( ( rule__LogicalOr__RightAssignment_1_2 ) )
            // InternalConstraints.g:2604:2: ( rule__LogicalOr__RightAssignment_1_2 )
            {
             before(grammarAccess.getLogicalOrAccess().getRightAssignment_1_2()); 
            // InternalConstraints.g:2605:2: ( rule__LogicalOr__RightAssignment_1_2 )
            // InternalConstraints.g:2605:3: rule__LogicalOr__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalOr__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalOrAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__Group_1__2__Impl"


    // $ANTLR start "rule__LogicalAnd__Group__0"
    // InternalConstraints.g:2614:1: rule__LogicalAnd__Group__0 : rule__LogicalAnd__Group__0__Impl rule__LogicalAnd__Group__1 ;
    public final void rule__LogicalAnd__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2618:1: ( rule__LogicalAnd__Group__0__Impl rule__LogicalAnd__Group__1 )
            // InternalConstraints.g:2619:2: rule__LogicalAnd__Group__0__Impl rule__LogicalAnd__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__LogicalAnd__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalAnd__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__Group__0"


    // $ANTLR start "rule__LogicalAnd__Group__0__Impl"
    // InternalConstraints.g:2626:1: rule__LogicalAnd__Group__0__Impl : ( ruleEquality ) ;
    public final void rule__LogicalAnd__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2630:1: ( ( ruleEquality ) )
            // InternalConstraints.g:2631:1: ( ruleEquality )
            {
            // InternalConstraints.g:2631:1: ( ruleEquality )
            // InternalConstraints.g:2632:2: ruleEquality
            {
             before(grammarAccess.getLogicalAndAccess().getEqualityParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleEquality();

            state._fsp--;

             after(grammarAccess.getLogicalAndAccess().getEqualityParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__Group__0__Impl"


    // $ANTLR start "rule__LogicalAnd__Group__1"
    // InternalConstraints.g:2641:1: rule__LogicalAnd__Group__1 : rule__LogicalAnd__Group__1__Impl ;
    public final void rule__LogicalAnd__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2645:1: ( rule__LogicalAnd__Group__1__Impl )
            // InternalConstraints.g:2646:2: rule__LogicalAnd__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalAnd__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__Group__1"


    // $ANTLR start "rule__LogicalAnd__Group__1__Impl"
    // InternalConstraints.g:2652:1: rule__LogicalAnd__Group__1__Impl : ( ( rule__LogicalAnd__Group_1__0 )* ) ;
    public final void rule__LogicalAnd__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2656:1: ( ( ( rule__LogicalAnd__Group_1__0 )* ) )
            // InternalConstraints.g:2657:1: ( ( rule__LogicalAnd__Group_1__0 )* )
            {
            // InternalConstraints.g:2657:1: ( ( rule__LogicalAnd__Group_1__0 )* )
            // InternalConstraints.g:2658:2: ( rule__LogicalAnd__Group_1__0 )*
            {
             before(grammarAccess.getLogicalAndAccess().getGroup_1()); 
            // InternalConstraints.g:2659:2: ( rule__LogicalAnd__Group_1__0 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( ((LA25_0>=14 && LA25_0<=15)) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalConstraints.g:2659:3: rule__LogicalAnd__Group_1__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__LogicalAnd__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

             after(grammarAccess.getLogicalAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__Group__1__Impl"


    // $ANTLR start "rule__LogicalAnd__Group_1__0"
    // InternalConstraints.g:2668:1: rule__LogicalAnd__Group_1__0 : rule__LogicalAnd__Group_1__0__Impl rule__LogicalAnd__Group_1__1 ;
    public final void rule__LogicalAnd__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2672:1: ( rule__LogicalAnd__Group_1__0__Impl rule__LogicalAnd__Group_1__1 )
            // InternalConstraints.g:2673:2: rule__LogicalAnd__Group_1__0__Impl rule__LogicalAnd__Group_1__1
            {
            pushFollow(FOLLOW_17);
            rule__LogicalAnd__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalAnd__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__Group_1__0"


    // $ANTLR start "rule__LogicalAnd__Group_1__0__Impl"
    // InternalConstraints.g:2680:1: rule__LogicalAnd__Group_1__0__Impl : ( () ) ;
    public final void rule__LogicalAnd__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2684:1: ( ( () ) )
            // InternalConstraints.g:2685:1: ( () )
            {
            // InternalConstraints.g:2685:1: ( () )
            // InternalConstraints.g:2686:2: ()
            {
             before(grammarAccess.getLogicalAndAccess().getLogicalAndLeftAction_1_0()); 
            // InternalConstraints.g:2687:2: ()
            // InternalConstraints.g:2687:3: 
            {
            }

             after(grammarAccess.getLogicalAndAccess().getLogicalAndLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__Group_1__0__Impl"


    // $ANTLR start "rule__LogicalAnd__Group_1__1"
    // InternalConstraints.g:2695:1: rule__LogicalAnd__Group_1__1 : rule__LogicalAnd__Group_1__1__Impl rule__LogicalAnd__Group_1__2 ;
    public final void rule__LogicalAnd__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2699:1: ( rule__LogicalAnd__Group_1__1__Impl rule__LogicalAnd__Group_1__2 )
            // InternalConstraints.g:2700:2: rule__LogicalAnd__Group_1__1__Impl rule__LogicalAnd__Group_1__2
            {
            pushFollow(FOLLOW_11);
            rule__LogicalAnd__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalAnd__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__Group_1__1"


    // $ANTLR start "rule__LogicalAnd__Group_1__1__Impl"
    // InternalConstraints.g:2707:1: rule__LogicalAnd__Group_1__1__Impl : ( ( rule__LogicalAnd__OpAssignment_1_1 ) ) ;
    public final void rule__LogicalAnd__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2711:1: ( ( ( rule__LogicalAnd__OpAssignment_1_1 ) ) )
            // InternalConstraints.g:2712:1: ( ( rule__LogicalAnd__OpAssignment_1_1 ) )
            {
            // InternalConstraints.g:2712:1: ( ( rule__LogicalAnd__OpAssignment_1_1 ) )
            // InternalConstraints.g:2713:2: ( rule__LogicalAnd__OpAssignment_1_1 )
            {
             before(grammarAccess.getLogicalAndAccess().getOpAssignment_1_1()); 
            // InternalConstraints.g:2714:2: ( rule__LogicalAnd__OpAssignment_1_1 )
            // InternalConstraints.g:2714:3: rule__LogicalAnd__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__LogicalAnd__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getLogicalAndAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__Group_1__1__Impl"


    // $ANTLR start "rule__LogicalAnd__Group_1__2"
    // InternalConstraints.g:2722:1: rule__LogicalAnd__Group_1__2 : rule__LogicalAnd__Group_1__2__Impl ;
    public final void rule__LogicalAnd__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2726:1: ( rule__LogicalAnd__Group_1__2__Impl )
            // InternalConstraints.g:2727:2: rule__LogicalAnd__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalAnd__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__Group_1__2"


    // $ANTLR start "rule__LogicalAnd__Group_1__2__Impl"
    // InternalConstraints.g:2733:1: rule__LogicalAnd__Group_1__2__Impl : ( ( rule__LogicalAnd__RightAssignment_1_2 ) ) ;
    public final void rule__LogicalAnd__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2737:1: ( ( ( rule__LogicalAnd__RightAssignment_1_2 ) ) )
            // InternalConstraints.g:2738:1: ( ( rule__LogicalAnd__RightAssignment_1_2 ) )
            {
            // InternalConstraints.g:2738:1: ( ( rule__LogicalAnd__RightAssignment_1_2 ) )
            // InternalConstraints.g:2739:2: ( rule__LogicalAnd__RightAssignment_1_2 )
            {
             before(grammarAccess.getLogicalAndAccess().getRightAssignment_1_2()); 
            // InternalConstraints.g:2740:2: ( rule__LogicalAnd__RightAssignment_1_2 )
            // InternalConstraints.g:2740:3: rule__LogicalAnd__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalAnd__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__Group_1__2__Impl"


    // $ANTLR start "rule__Equality__Group__0"
    // InternalConstraints.g:2749:1: rule__Equality__Group__0 : rule__Equality__Group__0__Impl rule__Equality__Group__1 ;
    public final void rule__Equality__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2753:1: ( rule__Equality__Group__0__Impl rule__Equality__Group__1 )
            // InternalConstraints.g:2754:2: rule__Equality__Group__0__Impl rule__Equality__Group__1
            {
            pushFollow(FOLLOW_29);
            rule__Equality__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Equality__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group__0"


    // $ANTLR start "rule__Equality__Group__0__Impl"
    // InternalConstraints.g:2761:1: rule__Equality__Group__0__Impl : ( ruleRelational ) ;
    public final void rule__Equality__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2765:1: ( ( ruleRelational ) )
            // InternalConstraints.g:2766:1: ( ruleRelational )
            {
            // InternalConstraints.g:2766:1: ( ruleRelational )
            // InternalConstraints.g:2767:2: ruleRelational
            {
             before(grammarAccess.getEqualityAccess().getRelationalParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleRelational();

            state._fsp--;

             after(grammarAccess.getEqualityAccess().getRelationalParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group__0__Impl"


    // $ANTLR start "rule__Equality__Group__1"
    // InternalConstraints.g:2776:1: rule__Equality__Group__1 : rule__Equality__Group__1__Impl ;
    public final void rule__Equality__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2780:1: ( rule__Equality__Group__1__Impl )
            // InternalConstraints.g:2781:2: rule__Equality__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Equality__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group__1"


    // $ANTLR start "rule__Equality__Group__1__Impl"
    // InternalConstraints.g:2787:1: rule__Equality__Group__1__Impl : ( ( rule__Equality__Group_1__0 )* ) ;
    public final void rule__Equality__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2791:1: ( ( ( rule__Equality__Group_1__0 )* ) )
            // InternalConstraints.g:2792:1: ( ( rule__Equality__Group_1__0 )* )
            {
            // InternalConstraints.g:2792:1: ( ( rule__Equality__Group_1__0 )* )
            // InternalConstraints.g:2793:2: ( rule__Equality__Group_1__0 )*
            {
             before(grammarAccess.getEqualityAccess().getGroup_1()); 
            // InternalConstraints.g:2794:2: ( rule__Equality__Group_1__0 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( ((LA26_0>=16 && LA26_0<=17)) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalConstraints.g:2794:3: rule__Equality__Group_1__0
            	    {
            	    pushFollow(FOLLOW_30);
            	    rule__Equality__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getEqualityAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group__1__Impl"


    // $ANTLR start "rule__Equality__Group_1__0"
    // InternalConstraints.g:2803:1: rule__Equality__Group_1__0 : rule__Equality__Group_1__0__Impl rule__Equality__Group_1__1 ;
    public final void rule__Equality__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2807:1: ( rule__Equality__Group_1__0__Impl rule__Equality__Group_1__1 )
            // InternalConstraints.g:2808:2: rule__Equality__Group_1__0__Impl rule__Equality__Group_1__1
            {
            pushFollow(FOLLOW_11);
            rule__Equality__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Equality__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1__0"


    // $ANTLR start "rule__Equality__Group_1__0__Impl"
    // InternalConstraints.g:2815:1: rule__Equality__Group_1__0__Impl : ( ( rule__Equality__Group_1_0__0 ) ) ;
    public final void rule__Equality__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2819:1: ( ( ( rule__Equality__Group_1_0__0 ) ) )
            // InternalConstraints.g:2820:1: ( ( rule__Equality__Group_1_0__0 ) )
            {
            // InternalConstraints.g:2820:1: ( ( rule__Equality__Group_1_0__0 ) )
            // InternalConstraints.g:2821:2: ( rule__Equality__Group_1_0__0 )
            {
             before(grammarAccess.getEqualityAccess().getGroup_1_0()); 
            // InternalConstraints.g:2822:2: ( rule__Equality__Group_1_0__0 )
            // InternalConstraints.g:2822:3: rule__Equality__Group_1_0__0
            {
            pushFollow(FOLLOW_2);
            rule__Equality__Group_1_0__0();

            state._fsp--;


            }

             after(grammarAccess.getEqualityAccess().getGroup_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1__0__Impl"


    // $ANTLR start "rule__Equality__Group_1__1"
    // InternalConstraints.g:2830:1: rule__Equality__Group_1__1 : rule__Equality__Group_1__1__Impl ;
    public final void rule__Equality__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2834:1: ( rule__Equality__Group_1__1__Impl )
            // InternalConstraints.g:2835:2: rule__Equality__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Equality__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1__1"


    // $ANTLR start "rule__Equality__Group_1__1__Impl"
    // InternalConstraints.g:2841:1: rule__Equality__Group_1__1__Impl : ( ( rule__Equality__RightAssignment_1_1 ) ) ;
    public final void rule__Equality__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2845:1: ( ( ( rule__Equality__RightAssignment_1_1 ) ) )
            // InternalConstraints.g:2846:1: ( ( rule__Equality__RightAssignment_1_1 ) )
            {
            // InternalConstraints.g:2846:1: ( ( rule__Equality__RightAssignment_1_1 ) )
            // InternalConstraints.g:2847:2: ( rule__Equality__RightAssignment_1_1 )
            {
             before(grammarAccess.getEqualityAccess().getRightAssignment_1_1()); 
            // InternalConstraints.g:2848:2: ( rule__Equality__RightAssignment_1_1 )
            // InternalConstraints.g:2848:3: rule__Equality__RightAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Equality__RightAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getEqualityAccess().getRightAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1__1__Impl"


    // $ANTLR start "rule__Equality__Group_1_0__0"
    // InternalConstraints.g:2857:1: rule__Equality__Group_1_0__0 : rule__Equality__Group_1_0__0__Impl rule__Equality__Group_1_0__1 ;
    public final void rule__Equality__Group_1_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2861:1: ( rule__Equality__Group_1_0__0__Impl rule__Equality__Group_1_0__1 )
            // InternalConstraints.g:2862:2: rule__Equality__Group_1_0__0__Impl rule__Equality__Group_1_0__1
            {
            pushFollow(FOLLOW_29);
            rule__Equality__Group_1_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Equality__Group_1_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1_0__0"


    // $ANTLR start "rule__Equality__Group_1_0__0__Impl"
    // InternalConstraints.g:2869:1: rule__Equality__Group_1_0__0__Impl : ( () ) ;
    public final void rule__Equality__Group_1_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2873:1: ( ( () ) )
            // InternalConstraints.g:2874:1: ( () )
            {
            // InternalConstraints.g:2874:1: ( () )
            // InternalConstraints.g:2875:2: ()
            {
             before(grammarAccess.getEqualityAccess().getEqualityLeftAction_1_0_0()); 
            // InternalConstraints.g:2876:2: ()
            // InternalConstraints.g:2876:3: 
            {
            }

             after(grammarAccess.getEqualityAccess().getEqualityLeftAction_1_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1_0__0__Impl"


    // $ANTLR start "rule__Equality__Group_1_0__1"
    // InternalConstraints.g:2884:1: rule__Equality__Group_1_0__1 : rule__Equality__Group_1_0__1__Impl ;
    public final void rule__Equality__Group_1_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2888:1: ( rule__Equality__Group_1_0__1__Impl )
            // InternalConstraints.g:2889:2: rule__Equality__Group_1_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Equality__Group_1_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1_0__1"


    // $ANTLR start "rule__Equality__Group_1_0__1__Impl"
    // InternalConstraints.g:2895:1: rule__Equality__Group_1_0__1__Impl : ( ( rule__Equality__OpAssignment_1_0_1 ) ) ;
    public final void rule__Equality__Group_1_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2899:1: ( ( ( rule__Equality__OpAssignment_1_0_1 ) ) )
            // InternalConstraints.g:2900:1: ( ( rule__Equality__OpAssignment_1_0_1 ) )
            {
            // InternalConstraints.g:2900:1: ( ( rule__Equality__OpAssignment_1_0_1 ) )
            // InternalConstraints.g:2901:2: ( rule__Equality__OpAssignment_1_0_1 )
            {
             before(grammarAccess.getEqualityAccess().getOpAssignment_1_0_1()); 
            // InternalConstraints.g:2902:2: ( rule__Equality__OpAssignment_1_0_1 )
            // InternalConstraints.g:2902:3: rule__Equality__OpAssignment_1_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Equality__OpAssignment_1_0_1();

            state._fsp--;


            }

             after(grammarAccess.getEqualityAccess().getOpAssignment_1_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__Group_1_0__1__Impl"


    // $ANTLR start "rule__Relational__Group__0"
    // InternalConstraints.g:2911:1: rule__Relational__Group__0 : rule__Relational__Group__0__Impl rule__Relational__Group__1 ;
    public final void rule__Relational__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2915:1: ( rule__Relational__Group__0__Impl rule__Relational__Group__1 )
            // InternalConstraints.g:2916:2: rule__Relational__Group__0__Impl rule__Relational__Group__1
            {
            pushFollow(FOLLOW_31);
            rule__Relational__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relational__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group__0"


    // $ANTLR start "rule__Relational__Group__0__Impl"
    // InternalConstraints.g:2923:1: rule__Relational__Group__0__Impl : ( ruleUnaryPrefix ) ;
    public final void rule__Relational__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2927:1: ( ( ruleUnaryPrefix ) )
            // InternalConstraints.g:2928:1: ( ruleUnaryPrefix )
            {
            // InternalConstraints.g:2928:1: ( ruleUnaryPrefix )
            // InternalConstraints.g:2929:2: ruleUnaryPrefix
            {
             before(grammarAccess.getRelationalAccess().getUnaryPrefixParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleUnaryPrefix();

            state._fsp--;

             after(grammarAccess.getRelationalAccess().getUnaryPrefixParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group__0__Impl"


    // $ANTLR start "rule__Relational__Group__1"
    // InternalConstraints.g:2938:1: rule__Relational__Group__1 : rule__Relational__Group__1__Impl ;
    public final void rule__Relational__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2942:1: ( rule__Relational__Group__1__Impl )
            // InternalConstraints.g:2943:2: rule__Relational__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Relational__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group__1"


    // $ANTLR start "rule__Relational__Group__1__Impl"
    // InternalConstraints.g:2949:1: rule__Relational__Group__1__Impl : ( ( rule__Relational__Group_1__0 )* ) ;
    public final void rule__Relational__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2953:1: ( ( ( rule__Relational__Group_1__0 )* ) )
            // InternalConstraints.g:2954:1: ( ( rule__Relational__Group_1__0 )* )
            {
            // InternalConstraints.g:2954:1: ( ( rule__Relational__Group_1__0 )* )
            // InternalConstraints.g:2955:2: ( rule__Relational__Group_1__0 )*
            {
             before(grammarAccess.getRelationalAccess().getGroup_1()); 
            // InternalConstraints.g:2956:2: ( rule__Relational__Group_1__0 )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( ((LA27_0>=39 && LA27_0<=42)) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalConstraints.g:2956:3: rule__Relational__Group_1__0
            	    {
            	    pushFollow(FOLLOW_32);
            	    rule__Relational__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getRelationalAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group__1__Impl"


    // $ANTLR start "rule__Relational__Group_1__0"
    // InternalConstraints.g:2965:1: rule__Relational__Group_1__0 : rule__Relational__Group_1__0__Impl rule__Relational__Group_1__1 ;
    public final void rule__Relational__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2969:1: ( rule__Relational__Group_1__0__Impl rule__Relational__Group_1__1 )
            // InternalConstraints.g:2970:2: rule__Relational__Group_1__0__Impl rule__Relational__Group_1__1
            {
            pushFollow(FOLLOW_11);
            rule__Relational__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relational__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1__0"


    // $ANTLR start "rule__Relational__Group_1__0__Impl"
    // InternalConstraints.g:2977:1: rule__Relational__Group_1__0__Impl : ( ( rule__Relational__Alternatives_1_0 ) ) ;
    public final void rule__Relational__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2981:1: ( ( ( rule__Relational__Alternatives_1_0 ) ) )
            // InternalConstraints.g:2982:1: ( ( rule__Relational__Alternatives_1_0 ) )
            {
            // InternalConstraints.g:2982:1: ( ( rule__Relational__Alternatives_1_0 ) )
            // InternalConstraints.g:2983:2: ( rule__Relational__Alternatives_1_0 )
            {
             before(grammarAccess.getRelationalAccess().getAlternatives_1_0()); 
            // InternalConstraints.g:2984:2: ( rule__Relational__Alternatives_1_0 )
            // InternalConstraints.g:2984:3: rule__Relational__Alternatives_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Relational__Alternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getRelationalAccess().getAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1__0__Impl"


    // $ANTLR start "rule__Relational__Group_1__1"
    // InternalConstraints.g:2992:1: rule__Relational__Group_1__1 : rule__Relational__Group_1__1__Impl ;
    public final void rule__Relational__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:2996:1: ( rule__Relational__Group_1__1__Impl )
            // InternalConstraints.g:2997:2: rule__Relational__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Relational__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1__1"


    // $ANTLR start "rule__Relational__Group_1__1__Impl"
    // InternalConstraints.g:3003:1: rule__Relational__Group_1__1__Impl : ( ( rule__Relational__RightAssignment_1_1 ) ) ;
    public final void rule__Relational__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3007:1: ( ( ( rule__Relational__RightAssignment_1_1 ) ) )
            // InternalConstraints.g:3008:1: ( ( rule__Relational__RightAssignment_1_1 ) )
            {
            // InternalConstraints.g:3008:1: ( ( rule__Relational__RightAssignment_1_1 ) )
            // InternalConstraints.g:3009:2: ( rule__Relational__RightAssignment_1_1 )
            {
             before(grammarAccess.getRelationalAccess().getRightAssignment_1_1()); 
            // InternalConstraints.g:3010:2: ( rule__Relational__RightAssignment_1_1 )
            // InternalConstraints.g:3010:3: rule__Relational__RightAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Relational__RightAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getRelationalAccess().getRightAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1__1__Impl"


    // $ANTLR start "rule__Relational__Group_1_0_0__0"
    // InternalConstraints.g:3019:1: rule__Relational__Group_1_0_0__0 : rule__Relational__Group_1_0_0__0__Impl rule__Relational__Group_1_0_0__1 ;
    public final void rule__Relational__Group_1_0_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3023:1: ( rule__Relational__Group_1_0_0__0__Impl rule__Relational__Group_1_0_0__1 )
            // InternalConstraints.g:3024:2: rule__Relational__Group_1_0_0__0__Impl rule__Relational__Group_1_0_0__1
            {
            pushFollow(FOLLOW_33);
            rule__Relational__Group_1_0_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relational__Group_1_0_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_0__0"


    // $ANTLR start "rule__Relational__Group_1_0_0__0__Impl"
    // InternalConstraints.g:3031:1: rule__Relational__Group_1_0_0__0__Impl : ( () ) ;
    public final void rule__Relational__Group_1_0_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3035:1: ( ( () ) )
            // InternalConstraints.g:3036:1: ( () )
            {
            // InternalConstraints.g:3036:1: ( () )
            // InternalConstraints.g:3037:2: ()
            {
             before(grammarAccess.getRelationalAccess().getGreaterLeftAction_1_0_0_0()); 
            // InternalConstraints.g:3038:2: ()
            // InternalConstraints.g:3038:3: 
            {
            }

             after(grammarAccess.getRelationalAccess().getGreaterLeftAction_1_0_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_0__0__Impl"


    // $ANTLR start "rule__Relational__Group_1_0_0__1"
    // InternalConstraints.g:3046:1: rule__Relational__Group_1_0_0__1 : rule__Relational__Group_1_0_0__1__Impl ;
    public final void rule__Relational__Group_1_0_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3050:1: ( rule__Relational__Group_1_0_0__1__Impl )
            // InternalConstraints.g:3051:2: rule__Relational__Group_1_0_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Relational__Group_1_0_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_0__1"


    // $ANTLR start "rule__Relational__Group_1_0_0__1__Impl"
    // InternalConstraints.g:3057:1: rule__Relational__Group_1_0_0__1__Impl : ( '>' ) ;
    public final void rule__Relational__Group_1_0_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3061:1: ( ( '>' ) )
            // InternalConstraints.g:3062:1: ( '>' )
            {
            // InternalConstraints.g:3062:1: ( '>' )
            // InternalConstraints.g:3063:2: '>'
            {
             before(grammarAccess.getRelationalAccess().getGreaterThanSignKeyword_1_0_0_1()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getRelationalAccess().getGreaterThanSignKeyword_1_0_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_0__1__Impl"


    // $ANTLR start "rule__Relational__Group_1_0_1__0"
    // InternalConstraints.g:3073:1: rule__Relational__Group_1_0_1__0 : rule__Relational__Group_1_0_1__0__Impl rule__Relational__Group_1_0_1__1 ;
    public final void rule__Relational__Group_1_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3077:1: ( rule__Relational__Group_1_0_1__0__Impl rule__Relational__Group_1_0_1__1 )
            // InternalConstraints.g:3078:2: rule__Relational__Group_1_0_1__0__Impl rule__Relational__Group_1_0_1__1
            {
            pushFollow(FOLLOW_34);
            rule__Relational__Group_1_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relational__Group_1_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_1__0"


    // $ANTLR start "rule__Relational__Group_1_0_1__0__Impl"
    // InternalConstraints.g:3085:1: rule__Relational__Group_1_0_1__0__Impl : ( () ) ;
    public final void rule__Relational__Group_1_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3089:1: ( ( () ) )
            // InternalConstraints.g:3090:1: ( () )
            {
            // InternalConstraints.g:3090:1: ( () )
            // InternalConstraints.g:3091:2: ()
            {
             before(grammarAccess.getRelationalAccess().getSmallerLeftAction_1_0_1_0()); 
            // InternalConstraints.g:3092:2: ()
            // InternalConstraints.g:3092:3: 
            {
            }

             after(grammarAccess.getRelationalAccess().getSmallerLeftAction_1_0_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_1__0__Impl"


    // $ANTLR start "rule__Relational__Group_1_0_1__1"
    // InternalConstraints.g:3100:1: rule__Relational__Group_1_0_1__1 : rule__Relational__Group_1_0_1__1__Impl ;
    public final void rule__Relational__Group_1_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3104:1: ( rule__Relational__Group_1_0_1__1__Impl )
            // InternalConstraints.g:3105:2: rule__Relational__Group_1_0_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Relational__Group_1_0_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_1__1"


    // $ANTLR start "rule__Relational__Group_1_0_1__1__Impl"
    // InternalConstraints.g:3111:1: rule__Relational__Group_1_0_1__1__Impl : ( '<' ) ;
    public final void rule__Relational__Group_1_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3115:1: ( ( '<' ) )
            // InternalConstraints.g:3116:1: ( '<' )
            {
            // InternalConstraints.g:3116:1: ( '<' )
            // InternalConstraints.g:3117:2: '<'
            {
             before(grammarAccess.getRelationalAccess().getLessThanSignKeyword_1_0_1_1()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getRelationalAccess().getLessThanSignKeyword_1_0_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_1__1__Impl"


    // $ANTLR start "rule__Relational__Group_1_0_2__0"
    // InternalConstraints.g:3127:1: rule__Relational__Group_1_0_2__0 : rule__Relational__Group_1_0_2__0__Impl rule__Relational__Group_1_0_2__1 ;
    public final void rule__Relational__Group_1_0_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3131:1: ( rule__Relational__Group_1_0_2__0__Impl rule__Relational__Group_1_0_2__1 )
            // InternalConstraints.g:3132:2: rule__Relational__Group_1_0_2__0__Impl rule__Relational__Group_1_0_2__1
            {
            pushFollow(FOLLOW_35);
            rule__Relational__Group_1_0_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relational__Group_1_0_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_2__0"


    // $ANTLR start "rule__Relational__Group_1_0_2__0__Impl"
    // InternalConstraints.g:3139:1: rule__Relational__Group_1_0_2__0__Impl : ( () ) ;
    public final void rule__Relational__Group_1_0_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3143:1: ( ( () ) )
            // InternalConstraints.g:3144:1: ( () )
            {
            // InternalConstraints.g:3144:1: ( () )
            // InternalConstraints.g:3145:2: ()
            {
             before(grammarAccess.getRelationalAccess().getGreaterOrEqualLeftAction_1_0_2_0()); 
            // InternalConstraints.g:3146:2: ()
            // InternalConstraints.g:3146:3: 
            {
            }

             after(grammarAccess.getRelationalAccess().getGreaterOrEqualLeftAction_1_0_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_2__0__Impl"


    // $ANTLR start "rule__Relational__Group_1_0_2__1"
    // InternalConstraints.g:3154:1: rule__Relational__Group_1_0_2__1 : rule__Relational__Group_1_0_2__1__Impl ;
    public final void rule__Relational__Group_1_0_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3158:1: ( rule__Relational__Group_1_0_2__1__Impl )
            // InternalConstraints.g:3159:2: rule__Relational__Group_1_0_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Relational__Group_1_0_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_2__1"


    // $ANTLR start "rule__Relational__Group_1_0_2__1__Impl"
    // InternalConstraints.g:3165:1: rule__Relational__Group_1_0_2__1__Impl : ( '>=' ) ;
    public final void rule__Relational__Group_1_0_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3169:1: ( ( '>=' ) )
            // InternalConstraints.g:3170:1: ( '>=' )
            {
            // InternalConstraints.g:3170:1: ( '>=' )
            // InternalConstraints.g:3171:2: '>='
            {
             before(grammarAccess.getRelationalAccess().getGreaterThanSignEqualsSignKeyword_1_0_2_1()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getRelationalAccess().getGreaterThanSignEqualsSignKeyword_1_0_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_2__1__Impl"


    // $ANTLR start "rule__Relational__Group_1_0_3__0"
    // InternalConstraints.g:3181:1: rule__Relational__Group_1_0_3__0 : rule__Relational__Group_1_0_3__0__Impl rule__Relational__Group_1_0_3__1 ;
    public final void rule__Relational__Group_1_0_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3185:1: ( rule__Relational__Group_1_0_3__0__Impl rule__Relational__Group_1_0_3__1 )
            // InternalConstraints.g:3186:2: rule__Relational__Group_1_0_3__0__Impl rule__Relational__Group_1_0_3__1
            {
            pushFollow(FOLLOW_31);
            rule__Relational__Group_1_0_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relational__Group_1_0_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_3__0"


    // $ANTLR start "rule__Relational__Group_1_0_3__0__Impl"
    // InternalConstraints.g:3193:1: rule__Relational__Group_1_0_3__0__Impl : ( () ) ;
    public final void rule__Relational__Group_1_0_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3197:1: ( ( () ) )
            // InternalConstraints.g:3198:1: ( () )
            {
            // InternalConstraints.g:3198:1: ( () )
            // InternalConstraints.g:3199:2: ()
            {
             before(grammarAccess.getRelationalAccess().getSmallerOrEqualLeftAction_1_0_3_0()); 
            // InternalConstraints.g:3200:2: ()
            // InternalConstraints.g:3200:3: 
            {
            }

             after(grammarAccess.getRelationalAccess().getSmallerOrEqualLeftAction_1_0_3_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_3__0__Impl"


    // $ANTLR start "rule__Relational__Group_1_0_3__1"
    // InternalConstraints.g:3208:1: rule__Relational__Group_1_0_3__1 : rule__Relational__Group_1_0_3__1__Impl ;
    public final void rule__Relational__Group_1_0_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3212:1: ( rule__Relational__Group_1_0_3__1__Impl )
            // InternalConstraints.g:3213:2: rule__Relational__Group_1_0_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Relational__Group_1_0_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_3__1"


    // $ANTLR start "rule__Relational__Group_1_0_3__1__Impl"
    // InternalConstraints.g:3219:1: rule__Relational__Group_1_0_3__1__Impl : ( '<=' ) ;
    public final void rule__Relational__Group_1_0_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3223:1: ( ( '<=' ) )
            // InternalConstraints.g:3224:1: ( '<=' )
            {
            // InternalConstraints.g:3224:1: ( '<=' )
            // InternalConstraints.g:3225:2: '<='
            {
             before(grammarAccess.getRelationalAccess().getLessThanSignEqualsSignKeyword_1_0_3_1()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getRelationalAccess().getLessThanSignEqualsSignKeyword_1_0_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__Group_1_0_3__1__Impl"


    // $ANTLR start "rule__UnaryPrefix__Group_1__0"
    // InternalConstraints.g:3235:1: rule__UnaryPrefix__Group_1__0 : rule__UnaryPrefix__Group_1__0__Impl rule__UnaryPrefix__Group_1__1 ;
    public final void rule__UnaryPrefix__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3239:1: ( rule__UnaryPrefix__Group_1__0__Impl rule__UnaryPrefix__Group_1__1 )
            // InternalConstraints.g:3240:2: rule__UnaryPrefix__Group_1__0__Impl rule__UnaryPrefix__Group_1__1
            {
            pushFollow(FOLLOW_36);
            rule__UnaryPrefix__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnaryPrefix__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Group_1__0"


    // $ANTLR start "rule__UnaryPrefix__Group_1__0__Impl"
    // InternalConstraints.g:3247:1: rule__UnaryPrefix__Group_1__0__Impl : ( () ) ;
    public final void rule__UnaryPrefix__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3251:1: ( ( () ) )
            // InternalConstraints.g:3252:1: ( () )
            {
            // InternalConstraints.g:3252:1: ( () )
            // InternalConstraints.g:3253:2: ()
            {
             before(grammarAccess.getUnaryPrefixAccess().getUnitaryMinusAction_1_0()); 
            // InternalConstraints.g:3254:2: ()
            // InternalConstraints.g:3254:3: 
            {
            }

             after(grammarAccess.getUnaryPrefixAccess().getUnitaryMinusAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Group_1__0__Impl"


    // $ANTLR start "rule__UnaryPrefix__Group_1__1"
    // InternalConstraints.g:3262:1: rule__UnaryPrefix__Group_1__1 : rule__UnaryPrefix__Group_1__1__Impl rule__UnaryPrefix__Group_1__2 ;
    public final void rule__UnaryPrefix__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3266:1: ( rule__UnaryPrefix__Group_1__1__Impl rule__UnaryPrefix__Group_1__2 )
            // InternalConstraints.g:3267:2: rule__UnaryPrefix__Group_1__1__Impl rule__UnaryPrefix__Group_1__2
            {
            pushFollow(FOLLOW_11);
            rule__UnaryPrefix__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnaryPrefix__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Group_1__1"


    // $ANTLR start "rule__UnaryPrefix__Group_1__1__Impl"
    // InternalConstraints.g:3274:1: rule__UnaryPrefix__Group_1__1__Impl : ( '-' ) ;
    public final void rule__UnaryPrefix__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3278:1: ( ( '-' ) )
            // InternalConstraints.g:3279:1: ( '-' )
            {
            // InternalConstraints.g:3279:1: ( '-' )
            // InternalConstraints.g:3280:2: '-'
            {
             before(grammarAccess.getUnaryPrefixAccess().getHyphenMinusKeyword_1_1()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getUnaryPrefixAccess().getHyphenMinusKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Group_1__1__Impl"


    // $ANTLR start "rule__UnaryPrefix__Group_1__2"
    // InternalConstraints.g:3289:1: rule__UnaryPrefix__Group_1__2 : rule__UnaryPrefix__Group_1__2__Impl ;
    public final void rule__UnaryPrefix__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3293:1: ( rule__UnaryPrefix__Group_1__2__Impl )
            // InternalConstraints.g:3294:2: rule__UnaryPrefix__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UnaryPrefix__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Group_1__2"


    // $ANTLR start "rule__UnaryPrefix__Group_1__2__Impl"
    // InternalConstraints.g:3300:1: rule__UnaryPrefix__Group_1__2__Impl : ( ( rule__UnaryPrefix__ExpressionAssignment_1_2 ) ) ;
    public final void rule__UnaryPrefix__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3304:1: ( ( ( rule__UnaryPrefix__ExpressionAssignment_1_2 ) ) )
            // InternalConstraints.g:3305:1: ( ( rule__UnaryPrefix__ExpressionAssignment_1_2 ) )
            {
            // InternalConstraints.g:3305:1: ( ( rule__UnaryPrefix__ExpressionAssignment_1_2 ) )
            // InternalConstraints.g:3306:2: ( rule__UnaryPrefix__ExpressionAssignment_1_2 )
            {
             before(grammarAccess.getUnaryPrefixAccess().getExpressionAssignment_1_2()); 
            // InternalConstraints.g:3307:2: ( rule__UnaryPrefix__ExpressionAssignment_1_2 )
            // InternalConstraints.g:3307:3: rule__UnaryPrefix__ExpressionAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__UnaryPrefix__ExpressionAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getUnaryPrefixAccess().getExpressionAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Group_1__2__Impl"


    // $ANTLR start "rule__UnaryPrefix__Group_2__0"
    // InternalConstraints.g:3316:1: rule__UnaryPrefix__Group_2__0 : rule__UnaryPrefix__Group_2__0__Impl rule__UnaryPrefix__Group_2__1 ;
    public final void rule__UnaryPrefix__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3320:1: ( rule__UnaryPrefix__Group_2__0__Impl rule__UnaryPrefix__Group_2__1 )
            // InternalConstraints.g:3321:2: rule__UnaryPrefix__Group_2__0__Impl rule__UnaryPrefix__Group_2__1
            {
            pushFollow(FOLLOW_11);
            rule__UnaryPrefix__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnaryPrefix__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Group_2__0"


    // $ANTLR start "rule__UnaryPrefix__Group_2__0__Impl"
    // InternalConstraints.g:3328:1: rule__UnaryPrefix__Group_2__0__Impl : ( () ) ;
    public final void rule__UnaryPrefix__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3332:1: ( ( () ) )
            // InternalConstraints.g:3333:1: ( () )
            {
            // InternalConstraints.g:3333:1: ( () )
            // InternalConstraints.g:3334:2: ()
            {
             before(grammarAccess.getUnaryPrefixAccess().getUnitaryNotAction_2_0()); 
            // InternalConstraints.g:3335:2: ()
            // InternalConstraints.g:3335:3: 
            {
            }

             after(grammarAccess.getUnaryPrefixAccess().getUnitaryNotAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Group_2__0__Impl"


    // $ANTLR start "rule__UnaryPrefix__Group_2__1"
    // InternalConstraints.g:3343:1: rule__UnaryPrefix__Group_2__1 : rule__UnaryPrefix__Group_2__1__Impl rule__UnaryPrefix__Group_2__2 ;
    public final void rule__UnaryPrefix__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3347:1: ( rule__UnaryPrefix__Group_2__1__Impl rule__UnaryPrefix__Group_2__2 )
            // InternalConstraints.g:3348:2: rule__UnaryPrefix__Group_2__1__Impl rule__UnaryPrefix__Group_2__2
            {
            pushFollow(FOLLOW_11);
            rule__UnaryPrefix__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnaryPrefix__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Group_2__1"


    // $ANTLR start "rule__UnaryPrefix__Group_2__1__Impl"
    // InternalConstraints.g:3355:1: rule__UnaryPrefix__Group_2__1__Impl : ( '!' ) ;
    public final void rule__UnaryPrefix__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3359:1: ( ( '!' ) )
            // InternalConstraints.g:3360:1: ( '!' )
            {
            // InternalConstraints.g:3360:1: ( '!' )
            // InternalConstraints.g:3361:2: '!'
            {
             before(grammarAccess.getUnaryPrefixAccess().getExclamationMarkKeyword_2_1()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getUnaryPrefixAccess().getExclamationMarkKeyword_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Group_2__1__Impl"


    // $ANTLR start "rule__UnaryPrefix__Group_2__2"
    // InternalConstraints.g:3370:1: rule__UnaryPrefix__Group_2__2 : rule__UnaryPrefix__Group_2__2__Impl ;
    public final void rule__UnaryPrefix__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3374:1: ( rule__UnaryPrefix__Group_2__2__Impl )
            // InternalConstraints.g:3375:2: rule__UnaryPrefix__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UnaryPrefix__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Group_2__2"


    // $ANTLR start "rule__UnaryPrefix__Group_2__2__Impl"
    // InternalConstraints.g:3381:1: rule__UnaryPrefix__Group_2__2__Impl : ( ( rule__UnaryPrefix__ExpressionAssignment_2_2 ) ) ;
    public final void rule__UnaryPrefix__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3385:1: ( ( ( rule__UnaryPrefix__ExpressionAssignment_2_2 ) ) )
            // InternalConstraints.g:3386:1: ( ( rule__UnaryPrefix__ExpressionAssignment_2_2 ) )
            {
            // InternalConstraints.g:3386:1: ( ( rule__UnaryPrefix__ExpressionAssignment_2_2 ) )
            // InternalConstraints.g:3387:2: ( rule__UnaryPrefix__ExpressionAssignment_2_2 )
            {
             before(grammarAccess.getUnaryPrefixAccess().getExpressionAssignment_2_2()); 
            // InternalConstraints.g:3388:2: ( rule__UnaryPrefix__ExpressionAssignment_2_2 )
            // InternalConstraints.g:3388:3: rule__UnaryPrefix__ExpressionAssignment_2_2
            {
            pushFollow(FOLLOW_2);
            rule__UnaryPrefix__ExpressionAssignment_2_2();

            state._fsp--;


            }

             after(grammarAccess.getUnaryPrefixAccess().getExpressionAssignment_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__Group_2__2__Impl"


    // $ANTLR start "rule__Primary__Group_0__0"
    // InternalConstraints.g:3397:1: rule__Primary__Group_0__0 : rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 ;
    public final void rule__Primary__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3401:1: ( rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 )
            // InternalConstraints.g:3402:2: rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1
            {
            pushFollow(FOLLOW_11);
            rule__Primary__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0"


    // $ANTLR start "rule__Primary__Group_0__0__Impl"
    // InternalConstraints.g:3409:1: rule__Primary__Group_0__0__Impl : ( '(' ) ;
    public final void rule__Primary__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3413:1: ( ( '(' ) )
            // InternalConstraints.g:3414:1: ( '(' )
            {
            // InternalConstraints.g:3414:1: ( '(' )
            // InternalConstraints.g:3415:2: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0__Impl"


    // $ANTLR start "rule__Primary__Group_0__1"
    // InternalConstraints.g:3424:1: rule__Primary__Group_0__1 : rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 ;
    public final void rule__Primary__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3428:1: ( rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 )
            // InternalConstraints.g:3429:2: rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2
            {
            pushFollow(FOLLOW_19);
            rule__Primary__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1"


    // $ANTLR start "rule__Primary__Group_0__1__Impl"
    // InternalConstraints.g:3436:1: rule__Primary__Group_0__1__Impl : ( ruleExpression ) ;
    public final void rule__Primary__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3440:1: ( ( ruleExpression ) )
            // InternalConstraints.g:3441:1: ( ruleExpression )
            {
            // InternalConstraints.g:3441:1: ( ruleExpression )
            // InternalConstraints.g:3442:2: ruleExpression
            {
             before(grammarAccess.getPrimaryAccess().getExpressionParserRuleCall_0_1()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getExpressionParserRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1__Impl"


    // $ANTLR start "rule__Primary__Group_0__2"
    // InternalConstraints.g:3451:1: rule__Primary__Group_0__2 : rule__Primary__Group_0__2__Impl ;
    public final void rule__Primary__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3455:1: ( rule__Primary__Group_0__2__Impl )
            // InternalConstraints.g:3456:2: rule__Primary__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2"


    // $ANTLR start "rule__Primary__Group_0__2__Impl"
    // InternalConstraints.g:3462:1: rule__Primary__Group_0__2__Impl : ( ')' ) ;
    public final void rule__Primary__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3466:1: ( ( ')' ) )
            // InternalConstraints.g:3467:1: ( ')' )
            {
            // InternalConstraints.g:3467:1: ( ')' )
            // InternalConstraints.g:3468:2: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2__Impl"


    // $ANTLR start "rule__Primary__Group_2__0"
    // InternalConstraints.g:3478:1: rule__Primary__Group_2__0 : rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1 ;
    public final void rule__Primary__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3482:1: ( rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1 )
            // InternalConstraints.g:3483:2: rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1
            {
            pushFollow(FOLLOW_37);
            rule__Primary__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__0"


    // $ANTLR start "rule__Primary__Group_2__0__Impl"
    // InternalConstraints.g:3490:1: rule__Primary__Group_2__0__Impl : ( () ) ;
    public final void rule__Primary__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3494:1: ( ( () ) )
            // InternalConstraints.g:3495:1: ( () )
            {
            // InternalConstraints.g:3495:1: ( () )
            // InternalConstraints.g:3496:2: ()
            {
             before(grammarAccess.getPrimaryAccess().getReferenceAction_2_0()); 
            // InternalConstraints.g:3497:2: ()
            // InternalConstraints.g:3497:3: 
            {
            }

             after(grammarAccess.getPrimaryAccess().getReferenceAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__0__Impl"


    // $ANTLR start "rule__Primary__Group_2__1"
    // InternalConstraints.g:3505:1: rule__Primary__Group_2__1 : rule__Primary__Group_2__1__Impl ;
    public final void rule__Primary__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3509:1: ( rule__Primary__Group_2__1__Impl )
            // InternalConstraints.g:3510:2: rule__Primary__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__1"


    // $ANTLR start "rule__Primary__Group_2__1__Impl"
    // InternalConstraints.g:3516:1: rule__Primary__Group_2__1__Impl : ( ( rule__Primary__ReferenceAssignment_2_1 ) ) ;
    public final void rule__Primary__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3520:1: ( ( ( rule__Primary__ReferenceAssignment_2_1 ) ) )
            // InternalConstraints.g:3521:1: ( ( rule__Primary__ReferenceAssignment_2_1 ) )
            {
            // InternalConstraints.g:3521:1: ( ( rule__Primary__ReferenceAssignment_2_1 ) )
            // InternalConstraints.g:3522:2: ( rule__Primary__ReferenceAssignment_2_1 )
            {
             before(grammarAccess.getPrimaryAccess().getReferenceAssignment_2_1()); 
            // InternalConstraints.g:3523:2: ( rule__Primary__ReferenceAssignment_2_1 )
            // InternalConstraints.g:3523:3: rule__Primary__ReferenceAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Primary__ReferenceAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getReferenceAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__1__Impl"


    // $ANTLR start "rule__Constant__Group_0__0"
    // InternalConstraints.g:3532:1: rule__Constant__Group_0__0 : rule__Constant__Group_0__0__Impl rule__Constant__Group_0__1 ;
    public final void rule__Constant__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3536:1: ( rule__Constant__Group_0__0__Impl rule__Constant__Group_0__1 )
            // InternalConstraints.g:3537:2: rule__Constant__Group_0__0__Impl rule__Constant__Group_0__1
            {
            pushFollow(FOLLOW_38);
            rule__Constant__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constant__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_0__0"


    // $ANTLR start "rule__Constant__Group_0__0__Impl"
    // InternalConstraints.g:3544:1: rule__Constant__Group_0__0__Impl : ( () ) ;
    public final void rule__Constant__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3548:1: ( ( () ) )
            // InternalConstraints.g:3549:1: ( () )
            {
            // InternalConstraints.g:3549:1: ( () )
            // InternalConstraints.g:3550:2: ()
            {
             before(grammarAccess.getConstantAccess().getIntConstantAction_0_0()); 
            // InternalConstraints.g:3551:2: ()
            // InternalConstraints.g:3551:3: 
            {
            }

             after(grammarAccess.getConstantAccess().getIntConstantAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_0__0__Impl"


    // $ANTLR start "rule__Constant__Group_0__1"
    // InternalConstraints.g:3559:1: rule__Constant__Group_0__1 : rule__Constant__Group_0__1__Impl ;
    public final void rule__Constant__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3563:1: ( rule__Constant__Group_0__1__Impl )
            // InternalConstraints.g:3564:2: rule__Constant__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Constant__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_0__1"


    // $ANTLR start "rule__Constant__Group_0__1__Impl"
    // InternalConstraints.g:3570:1: rule__Constant__Group_0__1__Impl : ( ( rule__Constant__ValueAssignment_0_1 ) ) ;
    public final void rule__Constant__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3574:1: ( ( ( rule__Constant__ValueAssignment_0_1 ) ) )
            // InternalConstraints.g:3575:1: ( ( rule__Constant__ValueAssignment_0_1 ) )
            {
            // InternalConstraints.g:3575:1: ( ( rule__Constant__ValueAssignment_0_1 ) )
            // InternalConstraints.g:3576:2: ( rule__Constant__ValueAssignment_0_1 )
            {
             before(grammarAccess.getConstantAccess().getValueAssignment_0_1()); 
            // InternalConstraints.g:3577:2: ( rule__Constant__ValueAssignment_0_1 )
            // InternalConstraints.g:3577:3: rule__Constant__ValueAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Constant__ValueAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getConstantAccess().getValueAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_0__1__Impl"


    // $ANTLR start "rule__Constant__Group_1__0"
    // InternalConstraints.g:3586:1: rule__Constant__Group_1__0 : rule__Constant__Group_1__0__Impl rule__Constant__Group_1__1 ;
    public final void rule__Constant__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3590:1: ( rule__Constant__Group_1__0__Impl rule__Constant__Group_1__1 )
            // InternalConstraints.g:3591:2: rule__Constant__Group_1__0__Impl rule__Constant__Group_1__1
            {
            pushFollow(FOLLOW_24);
            rule__Constant__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constant__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_1__0"


    // $ANTLR start "rule__Constant__Group_1__0__Impl"
    // InternalConstraints.g:3598:1: rule__Constant__Group_1__0__Impl : ( () ) ;
    public final void rule__Constant__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3602:1: ( ( () ) )
            // InternalConstraints.g:3603:1: ( () )
            {
            // InternalConstraints.g:3603:1: ( () )
            // InternalConstraints.g:3604:2: ()
            {
             before(grammarAccess.getConstantAccess().getRealConstantAction_1_0()); 
            // InternalConstraints.g:3605:2: ()
            // InternalConstraints.g:3605:3: 
            {
            }

             after(grammarAccess.getConstantAccess().getRealConstantAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_1__0__Impl"


    // $ANTLR start "rule__Constant__Group_1__1"
    // InternalConstraints.g:3613:1: rule__Constant__Group_1__1 : rule__Constant__Group_1__1__Impl ;
    public final void rule__Constant__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3617:1: ( rule__Constant__Group_1__1__Impl )
            // InternalConstraints.g:3618:2: rule__Constant__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Constant__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_1__1"


    // $ANTLR start "rule__Constant__Group_1__1__Impl"
    // InternalConstraints.g:3624:1: rule__Constant__Group_1__1__Impl : ( ( rule__Constant__ValueAssignment_1_1 ) ) ;
    public final void rule__Constant__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3628:1: ( ( ( rule__Constant__ValueAssignment_1_1 ) ) )
            // InternalConstraints.g:3629:1: ( ( rule__Constant__ValueAssignment_1_1 ) )
            {
            // InternalConstraints.g:3629:1: ( ( rule__Constant__ValueAssignment_1_1 ) )
            // InternalConstraints.g:3630:2: ( rule__Constant__ValueAssignment_1_1 )
            {
             before(grammarAccess.getConstantAccess().getValueAssignment_1_1()); 
            // InternalConstraints.g:3631:2: ( rule__Constant__ValueAssignment_1_1 )
            // InternalConstraints.g:3631:3: rule__Constant__ValueAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Constant__ValueAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getConstantAccess().getValueAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_1__1__Impl"


    // $ANTLR start "rule__Constant__Group_2__0"
    // InternalConstraints.g:3640:1: rule__Constant__Group_2__0 : rule__Constant__Group_2__0__Impl rule__Constant__Group_2__1 ;
    public final void rule__Constant__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3644:1: ( rule__Constant__Group_2__0__Impl rule__Constant__Group_2__1 )
            // InternalConstraints.g:3645:2: rule__Constant__Group_2__0__Impl rule__Constant__Group_2__1
            {
            pushFollow(FOLLOW_22);
            rule__Constant__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constant__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_2__0"


    // $ANTLR start "rule__Constant__Group_2__0__Impl"
    // InternalConstraints.g:3652:1: rule__Constant__Group_2__0__Impl : ( () ) ;
    public final void rule__Constant__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3656:1: ( ( () ) )
            // InternalConstraints.g:3657:1: ( () )
            {
            // InternalConstraints.g:3657:1: ( () )
            // InternalConstraints.g:3658:2: ()
            {
             before(grammarAccess.getConstantAccess().getStringConstantAction_2_0()); 
            // InternalConstraints.g:3659:2: ()
            // InternalConstraints.g:3659:3: 
            {
            }

             after(grammarAccess.getConstantAccess().getStringConstantAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_2__0__Impl"


    // $ANTLR start "rule__Constant__Group_2__1"
    // InternalConstraints.g:3667:1: rule__Constant__Group_2__1 : rule__Constant__Group_2__1__Impl ;
    public final void rule__Constant__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3671:1: ( rule__Constant__Group_2__1__Impl )
            // InternalConstraints.g:3672:2: rule__Constant__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Constant__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_2__1"


    // $ANTLR start "rule__Constant__Group_2__1__Impl"
    // InternalConstraints.g:3678:1: rule__Constant__Group_2__1__Impl : ( ( rule__Constant__ValueAssignment_2_1 ) ) ;
    public final void rule__Constant__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3682:1: ( ( ( rule__Constant__ValueAssignment_2_1 ) ) )
            // InternalConstraints.g:3683:1: ( ( rule__Constant__ValueAssignment_2_1 ) )
            {
            // InternalConstraints.g:3683:1: ( ( rule__Constant__ValueAssignment_2_1 ) )
            // InternalConstraints.g:3684:2: ( rule__Constant__ValueAssignment_2_1 )
            {
             before(grammarAccess.getConstantAccess().getValueAssignment_2_1()); 
            // InternalConstraints.g:3685:2: ( rule__Constant__ValueAssignment_2_1 )
            // InternalConstraints.g:3685:3: rule__Constant__ValueAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Constant__ValueAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getConstantAccess().getValueAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_2__1__Impl"


    // $ANTLR start "rule__Constant__Group_3__0"
    // InternalConstraints.g:3694:1: rule__Constant__Group_3__0 : rule__Constant__Group_3__0__Impl rule__Constant__Group_3__1 ;
    public final void rule__Constant__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3698:1: ( rule__Constant__Group_3__0__Impl rule__Constant__Group_3__1 )
            // InternalConstraints.g:3699:2: rule__Constant__Group_3__0__Impl rule__Constant__Group_3__1
            {
            pushFollow(FOLLOW_39);
            rule__Constant__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constant__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_3__0"


    // $ANTLR start "rule__Constant__Group_3__0__Impl"
    // InternalConstraints.g:3706:1: rule__Constant__Group_3__0__Impl : ( () ) ;
    public final void rule__Constant__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3710:1: ( ( () ) )
            // InternalConstraints.g:3711:1: ( () )
            {
            // InternalConstraints.g:3711:1: ( () )
            // InternalConstraints.g:3712:2: ()
            {
             before(grammarAccess.getConstantAccess().getBooleanConstantAction_3_0()); 
            // InternalConstraints.g:3713:2: ()
            // InternalConstraints.g:3713:3: 
            {
            }

             after(grammarAccess.getConstantAccess().getBooleanConstantAction_3_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_3__0__Impl"


    // $ANTLR start "rule__Constant__Group_3__1"
    // InternalConstraints.g:3721:1: rule__Constant__Group_3__1 : rule__Constant__Group_3__1__Impl ;
    public final void rule__Constant__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3725:1: ( rule__Constant__Group_3__1__Impl )
            // InternalConstraints.g:3726:2: rule__Constant__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Constant__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_3__1"


    // $ANTLR start "rule__Constant__Group_3__1__Impl"
    // InternalConstraints.g:3732:1: rule__Constant__Group_3__1__Impl : ( ( rule__Constant__ValueAssignment_3_1 ) ) ;
    public final void rule__Constant__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3736:1: ( ( ( rule__Constant__ValueAssignment_3_1 ) ) )
            // InternalConstraints.g:3737:1: ( ( rule__Constant__ValueAssignment_3_1 ) )
            {
            // InternalConstraints.g:3737:1: ( ( rule__Constant__ValueAssignment_3_1 ) )
            // InternalConstraints.g:3738:2: ( rule__Constant__ValueAssignment_3_1 )
            {
             before(grammarAccess.getConstantAccess().getValueAssignment_3_1()); 
            // InternalConstraints.g:3739:2: ( rule__Constant__ValueAssignment_3_1 )
            // InternalConstraints.g:3739:3: rule__Constant__ValueAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Constant__ValueAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getConstantAccess().getValueAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_3__1__Impl"


    // $ANTLR start "rule__ConstraintDocument__ReadingsAssignment_0"
    // InternalConstraints.g:3748:1: rule__ConstraintDocument__ReadingsAssignment_0 : ( ruleReading ) ;
    public final void rule__ConstraintDocument__ReadingsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3752:1: ( ( ruleReading ) )
            // InternalConstraints.g:3753:2: ( ruleReading )
            {
            // InternalConstraints.g:3753:2: ( ruleReading )
            // InternalConstraints.g:3754:3: ruleReading
            {
             before(grammarAccess.getConstraintDocumentAccess().getReadingsReadingParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleReading();

            state._fsp--;

             after(grammarAccess.getConstraintDocumentAccess().getReadingsReadingParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintDocument__ReadingsAssignment_0"


    // $ANTLR start "rule__ConstraintDocument__ConstraintsAssignment_1"
    // InternalConstraints.g:3763:1: rule__ConstraintDocument__ConstraintsAssignment_1 : ( ruleConstraint ) ;
    public final void rule__ConstraintDocument__ConstraintsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3767:1: ( ( ruleConstraint ) )
            // InternalConstraints.g:3768:2: ( ruleConstraint )
            {
            // InternalConstraints.g:3768:2: ( ruleConstraint )
            // InternalConstraints.g:3769:3: ruleConstraint
            {
             before(grammarAccess.getConstraintDocumentAccess().getConstraintsConstraintParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleConstraint();

            state._fsp--;

             after(grammarAccess.getConstraintDocumentAccess().getConstraintsConstraintParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintDocument__ConstraintsAssignment_1"


    // $ANTLR start "rule__Reading__TypeAssignment_1"
    // InternalConstraints.g:3778:1: rule__Reading__TypeAssignment_1 : ( RULE_ID ) ;
    public final void rule__Reading__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3782:1: ( ( RULE_ID ) )
            // InternalConstraints.g:3783:2: ( RULE_ID )
            {
            // InternalConstraints.g:3783:2: ( RULE_ID )
            // InternalConstraints.g:3784:3: RULE_ID
            {
             before(grammarAccess.getReadingAccess().getTypeIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getReadingAccess().getTypeIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__TypeAssignment_1"


    // $ANTLR start "rule__Reading__IdentifierAssignment_3"
    // InternalConstraints.g:3793:1: rule__Reading__IdentifierAssignment_3 : ( RULE_ID ) ;
    public final void rule__Reading__IdentifierAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3797:1: ( ( RULE_ID ) )
            // InternalConstraints.g:3798:2: ( RULE_ID )
            {
            // InternalConstraints.g:3798:2: ( RULE_ID )
            // InternalConstraints.g:3799:3: RULE_ID
            {
             before(grammarAccess.getReadingAccess().getIdentifierIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getReadingAccess().getIdentifierIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__IdentifierAssignment_3"


    // $ANTLR start "rule__Reading__NameAssignment_5"
    // InternalConstraints.g:3808:1: rule__Reading__NameAssignment_5 : ( RULE_ID ) ;
    public final void rule__Reading__NameAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3812:1: ( ( RULE_ID ) )
            // InternalConstraints.g:3813:2: ( RULE_ID )
            {
            // InternalConstraints.g:3813:2: ( RULE_ID )
            // InternalConstraints.g:3814:3: RULE_ID
            {
             before(grammarAccess.getReadingAccess().getNameIDTerminalRuleCall_5_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getReadingAccess().getNameIDTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reading__NameAssignment_5"


    // $ANTLR start "rule__Constraint__RuntimeAssignment_0"
    // InternalConstraints.g:3823:1: rule__Constraint__RuntimeAssignment_0 : ( ( 'runtime' ) ) ;
    public final void rule__Constraint__RuntimeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3827:1: ( ( ( 'runtime' ) ) )
            // InternalConstraints.g:3828:2: ( ( 'runtime' ) )
            {
            // InternalConstraints.g:3828:2: ( ( 'runtime' ) )
            // InternalConstraints.g:3829:3: ( 'runtime' )
            {
             before(grammarAccess.getConstraintAccess().getRuntimeRuntimeKeyword_0_0()); 
            // InternalConstraints.g:3830:3: ( 'runtime' )
            // InternalConstraints.g:3831:4: 'runtime'
            {
             before(grammarAccess.getConstraintAccess().getRuntimeRuntimeKeyword_0_0()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getConstraintAccess().getRuntimeRuntimeKeyword_0_0()); 

            }

             after(grammarAccess.getConstraintAccess().getRuntimeRuntimeKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__RuntimeAssignment_0"


    // $ANTLR start "rule__Constraint__NameAssignment_2"
    // InternalConstraints.g:3842:1: rule__Constraint__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Constraint__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3846:1: ( ( RULE_ID ) )
            // InternalConstraints.g:3847:2: ( RULE_ID )
            {
            // InternalConstraints.g:3847:2: ( RULE_ID )
            // InternalConstraints.g:3848:3: RULE_ID
            {
             before(grammarAccess.getConstraintAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getConstraintAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__NameAssignment_2"


    // $ANTLR start "rule__Constraint__ExpressionAssignment_4"
    // InternalConstraints.g:3857:1: rule__Constraint__ExpressionAssignment_4 : ( ruleExpression ) ;
    public final void rule__Constraint__ExpressionAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3861:1: ( ( ruleExpression ) )
            // InternalConstraints.g:3862:2: ( ruleExpression )
            {
            // InternalConstraints.g:3862:2: ( ruleExpression )
            // InternalConstraints.g:3863:3: ruleExpression
            {
             before(grammarAccess.getConstraintAccess().getExpressionExpressionParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getConstraintAccess().getExpressionExpressionParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__ExpressionAssignment_4"


    // $ANTLR start "rule__DeclaredArgument__NameAssignment"
    // InternalConstraints.g:3872:1: rule__DeclaredArgument__NameAssignment : ( RULE_ID ) ;
    public final void rule__DeclaredArgument__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3876:1: ( ( RULE_ID ) )
            // InternalConstraints.g:3877:2: ( RULE_ID )
            {
            // InternalConstraints.g:3877:2: ( RULE_ID )
            // InternalConstraints.g:3878:3: RULE_ID
            {
             before(grammarAccess.getDeclaredArgumentAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getDeclaredArgumentAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclaredArgument__NameAssignment"


    // $ANTLR start "rule__QuantifiedExpression__QuantifierAssignment_1"
    // InternalConstraints.g:3887:1: rule__QuantifiedExpression__QuantifierAssignment_1 : ( ruleQuantifier ) ;
    public final void rule__QuantifiedExpression__QuantifierAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3891:1: ( ( ruleQuantifier ) )
            // InternalConstraints.g:3892:2: ( ruleQuantifier )
            {
            // InternalConstraints.g:3892:2: ( ruleQuantifier )
            // InternalConstraints.g:3893:3: ruleQuantifier
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getQuantifierQuantifierEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQuantifier();

            state._fsp--;

             after(grammarAccess.getQuantifiedExpressionAccess().getQuantifierQuantifierEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__QuantifierAssignment_1"


    // $ANTLR start "rule__QuantifiedExpression__ArgumentAssignment_2"
    // InternalConstraints.g:3902:1: rule__QuantifiedExpression__ArgumentAssignment_2 : ( ruleDeclaredArgument ) ;
    public final void rule__QuantifiedExpression__ArgumentAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3906:1: ( ( ruleDeclaredArgument ) )
            // InternalConstraints.g:3907:2: ( ruleDeclaredArgument )
            {
            // InternalConstraints.g:3907:2: ( ruleDeclaredArgument )
            // InternalConstraints.g:3908:3: ruleDeclaredArgument
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getArgumentDeclaredArgumentParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleDeclaredArgument();

            state._fsp--;

             after(grammarAccess.getQuantifiedExpressionAccess().getArgumentDeclaredArgumentParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__ArgumentAssignment_2"


    // $ANTLR start "rule__QuantifiedExpression__QueryAssignment_3_1"
    // InternalConstraints.g:3917:1: rule__QuantifiedExpression__QueryAssignment_3_1 : ( ruleQueryExpression ) ;
    public final void rule__QuantifiedExpression__QueryAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3921:1: ( ( ruleQueryExpression ) )
            // InternalConstraints.g:3922:2: ( ruleQueryExpression )
            {
            // InternalConstraints.g:3922:2: ( ruleQueryExpression )
            // InternalConstraints.g:3923:3: ruleQueryExpression
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getQueryQueryExpressionParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQueryExpression();

            state._fsp--;

             after(grammarAccess.getQuantifiedExpressionAccess().getQueryQueryExpressionParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__QueryAssignment_3_1"


    // $ANTLR start "rule__QuantifiedExpression__ExpressionAssignment_5"
    // InternalConstraints.g:3932:1: rule__QuantifiedExpression__ExpressionAssignment_5 : ( ruleExpression ) ;
    public final void rule__QuantifiedExpression__ExpressionAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3936:1: ( ( ruleExpression ) )
            // InternalConstraints.g:3937:2: ( ruleExpression )
            {
            // InternalConstraints.g:3937:2: ( ruleExpression )
            // InternalConstraints.g:3938:3: ruleExpression
            {
             before(grammarAccess.getQuantifiedExpressionAccess().getExpressionExpressionParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getQuantifiedExpressionAccess().getExpressionExpressionParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpression__ExpressionAssignment_5"


    // $ANTLR start "rule__QueryUnion__OpAssignment_1_1"
    // InternalConstraints.g:3947:1: rule__QueryUnion__OpAssignment_1_1 : ( ( rule__QueryUnion__OpAlternatives_1_1_0 ) ) ;
    public final void rule__QueryUnion__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3951:1: ( ( ( rule__QueryUnion__OpAlternatives_1_1_0 ) ) )
            // InternalConstraints.g:3952:2: ( ( rule__QueryUnion__OpAlternatives_1_1_0 ) )
            {
            // InternalConstraints.g:3952:2: ( ( rule__QueryUnion__OpAlternatives_1_1_0 ) )
            // InternalConstraints.g:3953:3: ( rule__QueryUnion__OpAlternatives_1_1_0 )
            {
             before(grammarAccess.getQueryUnionAccess().getOpAlternatives_1_1_0()); 
            // InternalConstraints.g:3954:3: ( rule__QueryUnion__OpAlternatives_1_1_0 )
            // InternalConstraints.g:3954:4: rule__QueryUnion__OpAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__QueryUnion__OpAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getQueryUnionAccess().getOpAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__OpAssignment_1_1"


    // $ANTLR start "rule__QueryUnion__RightAssignment_1_2"
    // InternalConstraints.g:3962:1: rule__QueryUnion__RightAssignment_1_2 : ( ruleQueryIntersection ) ;
    public final void rule__QueryUnion__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3966:1: ( ( ruleQueryIntersection ) )
            // InternalConstraints.g:3967:2: ( ruleQueryIntersection )
            {
            // InternalConstraints.g:3967:2: ( ruleQueryIntersection )
            // InternalConstraints.g:3968:3: ruleQueryIntersection
            {
             before(grammarAccess.getQueryUnionAccess().getRightQueryIntersectionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQueryIntersection();

            state._fsp--;

             after(grammarAccess.getQueryUnionAccess().getRightQueryIntersectionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryUnion__RightAssignment_1_2"


    // $ANTLR start "rule__QueryIntersection__OpAssignment_1_1"
    // InternalConstraints.g:3977:1: rule__QueryIntersection__OpAssignment_1_1 : ( ( rule__QueryIntersection__OpAlternatives_1_1_0 ) ) ;
    public final void rule__QueryIntersection__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3981:1: ( ( ( rule__QueryIntersection__OpAlternatives_1_1_0 ) ) )
            // InternalConstraints.g:3982:2: ( ( rule__QueryIntersection__OpAlternatives_1_1_0 ) )
            {
            // InternalConstraints.g:3982:2: ( ( rule__QueryIntersection__OpAlternatives_1_1_0 ) )
            // InternalConstraints.g:3983:3: ( rule__QueryIntersection__OpAlternatives_1_1_0 )
            {
             before(grammarAccess.getQueryIntersectionAccess().getOpAlternatives_1_1_0()); 
            // InternalConstraints.g:3984:3: ( rule__QueryIntersection__OpAlternatives_1_1_0 )
            // InternalConstraints.g:3984:4: rule__QueryIntersection__OpAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__QueryIntersection__OpAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getQueryIntersectionAccess().getOpAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__OpAssignment_1_1"


    // $ANTLR start "rule__QueryIntersection__RightAssignment_1_2"
    // InternalConstraints.g:3992:1: rule__QueryIntersection__RightAssignment_1_2 : ( ruleAtomicQueryExpression ) ;
    public final void rule__QueryIntersection__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:3996:1: ( ( ruleAtomicQueryExpression ) )
            // InternalConstraints.g:3997:2: ( ruleAtomicQueryExpression )
            {
            // InternalConstraints.g:3997:2: ( ruleAtomicQueryExpression )
            // InternalConstraints.g:3998:3: ruleAtomicQueryExpression
            {
             before(grammarAccess.getQueryIntersectionAccess().getRightAtomicQueryExpressionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAtomicQueryExpression();

            state._fsp--;

             after(grammarAccess.getQueryIntersectionAccess().getRightAtomicQueryExpressionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryIntersection__RightAssignment_1_2"


    // $ANTLR start "rule__AtomicQueryExpression__NameAssignment_1_1"
    // InternalConstraints.g:4007:1: rule__AtomicQueryExpression__NameAssignment_1_1 : ( ( RULE_ID ) ) ;
    public final void rule__AtomicQueryExpression__NameAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4011:1: ( ( ( RULE_ID ) ) )
            // InternalConstraints.g:4012:2: ( ( RULE_ID ) )
            {
            // InternalConstraints.g:4012:2: ( ( RULE_ID ) )
            // InternalConstraints.g:4013:3: ( RULE_ID )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentCrossReference_1_1_0()); 
            // InternalConstraints.g:4014:3: ( RULE_ID )
            // InternalConstraints.g:4015:4: RULE_ID
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentIDTerminalRuleCall_1_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentIDTerminalRuleCall_1_1_0_1()); 

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentCrossReference_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__NameAssignment_1_1"


    // $ANTLR start "rule__AtomicQueryExpression__NotAssignment_1_2"
    // InternalConstraints.g:4026:1: rule__AtomicQueryExpression__NotAssignment_1_2 : ( ( 'not' ) ) ;
    public final void rule__AtomicQueryExpression__NotAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4030:1: ( ( ( 'not' ) ) )
            // InternalConstraints.g:4031:2: ( ( 'not' ) )
            {
            // InternalConstraints.g:4031:2: ( ( 'not' ) )
            // InternalConstraints.g:4032:3: ( 'not' )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_1_2_0()); 
            // InternalConstraints.g:4033:3: ( 'not' )
            // InternalConstraints.g:4034:4: 'not'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_1_2_0()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_1_2_0()); 

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__NotAssignment_1_2"


    // $ANTLR start "rule__AtomicQueryExpression__GroupAssignment_1_5"
    // InternalConstraints.g:4045:1: rule__AtomicQueryExpression__GroupAssignment_1_5 : ( RULE_STRING ) ;
    public final void rule__AtomicQueryExpression__GroupAssignment_1_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4049:1: ( ( RULE_STRING ) )
            // InternalConstraints.g:4050:2: ( RULE_STRING )
            {
            // InternalConstraints.g:4050:2: ( RULE_STRING )
            // InternalConstraints.g:4051:3: RULE_STRING
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getGroupSTRINGTerminalRuleCall_1_5_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getGroupSTRINGTerminalRuleCall_1_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__GroupAssignment_1_5"


    // $ANTLR start "rule__AtomicQueryExpression__NameAssignment_2_1"
    // InternalConstraints.g:4060:1: rule__AtomicQueryExpression__NameAssignment_2_1 : ( ( RULE_ID ) ) ;
    public final void rule__AtomicQueryExpression__NameAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4064:1: ( ( ( RULE_ID ) ) )
            // InternalConstraints.g:4065:2: ( ( RULE_ID ) )
            {
            // InternalConstraints.g:4065:2: ( ( RULE_ID ) )
            // InternalConstraints.g:4066:3: ( RULE_ID )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentCrossReference_2_1_0()); 
            // InternalConstraints.g:4067:3: ( RULE_ID )
            // InternalConstraints.g:4068:4: RULE_ID
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentIDTerminalRuleCall_2_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentIDTerminalRuleCall_2_1_0_1()); 

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentCrossReference_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__NameAssignment_2_1"


    // $ANTLR start "rule__AtomicQueryExpression__NotAssignment_2_2"
    // InternalConstraints.g:4079:1: rule__AtomicQueryExpression__NotAssignment_2_2 : ( ( 'not' ) ) ;
    public final void rule__AtomicQueryExpression__NotAssignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4083:1: ( ( ( 'not' ) ) )
            // InternalConstraints.g:4084:2: ( ( 'not' ) )
            {
            // InternalConstraints.g:4084:2: ( ( 'not' ) )
            // InternalConstraints.g:4085:3: ( 'not' )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_2_2_0()); 
            // InternalConstraints.g:4086:3: ( 'not' )
            // InternalConstraints.g:4087:4: 'not'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_2_2_0()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_2_2_0()); 

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__NotAssignment_2_2"


    // $ANTLR start "rule__AtomicQueryExpression__RangeAssignment_2_5"
    // InternalConstraints.g:4098:1: rule__AtomicQueryExpression__RangeAssignment_2_5 : ( RULE_DOUBLE ) ;
    public final void rule__AtomicQueryExpression__RangeAssignment_2_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4102:1: ( ( RULE_DOUBLE ) )
            // InternalConstraints.g:4103:2: ( RULE_DOUBLE )
            {
            // InternalConstraints.g:4103:2: ( RULE_DOUBLE )
            // InternalConstraints.g:4104:3: RULE_DOUBLE
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getRangeDOUBLETerminalRuleCall_2_5_0()); 
            match(input,RULE_DOUBLE,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getRangeDOUBLETerminalRuleCall_2_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__RangeAssignment_2_5"


    // $ANTLR start "rule__AtomicQueryExpression__ReferenceAssignment_2_7"
    // InternalConstraints.g:4113:1: rule__AtomicQueryExpression__ReferenceAssignment_2_7 : ( ( RULE_ID ) ) ;
    public final void rule__AtomicQueryExpression__ReferenceAssignment_2_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4117:1: ( ( ( RULE_ID ) ) )
            // InternalConstraints.g:4118:2: ( ( RULE_ID ) )
            {
            // InternalConstraints.g:4118:2: ( ( RULE_ID ) )
            // InternalConstraints.g:4119:3: ( RULE_ID )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getReferenceReferenceCrossReference_2_7_0()); 
            // InternalConstraints.g:4120:3: ( RULE_ID )
            // InternalConstraints.g:4121:4: RULE_ID
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getReferenceReferenceIDTerminalRuleCall_2_7_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getReferenceReferenceIDTerminalRuleCall_2_7_0_1()); 

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getReferenceReferenceCrossReference_2_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__ReferenceAssignment_2_7"


    // $ANTLR start "rule__AtomicQueryExpression__NameAssignment_3_1"
    // InternalConstraints.g:4132:1: rule__AtomicQueryExpression__NameAssignment_3_1 : ( ( RULE_ID ) ) ;
    public final void rule__AtomicQueryExpression__NameAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4136:1: ( ( ( RULE_ID ) ) )
            // InternalConstraints.g:4137:2: ( ( RULE_ID ) )
            {
            // InternalConstraints.g:4137:2: ( ( RULE_ID ) )
            // InternalConstraints.g:4138:3: ( RULE_ID )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentCrossReference_3_1_0()); 
            // InternalConstraints.g:4139:3: ( RULE_ID )
            // InternalConstraints.g:4140:4: RULE_ID
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentIDTerminalRuleCall_3_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentIDTerminalRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__NameAssignment_3_1"


    // $ANTLR start "rule__AtomicQueryExpression__NotAssignment_3_2"
    // InternalConstraints.g:4151:1: rule__AtomicQueryExpression__NotAssignment_3_2 : ( ( 'not' ) ) ;
    public final void rule__AtomicQueryExpression__NotAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4155:1: ( ( ( 'not' ) ) )
            // InternalConstraints.g:4156:2: ( ( 'not' ) )
            {
            // InternalConstraints.g:4156:2: ( ( 'not' ) )
            // InternalConstraints.g:4157:3: ( 'not' )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_3_2_0()); 
            // InternalConstraints.g:4158:3: ( 'not' )
            // InternalConstraints.g:4159:4: 'not'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_3_2_0()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_3_2_0()); 

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__NotAssignment_3_2"


    // $ANTLR start "rule__AtomicQueryExpression__ClazzAssignment_3_5"
    // InternalConstraints.g:4170:1: rule__AtomicQueryExpression__ClazzAssignment_3_5 : ( RULE_ID ) ;
    public final void rule__AtomicQueryExpression__ClazzAssignment_3_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4174:1: ( ( RULE_ID ) )
            // InternalConstraints.g:4175:2: ( RULE_ID )
            {
            // InternalConstraints.g:4175:2: ( RULE_ID )
            // InternalConstraints.g:4176:3: RULE_ID
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getClazzIDTerminalRuleCall_3_5_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getClazzIDTerminalRuleCall_3_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__ClazzAssignment_3_5"


    // $ANTLR start "rule__AtomicQueryExpression__NameAssignment_4_1"
    // InternalConstraints.g:4185:1: rule__AtomicQueryExpression__NameAssignment_4_1 : ( ( RULE_ID ) ) ;
    public final void rule__AtomicQueryExpression__NameAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4189:1: ( ( ( RULE_ID ) ) )
            // InternalConstraints.g:4190:2: ( ( RULE_ID ) )
            {
            // InternalConstraints.g:4190:2: ( ( RULE_ID ) )
            // InternalConstraints.g:4191:3: ( RULE_ID )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentCrossReference_4_1_0()); 
            // InternalConstraints.g:4192:3: ( RULE_ID )
            // InternalConstraints.g:4193:4: RULE_ID
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentIDTerminalRuleCall_4_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentIDTerminalRuleCall_4_1_0_1()); 

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNameDeclaredArgumentCrossReference_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__NameAssignment_4_1"


    // $ANTLR start "rule__AtomicQueryExpression__NotAssignment_4_3"
    // InternalConstraints.g:4204:1: rule__AtomicQueryExpression__NotAssignment_4_3 : ( ( 'not' ) ) ;
    public final void rule__AtomicQueryExpression__NotAssignment_4_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4208:1: ( ( ( 'not' ) ) )
            // InternalConstraints.g:4209:2: ( ( 'not' ) )
            {
            // InternalConstraints.g:4209:2: ( ( 'not' ) )
            // InternalConstraints.g:4210:3: ( 'not' )
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_4_3_0()); 
            // InternalConstraints.g:4211:3: ( 'not' )
            // InternalConstraints.g:4212:4: 'not'
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_4_3_0()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_4_3_0()); 

            }

             after(grammarAccess.getAtomicQueryExpressionAccess().getNotNotKeyword_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__NotAssignment_4_3"


    // $ANTLR start "rule__AtomicQueryExpression__ClazzAssignment_4_4"
    // InternalConstraints.g:4223:1: rule__AtomicQueryExpression__ClazzAssignment_4_4 : ( RULE_ID ) ;
    public final void rule__AtomicQueryExpression__ClazzAssignment_4_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4227:1: ( ( RULE_ID ) )
            // InternalConstraints.g:4228:2: ( RULE_ID )
            {
            // InternalConstraints.g:4228:2: ( RULE_ID )
            // InternalConstraints.g:4229:3: RULE_ID
            {
             before(grammarAccess.getAtomicQueryExpressionAccess().getClazzIDTerminalRuleCall_4_4_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAtomicQueryExpressionAccess().getClazzIDTerminalRuleCall_4_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicQueryExpression__ClazzAssignment_4_4"


    // $ANTLR start "rule__LogicalOr__OpAssignment_1_1"
    // InternalConstraints.g:4238:1: rule__LogicalOr__OpAssignment_1_1 : ( ( rule__LogicalOr__OpAlternatives_1_1_0 ) ) ;
    public final void rule__LogicalOr__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4242:1: ( ( ( rule__LogicalOr__OpAlternatives_1_1_0 ) ) )
            // InternalConstraints.g:4243:2: ( ( rule__LogicalOr__OpAlternatives_1_1_0 ) )
            {
            // InternalConstraints.g:4243:2: ( ( rule__LogicalOr__OpAlternatives_1_1_0 ) )
            // InternalConstraints.g:4244:3: ( rule__LogicalOr__OpAlternatives_1_1_0 )
            {
             before(grammarAccess.getLogicalOrAccess().getOpAlternatives_1_1_0()); 
            // InternalConstraints.g:4245:3: ( rule__LogicalOr__OpAlternatives_1_1_0 )
            // InternalConstraints.g:4245:4: rule__LogicalOr__OpAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__LogicalOr__OpAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getLogicalOrAccess().getOpAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__OpAssignment_1_1"


    // $ANTLR start "rule__LogicalOr__RightAssignment_1_2"
    // InternalConstraints.g:4253:1: rule__LogicalOr__RightAssignment_1_2 : ( ruleLogicalAnd ) ;
    public final void rule__LogicalOr__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4257:1: ( ( ruleLogicalAnd ) )
            // InternalConstraints.g:4258:2: ( ruleLogicalAnd )
            {
            // InternalConstraints.g:4258:2: ( ruleLogicalAnd )
            // InternalConstraints.g:4259:3: ruleLogicalAnd
            {
             before(grammarAccess.getLogicalOrAccess().getRightLogicalAndParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalAnd();

            state._fsp--;

             after(grammarAccess.getLogicalOrAccess().getRightLogicalAndParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOr__RightAssignment_1_2"


    // $ANTLR start "rule__LogicalAnd__OpAssignment_1_1"
    // InternalConstraints.g:4268:1: rule__LogicalAnd__OpAssignment_1_1 : ( ( rule__LogicalAnd__OpAlternatives_1_1_0 ) ) ;
    public final void rule__LogicalAnd__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4272:1: ( ( ( rule__LogicalAnd__OpAlternatives_1_1_0 ) ) )
            // InternalConstraints.g:4273:2: ( ( rule__LogicalAnd__OpAlternatives_1_1_0 ) )
            {
            // InternalConstraints.g:4273:2: ( ( rule__LogicalAnd__OpAlternatives_1_1_0 ) )
            // InternalConstraints.g:4274:3: ( rule__LogicalAnd__OpAlternatives_1_1_0 )
            {
             before(grammarAccess.getLogicalAndAccess().getOpAlternatives_1_1_0()); 
            // InternalConstraints.g:4275:3: ( rule__LogicalAnd__OpAlternatives_1_1_0 )
            // InternalConstraints.g:4275:4: rule__LogicalAnd__OpAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__LogicalAnd__OpAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getLogicalAndAccess().getOpAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__OpAssignment_1_1"


    // $ANTLR start "rule__LogicalAnd__RightAssignment_1_2"
    // InternalConstraints.g:4283:1: rule__LogicalAnd__RightAssignment_1_2 : ( ruleEquality ) ;
    public final void rule__LogicalAnd__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4287:1: ( ( ruleEquality ) )
            // InternalConstraints.g:4288:2: ( ruleEquality )
            {
            // InternalConstraints.g:4288:2: ( ruleEquality )
            // InternalConstraints.g:4289:3: ruleEquality
            {
             before(grammarAccess.getLogicalAndAccess().getRightEqualityParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEquality();

            state._fsp--;

             after(grammarAccess.getLogicalAndAccess().getRightEqualityParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalAnd__RightAssignment_1_2"


    // $ANTLR start "rule__Equality__OpAssignment_1_0_1"
    // InternalConstraints.g:4298:1: rule__Equality__OpAssignment_1_0_1 : ( ( rule__Equality__OpAlternatives_1_0_1_0 ) ) ;
    public final void rule__Equality__OpAssignment_1_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4302:1: ( ( ( rule__Equality__OpAlternatives_1_0_1_0 ) ) )
            // InternalConstraints.g:4303:2: ( ( rule__Equality__OpAlternatives_1_0_1_0 ) )
            {
            // InternalConstraints.g:4303:2: ( ( rule__Equality__OpAlternatives_1_0_1_0 ) )
            // InternalConstraints.g:4304:3: ( rule__Equality__OpAlternatives_1_0_1_0 )
            {
             before(grammarAccess.getEqualityAccess().getOpAlternatives_1_0_1_0()); 
            // InternalConstraints.g:4305:3: ( rule__Equality__OpAlternatives_1_0_1_0 )
            // InternalConstraints.g:4305:4: rule__Equality__OpAlternatives_1_0_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Equality__OpAlternatives_1_0_1_0();

            state._fsp--;


            }

             after(grammarAccess.getEqualityAccess().getOpAlternatives_1_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__OpAssignment_1_0_1"


    // $ANTLR start "rule__Equality__RightAssignment_1_1"
    // InternalConstraints.g:4313:1: rule__Equality__RightAssignment_1_1 : ( ruleRelational ) ;
    public final void rule__Equality__RightAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4317:1: ( ( ruleRelational ) )
            // InternalConstraints.g:4318:2: ( ruleRelational )
            {
            // InternalConstraints.g:4318:2: ( ruleRelational )
            // InternalConstraints.g:4319:3: ruleRelational
            {
             before(grammarAccess.getEqualityAccess().getRightRelationalParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleRelational();

            state._fsp--;

             after(grammarAccess.getEqualityAccess().getRightRelationalParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equality__RightAssignment_1_1"


    // $ANTLR start "rule__Relational__RightAssignment_1_1"
    // InternalConstraints.g:4328:1: rule__Relational__RightAssignment_1_1 : ( ruleUnaryPrefix ) ;
    public final void rule__Relational__RightAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4332:1: ( ( ruleUnaryPrefix ) )
            // InternalConstraints.g:4333:2: ( ruleUnaryPrefix )
            {
            // InternalConstraints.g:4333:2: ( ruleUnaryPrefix )
            // InternalConstraints.g:4334:3: ruleUnaryPrefix
            {
             before(grammarAccess.getRelationalAccess().getRightUnaryPrefixParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleUnaryPrefix();

            state._fsp--;

             after(grammarAccess.getRelationalAccess().getRightUnaryPrefixParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational__RightAssignment_1_1"


    // $ANTLR start "rule__UnaryPrefix__ExpressionAssignment_1_2"
    // InternalConstraints.g:4343:1: rule__UnaryPrefix__ExpressionAssignment_1_2 : ( ruleUnaryPrefix ) ;
    public final void rule__UnaryPrefix__ExpressionAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4347:1: ( ( ruleUnaryPrefix ) )
            // InternalConstraints.g:4348:2: ( ruleUnaryPrefix )
            {
            // InternalConstraints.g:4348:2: ( ruleUnaryPrefix )
            // InternalConstraints.g:4349:3: ruleUnaryPrefix
            {
             before(grammarAccess.getUnaryPrefixAccess().getExpressionUnaryPrefixParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleUnaryPrefix();

            state._fsp--;

             after(grammarAccess.getUnaryPrefixAccess().getExpressionUnaryPrefixParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__ExpressionAssignment_1_2"


    // $ANTLR start "rule__UnaryPrefix__ExpressionAssignment_2_2"
    // InternalConstraints.g:4358:1: rule__UnaryPrefix__ExpressionAssignment_2_2 : ( ruleUnaryPrefix ) ;
    public final void rule__UnaryPrefix__ExpressionAssignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4362:1: ( ( ruleUnaryPrefix ) )
            // InternalConstraints.g:4363:2: ( ruleUnaryPrefix )
            {
            // InternalConstraints.g:4363:2: ( ruleUnaryPrefix )
            // InternalConstraints.g:4364:3: ruleUnaryPrefix
            {
             before(grammarAccess.getUnaryPrefixAccess().getExpressionUnaryPrefixParserRuleCall_2_2_0()); 
            pushFollow(FOLLOW_2);
            ruleUnaryPrefix();

            state._fsp--;

             after(grammarAccess.getUnaryPrefixAccess().getExpressionUnaryPrefixParserRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryPrefix__ExpressionAssignment_2_2"


    // $ANTLR start "rule__Primary__ReferenceAssignment_2_1"
    // InternalConstraints.g:4373:1: rule__Primary__ReferenceAssignment_2_1 : ( ( RULE_ID ) ) ;
    public final void rule__Primary__ReferenceAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4377:1: ( ( ( RULE_ID ) ) )
            // InternalConstraints.g:4378:2: ( ( RULE_ID ) )
            {
            // InternalConstraints.g:4378:2: ( ( RULE_ID ) )
            // InternalConstraints.g:4379:3: ( RULE_ID )
            {
             before(grammarAccess.getPrimaryAccess().getReferenceReferenceCrossReference_2_1_0()); 
            // InternalConstraints.g:4380:3: ( RULE_ID )
            // InternalConstraints.g:4381:4: RULE_ID
            {
             before(grammarAccess.getPrimaryAccess().getReferenceReferenceIDTerminalRuleCall_2_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getReferenceReferenceIDTerminalRuleCall_2_1_0_1()); 

            }

             after(grammarAccess.getPrimaryAccess().getReferenceReferenceCrossReference_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ReferenceAssignment_2_1"


    // $ANTLR start "rule__Constant__ValueAssignment_0_1"
    // InternalConstraints.g:4392:1: rule__Constant__ValueAssignment_0_1 : ( RULE_INT ) ;
    public final void rule__Constant__ValueAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4396:1: ( ( RULE_INT ) )
            // InternalConstraints.g:4397:2: ( RULE_INT )
            {
            // InternalConstraints.g:4397:2: ( RULE_INT )
            // InternalConstraints.g:4398:3: RULE_INT
            {
             before(grammarAccess.getConstantAccess().getValueINTTerminalRuleCall_0_1_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getConstantAccess().getValueINTTerminalRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__ValueAssignment_0_1"


    // $ANTLR start "rule__Constant__ValueAssignment_1_1"
    // InternalConstraints.g:4407:1: rule__Constant__ValueAssignment_1_1 : ( RULE_DOUBLE ) ;
    public final void rule__Constant__ValueAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4411:1: ( ( RULE_DOUBLE ) )
            // InternalConstraints.g:4412:2: ( RULE_DOUBLE )
            {
            // InternalConstraints.g:4412:2: ( RULE_DOUBLE )
            // InternalConstraints.g:4413:3: RULE_DOUBLE
            {
             before(grammarAccess.getConstantAccess().getValueDOUBLETerminalRuleCall_1_1_0()); 
            match(input,RULE_DOUBLE,FOLLOW_2); 
             after(grammarAccess.getConstantAccess().getValueDOUBLETerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__ValueAssignment_1_1"


    // $ANTLR start "rule__Constant__ValueAssignment_2_1"
    // InternalConstraints.g:4422:1: rule__Constant__ValueAssignment_2_1 : ( RULE_STRING ) ;
    public final void rule__Constant__ValueAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4426:1: ( ( RULE_STRING ) )
            // InternalConstraints.g:4427:2: ( RULE_STRING )
            {
            // InternalConstraints.g:4427:2: ( RULE_STRING )
            // InternalConstraints.g:4428:3: RULE_STRING
            {
             before(grammarAccess.getConstantAccess().getValueSTRINGTerminalRuleCall_2_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getConstantAccess().getValueSTRINGTerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__ValueAssignment_2_1"


    // $ANTLR start "rule__Constant__ValueAssignment_3_1"
    // InternalConstraints.g:4437:1: rule__Constant__ValueAssignment_3_1 : ( ( rule__Constant__ValueAlternatives_3_1_0 ) ) ;
    public final void rule__Constant__ValueAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalConstraints.g:4441:1: ( ( ( rule__Constant__ValueAlternatives_3_1_0 ) ) )
            // InternalConstraints.g:4442:2: ( ( rule__Constant__ValueAlternatives_3_1_0 ) )
            {
            // InternalConstraints.g:4442:2: ( ( rule__Constant__ValueAlternatives_3_1_0 ) )
            // InternalConstraints.g:4443:3: ( rule__Constant__ValueAlternatives_3_1_0 )
            {
             before(grammarAccess.getConstantAccess().getValueAlternatives_3_1_0()); 
            // InternalConstraints.g:4444:3: ( rule__Constant__ValueAlternatives_3_1_0 )
            // InternalConstraints.g:4444:4: rule__Constant__ValueAlternatives_3_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Constant__ValueAlternatives_3_1_0();

            state._fsp--;


            }

             after(grammarAccess.getConstantAccess().getValueAlternatives_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__ValueAssignment_3_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000200010000000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000200010000002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000180080FC00F0L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000F00000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000042000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000080000010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000003002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x000000000000C000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x000000000000C002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000400200000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000400000000010L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000030002L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000078000000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000078000000002L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x00000000800C00F0L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x00000000000C00E0L});

}